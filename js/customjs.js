function execute_step1(){
    var valid = checkValid_step1();
    if (valid){
        var datastring = $("#myForm_step1").serialize();
		var url = "<?php echo site_url('Registration_controller/execute_step1_ajax/')?>";
		$('#loading').removeClass('hidden');
		$.post(url, datastring, function(data) {
	       var results = JSON.parse(data);
	       var student_registration_id = results.student_registration_id;
	       console.log(student_registration_id);
	       $("#student_registration_id").val(student_registration_id);
	       $("#step1_div").addClass("hidden");
	       $("#step2_div").removeClass("hidden");
	       $('#loading').addClass('hidden');
		});
    }
}
	
function execute_step2(){
    var valid = checkValid_step2();
    if (valid){
        var datastring = $("#myForm_step2").serialize();
		var url = "<?php echo site_url('Registration_controller/execute_step2_ajax/')?>";
		$('#loading').removeClass('hidden');
		$.post(url, datastring, function(data) {
	       var results = JSON.parse(data);
	       var sent = results.sent;
           var student_gender = results.student_gender;
	       console.log(sent + "  " + student_gender);
	       $("#step2_div").addClass("hidden");
	       $("#step3_div").removeClass("hidden");
	       if(student_gender == "M"){
	           $("#male_id").removeClass("hidden");
	       }else{
	           $("#female_id").removeClass("hidden");
	       }
	       $('#loading').addClass('hidden');
		});
    }
}



function checkValid_step1() {
	var responsible_name = document.getElementById("responsible_name").value;
	var responsible_mobile = document.getElementById("responsible_mobile").value;
	var responsible_email = document.getElementById("responsible_email").value;

	if (responsible_name== "" ) {
		$("#responsible_name_tooltip").css("display","block");
		$("#responsible_name_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#responsible_name_tooltip").css("display","none");
		$("#responsible_name_tooltip").css("visibility","none");
	}
	
	if (responsible_mobile== "" ) {
		$("#responsible_mobile_tooltip").css("display","block");
		$("#responsible_mobile_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#responsible_mobile_tooltip").css("display","none");
		$("#responsible_mobile_tooltip").css("visibility","none");
	}
	
	if (responsible_email == "" || !validateEmail(responsible_email)) {
		$("#responsible_email_tooltip").css("display","block");
		$("#responsible_email_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#responsible_email_tooltip").css("display","none");
		$("#responsible_email_tooltip").css("visibility","none");
	}
	return true;
}


function validateEmail(email) 
{
	//var re = /\S+@\S+\.\S+/;
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

function checkValid_step2() {

	var DOB_1 = $('#DOB_1').val();
	if(DOB_1 != ""){
		var isValidD = isValidMiladyDate(DOB_1);
	   	if(!isValidD){
			$("#DOB_1_invalid_tooltip").css("display","block");
			$("#DOB_1_invalid_tooltip").css("visibility","visible");
	   			
	   		return false;
	   	}else{
			$("#DOB_1_invalid_tooltip").css("display","none");
			$("#DOB_1_invalid_tooltip").css("visibility","none");
	   	}
	}

	var DOB_2 = $('#DOB_2').val();
	if(DOB_2 != ""){
		var isValidD = isValidDate(DOB_2);
	   	if(!isValidD){
			$("#DOB_2_invalid_tooltip").css("display","block");
			$("#DOB_2_invalid_tooltip").css("visibility","visible");
	   			
	   		return false;
	   	}else{
			$("#DOB_2_invalid_tooltip").css("display","none");
			$("#DOB_2_invalid_tooltip").css("visibility","none");
	   	}
	}
	return true;
}


function miladyHandler() {
	var DOB_1 = $('#DOB_1').val();
	if(DOB_1 != ""){
		var isValidD = isValidMiladyDate(DOB_1);
	   	if(!isValidD){
	   		return false;
	   	}  	
	}
	
	var str_array = DOB_1.split('/');
	var miladyDate =  jqOld.calendars.instance('gregorian','ar').newDate();
	var hijriDate =  jqOld.calendars.instance('ummalqura','ar').newDate();
	try {
		miladyDate = miladyDate.date(str_array[2], str_array[0], str_array[1]);
		hijriDate = hijriDate.fromJSDate(miladyDate.toJSDate());
		//alert(hijriDate);
		$("#DOB_2").val(hijriDate.formatDate());
	}
	catch(err) {
	   return false;
	}
}

function hijriHandler() {
	var DOB_2 = $('#DOB_2').val();
	if(DOB_2 != ""){
		var isValidD = isValidDate(DOB_2);
	   	if(!isValidD){
	   		return false;
	   	}
	}

	try {
	 $.ajax({
		  type: "post",
		  url: '<?php echo site_url('Registration_controller/convertToMilady'); ?>',
		  cache: false,
		  data: { 
		      'hijriDate': DOB_2
		  },
		  success: function(json){      
		  try{  
		   var miladyDate = jQuery.parseJSON(json);
		   //reformat from 2017-05-10 to 05/16/2017
		   if(miladyDate !=''){
			   var str_array = miladyDate.split('-');
			   var year = str_array[0];
			   var month = str_array[1];
			   var day = str_array[2];
			   miladyDate = month+"/"+day+"/"+year;
		   }
		   $("#DOB_1").val(miladyDate);
		   //alert( 'Success');
		  }catch(e) {  
		   //alert('Exception while request..');
		  }  
		  },
		  error: function(){      
		   //alert('Error while request..');
		  }
		 });
	}
	catch(err) {
	   return false;
	}
}


function isValidDate(dateVar){
	//Format: yyyy/mm/dd
	var dateReg = /^\d{4}\/\d{2}\/\d{2}$/;
	var isV = dateReg.test(dateVar);
	if(!isV){
		return false;
	} 
	
	var str_array = dateVar.split('/');
	var hijriDate =  jqOld.calendars.instance('ummalqura','ar').newDate();
	try {
	   hijriDate = hijriDate.date(str_array[0], str_array[1], str_array[2]);
	}
	catch(err) {
	   return false;
	}
	return true;
}

function isValidMiladyDate(dateVar){
	//Format: yyyy/mm/dd
	var dateReg = /^\d{2}\/\d{2}\/\d{4}$/;
	var isV = dateReg.test(dateVar);
	if(!isV){
		return false;
	} 
	
	var str_array = dateVar.split('/');
	var miladyDate =  jqOld.calendars.instance('gregorian','ar').newDate();
	try {
		miladyDate = miladyDate.date(str_array[2], str_array[0], str_array[1]);
	}
	catch(err) {
	   return false;
	}
	return true;
}