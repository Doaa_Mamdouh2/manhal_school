<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
	define("THANKS_1_M_AR","نشكر لكم تقديم طلبكم لتسجيل ابنكم في مدراس المنهل");
	define("THANKS_1_F_AR","نشكر لكم تقديم طلبكم لتسجيل ابنتكم  في مدراس المنهل");
	
	require APPPATH."src/Mailjet/php-mailjet-v3-simple.class.php";
	
	/*ini_set('memory_limit',  '12000M');
	ini_set('max_execution_time','120000');
	*/	
	
	ini_set('memory_limit', '256M');
	ini_set('upload_max_size', '64M');
	ini_set('post_max_size', '64M');
	ini_set('upload_max_filesize', '64M');
	ini_set('max_execution_time', '300');
	ini_set('max_input_time', '1000');
	
	class Registration_controller extends CI_Controller {
		function __construct() {
			parent::__construct ();
			$this->load->model ( "common" );
			$this->load->helper ( array (
			    'url',
			    'Common_functions'
			) );
		}
		function index($lang='ar')
		{
		    self::view_registration_form ($lang);
		}
		function view_registration_form($lang) {
			$data = array();
			$data['lang'] = $lang;
			$this->load->view ( 'registration/student_registration_step1.php', $data );
		}
		function demo2() {
			$data = '';
			$this->load->view ( 'registration/student_registration_all_steps.php', $data );
		}
		function execute_step1($lang='ar') {
			if (extract ( $_POST )) {
				date_default_timezone_set('Asia/Riyadh');
				$current_date_time = date("Y-m-d H:i:s");
				$dt = new DateTime($current_date_time);
				$current_date = $dt->format('Y-m-d');
				
				//To avoid the problem of pressing next button multiple times in the first step.
				//Get the last record in the table and remove it if the email_sent = N and have the same data and don't have any other data	
				$last_record = $this->common->getOneRow('student_registration','  ORDER BY `student_registration`.`reg_id` DESC' );
				/*if($last_record['email_sent'] == 'N' &&
					($last_record['responsible_name'] == $_POST ['responsible_name']) &&
					($last_record['responsible_mobile'] == $_POST ['responsible_mobile']) &&
					($last_record['responsible_email'] == $_POST ['responsible_email']) &&
					($last_record['student_name'] == "") &&
					($last_record['school'] == null) &&
					($last_record['grade'] == null)
				){
					$this->common->deleteRecord ('student_registration', "reg_id = '" . $last_record['reg_id']. "'");
				}*/
				
				/*if($last_record['email_sent'] == 'N'){
				    $this->common->deleteRecord ('registration_transactions', "reg_id = '" . $last_record['reg_id']. "'");
				    $this->common->deleteRecord ('delivered_notifications', "reg_id = '" . $last_record['reg_id']. "'");
				    $this->common->deleteRecord ('activities_log', "reg_id = '" . $last_record['reg_id']. "'");
					$this->common->deleteRecord ('student_registration', "reg_id = '" . $last_record['reg_id']. "'");
				}*/
				
				/* Assigning the request to the availabe customer service
				 * Case 1: According to the number:
				 *         if the number is exist before then assign the request to the same customer service
				 */
				$result = $this->common->getOneRow('student_registration', "where responsible_mobile ='" . $_POST ['responsible_mobile']."'");
				$agent_id_for_responsible = $result['agent_id'];
				if(isset($agent_id_for_responsible) && $agent_id_for_responsible != NULL)
					$assigned_agent_automatic = 'Y';
				else 
					$assigned_agent_automatic = 'N';
				$student_registration_id = $this->common->insertRecord ( "student_registration", array (
						"responsible_name" => $_POST ['responsible_name'],
						"responsible_mobile" => $_POST ['responsible_mobile'],
						"responsible_email" => $_POST ['responsible_email'],
						"creation_date"  => $current_date,
						"creation_date_time" => $current_date_time,
						"agent_id"  => $agent_id_for_responsible,
						"assigned_agent_automatic" => $assigned_agent_automatic,
				        "state_changed_date" => $current_date_time
				) );
				self::subscribe_responsible($student_registration_id);
				$data["student_registration_id"] = $student_registration_id;
				$data["educational_grade"] = $this->common->getAllRow ( "educational_grade", '  ORDER BY `educational_grade`.`id` ASC' );
				$data["schools"] = $this->common->getAllRow ( "school", '  ORDER BY `school`.`id` ASC' );
				$data["knowing_ways"] = $this->common->getAllRow ( "knowing_way", '  ORDER BY `knowing_way`.`id` ASC' );
				$data["lang"] = $lang;
				$this->load->view ( 'registration/student_registration_step2', $data );
			}
		}
		
		function execute_step1_ajax() {
			if (extract ( $_POST )) {
				date_default_timezone_set('Asia/Riyadh');
				$current_date_time = date("Y-m-d H:i:s");
				$dt = new DateTime($current_date_time);
				$current_date = $dt->format('Y-m-d');
				$student_registration_id = $this->common->insertRecord ( "student_registration", array (
						"responsible_name" => $_POST ['responsible_name'],
						"responsible_mobile" => $_POST ['responsible_mobile'],
						"responsible_email" => $_POST ['responsible_email'],
						"creation_date"  => $current_date,
						"creation_date_time" => $current_date_time
				) );
				
				$data["student_registration_id"] = $student_registration_id;
				//$mailSent = self::send_email("Ya rab", "ya raaaaaaaaaaab", "sara.magdy.a@gmail.com", "sara.magdy.a@gmail.com");
				echo json_encode($data);
			}
		}
		
		function execute_step2($lang='ar') {
			if (extract ( $_POST )) {
				$reg_id = $_POST ['student_registration_id'];
				$where = "reg_id ='".$reg_id."'";
				$result = $this->common->getOneRow('student_registration', "where reg_id =" . $reg_id);
				$agent_id = $result['agent_id'];
				$responsible_mobile = $result['responsible_mobile'];
				
				$value = array();
				$value['student_name']= $_POST ['student_name'];
				$value['department']= $_POST ['department'];
				$value['track']= $_POST ['track'];
				//$value['IBAN']= $_POST ['IBAN'];
				$value['knowing_way']= $_POST ['knowing_way'];
				$value['other_way']= $_POST ['other_way'];
				//$value['IBAN_source']= $_POST ['IBAN_source'];
				$value['school']= (isset($_POST ['school']) && $_POST ['school'] == "")? NUll: $_POST ['school'];
				$value['grade']= (isset($_POST ['grade']) && $_POST ['grade'] == "")? NUll: $_POST ['grade'];
				$value['old_school']= $_POST ['old_school'];
				
				/* $sourceDate = (isset($_POST ['DOB_1']) && $_POST ['DOB_1'] == "")? NUll: $_POST ['DOB_1'];
				if($sourceDate != NULL){
				    $date = new DateTime($sourceDate);
				    $value['DOB']= $date->format('Y-m-d');
				}else{
				    $value['DOB'] = NULL;
				} */
				
				/**
				 * SPECIAL CASE
				 */
				if(!isset($value['school']) || $value['school'] == NULL || $value['school'] ==""){
					$agent_id = 34; //All requests without school value assign to Mai
					$value['agent_id'] = $agent_id;
					$value['assigned_agent_automatic'] = 'Y';
				}
				//-------------- Assigning the request to the availabe customer service --------------
				if(!isset($agent_id) || $agent_id == NULL){ //The agent id not assigned in case 1
					//Case 2: According the specified grade
					if(isset($value['grade']) && $value['grade'] != NULL){
						//get all agents for this grade.
						$agents = $this->common->getAllRow ( "agent_grade", "where edu_grade_id = '" . $value['grade']."' and school_id ='" . $value['school']."'"); //TODO AND school
						//There is only one Agent for this grade 
						if(count ($agents) == 1){
							$value['agent_id'] =  $agents[0]['admin_id'];
							$value['assigned_agent_automatic'] = 'Y'; 
						//Case 3: More than one agent for this grade then send the request to the agent that has least number of requests.
						}else if(count ($agents) > 1){
							$agents_ids = ",";
							for($i = 0; $i < count ($agents); $i ++) {
								$agent_id = $agents[$i]['admin_id'];
								if (strpos($agents_ids, ",". $agent_id. ",") === FALSE){
									//echo "The comming id not exist in the string.";
									$agents_ids .= $agent_id . ",";
								}
							}
							$agents_ids_trim = trim($agents_ids,',');
							$agents = explode(',', $agents_ids_trim);
							$sql_query = "SELECT `agent_id`, COUNT(`agent_id`) AS `value_occurrence` "."
											FROM `student_registration` WHERE `current_state_code` = 'NEW' and `agent_id` IN (".$agents_ids_trim.") GROUP BY `agent_id` ".
											"ORDER BY `value_occurrence` ASC;";
							$query = $this->db->query($sql_query);
							$used_agents = $query->result();
							if ($used_agents && !empty($used_agents)) {
								//All agents have requests so select the first agent that has least number.
								if (count($used_agents) == count($agents)){ 
									$agent = $used_agents[0];
									$value['agent_id'] = $agent-> agent_id;
								}else { //Some agents have requests then select any of unused agents.
									foreach ($used_agents as $agent) {
										$used_agent_id = $agent-> agent_id;
										$agents_ids = str_replace($used_agent_id.',', '', $agents_ids);
									}
                					$agents_ids = trim($agents_ids,',');
                					$agents_ids_array = explode(',', $agents_ids);
                					if(count($agents_ids_array) == 1){
                					    $selected_agent = $agents_ids;
                					}else{
                					    $selected_agent = substr($agents_ids, 0, strpos($agents_ids, ','));
                					}
									$value['agent_id'] = $selected_agent;
								}
								$value['assigned_agent_automatic'] = 'Y'; 
							}else{
								// All of agents on this grade are available (has no requests) so select any one.
								$value['agent_id'] = $agent_id;
								$value['assigned_agent_automatic'] = 'Y';
							}
						}else{
							//TODO There are no agents for this grade
							$value['assigned_agent_automatic'] = 'N';
						}
						
						$value2 ['agent_id'] = $value['agent_id'];
						$value2 ['assigned_agent_automatic'] = $value ['assigned_agent_automatic'];
						//update all records of the same number with the agent_id value if there values = NULL
						$this->common->updateRecord('student_registration', $value2, " responsible_mobile ='" . $responsible_mobile."' and agent_id is NULL");
					}else{
						//TODO send email to the admin (يوجد طلب جديد في انتظار تعيين ممثل خدمة له)
						$value['assigned_agent_automatic'] = 'N';
					}
					
				}
				//------------------------------------------------------------------------------------------------------
				if($result['email_sent'] == "N"){
					$value['email_sent'] = "Y";
				}
				$affected_rows = $this->common->updateRecord('student_registration',$value,$where);
					
				if($result['email_sent'] == "N"){
					//send to admin -- loop on admins
					$admins = $this->common->getAllRow ( 'admin', "where type = 'ADMIN'" );
					for($j=0; $j< count($admins) ; $j++){
					    $admin_record =  $admins[$j];
	    				$notif_values = array ();
	    				$notif_values ['notification_type_code'] = 'M002';
	    				$notif_values ['to_user_id'] = $admin_record ['admin_id'];
	    				$notif_values ['reg_id'] = $reg_id;
	    				$notif_values ['waiting_days'] = '0';
	    				$sent_successfully = insert_delivered_notification($notif_values);
					}
				}
				
				//At the end all requests should have an agent.
				$this->assign_non_grade_registrations();
				
				//send to agent if any 
				$final_reg_rec = $this->common->getOneRow('student_registration', "where reg_id =" . $reg_id);
				log_message("error", "final_reg_rec agent id = ".$final_reg_rec['agent_id']);
				if(isset($final_reg_rec['agent_id']) && $final_reg_rec['agent_id'] != null && $final_reg_rec['agent_id'] != ""){
				    log_message("error", "M007 ");
				    $notif_values = array ();
				    $notif_values ['notification_type_code'] = 'M007';
				    $notif_values ['to_user_id'] = $final_reg_rec['agent_id'];
				    $notif_values ['reg_id'] = $reg_id;
				    $notif_values ['waiting_days'] = '0';
				    $sent_successfully = insert_delivered_notification($notif_values);
				}
				/*else{
				    //send to admin يوجد طلب جديد في انتظار تعيين ممثل خدمة له
				    for($j=0; $j< count($admins) ; $j++){
				        $admin_record =  $admins[$j];
				        log_message("error", "M003 ");
				        $notif_values = array ();
				        $notif_values ['notification_type_code'] = 'M003';
				        $notif_values ['to_user_id'] = $admin_record ['admin_id'];
				        $notif_values ['reg_id'] = $reg_id;
				        $notif_values ['waiting_days'] = '0';
				        $sent_successfully = insert_delivered_notification($notif_values);
				    }
				}*/
				
				self::send_thanks_email($reg_id, $lang);
				
				$sms_message = ($final_reg_rec['department'] == 'BOYS'? THANKS_1_M_AR:THANKS_1_F_AR);
				$sms_sent = sendSMS ('N', $final_reg_rec['responsible_mobile'], $sms_message);
				
				$sent = $sent_successfully;
				$data["sent"] = $sent;
				$data["department"] = $_POST ['department'];
				$data["track"] = $_POST ['track'];
				$data["lang"] = $lang;
				$this->load->view ( 'registration/student_registration_step3', $data );
			}
		}
		
		function execute_step2_ajax() {
			if (extract ( $_POST )) {
				$reg_id = $_POST ['student_registration_id'];
				$where = "reg_id ='".$reg_id."'";
				
				$value = array();
				$value['student_name']= $_POST ['student_name'];
				$value['department']= $_POST ['department'];
				$value['track']= $_POST ['track'];
				//$value['IBAN']= $_POST ['IBAN'];
				//$value['IBAN_source']= $_POST ['IBAN_source'];
				$value['knowing_way']= $_POST ['knowing_way'];
				$value['school']= $_POST ['school'];
				$value['grade']= $_POST ['grade'];
				$value['old_school']= $_POST ['old_school'];
				
				/* $sourceDate = $_POST ['DOB_1'];
				$date = new DateTime($sourceDate);
				$value['DOB']= $date->format('Y-m-d'); */
				
				$affected_rows = $this->common->updateRecord('student_registration',$value,$where);
				
				$where = "where reg_id =".$reg_id;
				$regRecord = $this->common->getOneRow('student_registration',$where);
				$responsible_name = $regRecord ['responsible_name'];
				if($regRecord ['department']== 'BOYS'){
					$param2 = "ابنكم";
				}else{
					$param2 = "ابنتكم";
				}
				
				$email_template = 'http://mhsedusa.com/registration_confirmation.html';
				$parameters = array($responsible_name , $param2);
				$email = $regRecord ['responsible_email'];
				$sent = 1;//self::send_html_email($email_template, $parameters, $email);
				$data["sent"] = $sent;
				$data["department"]=$_POST ['department'];
				$data["track"]=$_POST ['track'];
				
				//send admin mail
				//$admin_mail_sent = self::send_admin_mail($reg_id);

				echo json_encode($data);
			}
		}
		
		
		function convertToMilady() {
			if (extract ( $_POST )) {
				$hijriDate = $this->input->post ( 'hijriDate' );
				//$this->load->library('../controllers/dates_conversion');
				//$miladyDate= $this->dates_conversion->HijriToGregorian($hijriDate);
				$miladyDate= HijriToGregorian($hijriDate);
				echo json_encode ($miladyDate);
			}
		}
		function convertToHijri() {
			if (extract ( $_POST )) {
				$this->load->library('../controllers/dates_conversion');
				$miladyDate= $this->input->post ( 'miladyDate' );
				$hijriDate= $this->dates_conversion->GregorianToHijri($miladyDate);
				echo json_encode ( $hijriDate);
			}
		}
		
		function send_thanks_email($reg_id, $lang){
		    $final_reg_rec = $this->common->getOneRow('student_registration', "where reg_id =" . $reg_id);
		    if($lang == "ar"){
		        $subject = 'تأكيد من مدارس المنهل باستلام طلبكم';
		    }else{
		        $subject = 'Confirmation from Al-Manhal School receiving your request';
    		}
    		$user_message = get_mail_message("thanks_template.html", $subject, $reg_id, $lang);
		    send_email ($user_message, $subject, $final_reg_rec['responsible_email'], "", "", "");
		}
		
		function subscribe_responsible($reg_id){
		    $where = "reg_id ='".$reg_id."'";
		    $result = $this->common->getOneRow('student_registration', "where reg_id =" . $reg_id);
		    
		    $contact = array(
		        "Email"         =>  $result['responsible_email'],
		        "Name"          =>  $result['responsible_name'],
		        "Action"        =>  "addforce",
		        "Properties"    =>  array(
		            "responsible_name" =>  $result['responsible_name'],
		            "registration_date" => $result['creation_date_time']
		        )
		    );
		    
		    self::addDetailedContactToList($contact, "53618");
		}
		
		/**
		 *  @param  array   $contact    An array describing a contact.
		 *                              Example below the function.
		 *  @param  int     $listID     The ID of the list.
		 *
		 */
		// $contact array example
		/*  $contact = array(
		 *      "Email"         =>  "foo@bar.com",   // Mandatory field!
		 *      "Name"          =>  "FooBar",
		 *      "Action"        =>  "addnoforce",
		 *      "Properties"    =>  array(
		 *          "Prop1" =>  "value1",
		 *          ...
		 *      )
		 *  );
		 */
		function addDetailedContactToList ($contact, $listID)
		{
		    $apiKey = 'b4d7966e315f3dcd045aeeec216f98c2';
		    $secretKey = 'f13ed25e14a125899c11237061325623';
		    
		    $mj = new Mailjet($apiKey, $secretKey);
		    
		    $params = array(
		        "method" => "POST",
		        "ID" => $listID
		    );
		    $params = array_merge($params, $contact);
		    $result = $mj->contactslistManageContact($params);
		    if ($mj->_response_code == 201){
		        log_message("error", "success - detailed contact ".$contactID." added to the list ".$listID);
		    }  else{
		        log_message("error", "error - ".$mj->_response_code);
		    }
		    log_message("error", print_r($result, TRUE));
		    return $result;
		}
		
		function view_evaluation_form($lang, $reg_id) {
			$data = array();
			$data['lang'] = $lang;
			$data['reg_id'] = $reg_id;
			$this->load->view ( 'evaluation.php', $data );
		}
		
		function execute_evaluation($reg_id){
			if (extract ( $_POST )) {
				$value['evaluation'] = $_POST ['star'];
				$where = "reg_id ='".$reg_id."'";
				$affected_rows = $this->common->updateRecord('student_registration',$value,$where);
				redirect ('registration_controller/view_evaluation_sent_msg/ar/'.$reg_id);
			}
		}
		
		function view_evaluation_sent_msg($lang, $reg_id) {
			$data = array();
			$data['lang'] = $lang;
			$data['reg_id'] = $reg_id;
			$this->load->view ( 'evaluation_sent_msg.php', $data );
		}
		
		/*
		 * For Converting all requests from a specific agent to the others.
		 */
		function assign_agent_registrations(){
			//$non_grade_registrations = $this->common->getAllRow ("student_registration", " where grade is null and agent_id is null");
			$agent_registrations = $this->common->getAllRow ("student_registration", " where agent_id = '24'");
			foreach ($agent_registrations as $request){
				$reg_id = $request['reg_id'];
				$responsible_mobile = $request['responsible_mobile'];
				//echo "$reg_id , ";
				
				$agent_id = $this->get_agent_with_least_requests();
				//echo " agent_id = $agent_id <br>";
				$value['agent_id'] = $agent_id;
				
				$this->common->updateRecord('student_registration', $value, " reg_id ='" . $reg_id . "'");
				//update all records of the same number with the agent_id value if there values = NULL
				$this->common->updateRecord('student_registration', $value, " responsible_mobile ='" . $responsible_mobile . "'");
			}
		}
		
		function assign_non_grade_registrations(){
			//$non_grade_registrations = $this->common->getAllRow ("student_registration", " where grade is null and agent_id is null");
			$non_grade_registrations = $this->common->getAllRow ("student_registration", " where agent_id is null");
			foreach ($non_grade_registrations as $request){
				$reg_id = $request['reg_id'];
				$responsible_mobile = $request['responsible_mobile'];
				//echo "$reg_id , ";
				
				$agent_id = $this->get_agent_with_least_requests();
				//echo " agent_id = $agent_id <br>";
				$value['agent_id'] = $agent_id;
				
				$this->common->updateRecord('student_registration', $value, " reg_id ='" . $reg_id . "'");
				//update all records of the same number with the agent_id value if there values = NULL
				$this->common->updateRecord('student_registration', $value, " responsible_mobile ='" . $responsible_mobile . "'");
			}
		}
		
		function get_agent_with_least_requests(){
			//get all agents
			$agents = $this->common->getAllRow ("agent_grade", "");
			//$agents = $this->common->getAllRow ("agent_grade", "where admin_id IN (15, 17, 27, 28, 29, 30)"); //TODO select distinct
			$selected_agent = "";
			$agents_ids = ",";
			for($i = 0; $i < count ($agents); $i ++) {
				$agent_id = $agents[$i]['admin_id'];
				if (strpos($agents_ids, ",". $agent_id. ",") === FALSE){
					//echo "The comming id not exist in the string.";
					$agents_ids .= $agent_id . ",";
				}
			}
			$agents_ids_trim = trim($agents_ids,',');
			$agents = explode(',', $agents_ids_trim);
			$sql_query = "SELECT `agent_id`, COUNT(`agent_id`) AS `value_occurrence` "."
											FROM `student_registration` WHERE `current_state_code` = 'NEW' and `agent_id` IN (".$agents_ids_trim.") GROUP BY `agent_id` ".
											"ORDER BY `value_occurrence` ASC;";
			//echo $sql_query ."<br>";
			$query = $this->db->query($sql_query);
			$used_agents = $query->result();
			if ($used_agents && !empty($used_agents)) {
				//All agents have requests so select the first agent that has least number.
				if (count($used_agents) == count($agents)){
					$agent = $used_agents[0];
					$selected_agent= $agent-> agent_id;
				}else { //Some agents have requests then select any of unused agents.
					foreach ($used_agents as $agent) {
						$used_agent_id = $agent-> agent_id;
						$agents_ids = str_replace($used_agent_id.',', '', $agents_ids);
					}
					//$selected_agent = str_replace(',', '', $agents_ids);
					$agents_ids = trim($agents_ids,',');
					$agents_ids_array = explode(',', $agents_ids);
					if(count($agents_ids_array) == 1){
					    $selected_agent = $agents_ids;
					}else{
					    $selected_agent = substr($agents_ids, 0, strpos($agents_ids, ','));
					}
				}
			}else{
				// All of agents are available (has no requests) so select any one.
				$selected_agent = $agent_id;
			}
			
			return $selected_agent;
		}
		

		function test(){
		    echo "test";
		}
		
		
}
	?>