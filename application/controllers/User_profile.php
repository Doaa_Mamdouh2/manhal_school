<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_profile extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
        $this -> load -> model("common");
        $this->load->model("notification_model");
        if ($this->session->userdata('adminid') == '') {
            redirect('admin', 'refresh');
        }
    }
    
	function index() {
	   self::view_profile();
	}
	
	function view_profile() {
	    $data = array();
	    if ($this->session->userdata ( 'adminid' ) != '') {
	        /* get admin data*/
	        $admin_id = $this->session->userdata('adminid');
	        $where = "where admin_id =".$admin_id;
	        $result = $this->common->getOneRow('admin',$where);
	        $data ['admin_name'] = $result['name'];
	        $data ['admin_type'] = $result['type'];
	        $user_type_code = $result['type'];
	        
	        $notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($admin_id);
	        $data['notifications_list'] = $notifications['notifications_list'];
	        $data['count_unseen'] = $notifications['count_unseen'];
	        
	        $this->load->view('utils/profile',$data);
	    } else {
	        redirect('admin', 'refresh');
	    }
	}

}

