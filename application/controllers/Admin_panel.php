<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
	define("PLEASE_EVALUATE_US","برجاء تقييم الخدمة لمدارس المنهل");
	
	class Admin_panel extends CI_Controller {
		
		function __construct() {
			parent::__construct();
			$this->load->model("common");
			$this->load->model("registration_model");
			$this->load->model("transactions_model");
			$this->load->model("notification_model");
			$this->load->model("activities_model");
			$this->load->model("admin_model");
			$this->load->helper ( array (
			    'url',
			    'Common_functions'
			) );
			if ($this->session->userdata('adminid') == '') {
				redirect('admin', 'refresh');
			}
		}
		
		function index() {
            self::view_registrations();
		}
		
		function view_registrations($display_page = 1, $page_size = 30, $page_number = 1, $state = '0', $grade = '0', $search = 0) {
		    if ($this->session->userdata ( 'adminid' ) != '') {
				/* get admin data*/
				$admin_id = $this->session->userdata('adminid');
				$where = "where admin_id =".$admin_id;
				$result = $this->common->getOneRow('admin',$where);
				$data ['admin_name'] = $result['name'];
				$data ['admin_type'] = $result['type'];
				$user_type_code = $result['type'];
				
				$notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($admin_id);
				$data['notifications_list'] = $notifications['notifications_list'];
				$data['count_unseen'] = $notifications['count_unseen'];
				
				
				$ordered_states_and_counts = $this->registration_model->getAllOrderedStates ($user_type_code, $admin_id);
				if($ordered_states_and_counts != null){
					$ordered_states = $ordered_states_and_counts ['states'];
					$ordered_states_total_count = $ordered_states_and_counts ['sum'];
				}else{
					$ordered_states = array();
					$ordered_states_total_count = 0;
				}
				
				$registration_grades_and_counts = $this->registration_model->getAllGrades ($user_type_code, $admin_id);
				if($registration_grades_and_counts != null){
					$registration_grades = $registration_grades_and_counts ['educational_grades'];
					$registration_grades_total_count = $registration_grades_and_counts ['sum'];
				}else{
					$registration_grades = array();
					$registration_grades_total_count = 0;
				}
				
				$student_registrations = $this->registration_model->getRegistrations($page_size, $page_number, null, null, $user_type_code, $admin_id, $state, $grade);
				$total_count = $this->registration_model->countRegistrations(null, null, $user_type_code, $admin_id, $state, $grade);
				
				$data['ordered_states'] = $ordered_states;
				$data['registration_states_total_count'] = $ordered_states_total_count;
				$data['registration_grades'] = $registration_grades;
				$data['registration_grades_total_count'] = $registration_grades_total_count;
				$data['student_registrations'] = $student_registrations;
				$data['total_count'] = $total_count;
				$data['current_page'] = $page_number;
				$data["is_search"] = "0";
				$data["search_values"] = "";
				$data['user_type_code'] = $user_type_code;
				$data['selected_state'] = $state;
				$data['selected_grade'] = $grade;
				$data['available_states'] = $this->common->getAllRow("state", "");
				$data['query'] = "";
				$data['view_registrations'] = "1";
				$data['view_late_regs_lvl'] = "0";
				
				if ($display_page == 1) {
					$this->load->view('view_student_registrations', $data);
				} else {
					echo json_encode($data);
				}
			} else {
				redirect('admin', 'refresh');
			}
		}
		
		function view_student_registration($reg_id = 0, $send_eval = "N") {
			$data ['reg_id'] = $reg_id;
			$data ['responsible_name'] = "";
			$data ['responsible_mobile'] = "";
			$data ['responsible_email'] = 0;
			$data ['creation_date'] = "";
			$data ['student_name'] = "";
			$data ['department'] = "";
			$data ['track'] = "";
			//$data ['IBAN'] = "";
			//$data ['IBAN_source'] = "";
			$data ['knowing_way'] = "";
			$data ['other_way'] = "";
			$data ['school'] = "";
			$data ['grade'] = "";
			$data ['old_school'] = "";
			//$data ['DOB_1'] = "";
			//$data ['DOB_2'] = "";
			$data ['current_state_code'] = "";
			$data ['comment'] = "";
			$data ['agent_id'] = "";
			$data ['evaluation'] = "";
			$data ['available_states'] = $this->common->getAllRow("state", "");
			$data ['available_agents'] = $this->common->getAllRow("admin", "where type = 'AGENT' and active = 1 ");
			$data ['educational_grade'] = $this->common->getAllRow("educational_grade", '  ORDER BY `educational_grade`.`id` ASC');
			$data ['knowing_ways'] = $this->common->getAllRow ( "knowing_way", '  ORDER BY `knowing_way`.`id` ASC' );
			
			$admin_id = $this->session->userdata('adminid');
			$where = "where admin_id =".$admin_id;
			$adminRecord = $this->common->getOneRow('admin',$where);
			$data ['admin_name'] = $adminRecord['name'];
			$data ['admin_type'] = $adminRecord['type'];
			$data ['send_eval'] = $send_eval;
			
            $notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($admin_id);
            $data['notifications_list'] = $notifications['notifications_list'];
            $data['count_unseen'] = $notifications['count_unseen'];
            
			if ($reg_id > 0) {
				$where = "where reg_id =" . $reg_id . " ORDER BY creation_date_time DESC";
				$result = $this->common->getOneRow('student_registration', $where);
				
				$data ['reg_id'] = $result ['reg_id'];
				$data ['responsible_name'] = $result ['responsible_name'];
				$data ['responsible_mobile'] = $result ['responsible_mobile'];
				$data ['responsible_email'] = $result ['responsible_email'];
				$data ['creation_date'] = $result ['creation_date'];
				$data ['student_name'] = $result ['student_name'];
				$data ['department'] = $result ['department'];
				$data ['track'] = $result ['track'];
				//$data ['IBAN'] = $result ['IBAN'];
				//$data ['IBAN_source'] = $result ['IBAN_source'];
				$data ['knowing_way'] = $result ['knowing_way'];
				$data ['other_way'] = $result ['other_way'];
				$data ['school'] = $result ['school'];
				$data ['grade'] = $result ['grade'];
				$data ['old_school'] = $result ['old_school'];
				$data ['is_contacted'] = $result ['is_contacted'];
				//$data ['DOB_1'] = $result ['DOB'];
				//$data ['DOB_2'] = GregorianToHijri($result ['DOB']);
				$data ['evaluation'] = $result['evaluation'];
				if(isset($result ['notification_time']) && $result ['notification_time'] != null){
				    $data ['notification_time'] = date("m/d/Y", strtotime($result ['notification_time']));
				}else{
				    $data ['notification_time'] = NULL;
				}
				$data ['current_state_code'] = $result ['current_state_code'];
				$data ['comment'] = $result ['comment'];
				$data ['agent_id'] = $result ['agent_id'];
				// transactions
				$data ['transactions'] = $this->transactions_model->getRegTransactions($result ['reg_id']);
				if ($data ['transactions']) {
					$i = 0;
					foreach ($data ['transactions'] as $record) {
						$date_parts = explode(" ", $record ['date_time']);
						$hijri_date = GregorianToHijri($date_parts [0]);
						$data ['transactions'] [$i] ['date_time'] = $hijri_date . " " . $date_parts [1];
						$i ++;
					}
				}
				
				//Return all requests with the same responsible mobile.
				$requests = $this->common->getAllRow("student_registration", "where responsible_mobile = '" . $result ['responsible_mobile']."' and reg_id != '".$result ['reg_id']."'");
				$data['requests'] = $requests;
			}
			//$data['view_registrations'] = "1";
			$this->load->view('view_student_registration', $data);
		}
		
		function edit_student_registration($reg_id = 0){
			$data ['reg_id'] = $reg_id;
			$data ['responsible_name'] = "";
			$data ['responsible_email'] = "";
			$data ['creation_date'] = "";
			$data ['student_name'] = "";
			$data ['department'] = "";
			$data ['track'] = "";
			//$data ['IBAN'] = "";
			//$data ['IBAN_source'] = "";
			$data ['knowing_way'] = "";
			$data ['other_way'] = "";
			$data ['grade'] = "";
			$data ['old_school'] = "";
			//$data ['DOB_1'] = "";
			$data ['agent_id'] = "";
			
            $notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($admin_id);
            $data['notifications_list'] = $notifications['notifications_list'];
            $data['count_unseen'] = $notifications['count_unseen'];
           
			if (extract ( $_POST )) {
				$value ['responsible_name'] = $_POST ['responsible_name'];
				$value ['responsible_email'] = $_POST ['responsible_email'];
				$value ['creation_date'] = date("Y-m-d", strtotime($_POST ['creation_date']));
				$value ['creation_date_time'] = date("Y-m-d H:i:s", strtotime($_POST ['creation_date']));
				$value ['student_name'] = $_POST ['student_name'];
				$value ['department'] = $_POST ['department'];
				$value ['track'] = $_POST ['track'];
				//$value ['IBAN'] = $_POST ['IBAN'];
				//$value ['IBAN_source'] = $_POST ['IBAN_source'];
				$value ['knowing_way'] = $_POST ['knowing_way'];
				$value ['other_way'] = $_POST ['other_way'];
				$value ['grade'] = $_POST ['grade']==''? null : $_POST ['grade'];
				$value ['old_school'] = $_POST ['old_school'];
				//$sourceDate = $_POST ['DOB_1'];
				//$date = new DateTime($sourceDate);
				//$value ['DOB']= $date->format('Y-m-d');
	    		$value ['agent_id'] = $_POST ['agent_id']==''? null:$_POST ['agent_id'];
				if ($reg_id> 0) {
					$where = "reg_id ='" . $reg_id. "'";
					//get last agent before update then update the record
					$reg_record = $this->common->getOneRow ( 'student_registration', "where reg_id = '".$reg_id."'" ); 
					$last_agent = $reg_record['agent_id'];
					$this->common->updateRecord ( 'student_registration', $value, $where );
					//=====================Insert Activity for not admin user=====================
					$user_id = $this->session->userdata ( 'adminid' );
					$user_record = $this->common->getOneRow ( 'admin', "where admin_id = ".$user_id );
					if("ADMIN" != $user_record ['type']){ 
						insert_activity($user_id, 3, $reg_id);
					}
					
					//Update all records that have the same responsible number with the changed agent.
					if(isset($_POST ['agent_id']) && $_POST ['agent_id'] != NULL && $_POST ['agent_id'] != ""){
						if($last_agent != $_POST ['agent_id']){ // the agent is changed
							$where2 = "responsible_mobile ='" . $_POST['responsible_mobile']. "'";
							$value2 ['agent_id'] = $_POST ['agent_id'];
							$this->common->updateRecord ( 'student_registration', $value2, $where2 );
							
							//In case of changing the agent -> send notification to the admin that the requests are transfered to the agent successfuly.
							//send to admin -- loop on admins
							$admins = $this->common->getAllRow ( 'admin', "where type = 'ADMIN'" );
							for($j=0; $j< count($admins) ; $j++){
							    $admin_record =  $admins[$j];
    							$notif_values = array ();
    							$notif_values ['notification_type_code'] = 'M005';
    							$notif_values ['to_user_id'] = $admin_record ['admin_id'];
    							$notif_values ['reg_id'] = $reg_id;
    							$notif_values ['waiting_days'] = '0';
    							$sent_successfully = insert_delivered_notification($notif_values);
    							log_message("error", "current reg id = ". $reg_id);
							}
							
							//sending notification to all agents
							$responsible_registrations = $this->common->getAllRow ( "student_registration", "where responsible_mobile = '" . $_POST['responsible_mobile']."'");
							for($i = 0; $i < count ($responsible_registrations); $i ++) {
								$agent_id = $responsible_registrations[$i]['agent_id'];
								$reg_id= $responsible_registrations[$i]['reg_id'];
								$notif_values = array ();
								$notif_values ['notification_type_code'] = 'M007';
								$notif_values ['to_user_id'] = $agent_id;
								$notif_values ['reg_id'] = $reg_id;
								$notif_values ['waiting_days'] = '0';
								$sent_successfully = insert_delivered_notification($notif_values);
							}
						}
					}
				}
				
				redirect ('admin_panel/view_student_registration/'.$reg_id);
			}
		}
		
		function advanced_search($display_page = 1, $page_size = 30, $page_number = 1, $state = '0', $grade = '0', $view_late_regs_lvl = '0') {
			if ($this->session->userdata ( 'adminid' ) != '') {
				/* get admin data*/
				$admin_id = $this->session->userdata('adminid');
				$where = "where admin_id =".$admin_id;
				$result = $this->common->getOneRow('admin',$where);
				$data ['admin_name'] = $result['name'];
				$data ['admin_type'] = $result['type'];
				$user_type_code = $result['type'];
                
                $notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($admin_id);
                $data['notifications_list'] = $notifications['notifications_list'];
                $data['count_unseen'] = $notifications['count_unseen'];
                
				if (extract($_POST)) {
					$search_values = $_POST;
					if($view_late_regs_lvl == '0'){
    					$student_registrations = $this->registration_model->getRegistrations($page_size, $page_number, $search_values, '1', $user_type_code, $admin_id);
    					$total_count = $this->registration_model->countRegistrations($search_values, '1', $user_type_code, $admin_id);
					}else{
					    $output = $this->registration_model->getLateRegistrations($page_size, $page_number, $search_values, '1', $user_type_code, $admin_id, $state, $grade, $view_late_regs_lvl);
					    $student_registrations = $output ['late_registrations'];
					    $total_count = $output ['total_count'];
					}
					$data['student_registrations'] = $student_registrations;
					$data['total_count'] = $total_count;
					$data['current_page'] = $page_number;
					$data["is_search"] = "1";
					$data["search_values"] = $search_values;
					$data['count_unseen'] = 0;
					$data['notifications_list'] = array();
					$data['user_type_code'] = $user_type_code;
					$data['selected_state'] = $state;
					$data['selected_grade'] = $grade;
					$data['available_states'] = $this->common->getAllRow("state", "");
					$data['query'] = "";
			        $data['view_late_regs_lvl'] = $view_late_regs_lvl;
					
					if ($display_page == 1) {
					    if($view_late_regs_lvl == '0'){
    					    $data['view_registrations'] = "1";
					    }
					    $this->load->view('view_student_registrations', $data);
					} else {
						echo json_encode($data);
					}
				} else {
					$data ['available_states'] = $this->common->getAllRow("state", "  ORDER BY `state`.`id` ASC");
					$data ['educational_grade'] = $this->common->getAllRow("educational_grade", '  ORDER BY `educational_grade`.`id` ASC');
					$data ['available_agents'] = $this->common->getAllRow("admin", "where type = 'AGENT' and active = 1 ");
					$data ['view_late_regs_lvl'] = $view_late_regs_lvl;
					$this->load->view('advanced_search_form',$data);
				}
			} else {
				redirect('admin', 'refresh');
			}
		}
		
		function quick_search($display_page = 1, $page_size = 30, $page_number = 1, $state = '0', $grade = '0', $view_late_regs_lvl = '0') {
			if ($this->session->userdata ( 'adminid' ) != '') {
				/* get admin data*/
				$admin_id = $this->session->userdata('adminid');
				$where = "where admin_id =".$admin_id;
				$result = $this->common->getOneRow('admin',$where);
				$data ['admin_name'] = $result['name'];
				$data ['admin_type'] = $result['type'];
				$user_type_code = $result['type'];
                
                $notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($admin_id);
                $data['notifications_list'] = $notifications['notifications_list'];
                $data['count_unseen'] = $notifications['count_unseen'];
                
				if (extract($_POST)) {
					$search_values = $_POST;
					if($view_late_regs_lvl =='0'){
					    $student_registrations = $this->registration_model->getRegistrations($page_size, $page_number, $search_values, '2', $user_type_code, $admin_id);
					    $total_count = $this->registration_model->countRegistrations($search_values, '2', $user_type_code, $admin_id);
					}else{
					    $output = $this->registration_model->getLateRegistrations($page_size, $page_number, $search_values, '2', $user_type_code, $admin_id, $state, $grade, $view_late_regs_lvl);
					    $student_registrations = $output ['late_registrations'];
					    $total_count = $output ['total_count'];
					}
					$data['student_registrations'] = $student_registrations;
					$data['total_count'] = $total_count;
					$data['current_page'] = $page_number;
					$data["is_search"] = "2";
					$data["search_values"] = $search_values;
					$data['count_unseen'] = 0;
					$data['notifications_list'] = array();
					$data['user_type_code'] = $user_type_code;
					$data['selected_state'] = $state;
					$data['selected_grade'] = $grade;
					$data['available_states'] = $this->common->getAllRow("state", "");
					$data['query'] = $_POST['query'];
					$data['view_late_regs_lvl'] = $view_late_regs_lvl;
					
					if ($display_page == 1) {
						if($view_late_regs_lvl == '0'){
						    $data['view_registrations'] = "1";
						}
						$this->load->view('view_student_registrations', $data);
					} else {
						echo json_encode($data);
					}
				}
			} else {
				redirect('admin', 'refresh');
			}
		}
		
			
		function update_is_contacted_value() {
			if (extract($_POST)) {
				$reg_id = $_POST['reg_id'];
				$value['is_contacted'] = $_POST['is_contacted'];
				if ($reg_id != "") {
					$where = "reg_id ='" . $reg_id . "'";
					$this->common->updateRecord('student_registration', $value, $where);
					//=====================Insert Activity for not admin user=====================
					$user_id = $this->session->userdata ( 'adminid' );
					$user_record = $this->common->getOneRow ( 'admin', "where admin_id = ".$user_id );
					if("ADMIN" != $user_record ['type']){  // store the activities of not admin user
						insert_activity($user_id, 3, $reg_id);
					}
				}
			}
		}
		
		function changeState() {
			if (extract($_POST)) {
				$reg_id = $_POST['reg_id'];
				$value['current_state_code'] = $_POST['state'];
				if ($reg_id != "") {
					$where = "reg_id ='" . $reg_id . "'";
					$this->common->updateRecord('student_registration', $value, $where);
				}
			}
		}
		
		function add_transaction_ajax(){
			if (isset($_POST['reg_id'])) {
				self::add_transaction($_POST['reg_id'], "yes"); //without any new values, get the latest values from the last state( just store the time ).
			}
		}
		
		function add_transaction($student_reg_id = '', $come_from_registrations = "no") {
			//Adding default values in case of null for comment, is_contacted, state_code with the last state in the student_registration table
			$where = "where reg_id =" . $student_reg_id. " ORDER BY creation_date_time DESC";
			$result = $this->common->getOneRow('student_registration', $where);
			
			if (extract($_POST)) {
				date_default_timezone_set('Asia/Riyadh');
				
				if($come_from_registrations == "yes"){
					$value ['comment'] = "تم التواصل مع ولي الأمر";
					$value ['is_contacted'] = "Y";
				}else{
					$value ['comment'] = $_POST ['comment'];
					//Adding default value with the last state if it is not null
					if($value ['comment'] == NULL){
						if($result["comment"] != NULL){
							$value ['comment'] = $result["comment"];
						}else{
							$value ['comment'] = "";
						}
					}
					
					$value ['is_contacted'] = $_POST ['is_contacted'];
					//Adding default value with the last state if it is not null
					if($value ['is_contacted'] == NULL){
						if($result["is_contacted"] != NULL){
							$value ['is_contacted'] = $result["is_contacted"];
						}else{
							$value ['is_contacted'] = "N";
						}
					}
				}
				
				$value ['state_code'] = $_POST ['current_state_code'];
				//Adding default value with the last state if it is not null
				if($value ['state_code'] == NULL){
					if($result["current_state_code"] != NULL){
						$value ['state_code'] = $result["current_state_code"]; //Adding default value
					}else{
						$value ['state_code'] = "NEW";
					}
				}
				
				$old_state = $_POST ['old_state'];
				if($value ['state_code'] != $old_state){
					$value ['state_changed_date'] = date("Y-m-d H:i:s");
				}else{
					$value ['state_changed_date'] = NULL;
				}
				
				$notification_time= $_POST ['notification_time'];
				if($notification_time != NULL){
					$date = new DateTime($notification_time);
					$value ['notification_time'] = $date->format('Y-m-d');
				}else{
					$value ['notification_time'] = NULL;
				}
				
				$admin_id = $this->session->userdata('adminid');
				$value ['admin_id'] = $admin_id;
				$value ['reg_id'] = $student_reg_id;
				$value ['date_time'] = date("Y-m-d H:i:s");
				
				//Enable Send Evaluation to the client after adding a comment.
				$send_eval = "N";
				
				if(isset($value ['comment']) && $value ['comment'] != null && $value ['comment'] != ""){
					//Get last record in the transaction table and compare the comments if there is a change then enable evaluation button.
					$trans_record = $this->common->getOneRow ( 'registration_transactions', "order by id desc" );
					if($trans_record['comment'] != $value ['comment']){
						$send_eval = "Y";
					}
				}
				
				$this->common->insertRecord('registration_transactions', $value);
				
				//=====================Insert Activity for not admin user=====================
				$user_record = $this->common->getOneRow ( 'admin', "where admin_id = ".$admin_id );
				if("ADMIN" != $user_record ['type']){  // store the activities of not admin user
				    insert_activity($admin_id, 3, $student_reg_id);
				}
				
				//update student_registration table
				$reg_value['current_state_code'] = $value ['state_code'];
				$reg_value['comment'] = $value ['comment'];
				$reg_value['is_contacted'] = $value ['is_contacted'];
				$reg_value['notification_time'] = $value ['notification_time'];
				$old_state = $_POST ['old_state'];
				if($value ['state_code'] != $old_state){
				    $reg_value ['state_changed_date'] = date("Y-m-d H:i:s");
				}
				$where = "reg_id ='" . $student_reg_id. "'";
				$this->common->updateRecord('student_registration', $reg_value, $where);
			}
			redirect('Admin_panel/view_student_registration/' . $student_reg_id . "/". $send_eval);
		}
		
        function send_notif_to_admin(){
            $sent_successfully = false;
            if (extract($_POST)) {
                $reg_id = $_POST ['reg_id'];
                
                //send to admin -- loop on admins
                $admins = $this->common->getAllRow ( 'admin', "where type = 'ADMIN'" );
                for($j=0; $j< count($admins) ; $j++){
                    $admin_record =  $admins[$j];
                    $notif_values = array ();
                    $notif_values ['notification_type_code'] = 'M001';
                    $notif_values ['to_user_id'] = $admin_record ['admin_id'];
                    $notif_values ['reg_id'] = $reg_id;
                    $notif_values ['waiting_days'] = '0';
                    $sent_successfully = insert_delivered_notification($notif_values);
                }
            }

            $this->output->set_content_type ( "application/json" )->set_output ( json_encode ( array (
                'status' => $sent_successfully
            ) ) );
        }
        
        function send_eval_to_client(){
        	$sent_successfully = false;
        	if (extract($_POST)) {
        		$reg_id = $_POST ['reg_id'];
        		$final_reg_rec = $this->common->getOneRow('student_registration', "where reg_id =" . $reg_id);
        		$evaluation_url = site_url('Registration_controller/view_evaluation_form/ar/' . $reg_id);
        		$sms_message = PLEASE_EVALUATE_US . " " . $evaluation_url;
        		$sms_sent = sendSMS ('Y', $final_reg_rec['responsible_mobile'], $sms_message);
        	}
        	
        	$this->output->set_content_type ( "application/json" )->set_output ( json_encode ( array (
        			'status' => $sms_sent
        	) ) );
        }
         
        function view_dashboard_reports(){
            $data = array();
            $logged_in_user =  $this->common->getOneRow ( 'admin', "where admin_id = ". $this->session->userdata ( 'adminid' ));
                
            if ($this->session->userdata ( 'adminid' ) != '' && $logged_in_user != NULL  && isset ($logged_in_user['type']) && $logged_in_user['type'] == 'ADMIN') {
                /* get admin data*/
                $admin_id = $this->session->userdata('adminid');
                $where = "where admin_id =".$admin_id;
                $result = $this->common->getOneRow('admin',$where);
                $data ['admin_name'] = $result['name'];
                $data ['admin_type'] = $result['type'];
                $user_type_code = $result['type'];
                
                $notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($admin_id);
                $data['notifications_list'] = $notifications['notifications_list'];
                $data['count_unseen'] = $notifications['count_unseen'];
                
                //report 1
                $ordered_states_and_counts = $this->registration_model->getAllOrderedStates ($user_type_code, $admin_id);
                if($ordered_states_and_counts != null){
                    $ordered_states = $ordered_states_and_counts ['states'];
                    $ordered_states_total_count = $ordered_states_and_counts ['sum'];
                }else{
                    $ordered_states = array();
                    $ordered_states_total_count = 0;
                }
                $data['ordered_states'] = $ordered_states;
                $data['ordered_states_total_count'] = $ordered_states_total_count;
                
                //report 2
                //Query to test : select creation_date, count(creation_date) from student_registration group by creation_date
                $registrationsCountInLast60Days = $this->registration_model->getRegistrationsCountInLast60Days();
                //log_message("error", "registrationsCountInLast60Days:: ".print_r($registrationsCountInLast60Days, TRUE));
                $data['registrationsCountInLast60Days'] = $registrationsCountInLast60Days;
                
                //report 3
                //Query to test each agent : select current_state_code, count(current_state_code) from student_registration where agent_id = 2 group by current_state_code
                $output = $this->registration_model->getAgentsRegistrationsCounts();
                //log_message("error", "agents_registrationsCounts:: ".print_r($output, TRUE));
                $data['agents_registrationsCounts'] = $output['agents_registrationsCounts'];
                $data['sum_new'] = $output['sum_new'];
                $data['sum_have_progress'] = $output['sum_have_progress'];
                
                //report 4
                $output = $this->registration_model->getAgentsPerformance();
                //log_message("error", "agents_registrationsCounts:: ".print_r($output, TRUE));
                $data['agents_performance'] = $output['agents_performance'];
                $data['sum_all'] = $output['sum_all'];
                $data['sum_delayed'] = $output['sum_delayed'];
                
                //report 5
                $current_page = 1;
                $total_activities_num = 0;
                $current_page_size = 0;
                $page_size = 30;
                $offset = 0;
                $has_prev = false;
                $has_next = false;
                $dashboard_activities = $this->activities_model->getDashboardActivities($offset, $page_size);
                if($dashboard_activities){
                    $total_activities_num = $dashboard_activities [0] ['count_all'];
                    $current_page_size = count($dashboard_activities);
                }
                $total_pages_num = ceil ( $total_activities_num / $page_size );
                if ($current_page > 1) {
                    $has_prev = true;
                }
                if ($current_page < $total_pages_num) {
                    $has_next = true;
                }
                $data ['dashboard_activities'] = $dashboard_activities;
                $data ['current_page'] = 1;
                $data ['total_pages_num'] = $total_pages_num;
                $data ['page_size'] = $page_size;
                $data ['total_activities_num'] = $total_activities_num;
                $data ['current_page_size'] = $current_page_size;
                $data ['has_prev'] = $has_prev;
                $data ['has_next'] = $has_next;
                
                $data['view_dashboard_reports'] = "1";
                $this->load->view('view_dashboard_reports', $data);
            } else {
                redirect('admin', 'refresh');
            }
        }
        
        function view_usage_report(){
            $has_prev = false;
            $has_next = false;
            $page_size = 30;
            $total_activities_num = 0;
            $total_pages_num = 0;
            $current_page_size = 0;
            $current_page = 1;
            
            if (extract ( $_POST )) {
                $current_page = $this->input->post ( 'current_page' );
            }
            
            $offset = ($current_page * $page_size) - $page_size;
            
            $dashboard_activities = $this->activities_model->getDashboardActivities($offset, $page_size);
            
            if($dashboard_activities){
                $total_activities_num = $dashboard_activities [0] ['count_all'];
                $current_page_size = count($dashboard_activities);
            }
            
            $total_pages_num = ceil ( $total_activities_num / $page_size );
            
            if ($current_page > 1) {
                $has_prev = true;
            }
            if ($current_page < $total_pages_num) {
                $has_next = true;
            }
            
            $data ['current_page'] = $current_page;
            $data ['total_pages_num'] = $total_pages_num;
            $data ['page_size'] = $page_size;
            $data ['total_activities_num'] = $total_activities_num;
            $data ['current_page_size'] = $current_page_size;
            $data ['has_prev'] = $has_prev;
            $data ['has_next'] = $has_next;
            $data ['dashboard_activities'] = $dashboard_activities;
            
            $faq_View = $this->load->view ( 'view_dashboard_reports', $data, TRUE );
            echo json_encode ( $faq_View );
            
        }
        
        function view_late_registrations($display_page = 1, $page_size = 30, $page_number = 1, $state = '0', $grade = '0', $search = 0, $view_late_regs_lvl){
            if ($this->session->userdata ( 'adminid' ) != '') {
                /* get admin data*/
                $admin_id = $this->session->userdata('adminid');
                $where = "where admin_id =".$admin_id;
                $result = $this->common->getOneRow('admin',$where);
                $data ['admin_name'] = $result['name'];
                $data ['admin_type'] = $result['type'];
                $user_type_code = $result['type'];
                
                $notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($admin_id);
                $data['notifications_list'] = $notifications['notifications_list'];
                $data['count_unseen'] = $notifications['count_unseen'];
                                
                $output = $this->registration_model->getLateRegistrations($page_size, $page_number, null, null, $user_type_code, $admin_id, $state, $grade, $view_late_regs_lvl);
                $student_registrations = $output ['late_registrations'];
                $total_count = $output ['total_count'];
                $ordered_states = $output ['states'];
                $ordered_states_total_count = $output ['states_sum'];
                $registration_grades = $output ['educational_grades'];
                $registration_grades_total_count = $output ['grades_sum'];
                
                $data['ordered_states'] = $ordered_states;
                $data['registration_states_total_count'] = $ordered_states_total_count;
                $data['registration_grades'] = $registration_grades;
                $data['registration_grades_total_count'] = $registration_grades_total_count;
                $data['student_registrations'] = $student_registrations;
                $data['total_count'] = $total_count;
                $data['current_page'] = $page_number;
                $data["is_search"] = "0";
                $data["search_values"] = "";
                $data['user_type_code'] = $user_type_code;
                $data['selected_state'] = $state;
                $data['selected_grade'] = $grade;
                $data['available_states'] = $this->common->getAllRow("state", "");
                $data['query'] = "";
                $data['view_late_regs_lvl'] = $view_late_regs_lvl;
                
                if ($display_page == 1) {
                    $this->load->view('view_student_registrations', $data);
                } else {
                    echo json_encode($data);
                }
            } else {
                redirect('admin', 'refresh');
            }
            
            
        }
        
        function delete_registration($reg_id) {
        	$this->common->deleteRecord ('activities_log', "reg_id = '" . $reg_id. "'");
        	$this->common->deleteRecord ('delivered_notifications', "reg_id = '" . $reg_id. "'");
        	$this->common->deleteRecord ('registration_transactions', "reg_id = '" . $reg_id. "'");
        	$this->common->deleteRecord ('student_registration', "reg_id = '" . $reg_id. "'");
        	
        	redirect ('admin_panel/view_registrations');
        }
        
        function view_agents($display_page = 1, $page_size = 30, $page_number = 1, $state = '0', $grade = '0', $search = 0) {
        	if ($this->session->userdata ( 'adminid' ) != '') {
        		/* get admin data*/
        		$admin_id = $this->session->userdata('adminid');
        		$where = "where admin_id =".$admin_id;
        		$result = $this->common->getOneRow('admin',$where);
        		$data ['admin_name'] = $result['name'];
        		$data ['admin_type'] = $result['type'];
        		$user_type_code = $result['type'];
        		
        		$notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($admin_id);
        		$data['notifications_list'] = $notifications['notifications_list'];
        		$data['count_unseen'] = $notifications['count_unseen'];
        		
        		$agents = $this->admin_model->getAgents($page_size, $page_number);
        		$total_count = $this->admin_model->getAgentsCount();
        		
        		$data['agents'] = $agents;
        		$data['total_count'] = $total_count;
        		$data['current_page'] = $page_number;
        		$data['user_type_code'] = $user_type_code;
        		$data['view_agents'] = "1";
        		
        		if ($display_page == 1) {
        			$this->load->view('view_agents', $data);
        		} else {
        			echo json_encode($data);
        		}
        	} else {
        		redirect('admin', 'refresh');
        	}
        }
        
        function delete_agent($agent_id) {
        	
        	$this->common->deleteRecord ('activities_log', "user_id = '" . $agent_id. "'");
        	$this->common->deleteRecord ('agent_grade', "admin_id = '" . $agent_id. "'");
        	$this->common->deleteRecord ('delivered_notifications', "to_user_id = '" . $agent_id. "'");
        	//$this->common->deleteRecord ('registration_transactions', "admin_id = '" . $agent_id. "'");
        	//$this->common->deleteRecord ('student_registration', "agent_id = '" . $agent_id. "'");
        	
        	$this->common->deleteRecord ('admin', "admin_id = '" . $agent_id. "'");
        		
        	redirect ('admin_panel/view_agents');
        }
        
        
        function add_edit_agent($agent_id = 0) {
        	if ($this->session->userdata ( 'adminid' ) != '') {
        		/* get admin data*/
        		$admin_id = $this->session->userdata('adminid');
        		$where = "where admin_id =".$admin_id;
        		$result = $this->common->getOneRow('admin',$where);
        		$data ['admin_name'] = $result['name'];
        		$data ['admin_type'] = $result['type'];
        		$user_type_code = $result['type'];
        		
        		$notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($admin_id);
        		$data['notifications_list'] = $notifications['notifications_list'];
        		$data['count_unseen'] = $notifications['count_unseen'];
        		
        		
	        	$data ['admin_id'] = $agent_id;
	        	$data ['name'] = "";
	        	$data ['mobile_number'] = "";
	        	$data ['email'] = "";
	        	$data ['user_name'] = "";
	        	$data ['password'] = "";
	        	$data ['type'] = "AGENT";
	        	$data ['educational_grades'] = $this->common->getAllRow("educational_grade", '  ORDER BY `educational_grade`.`id` ASC');
	        	$data ['schools'] = $this->common->getAllRow ( "school", '  ORDER BY `school`.`id` ASC' );
	        	$data ['available_agents'] = $this->common->getAllRow("admin", "where type = 'AGENT' and active = 1 ");
	        	
	        	if (extract ( $_POST )) {
	        		$value ['name'] = $_POST ['name'];
	        		$value ['mobile_number'] = $_POST ['mobile_number'];
	        		$value ['email'] = $_POST ['email'];
	        		$value ['user_name'] = $_POST ['user_name'];
	        		$value ['password'] = $_POST ['password'];
	        		$value ['type'] = "AGENT";
	        		$agent_grades = $_POST ['grade'];
	        		$moved_agent_id= $_POST ['moved_agent_id'];
	        		
	        		$final_agent_id = $agent_id;
	        		//---------EDIT CASE----------
	        		if ($agent_id > 0) {
	        			$where = "admin_id ='" . $agent_id. "'";
	        			$this->common->updateRecord ('admin', $value, $where);
	        			
	        			//Delete the old records and insert new ones
	        			$this->common->deleteRecord ('agent_grade', "admin_id = '" . $agent_id. "'");
	        			if($moved_agent_id > 0){
	        				$value3['agent_id'] = $moved_agent_id;
	        				$this->common->updateRecord('student_registration', $value3, " agent_id ='" . $agent_id."'");
	        			}
	        		//---------ADD CASE----------
	        		} else {
	        			$new_agent_id = $this->common->insertRecord ('admin', $value);
	        			$final_agent_id = $new_agent_id;
	        		}
	        		// Insert the new agent grades
	        		if(isset($agent_grades)){
		        		foreach ($agent_grades as $agent_grade){
		        			$school_grade = explode("_", $agent_grade);
		        			$school_code = $school_grade[0];
		        			if($school_code == "SCHOOL1"){
		        				$school_id = 1;
		        			}else{
		        				$school_id = 2;
		        			}
		        			
		        			$value2['admin_id'] = $final_agent_id;
		        			$value2['edu_grade_id'] = $school_grade[1];
		        			$value2['school_id'] = $school_id;
		        			$this->common->insertRecord ('agent_grade', $value2); 
		        		}
	        		}
	        		redirect ('admin_panel/view_agents');
	        	}
	        	
	        	if ($agent_id > 0) {
	        		$where = "where admin_id ='" . $agent_id. "'";
	        		$result = $this->common->getOneRow ('admin', $where );
	        		$data ['admin_id'] = $result ['admin_id'];
	        		$data ['name'] = $result ['name'];
	        		$data ['mobile_number'] = $result ['mobile_number'];
	        		$data ['email'] = $result ['email'];
	        		$data ['user_name'] = $result ['user_name'];
	        		$data ['password'] = $result ['password'];
	        		$data ['agents'] = 1;
	        		$data ['agent_grades'] = $this->common->getAllRow ("agent_grade", $where); 
	        	}
	        	
	        	$this->load->view ('add_edit_agent', $data);
	        } else {
	        	redirect('admin', 'refresh');
	        }
		}
		
		
	}
	