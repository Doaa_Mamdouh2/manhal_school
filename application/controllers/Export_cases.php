<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Export_cases extends CI_Controller {
	
	function __construct() {
		parent::__construct ();
		/*if ($this->session->userdata('adminid') == '') {
			redirect('admin', 'refresh');
		}*/
	}
	function index() {
		self::excel_export ();
	}
	function excel_export($from_date = null, $to_date = null, $not_interested = 0) {
		
		$this->load->model ( "registration_model" );
		
		$data = $this->registration_model->getExportedRegistrationsData($from_date, $to_date, $not_interested);
		
		//print_r($data);
		$file_name = 'data.xlsx';
		$spreadsheet = new Spreadsheet();
		$spreadsheet->setActiveSheetIndex(0);
		$sheet = $spreadsheet->getActiveSheet()->setRightToLeft(true);
		
		foreach(range('A','O') as $columnID) {
			$spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setWidth(15);
		}
		
		$sheet->setCellValue('A1', 'رقم الطلب')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValue('B1', 'اسم ولي الأمر')->getStyle('B1')->getFont()->setBold(true);
		$sheet->setCellValue('C1', 'رقم الجوال')->getStyle('C1')->getFont()->setBold(true);
		$sheet->setCellValue('D1', 'البريد الإلكتروني')->getStyle('D1')->getFont()->setBold(true);
		$sheet->setCellValue('E1', 'تاريخ التسجيل')->getStyle('E1')->getFont()->setBold(true);
		$sheet->setCellValue('F1', 'اسم الطالب')->getStyle('F1')->getFont()->setBold(true);
		$sheet->setCellValue('G1', 'الفرع')->getStyle('G1')->getFont()->setBold(true);
		$sheet->setCellValue('H1', 'تم التواصل')->getStyle('H1')->getFont()->setBold(true);
		$sheet->setCellValue('I1', 'الحالة')->getStyle('I1')->getFont()->setBold(true);
		$sheet->setCellValue('J1', 'تعليق')->getStyle('J1')->getFont()->setBold(true);
		$sheet->setCellValue('K1', 'الوسيلة التى تم معرفة المدرسة من خلالها')->getStyle('K1')->getFont()->setBold(true);
		$sheet->setCellValue('L1', 'القسم')->getStyle('L1')->getFont()->setBold(true);
		$sheet->setCellValue('M1', 'المسار')->getStyle('M1')->getFont()->setBold(true);
		$sheet->setCellValue('N1', 'المرحلة الدراسية')->getStyle('N1')->getFont()->setBold(true);
		$sheet->setCellValue('O1', 'ممثل الخدمة')->getStyle('O1')->getFont()->setBold(true);;
		
		
		$count = 2;
		
		foreach($data as $row)
		{
			$school_value = "";
			$school = $row['school'];
			if($school == "1"){
				$school_value = "فرع التعاون";
			}else if($school == "2"){
				$school_value = "فرع المربع";
			}
			
			$track_value = "";
			$track = $row['track'];
			if($track == "PUB"){
				$track_value = "العام";
			}else if($track == "INTERNATIONAL"){
				$track_value = "الدولى";
			}
			
			$is_contacted_value = "";
			$is_contacted = $row['is_contacted'];
			if($is_contacted == "Y"){
				$is_contacted_value = "نعم";
			}else if($is_contacted == "N"){
				$is_contacted_value = "لا";
			}
			
			$department_value = "";
			$department = $row['department'];
			if($department == "BOYS"){
				$department_value = "بنين";
			}else if($department == "GIRLS"){
				$department_value = "بنات";
			}
			
			$sheet->setCellValue('A' . $count, $row['reg_id']);
			$sheet->setCellValue('B' . $count, $row['responsible_name']);
			$sheet->setCellValue('C' . $count, $row['responsible_mobile']);
			$sheet->setCellValue('D' . $count, $row['responsible_email']);
			$sheet->setCellValue('E' . $count, $row['creation_date']);
			$sheet->setCellValue('F' . $count, $row['student_name']);
			$sheet->setCellValue('G' . $count, $school_value);
			$sheet->setCellValue('H' . $count, $is_contacted_value);
			$sheet->setCellValue('I' . $count, $row['state_name_ar']);
			$sheet->setCellValue('J' . $count, $row['comment']);
			$sheet->setCellValue('K' . $count, $row['knowing_way']);
			$sheet->setCellValue('L' . $count, $department_value);
			$sheet->setCellValue('M' . $count, $track_value);
			$sheet->setCellValue('N' . $count, $row['grade']);
			$sheet->setCellValue('O' . $count, $row['agent_name']);
			
			$count++;
		}
		
		$writer = new Xlsx($spreadsheet);
		
		$fileName = "طلبات التسجيل_". date("Y-m-d");
		if($not_interested != 0){
		    $fileName .= "_غير مهتم";
		}else{
		    $fileName .= "_الكل";
		}
		
		//header('Content-Type: application/vnd.ms-excel');
		//header('Content-Disposition: attachment;filename="'. $fileName.'.xlsx"');
		//header('Cache-Control: max-age=0');
		
		//$writer->save('php://output');
		$writer->save('/home/mhschoolscom/db_exports/'.$fileName.'.xlsx');
		
	}
}