<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

require APPPATH . '/libraries/REST_Controller.php';

class Cron_admin_notification extends REST_Controller {
    
	function send_admin_notification_get() {
	    date_default_timezone_set ( 'Asia/Riyadh' );
	    $current_date = date ( "Y-m-d H:i:s" );
	    $from15mins = date ( "Y-m-d H:i:s", strtotime ( "-15 minutes", strtotime ( $current_date) ) );
	    //log_message ("error", "current_date = ".$current_date."  from15mins= " .$from15mins);
	    $this->load->model ( "common" );
	    $where = " where admin_notified = 'N' and creation_date_time <= '".$from15mins."'";
	    $student_registrations = $this->common->getAllRow ( "student_registration", $where);
	    if ($student_registrations) {
	        //log_message ("error", "num of regs = ".count($student_registrations));
	        
	        foreach ( $student_registrations as $regRecord ) {
	            $reg_id = $regRecord ['reg_id'];
	            $responsible_name = $regRecord ['responsible_name'];
	            $responsible_mobile = $regRecord ['responsible_mobile'];
	            $responsible_email = $regRecord ['responsible_email'];
	            $student_name = $regRecord ['student_name'];
	            if($regRecord ['department']== 'BOYS'){
	                $department = "بنين";
	            }else{
	                $department = "بنات";
	            }
	            
	            if($regRecord ['track']== 'PUB'){
	            	$track = "العام";
	            }else{
	            	$track = "الدولى";
	            }
	            
	            $DOB = $regRecord ['DOB'];
	            $DOB_HIJRI = self::GregorianToHijri ($DOB);
	            $IBAN = $regRecord ['IBAN'];
	            $IBAN_source = $regRecord ['IBAN_source'];
	            $school = $regRecord ['school'];
	            $grade = $regRecord ['grade'];
	            $old_school = $regRecord ['old_school'];
	            $knowing_way = $regRecord ['knowing_way'];
	            $email_template = "http://mhsedusa.com/admin_registration_confirmation.html";
	            $parameters = array($responsible_name, $responsible_mobile, $responsible_email, $student_name,
	            		$department, $track, $DOB, $DOB_HIJRI, $IBAN, $IBAN_source, $school, $grade, $old_school, $knowing_way);
	            $mail_sent = false;
	            //====================== old way of sending email ====================
	            /*
	            $subject = 'استلام طلب مقدم لمدارس المنهل';
	            $from_mail = 'info@mhsedusa.com';
	            $to_mail = $from_mail; //"info@mhsedusa.edu";
	            $headers = "From: Manhal <$from_mail> \r\n";
	            $headers .= "MIME-Version: 1.0\r\n";
	            $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
	            
	            $body = self::get_web_page( $email_template);
	            $message = self::getFinalMessage ( $body, $parameters );
	            //log_message ("error", "message = ".$message);
	            $mailSent = mail($to_mail, $subject, $message, $headers);
	            */ 
	            // ==================================================================

	            $subject = 'استلام طلب مقدم لمدارس المنهل';
	            $from_mail = 'info@mhsedusa.com';
	            $to_mail = 'noura@almanhal.edu.sa';
	            //$to_mail = $from_mail;
	            $body = self::get_web_page( $email_template);
	            $message = self::getFinalMessage ( $body, $parameters );
	            $this->load->library('email');
                
 
                $this->email->from($from_mail, 'Mhsedusa');
                $this->email->to($to_mail);
                $this->email->subject($subject);
                $this->email->message($message);
                 
                $mailSent = $this->email->send();

	            if($mailSent){
	                //log_message ("error", "mail sent i will update the flag of = ".$reg_id);
	                $value['admin_notified']= 'Y';
	                $where2 = "reg_id ='".$reg_id."'";
	                $affected_rows = $this->common->updateRecord('student_registration',$value,$where2);
	            }
	        }
	    }
	}

	function get_web_page( $url ){
		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, $url);
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		$contents = curl_exec($ch);
		if (curl_errno($ch)) {
			echo curl_error($ch);
			echo "\n<br />";
			$contents = '';
		} else {
			curl_close($ch);
		}
		
		if (!is_string($contents) || !strlen($contents)) {
			echo "Failed to get contents.";
			$contents = '';
		}
	
		return $contents;
	}
    	
	function GregorianToHijri($gregorian_date) {
		// $gregorian_date = "2017-02-26";
		if ($gregorian_date == null || $gregorian_date == "" || $gregorian_date == " " || $gregorian_date == "0000-00-00") {
			return "";
		} else {
			$date_parts = explode("-", $gregorian_date);
			$y = ltrim($date_parts[0], '0');
			$m = ltrim($date_parts[1], '0');
			$d = ltrim($date_parts[2], '0');
			
			$jd = GregorianToJD($m, $d, $y);
			$jd = $jd - 1948440 + 10632;
			$n  = (int)(($jd - 1) / 10631);
			$jd = $jd - 10631 * $n + 354;
			$j  = ((int)((10985 - $jd) / 5316)) *
			((int)(50 * $jd / 17719)) +
			((int)($jd / 5670)) *
			((int)(43 * $jd / 15238));
			$jd = $jd - ((int)((30 - $j) / 15)) *
			((int)((17719 * $j) / 50)) -
			((int)($j / 16)) *
			((int)((15238 * $j) / 43)) + 29;
			$m  = (int)(24 * $jd / 709);
			$d  = $jd - (int)(709 * $m / 24);
			$y  = 30*$n + $j - 30;
			
			if (strlen($d) < 2) {
				$d = "0".$d;
			}
			if (strlen($m) < 2) {
				$m = "0".$m;
			}
			$hijri_date = $y."/".$m."/".$d;
			return $hijri_date;
		}
		
	}
	
	//Replace each '##' with real value
	function getFinalMessage($body, $parameters) {
		$msg = $body;
		if ($parameters != null) {
			foreach ($parameters as $param) {
				$pos = stripos($msg,"##");
				$msg = substr_replace($msg,$param,$pos,strlen("##"));
			}
		}
		return $msg;
	}
	
}

?>
