<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once '././vendor/autoload.php';
require_once '././src/Google/autoload.php';
require_once '././src/Google/Client.php';

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() {
        parent::__construct ();
        $this->load->helper ( array (
            'url'
        ) );
    }
    
    public function index()
	{
	    //$this->load->view('welcome_message');
	    
	    self::google_sheet();
	}
	
	function google_sheet(){
	    
	    // Get the API client and construct the service object.
	    $client = self::getClient();
	    $service = new Google_Service_Sheets($client);
	    
	    // Prints the names and majors of students in a sample spreadsheet:
	    // https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
	    $spreadsheetId = '1NvJ_m9J7mQOYyKrj1bUHIhDdJXdlgB1aaSMccTRx2iA';
	    $range = 'A1:B1';
	    $response = $service->spreadsheets_values->get($spreadsheetId, $range);
	    $values = $response->getValues();
	    log_message("error", print_r($values,true));
	    if (empty($values)) {
	        log_message("error", "No data found.\n");
	    } else {
	        //print "Name, Major:\n";
	        foreach ($values as $row) {
	            // Print columns A and E, which correspond to indices 0 and 4.
	            log_message("error", "row[0]= ".$row[0]);
	            log_message("error", "row[1]= ".$row[1]);
	        }
	    }
	    
	    $options = array('valueInputOption' => 'RAW');
	    $values = [
	        ["Name", "Roll No.", "Contact"],
	        ["Anis", "001", "+88017300112233"],
	        ["Ashik", "002", "+88017300445566"]
	    ];
	    $body   = new Google_Service_Sheets_ValueRange(['values' => $values]);
	    
	    $result = $service->spreadsheets_values->update($spreadsheetId, 'A1:C3', $body, $options);
	    log_message("error",($result->updatedRange. PHP_EOL));
	}
	
	/**
	 * Returns an authorized API client.
	 * @return Google_Client the authorized client object
	 */
	function getClient()
	{
	    $client = new Google_Client();
	    $client->setApplicationName('Google Sheets API PHP Quickstart');
	    $client->setScopes(Google_Service_Sheets::SPREADSHEETS);
	    $client->setAuthConfig('./cred/client_secret.json');
	    $redirect = site_url('/');
	    $client->setRedirectUri($redirect);
	    $client->setAccessType('offline');
	    $client->setApprovalPrompt('force');
	    
	    // Load previously authorized credentials from a file.
	   $credentialsPath = self::expandHomeDirectory('credentials.json');
	   if (file_exists($credentialsPath)) {
	        $accessToken = json_decode(file_get_contents($credentialsPath), true);
	    } else {
	        log_message("error", "hereeeeeeee: 1");
	        // Request authorization from the user.
	        $authUrl = $client->createAuthUrl();
	        log_message("error", "hereeeeeeee: 2 ");
	        
	        //print 'Enter verification code: ';
	        $authCode = "4/AAD0kkqYTUY4g2d-bjxvMruApgJ_gYvIaOCsJioSGCK17tSKGxT35JUuwD8TvBFbOQNQKKpafhtIou2lnn_sGis";
	        
	        // Exchange authorization code for an access token.
	        $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
	        log_message("error", "hereeeeeeee: 3");
	        log_message("error", "accessToken: ".print_r($accessToken,true));
	        
	        // Store the credentials to disk.
	        if (!file_exists(dirname($credentialsPath))) {
	            mkdir(dirname($credentialsPath), 0700, true);
	        }
	        file_put_contents($credentialsPath, json_encode($accessToken));
	        printf("Credentials saved to %s\n", $credentialsPath);
	    }
	    //$accessToken ="ya29.GluwBZWQbQ73mxB3uzQxQihp0boTeOLeXL3-UWxiPhkuBsj-V-aiR1swMpXSKneZo-H4rpfD-DcvpYQ6pm_cdDVzutd988Gjuu9aqiB7EENCORL06zxSn6qIsVMN";
	    //$refreshToken = "1/DwuI5070qjPwDhAY593NCo7k19HrVvcY2_MhSJXRQpY";
	    $client->setAccessToken($accessToken);
	    
	    //$refreshToken =  "1/Xb_Is8Zi5sGsOSWUBzJdJGVG0s6zQ9EX3AvOHyFZhgQ";
	    //$client->refreshToken($refreshToken);
	    
	    
	    return $client;
	}
	
	/**
	 * Expands the home directory alias '~' to the full path.
	 * @param string $path the path to expand.
	 * @return string the expanded path.
	 */
	function expandHomeDirectory($path)
	{
	    $homeDirectory = getenv('HOME');
	    if (empty($homeDirectory)) {
	        $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
	    }
	    return str_replace('~', realpath($homeDirectory), $path);
	}
	
}
