<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
        $this -> load -> model("common");
        $this->load->helper(array('url','Common_functions'));
    }
    
	function index()
	{
	   $data['msg']="";
	   if($this->session->userdata('adminid')=='') {		
	   	$data['user_name']='';
	   	$this->load->view('admin_login',$data);
	   } else{
	       $where = "where admin_id =".$this->session->userdata('adminid');
	       $row_admin=$this->common->getOneRow("admin",$where);
	       if($row_admin['type'] == 'ADMIN'){
	           redirect('admin/panel','refresh');
	       }else{
	           redirect('agent_accountant/panel','refresh');
	       }
	   }
	   
	}
	
	function login () {
		if(extract($_POST)) {
		
			$username=$this->common->mysql_safe_string($this->input->post('user_name'));
			
			$password=$this->common->mysql_safe_string($this->input->post('password'));
			
			if($username!="" && $password!="") {		
					$table="admin";
					$where="where user_name='".$username."' and password='".$password."'";
					$row=$this->common->numRow($table,$where);
					if($row == 1) {		
						$table="admin";
						$where="where user_name='".$username."' and password='".$password."'";	
						$row_admin=$this->common->getOneRow($table,$where);
						$userdata = array('adminid'=>$row_admin['admin_id'],'admin_name'=>$row_admin['user_name'],'admin_logged'=>TRUE);
						$this->session->set_userdata($userdata);
						if($row_admin['type'] != "ADMIN"){
							insert_activity($row_admin['admin_id'], 1);
						}
						$data['wrong']="";
						if($row_admin['type'] == 'ADMIN'){
						      redirect('admin/panel','refresh');
						}else{
						    redirect('agent_accountant/panel','refresh');
						}
					} else {	
						$data['wrong']="Username or password is  incorrect.";
						$this->load->view('admin_login',$data);		
					}
				
			} else {
				$data['msg']="Required fields cannot be left blank.";
				$this->load->view('admin_login',$data);//Executes if username is entered incorrect.	
			}
			
		} else {
			$data['msg']="Required fields cannot be left blank.";
			$this->load->view('admin_login',$data);
		}
	}
	
	function logoff()
	{
		$user_id = $this->session->userdata ( 'adminid' );
		$user_record = $this->common->getOneRow ( 'admin', "where admin_id = ".$user_id );
		if("ADMIN" != $user_record ['type']){  // store the activities of not admin user
			insert_activity($user_id, 2);
		}
		
	    $array_items = array('adminid' => '','admin_user' => '','admin_logged'=>'',/*'super_admin'=>''*/);
		$this->session->unset_userdata($array_items);
		$userdata = array('adminid'=>'','admin_name'=>'','admin_logged'=>FALSE);
		$this->session->set_userdata($userdata);
		redirect('admin','refresh');
	}
	
}

