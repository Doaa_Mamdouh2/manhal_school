<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

require APPPATH . '/libraries/REST_Controller.php';

class Cron_notifications extends REST_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model("common");
        $this->load->helper ( array (
            'url',
            'Common_functions'
        ) );
    }
    
    /**
     * the cases which we send admin notifications about are:
     * 1) requests already he made a reminder for the request in the same day
     * 2) بدء بإجراءات التسجيل (يتم تذكير الآدمن بعد 5 أيام عمل من هذه الحالة)
     */
	function send_admin_notifications_get() {
	    log_message("error", "start send_admin_notifications");
	    date_default_timezone_set ( 'Asia/Riyadh' );
	    $current_date = date ( "Y-m-d" );
	    $today = new DateTime ( $current_date );
	    
	    //send to admin -- loop on admins
	    $admins = $this->common->getAllRow ( 'admin', "where type = 'ADMIN'" );
	    for($j=0; $j< count($admins) ; $j++){
	        $admin_record = $admins[$j];
    	    $where = " where notification_time = '".$current_date."'";
    	    $student_registrations = $this->common->getAllRow ( "student_registration", $where);
    	    if ($student_registrations) {
    	        log_message ("error", "num of regs of today reminder of the admin= ".count($student_registrations));
    	        foreach ( $student_registrations as $regRecord ) {
    	            $notif_values = array ();
    	            $notif_values ['notification_type_code'] = 'M006';
    	            $notif_values ['to_user_id'] = $admin_record ['admin_id'];
    	            $notif_values ['reg_id'] = $regRecord['reg_id'];
    	            $notif_values ['waiting_days'] = '0';
    	            $sent_successfully = insert_delivered_notification($notif_values);
    	        }
    	    }
    	    $where = " where current_state_code = 'START_REGISTRATION_PROCESS' and state_changed_date IS NOT NULL";
    	    $student_registrations = $this->common->getAllRow ( "student_registration", $where);
    	    if ($student_registrations) {
    	        log_message ("error", "num of regs in START_REGISTRATION_PROCESS state= ".count($student_registrations));
    	        foreach ( $student_registrations as $regRecord ) {
    	            $change_date = new DateTime ( $regRecord ['state_changed_date'] );
    	            $number_working_days = number_of_working_days($change_date->format('Y-m-d'), $today->format('Y-m-d'), array());
    	            log_message ("error", "number_working_days = $number_working_days between ". $change_date->format('Y-m-d')."  and ".$today->format('Y-m-d') );
    	            if ($number_working_days == 5){
        	            $notif_values = array ();
        	            $notif_values ['notification_type_code'] = 'M004';
        	            $notif_values ['to_user_id'] = $admin_record ['admin_id'];
        	            $notif_values ['reg_id'] = $regRecord['reg_id'];
        	            $notif_values ['waiting_days'] = '0';
        	            $sent_successfully = insert_delivered_notification($notif_values);
    	            }
    	        }
    	    }
	    }
	    
	}
		
	/**
	 * 	يستمر النظام بتذكير ممثل الخدمة يوميا الساعة 10 صباحاً بكل الطلبات (الجديدة + المضاف لها تذكير + 6 أيام عمل بعد تحديد حالة (مهتم بالتسجيل) + في حال تم الاتصال ولم يتم الرد في خلال يوم   ). 
	 * the cases which we send agent notifications about are:
	 * 1) requests already he made a reminder for it in the same day
	 * 2) الجديدة 
	 * 3) 6 أيام عمل بعد تحديد حالة (مهتم بالتسجيل) 
	 * 4)  في حال تم الاتصال ولم يتم الرد في خلال يوم   
	 */
	function send_agent_notifications_get() {
	    log_message("error", "start send_agent_notifications");
	    date_default_timezone_set ( 'Asia/Riyadh' );
	    $current_date = date ( "Y-m-d" );
	    $today = new DateTime ( $current_date );
	    $agents = $this->common->getAllRow("admin", "where type = 'AGENT' and active = 1 ");
	    
	    for($i = 0; $i < count ( $agents ); $i ++) {
	        $agent_record = $agents[$i];
	        
	        //case 1
	        $where = " where agent_id = '".$agent_record ['admin_id']."' and notification_time = '".$current_date."'";
	        $student_registrations = $this->common->getAllRow ( "student_registration", $where);
	        if ($student_registrations) {
	            log_message ("error", "num of regs of today reminder of agent = ".$agent_record ['admin_id'] ."is ".count($student_registrations));
	            foreach ( $student_registrations as $regRecord ) {
	                $notif_values = array ();
	                $notif_values ['notification_type_code'] = 'M009';
	                $notif_values ['to_user_id'] = $agent_record ['admin_id'];
	                $notif_values ['reg_id'] = $regRecord['reg_id'];
	                $notif_values ['waiting_days'] = '0';
	                $sent_successfully = insert_delivered_notification($notif_values);
	            }
	        }
	        
	        //case 2
	        $where = " where current_state_code = 'NEW' and agent_id ='".$agent_record ['admin_id']."'";
	        $student_registrations = $this->common->getAllRow ( "student_registration", $where);
	        if ($student_registrations) {
	            log_message ("error", "num of regs in NEW state= ".count($student_registrations));
	            foreach ( $student_registrations as $regRecord ) {
                    $notif_values = array ();
                    $notif_values ['notification_type_code'] = 'M008';
                    $notif_values ['to_user_id'] = $agent_record ['admin_id'];
                    $notif_values ['reg_id'] = $regRecord['reg_id'];
                    $notif_values ['waiting_days'] = '0';
                    $sent_successfully = insert_delivered_notification($notif_values);
	            }
	        }
	        
	        //case 3
	        $where = " where current_state_code = 'INTERESTED_IN_REGISTRATION' and state_changed_date IS NOT NULL and agent_id ='".$agent_record ['admin_id']."'";
	        $student_registrations = $this->common->getAllRow ( "student_registration", $where);
	        if ($student_registrations) {
	            log_message ("error", "num of regs in INTERESTED_IN_REGISTRATION state= ".count($student_registrations));
	            foreach ( $student_registrations as $regRecord ) {
	                $change_date = new DateTime ( $regRecord ['state_changed_date'] );
	                $number_working_days = number_of_working_days($change_date->format('Y-m-d'), $today->format('Y-m-d'), array());
	                log_message ("error", "number_working_days = $number_working_days between ". $change_date->format('Y-m-d')."  and ".$today->format('Y-m-d') );
	                if ($number_working_days == 6){
	                    $notif_values = array ();
	                    $notif_values ['notification_type_code'] = 'M010';
	                    $notif_values ['to_user_id'] = $agent_record ['admin_id'];
	                    $notif_values ['reg_id'] = $regRecord['reg_id'];
	                    $notif_values ['waiting_days'] = '0';
	                    $sent_successfully = insert_delivered_notification($notif_values);
	                }
	            }
	        }
	        
	        //case 4
	        $where = " where current_state_code = 'NOT_ANSWERED' and state_changed_date IS NOT NULL and agent_id ='".$agent_record ['admin_id']."'";
	        $student_registrations = $this->common->getAllRow ( "student_registration", $where);
	        if ($student_registrations) {
	            log_message ("error", "num of regs in NOT_ANSWERED state= ".count($student_registrations));
	            foreach ( $student_registrations as $regRecord ) {
	                $change_date = new DateTime ( $regRecord ['state_changed_date'] );
	                $number_working_days = number_of_working_days($change_date->format('Y-m-d'), $today->format('Y-m-d'), array());
	                log_message ("error", "number_working_days = $number_working_days between ". $change_date->format('Y-m-d')."  and ".$today->format('Y-m-d') );
	                if ($number_working_days == 1){
	                    $notif_values = array ();
	                    $notif_values ['notification_type_code'] = 'M011';
	                    $notif_values ['to_user_id'] = $agent_record ['admin_id'];
	                    $notif_values ['reg_id'] = $regRecord['reg_id'];
	                    $notif_values ['waiting_days'] = '0';
	                    $sent_successfully = insert_delivered_notification($notif_values);
	                }
	            }
	        }
	    }//agents loop
	}
	
	
	
}

?>
