<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Seen_notifications extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
        $this -> load -> model("common");
		if($this->session->userdata('adminid')=='') {
		    redirect('admin', 'refresh');
		}
    }
    
	function index() {
	   self::update_seen_notifications();
	}
	
	
	
	
	function update_seen_notifications() {
		//echo "here1";
		if(extract($_POST)) {	
			$last_id= $_POST['last_id'];
			//echo "here=".$last_id;
			$user_id = $this->session->userdata('adminid');
			$value['seen'] = "Y";
			$where = "to_user_id = '".$user_id."' AND ( email_id < '".$last_id."' OR email_id = '".$last_id."' )";
			$this->common->updateRecord('delivered_notifications',$value,$where);
			//redirect($_POST['page']);
		}	
	}
	
	function update_user_token() {
		if(extract($_POST)) {			
			$user_id = $this->session->userdata('adminid');
			$update['gcm_id'] = $_POST['token'];
			$this -> common -> updateRecord("user",$update,"user_id ='$user_id'");
			//redirect($_POST['page']);
		}
	}
	
	function get_unseen_notifications() {
		$user_id = $this->session->userdata('adminid');
		$this -> load -> model("notification_model");
   		$notifications_data = $this -> notification_model -> getUnseenNotificationsForUser($user_id);
		echo json_encode($notifications_data);
	}
	
	
}
