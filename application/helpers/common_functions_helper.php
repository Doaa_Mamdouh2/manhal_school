<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('insert_delivered_notification'))
{
	
    /**
     * insert delivered notification after getting its message from the notifications table
     * @param unknown $values
     * @param unknown $email_active
     * @param unknown $sms_active
     * @return unknown
     */
    function insert_delivered_notification($values) {
        $CI = get_instance();
        $CI->load->model ( "notification_model" );
        $user_record = $CI->common->getOneRow ( 'admin', "where admin_id ='" . $values ['to_user_id']."'" );
        $notif_type = $CI->common->getOneRow ( 'notifications', "where notification_code ='" . $values ['notification_type_code']."'" );
        $values ['message'] = $notif_type['notification_name']. " (".get_formatted_reg_id($values ['reg_id']).")";
        $values ['email_id'] = $CI->notification_model->getDeliveredNotificationId ();
        
        date_default_timezone_set('Asia/Riyadh');
        $current_date = date ( "Y-m-d H:i:s" );
        $values ['sent_date_time'] = $current_date;
        $num = $CI->common->insertRecord ( 'delivered_notifications', $values );
        $sent_successfully = true;
        
        if($notif_type['email_active'] == 'Y'){
            if(isset($user_record['email']) && $user_record['email'] != ""){
                $user_message = $values ['message'];
                $subject =  $values ['message'];
                $to = $user_record['email'];
                $cc = null;
                $bcc = null;
                if($notif_type['template_name'] != null && $notif_type['template_name'] != ""){
                    $user_message = get_mail_message($notif_type['template_name'], $values ['message'], $values ['reg_id']);
                }
                $mail_sent = send_email ($user_message, $subject, $to, $from, $cc, $bcc);
                if($mail_sent != 1){
                    $sent_successfully = false;
                }
            }
        }
        
        if($notif_type['sms_active'] == 'Y'){
            if(isset($user_record['mobile_number']) && $user_record['mobile_number'] != ""){
                //$sms_sent = sendSMS ($notif_type['sms_active'], $user_record['mobile_number'], $values ['message']);
                //TODO check sms sent successfully
            }
        }
        
        return $sent_successfully;
    }
      
}


if ( ! function_exists('get_mail_message'))
{
    function get_mail_message($template_name, $subject, $reg_id, $lang = 'ar'){
        $CI = get_instance();
        $where = "where reg_id =".$reg_id;
        $regRecord = $CI->common->getOneRow('student_registration',$where);
        $responsible_name = $regRecord ['responsible_name'];
        $responsible_mobile = $regRecord ['responsible_mobile'];
        $responsible_email = $regRecord ['responsible_email'];
        $student_name = $regRecord ['student_name'];
        if($regRecord ['department']== 'BOYS'){
            $department = "بنين";
        }else{
            $department = "بنات";
        }
        if($regRecord ['track'] == 'PUB'){
        	$track = "العام";
        }else{
        	$track = "الدولى";
        }
        /* $DOB = $regRecord ['DOB'];
        $DOB_HIJRI = GregorianToHijri ($DOB);
        $IBAN = $regRecord ['IBAN'];
        $IBAN_source = $regRecord ['IBAN_source']; */
        $knowing_way = $regRecord ['knowing_way'];
        $school = $regRecord ['school'];
        $school_name = "";
        if($school != NUll){
        	$schoolRecord = $CI->common->getOneRow('school',"where id ='".$school."'");
        	$school_name = $schoolRecord['name'];
        }
        $grade = $regRecord ['grade'];
        $grade_name ="";
        if($grade != NUll){
            $gradeRecord = $CI->common->getOneRow('educational_grade',"where id ='".$grade."'");
            $grade_name = $gradeRecord['desc_ar'];
        }
        $old_school = $regRecord ['old_school'];
        $email_template =  base_url () .$template_name;
        $url = site_url('Admin_panel/view_student_registration/' . $reg_id);
        if($template_name == "register_new_template.html"){
            $parameters = array($responsible_name, $responsible_email, $student_name,
            	$department, $track, $school_name, $grade_name, $old_school, $knowing_way, $url);
        }  else if($template_name == "review_template.html"){
            $parameters = array($url, $subject);
        }  else if($template_name == "thanks_template.html"){
            $responsible_name = $regRecord ['responsible_name'];
            if($regRecord ['department']== 'BOYS'){
                $param2 = "ابنكم";
            }else{
                $param2 = "ابنتكم";
            }
            $parameters = array($responsible_name , $param2);
        }
        $body = get_web_page( $email_template);
        $message = getFinalMessage ( $body, $parameters );
        
        //log_message('error', '$message: '.$message);
        return $message;
    }
}


if ( ! function_exists('send_email'))
{
	function send_email($user_message, $subject, $to, $from, $cc, $bcc) {
        $CI = get_instance();
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'in-v3.mailjet.com';
        $config['smtp_port'] = '80';
        $config['smtp_user'] = 'b4d7966e315f3dcd045aeeec216f98c2';
        $config['smtp_pass'] = 'f13ed25e14a125899c11237061325623';
        
//         $config['protocol'] = 'ssmtp';
//         $config['smtp_host'] = 'ssl://ssmtp.gmail.com';
//         $config['smtp_port'] = '465';
//         $config['smtp_user'] = 'aurora.sdp2019@gmail.com';
//         $config['smtp_pass'] = 'aurora.sdp2019_company';
        
        $config['charset'] = 'utf-8';
        $config['mailtype'] = 'html';
        $config['newline'] = "\r\n";
        
        $CI->load->library('email');
        
        $CI->email->initialize ( $config );
        $CI->email->from('pr@mhschools.com.sa', 'Al Manhal Schools');
        $CI->email->cc ( $cc );
        $CI->email->bcc ( $bcc );
        $CI->email->to($to);
        $CI->email->subject($subject);
        $CI->email->message($user_message);
        log_message("error", "start sending mail");
        $output = $CI->email->send();
        log_message("error", "sending mail output = ".$output);
        return json_encode($output);
    } 
}

/* if ( ! function_exists('sendEmail'))
{
	function sendEmail() {
		// Create a new Object
		$apiKey = 'b4d7966e315f3dcd045aeeec216f98c2';
		$secretKey = 'f13ed25e14a125899c11237061325623';
		
		$mj = new Mailjet($apiKey, $secretKey);
		$params = array(
				"method" => "POST",
				"from" => "pr@mhschools.com.sa",
				"to" => "eng.doaa2011@gmail.com",
				"subject" => "yaraaab",
				"text" => "testttttt",
				"html" => "html msg"
		);
		$result = $mj->sendEmail($params);
		
		if ($mj->_response_code == 200) {
			//echo "success - email sent";
			print '<script type="text/javascript">';
			print 'alert("email successfully sent!")';
			print '</script>';
		} elseif ($mj->_response_code == 400) {
			//echo "error - " . $mj->_response_code;
			print '<script type="text/javascript">';
			print 'alert("Email Successfully Sent..!")';
			print '</script>';
		} elseif ($mj->_response_code == 401) {
			//echo "error - " . $mj->_response_code;
			print '<script type="text/javascript">';
			print 'alert("Unauthorized! You have specified an incorrect ApiKey or username/password couple.")';
			print '</script>';
		} elseif ($mj->_response_code == 404) {
			//echo "error - " . $mj->_response_code;
			print '<script type="text/javascript">';
			print 'alert("Not Found! The method your are trying to reach don\'t exists.")';
			print '</script>';
		} elseif ($mj->_response_code == 405) {
			//echo "error - " . $mj->_response_code;
			print '<script type="text/javascript">';
			print 'alert("Method Not Allowed! You made a POST request instead of GET, or the reverse.")';
			print '</script>';
		} else {
			print '<script type="text/javascript">';
			print 'alert(" Internal Server Error! Status returned when an unknow error occurs")';
			print '</script>';
		}
		
		return $result;
	}
} */

if ( ! function_exists('get_web_page'))
{
    /* function get_web_page($url){
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        $contents = curl_exec($ch);
        if (curl_errno($ch)) {
            //echo curl_error($ch);
            //echo "\n<br />";
            $contents = '';
        } else {
            curl_close($ch);
        }
        
        if (!is_string($contents) || !strlen($contents)) {
           // echo "Failed to get contents.";
            $contents = '';
        }
        
        return $contents;
    } */
    
    function get_web_page ($url) {
    	$ch = curl_init();
    	
    	curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    	curl_setopt($ch, CURLOPT_HEADER, 0);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    	curl_setopt($ch, CURLOPT_URL, $url);
    	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    	
    	$data = curl_exec($ch);
    	curl_close($ch);
    	
    	return $data;
    }
}

if ( ! function_exists('getFinalMessage'))
{
    //Replace each '##' with real value
    function getFinalMessage($body, $parameters) {
        $msg = $body;
        if ($parameters != null) {
            foreach ($parameters as $param) {
                $pos = stripos($msg,"##");
                $msg = substr_replace($msg,$param,$pos,strlen("##"));
            }
        }
        return $msg;
    }
}

if ( ! function_exists('sendSMS'))
{
    // Send SMS API using CURL method
	function sendSMS($sms_active, $mobile, $msg) {
		if ($sms_active == "Y") {
		    //$mobile = "966568272642"; for test
			global $arraySendMsg;
			$url = "www.mobily.ws/api/msgSend.php";
			$numbers = substr_replace ( $mobile, "966", 0, 1 );
			$userAccount = "966504211994";
			$passAccount = "live123";
			$applicationType = "68";
			$sender = "IbnAlshaikh";
			$MsgID = "15176";
			$timeSend = 0;
			$dateSend = 0;
			$deleteKey = 0;
			$viewResult = 1;
			$msg = Unicode_decode ($msg);
			
			$sender = urlencode ( $sender );
			
			$domainName = "";
			$stringToPost = "mobile=" . $userAccount . "&password=" . $passAccount . "&numbers=" . $mobile . "&sender=" . $sender . "&msg=" . $msg . "&timeSend=" . $timeSend . "&dateSend=" . $dateSend . "&applicationType=" . $applicationType . "&domainName=" . $domainName ;
			//$stringToPost = $stringToPost  . "&msgId=" . $MsgID. "&deleteKey=" . $deleteKey. "&lang=3";
			log_message("error", " stringToPost: " . $stringToPost);
			
			$ch = curl_init ();
			curl_setopt ( $ch, CURLOPT_URL, $url );
			curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
			curl_setopt ( $ch, CURLOPT_HEADER, 0 );
			curl_setopt ( $ch, CURLOPT_TIMEOUT, 5 );
			curl_setopt ( $ch, CURLOPT_POST, 1 );
			curl_setopt ( $ch, CURLOPT_POSTFIELDS, $stringToPost );
			$result = curl_exec ( $ch );
			
			log_message("error", " result: " . $result);

			// if($viewResult)
			// $result = printStringResult(trim($result) , $arraySendMsg);
            log_message("error", "SMS SENT OUT = ".trim($result));
			return $result;
		}
	}
}
if ( ! function_exists('Unicode_decode'))
{
function Unicode_decode($text) {
     return implode(unpack('H*', iconv("UTF-8", "UCS-4BE", $text)));
 }
}

if ( ! function_exists('GregorianToHijri'))
{
    function GregorianToHijri($gregorian_date) {
        // $gregorian_date = "2017-02-26";
        if ($gregorian_date == null || $gregorian_date == "" || $gregorian_date == " " || $gregorian_date == "0000-00-00") {
            return "";
        } else {
            $date_parts = explode("-", $gregorian_date);
            $y = ltrim($date_parts[0], '0');
            $m = ltrim($date_parts[1], '0');
            $d = ltrim($date_parts[2], '0');
            
            $jd = GregorianToJD($m, $d, $y);
            $jd = $jd - 1948440 + 10632;
            $n = (int) (($jd - 1) / 10631);
            $jd = $jd - 10631 * $n + 354;
            $j = ((int) ((10985 - $jd) / 5316)) *
            ((int) (50 * $jd / 17719)) +
            ((int) ($jd / 5670)) *
            ((int) (43 * $jd / 15238));
            $jd = $jd - ((int) ((30 - $j) / 15)) *
            ((int) ((17719 * $j) / 50)) -
            ((int) ($j / 16)) *
            ((int) ((15238 * $j) / 43)) + 29;
            $m = (int) (24 * $jd / 709);
            $d = $jd - (int) (709 * $m / 24);
            $y = 30 * $n + $j - 30;
            
            if (strlen($d) < 2) {
                $d = "0" . $d;
            }
            if (strlen($m) < 2) {
                $m = "0" . $m;
            }
            $hijri_date = $y . "/" . $m . "/" . $d;
            return $hijri_date;
        }
    }
}

if ( ! function_exists('HijriToGregorian'))
{
    function HijriToGregorian($hijri_date) {
        //$hijri_date = "1438/05/29";
        if ($hijri_date == null || $hijri_date == "" || $hijri_date == " " || $hijri_date == "0000-00-00") {
            return "";
        } else {
            $date_parts = explode("/", $hijri_date);
            $y = ltrim($date_parts[0], '0');
            $m = ltrim($date_parts[1], '0');
            $d = ltrim($date_parts[2], '0');
            
            $date = (int)((11 * $y + 3) / 30) + 354 * $y + 30 * $m - (int)(($m - 1) / 2) + $d + 1948440 - 385;
            
            $gregorian_date = jdtogregorian($date);
            $date_parts = explode("/", $gregorian_date);
            $d = $date_parts[1];
            if (strlen($d) < 2) {
                $d = "0".$d;
            }
            $m = $date_parts[0];
            if (strlen($m) < 2) {
                $m = "0".$m;
            }
            $y = $date_parts[2];
            
            $gregorian_date = $y."-".$m."-".$d;
            return $gregorian_date;
        }
        
    }
}


if ( ! function_exists('number_of_working_days'))
{
    //The function returns the no. of business days between two dates and it skips the holidays
    function number_of_working_days($startDate,$endDate,$holidays){
        date_default_timezone_set ( 'Asia/Riyadh' );
        $begin = strtotime($startDate);
        $end   = strtotime($endDate);
        if ($begin > $end) {
            return 0;
        } else {
            $no_days  = 0;
            while ($begin <= $end) {
                $what_day = date("N", $begin);
                if (!in_array($what_day, [5,6]) ) // 5 and 6 are weekend
                    $no_days++;
                    $begin += 86400; // +1 day
            };
            
            return $no_days;
        }
    }
}

if ( ! function_exists('insert_activity'))
{
	function insert_activity($user_id, $activity_type_id, $reg_id = NULL) {
		$CI = get_instance();
		$values = array ();
		$values ['user_id'] = $user_id;
		$values ['activity_type_id'] = $activity_type_id;
		$values ['user_IP'] = get_client_ip ();
		date_default_timezone_set ( 'Asia/Riyadh' );
		$current_date_time = date ( "Y-m-d H:i:s" );
		$values ['date_time'] = $current_date_time;
		$values ['reg_id'] = $reg_id;
		$CI->common->insertRecord ( 'activities_log', $values );
	}
}

if ( ! function_exists('get_client_ip'))
{
	function get_client_ip() {
		$ipaddress = '';
		if (isset($_SERVER['HTTP_CLIENT_IP']))
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
			else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
				$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
				else if(isset($_SERVER['HTTP_X_FORWARDED']))
					$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
					else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
						$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
						else if(isset($_SERVER['HTTP_FORWARDED']))
							$ipaddress = $_SERVER['HTTP_FORWARDED'];
							else if(isset($_SERVER['REMOTE_ADDR']))
								$ipaddress = $_SERVER['REMOTE_ADDR'];
								else
									$ipaddress = 'UNKNOWN';
									return $ipaddress;
	}
}

if ( ! function_exists('get_formatted_reg_id'))
{
    function get_formatted_reg_id ($reg_id){
        $formatted_reg_id = sprintf ( "%'.05d", $reg_id );
        return "R" . $formatted_reg_id;
    }
    
}


