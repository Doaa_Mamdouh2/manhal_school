
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<?php
// session_start ();
$this->load->view('localization/lang');
$this->load->view('localization/txt');
defineLocale();
defineStrings();
?>
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
function checkSubmit(e) {
	   if(e && e.keyCode == 13) {
	      document.forms[0].submit();
	   }
	}
</script>
<meta charset="utf-8" />
<title><?php echo SCHOOL_SITE_TITLE_NEW; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link
	href="<?=base_url()?>assets/global/plugins/select2/css/select2.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/select2/css/select2-bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?=base_url()?>assets/global/css/components-rtl.min.css"
	rel="stylesheet" id="style_components" type="text/css" />
<link href="<?=base_url()?>assets/global/css/plugins-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?=base_url()?>assets/pages/css/login-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<!-- END THEME LAYOUT STYLES -->
<link rel="icon" href="<?php echo base_url()?>images/header_logo.png">


<style type="text/css">

.login {
    background-color: #282b3a !important;
}

.btn.blue:not(.btn-outline) {
    color: #FFF;
    background-color: #282b3a;
    border-color: #282b3a;
}
.font-blue {
    color: #282b3a !important;
}
</style>

</head>
<!-- END HEAD -->

<body class=" login">
	<div class="logo"></div>
	<!-- BEGIN LOGIN -->
	<div class="content" style="text-align: center">
		<!-- BEGIN LOGIN FORM -->
		<div style="text-align: center">
			<img src="<?=base_url()?>images/header_logo.png">
		</div>


		<form role="form" id="myForm" method="post"
			action="<?=site_url('admin/login')?>"
			onKeyPress="return checkSubmit(event)">
			<h3 class="form-title font-blue"><?php echo SIGN_IN; ?></h3>
			<div class="alert alert-danger display-hide">
				<button class="close" data-close="alert"></button>
				<span> <?php echo INSERT_USERNAME_PASSWORD; ?>  </span>
			</div>
			<div class="form-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<label class="control-label visible-ie8 visible-ie9"><?php echo USERNAME; ?></label>
				<input class="form-control form-control-solid placeholder-no-fix"
					type="text" autocomplete="off"
					placeholder="<?php echo USERNAME; ?>" name="user_name"
					id="user_name" />
			</div>
			<div class="form-group">
				<label class="control-label visible-ie8 visible-ie9"><?php echo PASSWORD; ?></label>
				<input class="form-control form-control-solid placeholder-no-fix"
					type="password" autocomplete="off"
					placeholder="<?php echo PASSWORD; ?>" name="password" />
			</div>
			<div class="form-group" <?php if (isset($wrong) && $wrong!= ""){?>
				style="display: block; color: red;" <?php }else {?>
				style="display: none; color:red;" <?php }?>>
				<?php echo INVALID_USERNAME_PASSWORD;?>
			</div>

			<div class="form-group" <?php if (isset($msg) && $msg!= ""){?>
				style="display: block; color: red;" <?php }else {?>
				style="display: none; color:red;" <?php }?>>
				<?php echo INSERT_USERNAME_PASSWORD;?>
			</div>

			<div class="form-actions" style="text-align: center">
				<button type="submit" class="btn blue uppercase"><?php echo LOGIN; ?></button>
			</div>
		</form>
		<!-- END LOGIN FORM -->
	</div>

	<!--[if lt IE 9]>
<script src="<?=base_url()?>assets/global/plugins/respond.min.js"></script>
<script src="<?=base_url()?>assets/global/plugins/excanvas.min.js"></script> 
<script src="<?=base_url()?>assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
	<!-- BEGIN CORE PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-validation/js/additional-methods.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/select2/js/select2.full.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/login.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<!-- END THEME LAYOUT SCRIPTS -->

<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135637603-1"></script>

<script>

  window.dataLayer = window.dataLayer || [];

  function gtag(){dataLayer.push(arguments);}

  gtag('js', new Date());

 

  gtag('config', 'UA-135637603-1');

</script>

</body>

</html>