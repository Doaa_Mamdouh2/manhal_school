<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<?php
// session_start ();
$this->load->view('localization/lang');
$this->load->view('localization/txt');
defineLocale();
defineStrings();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
	
        <meta charset="utf-8" />
        <title><?php echo SCHOOL_SITE_TITLE_NEW. ' | '. DASHBOARD_REPORTS; ?></title>
        <link rel="icon" href="<?php echo base_url()?>images/header_logo.png">
        <link rel="shortcut icon" href="<?=base_url()?>images/header_logo.png" />
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="author" />
        <link rel="stylesheet"
        	href="<?=base_url()?>assets/css/bootstrap.min.css">
        
        <link
        	href="<?=base_url()?>assets/global/plugins/bootstrap-table/bootstrap-table.min.css"
        rel="stylesheet" type="text/css" />
        
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link
            href="<?= base_url() ?>assets/css/student_registrations.css"
                rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
        
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?=base_url()?>assets/global/css/components-rtl.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?=base_url()?>assets/global/css/plugins-rtl.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?=base_url()?>assets/layouts/layout3/css/layout-rtl.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/layouts/layout3/css/themes/default-rtl.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?=base_url()?>assets/layouts/layout3/css/custom-rtl.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        
        
        <script
        src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<script type="text/javascript">

    window.onload = function() {
        //var obj = jQuery.parseJSON(json);
    	initAmChart(<?php echo json_encode($registrationsCountInLast60Days) ?>);
    };


 function initAmChart(chartData) {
    if (typeof(AmCharts) === 'undefined' || $('#dashboard_amchart_report').size() === 0) {
        return;
    }
    
    var chart = AmCharts.makeChart("dashboard_amchart_report", {
        type: "serial",
        fontSize: 12,
        dataDateFormat: "YYYY-MM-DD",
        dataProvider: chartData,
        theme: "light",
        addClassNames: true,
        startDuration: 1,
        gridAboveGraphs: true,
        color: "#6c7b88",
        marginRight: 50,
        marginLeft:50,

        categoryField: "date",
        categoryAxis: {
            autoGridCount: false,
            gridCount: 50,
            gridAlpha: 0.1,
            gridColor: "#FFFFFF",
            axisColor: "#555555",
            labelRotation: 45
        },

        valueAxes: [{
            id: "a1",
            title: "<?php echo REPORT2_TITLE; ?>",
            gridAlpha: 0.1,
            axisAlpha: 0.1,
            position : "right"
        }],
        graphs: [{
            id: "g1",
            valueField: "created",
            title: "<?php echo CREATED_REGISTRATIONS_COUNT; ?>",
            type: "column",
            fillAlphas: 0.7,
            valueAxis: "a1",
            lineColor: "#08a3cc",
            balloonText: "<span style='font-size:12px;'>[[title]] <?php echo IN; ?> [[category]]:<br><span style='font-size:14px;'>[[value]]</span> <?php echo REGISTRATION; ?></span>",
            legendValueText: "[[title]] <?php echo IN; ?> [[category]] : [[value]] <?php echo REGISTRATION; ?>"
        }],

        chartCursor: {
            zoomable: false,
            categoryBalloonDateFormat: "DD",
            cursorAlpha: 0,
            categoryBalloonColor: "#e26a6a",
            categoryBalloonAlpha: 0.8,
            valueBalloonsEnabled: false
        }
       
    });

  
}


   function ajaxUpdateActivities(newpage){
		$('#loading').removeClass('hidden');
		var current_page = $("#calculated_current_page").val();
		var postdata = {'current_page' : current_page};
		var url = "<?php echo site_url('admin_panel/view_usage_report')?>";
		$.post(url, postdata, function(data) {
	            var results = JSON.parse(data);
	            var newSearchArea = $(results).find("#activities_row");
	            $("#activities").html(newSearchArea);	
	            $('#loading').addClass('hidden');
	        });
    }
    
    function next_page(){
    	var current_page = parseInt($("#calculated_current_page").val()) ;
    	var newpage = (current_page+1);
    	$("#calculated_current_page").val(newpage);
    	ajaxUpdateActivities(newpage);
    }
    
    function prev_page(){
    	var current_page = parseInt($("#calculated_current_page").val()) ;
    	var newpage = (current_page-1);
    	$("#calculated_current_page").val(newpage);
    	ajaxUpdateActivities(newpage);
    }
 

            
</script>

<style>
.amcharts-chart-div a {
	display: none !important;
}


#dashboard_amchart_report {
	width: 100%;
	height: 500px;
}

.table-custom th, .table-custom td { min-width: 60px; max-width: 90px; word-wrap:break-word;}


.table-custom{
     max-width:none !important;
}
	
.rep2-custom-class{
	max-width:90% !important;
	margin:auto;
	text-align:center;
	 -webkit-overflow-scrolling: touch;
}

.custom-table-header{
	padding-top:8px !important;
	padding-bottom:8px !important;
	padding-left:4px !important;
	padding-right:4px !important;
}

.custom-portlet{
	margin-left: 0px !important;
 	margin-right: 0px !important;
 	padding-left: 0px !important;
 	padding-right: 0px !important;
}

.caption-custom{
	width:75% !important;
}

.container-outer {
  display: flex;
  overflow-x: scroll;
}

.container-inner {
  min-width:800px;
  text-align: center;
  margin: auto;
}


@media only screen and (max-width: 500px) {
	.custom-table-cell{
		max-width:70px;
	}
}

@media only screen and (max-width: 450px) {
	.custom-table-cell{
		max-width:70px;
	}
}

@media only screen and (max-width: 400px) {
	.custom-table-cell{
		max-width:60px;
	}
}

@media only screen and (max-width: 330px) {
	.custom-table-cell{
		max-width:60px;
	}
}


@media only screen and (max-width: 300px) {
	.custom-table-cell{
		max-width:50px;
	}
}

	#loading_reports {
	   width: 100%;
	   height: 100%;
	   top: 0;
	   left: 0;
	   position: fixed;
	   display: block;
	   opacity: 0.7;
	   background-color: #fff;
	   z-index: 99;
	   text-align: center;
	}

	#loading-image {
	  position: absolute;
	  top: 35%;
	  left: 40%;
	  z-index: 100;
	}
	
	
	#loading-text {
        position: absolute;
        top: 65%;
        left: 46%;
        margin-top: -25px;
        margin-left: -50px;
        font-size: 16px;
        font-weight: bold;
        color: #054A35;
	    z-index: 200;
	}
	
    
    #all-reports {
        padding: 10px;
        background-color: #9fcad6;
    }
    
    .count-title{
        text-align: center;
        margin: auto;
        word-wrap: break-word;
        overflow-wrap: break-word;
    }

    .label-success{
        background-color: #3B467E !important
    }
    
</style>



    </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid">
	
	<div id="loading_reports" class="hidden">
	  <img id="loading-image" src="<?php echo base_url(); ?>images/Ajax-loader2.gif" alt="Loading..." />
	  <div id="loading-text"><?php echo DASHBOARD_REPORTS_LOADING;?></div>
	</div>
	
        <div id="all-page" class="container shadow">
            <div class="row">
                <div class="col-md-13">
                    <?php
                    $this->load->view('utils/schoolHeaderAdmin');
                    ?>

                    <div class="page-wrapper-row full-height">
                        <div class="page-wrapper-middle">
                            <!-- BEGIN CONTAINER -->
                            <div class="page-container">
                                <!-- BEGIN CONTENT -->
                                <div class="page-content-wrapper">
                                    <!-- BEGIN CONTENT BODY -->
                                    <div id="loading" class="hidden">
                                        <img id="loading-image" src="<?= base_url() ?>images/Ajax-loader2.gif" alt="Loading...">
                                    </div>
                                    <!-- BEGIN PAGE HEAD-->
                                    <div class="container">
                                        <div class="page-content-inner">

                                            <!-- BEGIN PAGE TITLE -->
                                            <div class="portlet light portlet-fit ">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <div class="page-title" style="padding-top: 0px !important;">
                                                            <h2>
                                                                <span style="color: #0E4278 !important;"><?php echo DASHBOARD_REPORTS; ?>
																
                                                                  </span>
                                                            </h2>

                                                        </div>
                                                    </div>
                                                   
                                                </div>
                                            </div>


                                            <!-- BEGIN PAGE CONTENT BODY -->
                                            <div class="page-content">
                                                <div class="portlet-body">
                                                    
										<div id="all-reports" class="col-md-12">

								<div id="report1">
									<div id="dash_1" class="row">
										<?php if ($ordered_states) { 
										    for($i = 0; $i < count ( $ordered_states ); $i ++) { 
										        $font_class = "";
										        $progress_bar_color_class = "";
										        if ($i == 0) {
										            $font_class = "font-green-dark";
										            $progress_bar_color_class = "green-dark";
										        } elseif ($i == 1) {
										            $font_class = "font-yellow-lemon";
										            $progress_bar_color_class = "yellow-lemon";
										        } elseif ($i == 2) {
										            $font_class = "font-green-sharp";
										            $progress_bar_color_class = "green-sharp";
    										    } elseif ($i == 3) {
    										        $font_class = "font-blue-sharp";
    										        $progress_bar_color_class = "blue-sharp";
    										    } elseif ($i == 4) {
    										        $font_class = "font-purple-soft";
    										        $progress_bar_color_class = "purple-soft";
        										} elseif ($i == 5) {
        										    $font_class = "font-red-haze";
        										    $progress_bar_color_class = "red-haze";
        										} elseif ($i == 6) {
        										    $font_class = "font-grey-cascade";
        										    $progress_bar_color_class = "grey-cascade";
                                                } elseif ($i == 7) {
                                                    $font_class = "font-red";
                                                    $progress_bar_color_class = "red";
                                                } elseif ($i == 8) {
                                                    $font_class = "font-blue-madison";
                                                    $progress_bar_color_class = "blue-madison";
                                                }
                                                $percent = number_format( ($ordered_states [$i] ["count"]/$ordered_states_total_count) * 100, 1 );
										        ?>
        										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        											<div class="dashboard-stat2 ">
        												<div class="display">
        													<div class="number">
        														<h3 class="<?php echo $font_class;?>">
        															<span data-counter="counterup"
        																data-value="<?=$ordered_states [$i] ["count"]?>"><?=$ordered_states [$i] ["count"]?></span>
        														</h3>
        														<small><?php echo $ordered_states [$i] ["state_name_ar"]; ?></small>
        													</div>
        													<div class="icon">
        														<i class="icon-pie-chart"></i>
        													</div>
        												</div>
        												<div class="progress-info">
        													<div class="progress">
        														<span style="width: <?=$percent?>%;"
        															class="progress-bar progress-bar-success <?php echo $progress_bar_color_class;?>"> <span
        															class="sr-only"><?=$percent?>% <?php echo PROGRESS;?></span>
        														</span>
        													</div>
        													<div class="status">
        														<div class="status-title"><?php echo PROGRESS;?></div>
        														<div class="status-number"><?=$percent?>%</div>
        													</div>
        												</div>
        											</div>
        										</div>
						
										<?php } }?>
                                        		
                    						</div>
                    					</div>
                    					
                                       <div id="report2" class="row">
    											<div class="portlet light">
    												<div class="portlet-title">
    													<div class="caption caption-md">
    														<span class="caption-subject bold uppercase font-dark"><?php echo REPORT2_TITLE; ?></span>
    														<span class="caption-helper"><?php echo REPORT2_SMALL_TITLE; ?></span>
    													</div>
    													<div class="actions">
    														<a class="btn btn-circle btn-icon-only btn-default fullscreen"
    															href="#"> </a>
    													</div>
    												</div>
    												<div class="portlet-body" >
    													<div class="container-outer">
    													   <div class="container-inner">
    													   		<div id="dashboard_amchart_report" class="CSSAnimationChart"></div>
    													   </div>
    													</div>
    
    												</div>
    											</div>
        								</div>
        								
                						<div id="report3_4">		
                							<div class="row">
        										<div id="col1" class="col-md-6 col-sm-6">
        											<div class="portlet light custom-portlet">
        												<div class="portlet-title">
        													<div class="caption caption-md caption-custom">
        														<i class="icon-bar-chart font-dark hide"></i> <span
        															class="caption-subject font-green-steel bold uppercase"
        															style="padding-right: 15px;">
        															<?php echo REPORT3_TITLE; ?></span>
        														<span class="caption-helper"></span>
        													</div>
        												</div>
        												<div class="portlet-body">
        													<div class="row number-stats margin-bottom-30">
        														<div class="col-md-6 col-sm-6 col-xs-6">
        															<div class="stat-left">
        																<div class="stat-chart">
        																	<!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
        																	<div id="sparkline_bar2"></div>
        																</div>
        																<div class="stat-number">
        																	<div class="title count-title"><?php echo TOTAL_NEW_REGISTRATIONS; ?></div>
        																	<div class="number" style="text-align: center;"><?=$sum_new?></div>
        																</div>
        															</div>
        														</div>
        														<div class="col-md-6 col-sm-6 col-xs-6">
        															<div class="stat-right">
        																<div class="stat-chart">
        																	<!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
        																	<div id="sparkline_bar6"></div>
        																</div>
        																<div class="stat-number">
        																	<div class="title count-title"><?php echo TOTAL_HAVE_REGISTRATIONS; ?></div>
        																	<div class="number" style="text-align: center;"><?=$sum_have_progress?></div>
        																</div>
        															</div>
        														</div>
        													</div>
        													<div style="overflow-x: auto !important;" class="rep2-custom-class">
        														<table class="table table-hover table-light table-custom">
        															<thead >
        																<tr class="uppercase">
        																	<th class="custom-table-header"><?php echo REPORT3_AGENT; ?></th>
        																	<th class="custom-table-header"><?php echo REPORT3_NEW; ?></th>
        																	<th class="custom-table-header"><?php echo REPORT3_UNDER_PROGRESS; ?></th>
        																	<th class="custom-table-header"><?php echo REPORT3_FINISHED; ?></th>
        																	<th class="custom-table-header"><?php echo REPORT3_RATE; ?></th>
        																</tr>
        															</thead>
        														<?php
                                									for($i = 0; $i < count ( $agents_registrationsCounts ); $i ++) {
                                									    $have_progress_count = $agents_registrationsCounts [$i] ['count2'] + $agents_registrationsCounts [$i] ['count3'];
                                									    $all_count = $have_progress_count + $agents_registrationsCounts [$i] ['count1'];
                                									    if($all_count == 0){
                                									       $rate = 0; 
                                									    }else{
                                									       $rate = number_format( ($have_progress_count/$all_count) * 100, 1 );
                                									    }
                                										?>
        														<tr>
        																<td class="custom-table-cell"><a href="javascript:;" class="primary-link"><?=$agents_registrationsCounts [$i] ['agent_name']?></a> </td>
        																<td><?=$agents_registrationsCounts [$i] ['count1']?></td>
        																<td><?=$agents_registrationsCounts [$i] ['count2']?></td>
        																<td><?=$agents_registrationsCounts [$i] ['count3']?></td>
        																<td><span class="bold theme-font"><?=$rate?>%</span></td>
        															</tr>
        															<?php }	?>
        
        													</table>
        													</div>
        												</div>
        											</div>
        										</div>
        										
        										<div id="col2" class="col-md-6 col-sm-6">
        											<div class="portlet light custom-portlet">
        												<div class="portlet-title">
        													<div class="caption caption-md caption-custom">
        														<i class="icon-bar-chart font-dark hide"></i> <span
        															class="caption-subject font-green-steel bold uppercase"
        															style="padding-right: 15px;">
        															<?php echo REPORT4_TITLE; ?></span>
        														<span class="caption-helper"></span>
        													</div>
        												</div>
        												<div class="portlet-body">
        													<div class="row number-stats margin-bottom-30">
        														<div class="col-md-6 col-sm-6 col-xs-6">
        															<div class="stat-left">
        																<div class="stat-chart">
        																	<!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
        																	<div id="sparkline_bar5"></div>
        																</div>
        																<div class="stat-number">
        																	<div class="title count-title"><?php echo TOTAL_REGISTRATIONS_COUNT; ?></div>
        																	<div class="number" style="text-align: center;"><?=$sum_all?></div>
        																</div>
        															</div>
        														</div>
        														<div class="col-md-6 col-sm-6 col-xs-6">
        															<div class="stat-right">
        																<div class="stat-chart">
        																	<!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
        																	<div id="sparkline_bar"></div>
        																</div>
        																<div class="stat-number">
        																	<div class="title count-title"><?php echo DELAYED_REGISTRATIONS_COUNT; ?></div>
        																	<div class="number" style="text-align: center;"><?=$sum_delayed?></div>
        																</div>
        															</div>
        														</div>
        													</div>
        													<div style="overflow-x: auto !important;" class="rep2-custom-class">
        														<table class="table table-hover table-light table-custom">
        															<thead >
        																<tr class="uppercase">
        																	<th class="custom-table-header"><?php echo REPORT3_AGENT; ?></th>
        																	<th class="custom-table-header"><?php echo TOTAL_REGISTRATIONS_COUNT; ?></th>
        																	<th class="custom-table-header"><?php echo DELAYED_REGISTRATIONS_COUNT; ?></th>
        																	<th class="custom-table-header"><?php echo DELAYED_RATE; ?></th>
        																</tr>
        															</thead>
        													<?php
        													   for($i = 0; $i < count ( $agents_performance ); $i ++) {
        													       if($agents_performance [$i] ['count1'] == 0){
        													           $rate = 0;
        													       }else{
        													            $rate = number_format( ($agents_performance [$i] ['count2']/$agents_performance [$i] ['count1']) * 100, 1 );
        													       }
                                										?>
        														<tr>
        																<td class="custom-table-cell"><a href="javascript:;" class="primary-link"><?=$agents_performance [$i] ['agent_name']?></a> </td>
        																<td><?=$agents_performance [$i] ['count1']?></td>
        																<td><?=$agents_performance [$i] ['count2']?></td>
        																<td><span class="bold theme-font"><?=$rate?>%</span></td>
        															</tr>
        															<?php }	?>
        
        													</table>
        													</div>
        												</div>
        											</div>
        										</div>
        										
        									</div>
        								</div>
									
				<!-- START ACTIVITIES -->
				<div id = "activities">
								<?php if ($dashboard_activities) {?>
								<div id="activities_row" class="row">
										<div class="col-md-12">
											<div class="portlet light portlet-fit ">
												<div class="portlet-title">
													<div class="caption">
														<span class="caption-subject font-dark bold uppercase"><?php echo DASHBOARD_ACTIVITIES; ?></span>
													</div>

												</div>
												<div class="portlet-body">
													<div>
													   <div>

															<div
																data-always-visible="1" data-rail-visible="0">
																<ul class="feeds">
                                                                    <?php
									if ($dashboard_activities) {
										for($i = 0; $i < count ( $dashboard_activities ); $i ++) {
											if (isset($dashboard_activities[$i]["message"])){
											?>
											
                                                                        <li>
																		<div class="col1">
																			<div class="cont">
																				<div class="cont-col1">
																					<div class="label label-sm label-success">
																						<i class="fa fa-bell-o"></i>
																					</div>
																				</div>
																				<div class="cont-col2">
																					<div class="desc"> <?=$dashboard_activities[$i]["message"];?>
                                                                                        </div>
																				</div>
																			</div>
																		</div>
																		<div class="col2">
																			<div class="date"> <?=$dashboard_activities[$i]["date_time_diff"];?> </div>
																		</div>
																	</li>
                                                                        <?php
											}
										}
									}
									?>
                                                                        
                                                                    </ul>
															</div>
														</div>

													</div>
												</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<form id="myForm2" role="form" method="post"
											class="form-horizontal">
											<input type="hidden" name="calculated_current_page"
															id="calculated_current_page" value="<?php echo $current_page;?>">
											<div class="form-body">
												<div>
													<div class="row">
														<div style="text-align: center; margin: auto;">
															<input type="button" class="bold btn blue-sharp"
																style="border-radius: 4px !important;"
																onClick="javascript:prev_page()" <?php if(!$has_prev){?>
																disabled <?php }?> value="<?php echo PREVIOUS ;?>" /> <span
																style="margin-left: 5px; margin-right: 5px; text-align: center;"><b><?php echo PAGE_NUM. ": ".$current_page.' / '.$total_pages_num; ?></b></span>

															<input type="button" class="bold btn blue-sharp"
															style="border-radius: 4px !important;"
																onClick="javascript:next_page()" <?php if(!$has_next){?>
																disabled <?php }?> value="<?php echo NEXT ;?>" />

														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
											</div>
										</div>
									</div>
									
					
				<?php }?>
								<!-- END ACTIVITIES -->
				</div>



										</div>
													
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>				
                            </div>
                        </div>


                    </div>
                </div>
                </div>
                </div>
               
        <?php $this->load->view('utils/schoolFooter.php'); ?>
        <!--[if lt IE 9]>
        <script src="<?= base_url() ?>assets/global/plugins/respond.min.js"></script>
        <script src="<?= base_url() ?>assets/global/plugins/excanvas.min.js"></script> 
        <script src="<?= base_url() ?>assets/global/plugins/ie8.fix.min.js"></script> 
        <![endif]-->
            <!-- BEGIN CORE PLUGINS -->
        <script src="<?=base_url()?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
                <!-- END CORE PLUGINS -->
                <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?=base_url()?>assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
                <!-- END PAGE LEVEL PLUGINS -->
                <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?=base_url()?>assets/global/scripts/app.min.js" type="text/javascript"></script>
                <!-- END THEME GLOBAL SCRIPTS -->
                <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?=base_url()?>assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
                <!-- END PAGE LEVEL SCRIPTS -->
                <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
                <!-- END THEME LAYOUT SCRIPTS -->

<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135637603-1"></script>

<script>

  window.dataLayer = window.dataLayer || [];

  function gtag(){dataLayer.push(arguments);}

  gtag('js', new Date());

 

  gtag('config', 'UA-135637603-1');

</script>
                </body>

                </html>