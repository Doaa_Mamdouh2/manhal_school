
<?php
// session_start();
$this->load->view('localization/lang');
$this->load->view('localization/txt');
defineLocale();
defineStrings();

?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title><?php echo SCHOOL_SITE_TITLE_NEW; ?></title>
<link rel="icon" href="<?php echo base_url()?>images/header_logo.png">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="author" />

<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-table/bootstrap-table.min.css"
	rel="stylesheet" type="text/css" />

<link
	href="<?= base_url() ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css"
	rel="stylesheet" type="text/css" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?= base_url() ?>assets/css/student_registrations.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/global/plugins/morris/morris.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/global/plugins/fullcalendar/fullcalendar.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?= base_url() ?>assets/global/css/components-rtl.min.css"
	rel="stylesheet" id="style_components" type="text/css" />
<link href="<?= base_url() ?>assets/global/css/plugins-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link
	href="<?= base_url() ?>assets/layouts/layout3/css/layout-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/layouts/layout3/css/themes/default-rtl.min.css"
	rel="stylesheet" type="text/css" id="style_color" />
<link
	href="<?= base_url() ?>assets/layouts/layout3/css/custom-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon"
	href="<?= base_url() ?>assets/layouts/layout3/img/shortcut-icon.png" />



<script type="text/javascript">

	function validateEmail(email) {
		//var re = /\S+@\S+\.\S+/;
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}
	
	function checkValid(email_valdation) {

		var name = null;
		if(document.forms["myForm"]["name"] != null){
			name = document.forms["myForm"]["name"].value;
		}
		var mobile_number = null;
		if(document.forms["myForm"]["mobile_number"] != null){
			mobile_number = document.forms["myForm"]["mobile_number"].value;
		}
		var email = null;
		if(document.forms["myForm"]["email"] != null){
			email = document.forms["myForm"]["email"].value;
		}
		var user_name = null;
		if(document.forms["myForm"]["user_name"] != null){
			user_name = document.forms["myForm"]["user_name"].value;
		}
		var password = null;
		if(document.forms["myForm"]["password"] != null){
			password = document.forms["myForm"]["password"].value;
		}
		var retyped_password = null;
		if(document.forms["myForm"]["retyped_password"] != null){
			retyped_password = document.forms["myForm"]["retyped_password"].value;
		}
		

		if(mobile_number != ""){
			if (mobile_number.length > 15) {
				$("#mobile_number_tooltip").css("display","block");
				$("#mobile_number_tooltip").css("visibility","visible");
				return false;
			} else{
				$("#mobile_number_tooltip").css("display","none");
				$("#mobile_number_tooltip").css("visibility","none");
			}
		}
		
		if (name == "") {
			$("#name_tooltip").css("display","block");
			$("#name_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#name_tooltip").css("display","none");
			$("#name_tooltip").css("visibility","none");
		}
		if (email == "") {
			$("#email_tooltip").css("display","block");
			$("#email_tooltip").css("visibility","visible");
			return false;
		} else if (!validateEmail(email)) {
			$("#email_tooltip").text(email_valdation);
			$("#email_tooltip").css("display","block");
			$("#email_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#email_tooltip").css("display","none");
			$("#email_tooltip").css("visibility","none");
		}

		if (user_name == "") {
			$("#user_name_tooltip").css("display","block");
			$("#user_name_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#user_name_tooltip").css("display","none");
			$("#user_name_tooltip").css("visibility","none");
		}
		if (password == "") {
			$("#password_tooltip").css("display","block");
			$("#password_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#password_tooltip").css("display","none");
			$("#password_tooltip").css("visibility","none");
		}
		if (retyped_password == "" || password != retyped_password) {
			$("#retyped_password_tooltip").css("display","block");
			$("#retyped_password_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#retyped_password_tooltip").css("display","none");
			$("#retyped_password_tooltip").css("visibility","none");
		}
	
	}

	function numberKeyDown(e) {
		var keynum;
	    if(window.event) { // IE                    
	      keynum = e.keyCode;
	    } else if(e.which){ // Netscape/Firefox/Opera                   
	      keynum = e.which;
	    }

	    //console.log(keynum);
	    
	 // Allow: backspace, delete, tab, escape, enter and . 
	    if ($.inArray(keynum, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
	         // Allow: Ctrl+A, Command+A
	        (keynum === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
	        // Allow: Ctrl+C, Command+C
	        (keynum === 67 && (e.ctrlKey === true || e.metaKey === true)) || 
	        // Allow: Ctrl+X, Command+X
	        (keynum === 88 && (e.ctrlKey === true || e.metaKey === true)) || 
	         // Allow: home, end, left, right, down, up
	        (keynum >= 35 && keynum <= 37) ||
	        // + for mobile numbers
	        (keynum === 107 )) {
	             // let it happen, don't do anything
	             return;
	    }
	    
	    if(((e.shiftKey || (keynum < 48 || keynum > 57)) && (keynum < 96 || keynum > 105)) || (keynum >= 38 && keynum <= 40)){
			 e.preventDefault();
		}
	}

</script>

<style type="text/css">
.quick-search {
	padding-top: 9px !important;
	padding-bottom: 9px !important;
	background: #E5E5E5;
	border: #E5E5E5;
}

.btn-searchColor {
	padding-top: 9px !important;
	padding-bottom: 9px !important;
}
</style>

<style>
.scrollable-custom-class {
	max-width: 80% !important;
	margin: auto;
	text-align: center;
	-webkit-overflow-scrolling: touch;
}
</style>

<style type="text/css">
.table {
	max-width: none !important;
}

.table-scrollable {
	-webkit-overflow-scrolling: touch;
}

.bootstrap-switch-container {
	direction: ltr;
}

.portlet.blue, .portlet.box.blue>.portlet-title, .portlet>.portlet-body.blue {
    background-color: #9fcad6 !important
}

</style>


</head>
<!-- END HEAD -->

<body class="page-container-bg-solid">
	<div id="all-page" class="container shadow">
		<div class="row">
			<div class="col-md-13">
                    <?php
                    $this->load->view('utils/schoolHeaderAdmin');
					$this->load->view('utils/delete_alert');
                    ?>

                    <div class="page-wrapper-row full-height">
					<div class="page-wrapper-middle">
						<!-- BEGIN CONTAINER -->
						<div class="page-container">
							<!-- BEGIN CONTENT -->
							<div class="page-content-wrapper">
								<!-- BEGIN CONTENT BODY -->
								<div id="loading" class="hidden">
									<img id="loading-image"
										src="<?= base_url() ?>images/Ajax-loader2.gif"
										alt="Loading...">
								</div>
								<!-- BEGIN PAGE HEAD-->
								<div class="container">
									<div class="page-content-inner">

										<div class="portlet box blue">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i>
							<?php
							if ($name) {
								echo $name;
							} else {
								echo ADD_RECORD;
							}
							?>
						</div>
					</div>
					<div class="portlet-body form">
						<!-- BEGIN FORM-->
						<form id="myForm" role="form" method="post"
							onsubmit="javascript:return checkValid('<?php echo INVALID_EMAIL; ?>');"
							action="<?php echo site_url('admin_panel/add_edit_agent/'.$admin_id)?>"
							class="form-horizontal">
							<div class="form-body">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-3 control-label"><?php echo AGENT_NAME; ?><span
												class="required"> * </span></label>
											<div class="col-md-6">
												<input type="text" class="form-control " name="name"
													value="<?php echo  $name; ?>"
													onfocus="javascript:removeTooltip('name_tooltip');">
												<span id="name_tooltip" class="tooltiptext"
													style="display: none; color: red;"><?php echo FILL_THIS_FIELD; ?></span>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-3 control-label"><?php echo MOBILE_NUMBER; ?> </label>
											<div class="col-md-6">
												<input type="text" class="form-control " name="mobile_number" onkeydown="numberKeyDown(event)"
													value="<?php echo  $mobile_number; ?>" id="mobile_number"
													onfocus="javascript:removeTooltip('mobile_number_tooltip');">
												<span id="mobile_number_tooltip" class="tooltiptext"
													style="display: none; color: red;"><?php echo MOBILE_NUMBER_INVALID; ?></span>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-3 control-label"><?php echo EMAIL; ?> <span
												class="required"> * </span></label>
											<div class="col-md-6">
												<input type="text" class="form-control " name="email"
													value="<?php echo  $email; ?>" id="email"
													onfocus="javascript:removeTooltip('email_tooltip');">
												<span id="email_tooltip" class="tooltiptext"
													style="display: none; color: red;"><?php echo FILL_THIS_FIELD; ?></span>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-3 control-label"><?php echo USERNAME; ?> <span
												class="required"> * </span></label>
											<div class="col-md-6">
												<input type="text" class="form-control " name="user_name"
													value="<?php echo  $user_name; ?>" id="user_name"
													onfocus="javascript:removeTooltip('user_name_tooltip');">
												<span id="user_name_tooltip" class="tooltiptext"
													style="display: none; color: red;"><?php echo FILL_THIS_FIELD; ?></span>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-3 control-label"><?php echo PASSWORD; ?> </label>
											<div class="col-md-6">
												<input type="password" class="form-control " name="password"
													value="<?php echo  $password; ?>" id="password"
													onfocus="javascript:removeTooltip('password_tooltip');"> <span
													id="password_tooltip" class="tooltiptext"
													style="display: none; color: red;"><?php echo FILL_THIS_FIELD; ?></span>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-3 control-label"><?php echo REPEAT_PASSWORD; ?> </label>
											<div class="col-md-6">
												<input type="password" class="form-control "
													value="<?php echo $password; ?>" id="retyped_password"
													onfocus="javascript:removeTooltip('retyped_password_tooltip');">
												<span id="retyped_password_tooltip" class="tooltiptext"
													style="display: none; color: red"><?php echo INCORRECT_REPEATED_PASSWORD; ?></span>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<!--  <div class="col-md-6">
										<div class="form-group">
											<label class="col-md-3 control-label"><?php echo AGENT_GRADES; ?> </label>
											<div class="col-md-6">
											<?php foreach ($educational_grades as $edu_grade){
												$grade_id = $edu_grade['id']; 
												$grade_desc = $edu_grade['desc_ar'];
											?>
												<input type="checkbox" id="grade" name="grade[]" value="<?php echo $grade_id; ?>">&nbsp;
  												<label for="grade"> <?php echo $grade_desc; ?></label><br>
  											<?php } ?>
											</div>
										</div>
									</div>
									-->
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-3 control-label"><?php echo AGENT_GRADES; ?> </label>
											<div class="col-md-6">
												<table class="table table-striped table-bordered table-advance table-hover">
													<tbody>
														<tr>
															<th class="tableHeader" bgcolor="#abb6f9"><?php echo STUDENT_GRADE; ?></th>
															<?php foreach ($schools as $school){
																$school_id = $school['id']; 
																$school_name = $school['name'];
															?>
															<th class="tableHeader" bgcolor="#abb6f9"><?php echo $school_name; ?></th>
															<?php } ?>
														</tr>
														
													<!-- START DYNAMIC TABLE -->
														<?php
														
														if ($educational_grades&& count($educational_grades) > 0) {
															for($i = 0; $i < count ( $educational_grades); $i ++) {
																$grade_desc= $educational_grades[$i]['desc_ar'];
																$grade_id = $educational_grades[$i]['id'];
														?>
														<tr data-id="<?php echo $grade_id?>">
															<td bgcolor="#abb6f9"><?php echo $grade_desc?></td>
															<?php
																$school_1 = "";
																$school_2 = "";
																
																if($agent_grades){
																	for($j = 0; $j < count ($agent_grades); $j ++) {
																		$agent_grade_id = $agent_grades[$j]['edu_grade_id'];
																		$agent_school_id = $agent_grades[$j]['school_id'];
																		if($agent_grade_id == $grade_id){
																			if($agent_school_id == "1")
																				$school_1 = "yes";
																			else if($agent_school_id== "2")
																				$school_2= "yes";
																		}
																	}
																} 
																
															?>
															<td align="center"><input type="checkbox" name="grade[]" value = "SCHOOL1_<?php echo $grade_id?>" class="make-switch" data-size="small" 
															<?php if(isset($school_1) && $school_1 == "yes"){?>
																	checked
																<?php }?>  
																data-on-color="primary" data-off-color="default">
															</td>
															<td align="center"><input type="checkbox" name="grade[]" value = "SCHOOL2_<?php echo $grade_id?>" class="make-switch" data-size="small" 
															<?php if(isset($school_2) && $school_2== "yes"){?>
																	checked
																<?php }?>  
																data-on-color="primary" data-off-color="default">
															</td>
														</tr>
														<?php
															}//end forloop
														} ?>
														<!-- END DYNAMIC TABLE -->
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<?php if($admin_id > 0){?>
                                    <div class="col-md-6">
										<div class="form-group">
											<label class="col-md-3 control-label"><?php echo MOVE_REGS; ?></label>
											<div class="col-md-6">
												<select class="form-control " name="moved_agent_id"
														onfocus="javascript:removeTooltip('agent_id_tooltip');"
                                                        id="moved_agent_id">
											
												<option value=""></option>
												<?php
													if ($available_agents) {
														for($i = 0; $i < count ( $available_agents); $i ++) {
															$available_agent_id = $available_agents[$i] ["admin_id"];
															$available_agent_name = $available_agents[$i] ["name"];
															?>
															<?php if ($available_agent_id != $admin_id){?>
															<option value="<?php echo $available_agent_id?>" > <?php echo $available_agent_name?></option>
															<?php }
														}
													} else {
												?>
												<option value=""></option>
												<?php } ?>
												</select>
											</div>
										</div>
									</div>
                                    <?php }?>
								</div>
								
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn btn-circle blue"><?php
											if ($name) {
												echo EDIT;
											} else {
												echo INSERT;
											}
											?></button>

											<a href="<?php echo site_url('admin_panel/view_agents')?>">
												<button type="button" class="btn btn-circle blue btn-outline">
													<?php echo CANCEL; ?>
												</button>
											</a>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!-- END FORM-->
					</div>
				</div>
									</div>
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
</div>
</div>
	
	<?php $this->load->view('utils/schoolFooter');?>	
    <!--[if lt IE 9]>
    <script src="<?= base_url() ?>assets/global/plugins/respond.min.js"></script>
    <script src="<?= base_url() ?>assets/global/plugins/excanvas.min.js"></script> 
    <script src="<?= base_url() ?>assets/global/plugins/ie8.fix.min.js"></script> 
    <![endif]-->


			<!-- BEGIN CORE PLUGINS -->
			<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
				type="text/javascript"></script>
			<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
				type="text/javascript"></script>
			<!-- END CORE PLUGINS -->
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<script src="<?= base_url() ?>assets/global/plugins/moment.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/morris/morris.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/morris/raphael-min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/counterup/jquery.waypoints.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/counterup/jquery.counterup.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/fullcalendar/fullcalendar.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/flot/jquery.flot.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/flot/jquery.flot.resize.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/flot/jquery.flot.categories.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
				type="text/javascript"></script>

			<script
				src="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js"
				type="text/javascript"></script>

			<script
				src="<?=base_url()?>assets/global/plugins/bootstrap-table/bootstrap-table.min.js"
				type="text/javascript"></script>

			<script src="<?= base_url() ?>assets/pages/scripts/dashboard.min.js"
				type="text/javascript"></script>

			<!-- END PAGE LEVEL PLUGINS -->
			<!-- BEGIN THEME GLOBAL SCRIPTS -->
			<script src="<?=base_url()?>assets/global/scripts/app.min.js"
				type="text/javascript"></script>
			<!-- END THEME GLOBAL SCRIPTS -->
			<!-- BEGIN PAGE LEVEL SCRIPTS -->
			<script src="<?=base_url()?>assets/pages/scripts/profile.min.js"
				type="text/javascript"></script>
			<!-- END PAGE LEVEL SCRIPTS -->
			<!-- BEGIN THEME LAYOUT SCRIPTS -->
			<script
				src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
				type="text/javascript"></script>

			<!-- END THEME LAYOUT SCRIPTS -->

			<script>
      $(function () {
    
  		$('#quickSearchForm').on('keyup keypress', function(e) {
			  var keyCode = e.keyCode || e.which;
			  if (keyCode === 13) {
	        		var searchVal = $("#quick_search_input").val().trim();
	        		apply_quick_search();
			}
    	});
        $(document).on('keyup','#quick_search_input',function(event) { 
      	    if(event.keyCode == 13){
      	    	apply_quick_search();
      	    }
      	});
    		    
      });

  
		
  </script>

<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135637603-1"></script>

<script>

  window.dataLayer = window.dataLayer || [];

  function gtag(){dataLayer.push(arguments);}

  gtag('js', new Date());

 

  gtag('config', 'UA-135637603-1');

</script>

</body>

</html>
