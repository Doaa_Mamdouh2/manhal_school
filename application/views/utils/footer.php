	
 <div class="page-wrapper-row">
                <div class="page-wrapper-bottom">
                    <!-- BEGIN FOOTER -->
                    <!-- BEGIN PRE-FOOTER -->
                    <div class="page-prefooter">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4 col-sm-6 col-xs-12 footer-block">
                                    <h2><?php echo ABOUT_FOOTER?></h2>
                                    <p><?php echo ABOUT_TEXT ?> </p>
                                </div>
                               
                                <div class="col-md-4 col-sm-6 col-xs-12 footer-block">
                                    <h2><?php echo MADE_BY ?></h2>
                                    <address class="margin-bottom-40"><?php echo MADE_BY_TEXT ?>
                                        <br>
                                        <a href="http://soom.com.sa/">soom.com.sa</a>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PRE-FOOTER -->
                    <!-- BEGIN INNER FOOTER -->
                    <div class="page-footer">
                        <div class="container"> <?php echo COPY_RIGHTS ?> &copy; <?php echo COPY_RIGHTS_YEAR ?>
                        </div>
                    </div>
                    <div class="scroll-to-top">
                        <i class="icon-arrow-up"></i>
                    </div>
                    <!-- END INNER FOOTER -->
                    <!-- END FOOTER -->
                </div>
            </div>
