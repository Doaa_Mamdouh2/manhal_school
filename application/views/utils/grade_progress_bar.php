<html>
<head>
<script>
// add the highlighting color if the filter is not selected
function mouseOver (grade){
	var element = document.getElementById(grade);
	if(!element.classList.contains("done"))
	element.className += " mouseover";
}

// remove the highlighting color if the filter is not selected
function mouseOut (grade){
	var element = document.getElementById(grade);
	if(!element.classList.contains("done"))
        element.classList.remove("mouseover");
	
}

// while clicking in certain filter add the class "done" (Green) and apply the action
function grade_filter(grade) {
   // removr the done class from the current filter
   var elements = document.getElementsByClassName("done");
    //for(var i = 0, length = elements.length; i < length; i++) {
         elements[1].classList.remove("done");
    //}
    // remove the mouseover class and add the "done" class for the selected filter
    var selectedDiv= document.getElementById(grade);
    selectedDiv.classList.remove("mouseover");
    selectedDiv.className += " done";

    var view_late_regs_lvl = <?= $view_late_regs_lvl ?>;
    
    // applying action depending on values
	if(grade == "grade_display_all"){
		if(view_late_regs_lvl == "0"){
			window.location="<?=site_url('admin_panel/view_registrations').'/1/30/1/'.$selected_state.'/0/0'?>";
		}else{
			window.location="<?=site_url('admin_panel/view_late_registrations').'/1/30/1/'.$selected_state.'/0/0/'.$view_late_regs_lvl?>";
		}
	} else{
		if(view_late_regs_lvl == "0"){
			window.location="<?=site_url('admin_panel/view_registrations').'/1/30/1/'.$selected_state.'/'; ?>" +grade+'/0';
		}else{
			window.location="<?=site_url('admin_panel/view_late_registrations').'/1/30/1/'.$selected_state.'/'; ?>" +grade+'/0/'+view_late_regs_lvl;
		}
	}
}
</script>

<style type="text/css">
@media only screen and (max-width: 991px) {
	.grade-box {
        width: 48%;
        margin: 1%;
        float: right !important;
	}
}
</style>

</head>

<body>
	<div class="portlet ">
	<?php if (isset($student_registrations)){?>
		<div class="portlet-title" style="border-bottom: none !important;">
			<div class="caption"><span
					class="caption-subject font-blue bold uppercase"><?php echo GRADE_FILTER;?></span>
			</div>
		</div>
		<?php }?>
		
		<div class="portlet-body">
			<div class="mt-element-step">
				<div class="row step-thin">
					<div id="grade_display_all" onclick="grade_filter('grade_display_all')"
						onmouseover="mouseOver('grade_display_all')"
						onmouseout="mouseOut('grade_display_all')"
						class="col-pb-3 bg-grey mt-step-col grade-box 
						<?php if($selected_grade == null || $selected_grade == '0'){?>
					done
					<?php } ?>
					">
						<div class="mt-step-title uppercase font-grey-cascade"><?= DIAPLAY_ALL ?></div>
						<div class="mt-step-content font-grey-cascade"> <?= $registration_grades_total_count?></div>
					</div>
					<?php
					for($i = 0; $i < count ( $registration_grades ); $i ++) {
						?>
						<div id="<?= $registration_grades[$i]["grade_id"] ?>"
						<?php if(isset($student_registrations)){?>
						onclick="grade_filter('<?= $registration_grades[$i]["grade_id"] ?>')"
						onmouseover="mouseOver('<?= $registration_grades[$i]["grade_id"] ?>')"
						onmouseout="mouseOut('<?= $registration_grades[$i]["grade_id"] ?>')"
						<?php }?>
						class=" col-pb-7
						      
							 bg-grey mt-step-col grade-box 
						<?php if((isset ( $selected_grade ) && $registration_grades [$i] ["grade_id"] == $selected_grade)){?>
							done
							<?php }?>
							">
						<!-- seeting name of each grade -->
						<div class="mt-step-title uppercase font-grey-cascade"><?= trim($registration_grades[$i]["desc_ar"]) ?></div>

						<div class="mt-step-content font-grey-cascade">
					<?php if(isset($student_registrations)){
					           echo $registration_grades [$i] ["count"];
    							} else {echo "";}
										?></div>
					</div>
						<?php }?>
				</div>
			</div>
		</div>
	</div>
</body>
</html>