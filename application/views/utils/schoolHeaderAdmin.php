<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title><?php echo SCHOOL_SITE_TITLE_NEW; ?></title>

<link rel="icon" href="<?php echo base_url()?>images/header_logo.png">
<link rel="shortcut icon" href="<?=base_url()?>images/header_logo.png" />

<link rel="stylesheet"  href="<?=base_url()?>css/bootstrap.css">
<link rel="stylesheet"  href="<?=base_url()?>css/bootstrap-rtl.css">
<link rel="stylesheet"  href="<?=base_url()?>css/style.css">
<link rel="stylesheet"  href="<?=base_url()?>css/hover.css"media="all">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/morris/morris.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?=base_url()?>assets/global/css/components-rtl.min.css"
	rel="stylesheet" id="style_components" type="text/css" />
<link href="<?=base_url()?>assets/global/css/plugins-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link
	href="<?=base_url()?>assets/layouts/layout3/css/layout-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/layouts/layout3/css/themes/default-rtl.min.css"
	rel="stylesheet" type="text/css" id="style_color" />
<link
	href="<?=base_url()?>assets/layouts/layout3/css/custom-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"
	rel="stylesheet" type="text/css" />
<!-- PROFILE STYLE -->
<link href="<?=base_url()?>assets/pages/css/profile-rtl.min.css" rel="stylesheet"
	type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap/css/ccm_css.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->

<link href="<?php echo base_url()?>assets/css/student_registrations.css" rel="stylesheet">
<link href="<?php echo base_url()?>css/Content/custom_bootstrap_rtl_css.css"
	rel="stylesheet">

<script src="<?php echo base_url()?>js/Scripts/jquery-1.10.2.min.js"></script>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<script>
$(document).ready(function(){
    $("#delBtn").click(function(){
        $("#delNotify").show();
    });
    $("#cancel").click(function(){
        $("#delNotify").hide();
    });
    $("#delNotify").hide();

    $('form').on('focus', 'input[type=number]', function(e) {
    	$(this).on('wheel', function(e) {
    		e.preventDefault();
    	});
        });

        $('form').on('blur', 'input[type=number]', function(e) {
        	$(this).off('wheel');
        });

        $('form').on('keydown', 'input[type=number]', function(e) {
    	if ( e.which == 38 || e.which == 40 )
    		e.preventDefault();
        }); 


        $('form').on('focus', 'input[type=number]', function (e) {
    	$(this).on('mousewheel.disableScroll', function (e) {
    		e.preventDefault();
    	});
        });
        $('form').on('blur', 'input[type=number]', function (e) {
        	$(this).off('mousewheel.disableScroll');
        });	 
    	 
});
</script>

<link rel="stylesheet" type="text/css"
	href="<?=base_url()?>assets/css/jquery.calendars.picker.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?=base_url()?>assets/js/jquery.plugin.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.all.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.plus.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.ummalqura.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.ummalqura-ar.js"></script>
<script>
var jqOld = jQuery.noConflict();
jqOld( function() {
	//jqOld( "input.date" ).calendarsPicker({calendar: jqOld.calendars.instance('ummalqura','ar')});

	jqOld("input.date").each(function() {
	    var readonly = jqOld(this).attr("readonly");
	    if(readonly && readonly.toLowerCase()!=='false') { // this is readonly
	        console.log('this is a read only field');
	    }else{
	    	jqOld(this).calendarsPicker({calendar: jqOld.calendars.instance('ummalqura','ar')});
	    }
	});
	
  } );
 jqOld( function() {
	    //jqOld( "input.dateMilady" ).calendarsPicker({calendar: jqOld.calendars.instance('gregorian','ar')});

		jqOld("input.dateMilady").each(function() {
		    var readonly = jqOld(this).attr("readonly");
		    if(readonly && readonly.toLowerCase()!=='false') { // this is readonly
		        console.log('this is a read only field');
		    }else{
		    	jqOld(this).calendarsPicker({calendar: jqOld.calendars.instance('gregorian','ar')});
		    }
		});
	    
  } );
 </script>
 
<script>
/*  $( function() {
    $( ".date" ).calendarsPicker({calendar: $.calendars.instance('ummalqura','ar')});
  } );*/
  </script>

<script>
var violated = false;
function numberKeyDown(e) {
	var keynum;
    if(window.event) { // IE                    
      keynum = e.keyCode;
    } else if(e.which){ // Netscape/Firefox/Opera                   
      keynum = e.which;
    }
    //alert (keynum);
 // Allow: backspace, delete, tab, escape, enter, up and down.
    if ($.inArray(keynum, [46, 8, 9, 27, 13,0,39,38,40]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (keynum === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
        // Allow: Ctrl+C, Command+C
        (keynum === 67 && (e.ctrlKey === true || e.metaKey === true)) || 
        // Allow: Ctrl+X, Command+X
        (keynum === 88 && (e.ctrlKey === true || e.metaKey === true)) || 
     // Allow: Ctrl+V, Command+V
        (keynum === 86 && (e.ctrlKey === true || e.metaKey === true)) ||
    	 // Allow: Ctrl+Z, Command+Z
        (keynum === 90 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (keynum >= 35 && keynum <= 37)) {
             // let it happen, don't do anything
             return;
    }
    
    if(((keynum < 48 || keynum > 57) && (keynum < 96 || keynum > 105)) || (keynum >= 38 && keynum <= 40)){
		 e.preventDefault();
	}
}
function textKeyDown(e,elem) {
	var keynum;
    if(window.event) { // IE                    
      keynum = e.keyCode;
    } else if(e.which){ // Netscape/Firefox/Opera                   
      keynum = e.which;
    }
//	alert (keynum);
 // Allow: backspace, delete, tab, escape, enter, up and down.
    if ($.inArray(keynum, [46, 8, 9, 27, 13, 110, 190,39,38,40]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (keynum === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
        // Allow: Ctrl+C, Command+C
        (keynum === 67 && (e.ctrlKey === true || e.metaKey === true)) || 
        // Allow: Ctrl+X, Command+X
        (keynum === 88 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (keynum >= 35 && keynum <= 37)) {
             // let it happen, don't do anything
             return;
    }
	
    if(((keynum >= 48 && keynum <= 57) || (keynum >= 96 && keynum <= 105)) || (keynum >= 38 && keynum <= 40)){
		 e.preventDefault();
	}
}

function dateValidation(dateVar){
	//Format: yyyy/mm/dd
	var dateReg = /^\d{4}\/\d{2}\/\d{2}$/;
	var isV = dateReg.test(dateVar);
	if(!isV){
		return false;
	} 
	return true;
}
function parseArabic(str) {
    return ( str.replace(/[٠١٢٣٤٥٦٧٨٩]/g, function(d) {
        return d.charCodeAt(0) - 1632;
    }).replace(/[۰۱۲۳۴۵۶۷۸۹]/g, function(d) {
        return d.charCodeAt(0) - 1776;
    }) );
}

function textNumViolation(){
var violate = false;
	$('*[id*=valid_span]:visible').each(function() {
	    //     alert($(this).text());
		 
		 violate = true;
	});
	return violate ;
}

function replacAllArabToEng(){
	$(':input').each(function(){
	         var attr = $(this).attr('act');
		if (typeof attr !== typeof undefined){
			$(this).val(parseArabic($(this).val()));
			//alert($(this).val());
		 }
	});
}

function handlingTextNumberViolation (item){
	var attr = item.attr('act');
    			 
    			var val = parseArabic(item.val());
    			//alert(val);
				var matches = val.match(/\d+/g);
				
				if (item.attr("class").indexOf("date")>=0 && val!="" &&!dateValidation(val))
					$('#valid_span_'+item.attr('name')).css("display","block");
				else if ((item.attr("class").indexOf("date")<0 && (matches != null && typeof attr == typeof undefined))||(typeof attr !== typeof undefined && !$.isNumeric(val) && val!="")) {
					$('#valid_span_'+item.attr('name')).css("display","block");
					
				}else /*if(!violated || (violated && val!="")||item.attr("type") == "text")*/{
					$('#valid_span_'+item.attr('name')).css("display","none");
					/*if(item.attr("type") == "number"){
						violated  = false;
					}*/
				}
}
 $(document).ready(function(){
 	<?php if ($this->session->flashdata('confirmation_message')) { ?>
 	bootbox.dialog({
		message: '<p>'+"<?= $this->session->flashdata('confirmation_message');?>"+' </p>',
		buttons: {
			confirm: {
				label: "<?= HIDE?>",
				className: 'green'
			}
		}
	});
<?php }?>
	    $('.menu-dropdown , .mt-step-col , .btn-outline  , #no-popup').click(function() {
        	$('#loading').removeClass('hidden');
   	 });
   	 $('#add_drop-menu , #load_drop-menu , .view-only , #case_detail').click(function() {
   	 	$('#loading').addClass('hidden');
   	 });
	    
   // Disable scroll when focused on a number input.
    $('form').on('focus', 'input[type=number]', function(e) {
	$(this).on('wheel', function(e) {
		e.preventDefault();
	});
    });

    // Restore scroll on number inputs.
    $('form').on('blur', 'input[type=number]', function(e) {
    	$(this).off('wheel');
    });

    // Disable up and down keys.
    $('form').on('keydown', 'input[type=number]', function(e) {
	if ( e.which == 38 || e.which == 40 )
		e.preventDefault();
    }); 

    //disable mousewheel on a input number field when in focus
    //(to prevent Cromium browsers change the value when scrolling)
    $('form').on('focus', 'input[type=number]', function (e) {
	$(this).on('mousewheel.disableScroll', function (e) {
		e.preventDefault();
	});
    });
    $('form').on('blur', 'input[type=number]', function (e) {
    	$(this).off('mousewheel.disableScroll');
    });	 
	
});
</script>


<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	
<script>
var update = 0;
var pushNotification = false;
var pushNotificationId = "";
var count = 0;
var title = document.title;
// update the tab title the returned count_unseen after login 
<?php if ( $count_unseen > 0 ){?>
	var newTitle = '(' + <?= $count_unseen ?> + ') ' + title;
	document.title = newTitle;
<?php } ?>
// function called on interval every 20 sec
function updateNotifications() {
	// make ajax call to get latest count of unseen notifications and list of them
	$.ajax({  
 		type    : "POST",  
 		url     : "<?=site_url('seen_notifications/get_unseen_notifications' )?>",
 		dataType:"json",
 		success : function(data){
 			console.log(data);
			// count returned from ajax call
 			var count_unseen = data["count_unseen"];
			// current displayed count in red badge
 			var count = $("#notify_count").text();
			//intialize the count in case of there is not displayed count in screen
 			if(count == "") {
   				count = "0";
   			}
			
			// if returned count from the ajax call is more than 0 update tab title and red badge
 			if(count_unseen > 0 ){
 				var newTitle = '(' + count_unseen + ') ' + title;
	    		document.title = newTitle;
 			} else {
				 document.title = title;
			}
		
	   		if ( count_unseen == 0){
	   			$("#notify_count").html("");
	   		}else{
	   			$("#bell").html('<i class="icon-bell" onclick="resetCounter()"></i><span onclick="resetCounter()" id="notify_count" class="badge badge-default">'+count_unseen+ '</span>');
	   		}
 		
	        // the unseen notifications list returned from ajax call
 			var notifications_list = data["notifications_list"];
 			//alert(count_unseen);
 			//alert(count);
 			if ( count_unseen > count){
				// displaying just the notifications which are not displayed in the list 
				// decending to the the newest in the top of the list using "prepend" function
 				for (i = count_unseen-count-1; i >= 0; i--){
 	 				console.log(notifications_list[i]["reg_id"]);
	 	   			// 	add new item in the list of id "notifications"
 		   			var listContainer = $('#notifications');
					// link of the href to the related case
					var caseLink = "<?=site_url('Admin_panel/view_student_registration')?>"+"/"+notifications_list[i]["reg_id"];
 	   				listContainer.prepend('<li><a href="'+caseLink +'"><span class="time">'+notifications_list[i]["sent_date_time"]+'</span><span class="details"> '+notifications_list[i]["message"]+'</span></a></li>');
 				}
				// this two var to check if there are new unseen notifications then send the last id
				// to the sendLastSeenNotificationId to set all notifications till this id as "seen"
 				pushNotification = true;
	   			pushNotificationId = notifications_list[0]["email_id"];
 			}
 		}
	});
}
// starting interval
function newUpdate() {
    update = setInterval(updateNotifications, 20000);
}

newUpdate();
  // this code on clicking on notify bell
  function resetCounter(){


	  console.log("reset");
	  // stoping the interval while updating last seen notification to avoid conflicts
	  clearInterval(update);
	  //reset tab title
	  document.title = title;
	  $("#notify_count").html("");
	  
	 // send the last item id to check last seen item
	 // check the flag of the sent notifications
	 if(pushNotification){
		 sendLastSeenNotificationId (pushNotificationId);
		 pushNotification = false;
		 pushNotificationId = "";
	 }else{
	 <?php  if($notifications_list){?>
	 sendLastSeenNotificationId ('<?= $notifications_list[0]["email_id"]?>');
	 <?php } else {?>
	 // in case of there is no notifications for the current user return the interval
	 update = setInterval(updateNotifications, 20000);
	 <?php }?>
	 }
	   	return;
 }
 // this function to set all notifications till this email_id as "seen"
 function sendLastSeenNotificationId (email_id){
	 
	   	$.ajax({  
 		type    : "POST",  
 		url     : "<?=site_url('seen_notifications/update_seen_notifications' )?>",  
 		data    : {
     	last_id: email_id,
     	page: '<?= $page ?>'
 	},
 	complete : function(data){
		// return the interval after calling LastSeenNotificationId
 		update = setInterval(updateNotifications, 20000);
     	console.log(data);
 	}
	});
		
 }
 
 function _(el){
	    return document.getElementById(el);
	}


</script>


<style type="text/css">
.page-header .page-header-menu {
	background: #82bdcd !important;
}

.portlet.box.green>.portlet-title, .portlet.green, .portlet>.portlet-body.green
	{
	background-color: #82bdcd !important;
}

.page-footer {
	background-color: #82bdcd !important;
}

.page-wrapper .page-wrapper-middle {
	background: none !important;
}

.dropdown-menu {
	background: #03366A !important;
}

.navbar-nav>li>a:hover {
	background: #82bdcd !important;
}

.menu-dropdown classic-menu-dropdown:hover {
	background: #82bdcd !important;
}

.page-header .page-header-menu .hor-menu .navbar-nav>li.open>a,
	.page-header .page-header-menu .hor-menu .navbar-nav>li:hover>a,
	.page-header .page-header-menu .hor-menu .navbar-nav>li>a:active,
	.page-header .page-header-menu .hor-menu .navbar-nav>li>a:focus,
	.page-header .page-header-menu .hor-menu .navbar-nav>li>a:hover {
	background: #82bdcd !important;
	text-shadow: 2px 2px 2px rgba(255, 255, 255, 0.46);
}

.table>thead>tr>th {
    background-color:  #82bdcd !important;
}

.btn.green:not(.btn-outline) {
    color: #FFF;
    background-color: #82bdcd !important;
    border-color: #82bdcd !important;
}

.form-horizontal .control-label {
    text-align: right !important;
}

.form-control{
    font-size: 12px !important;    
}

select.form-control{
    padding-top: 2px !important;
}

.bootstrap-switch-container{
    direction: ltr;
}

</style>


<style type="text/css">

	.mobile-num {
		font-size: 18px !important;
		color: red !important;
	}
    .fragment {
        font-size: 12px;
        font-family: tahoma;
        height: 30px;
        width: 187px;
        border: 1px solid #ccc;
        color: #555;
        display: block;
        padding: 5px;
        box-sizing: border-box;
        text-decoration: none;
    	background-color: rgba(85, 85, 85, 0.09) !important;
    }
    
    .custom-green-button{
    	color: #FFF;
        background-color: #007c5a;
        border-color: #007c5a;
        float: right;
    }
    
    .custom-red-button{
    	color: #fff;
        background-color: #ed6b75;
        border-color: #ea5460;
    	margin-right: 1%;
    }
    
    
    .table{
         max-width:none !important;
    }
    .table-scrollable{
    	-webkit-overflow-scrolling: touch;
    }
    
    @media (max-width: 480px){
        .page-header .page-header-top .top-menu {
            display: block !important;
        }
        .page-header {
            padding-top: 20px !important;
        }
    }
    
    @media (max-width: 350px){
        #advSearchBtn{
            display: block;
            
        }
    }

    @media (min-width: 800px){
        .header_logo{
            margin-top: -50px !important;
        }
    }
    
    .scrollable-custom-class {
    	max-width: 80% !important;
    	margin: auto;
    	text-align: center;
    	-webkit-overflow-scrolling: touch;
    }
    
    .table {
    	max-width: none !important;
    }
    
    .table-scrollable {
    	-webkit-overflow-scrolling: touch;
    }
    
    .bootstrap-switch-container {
    	direction: ltr;
    }
    
    #ContentPlaceHolder1_uxTitlePage{
        display: inline !important;
        padding-left: 10px !important;
    }
    
    .webContent-Title {
        padding-bottom: 25px !important;
    }
       
    .form-control, output {
        font-size: 14px !important;
    }
    
    @media (max-width: 655px) {
        .webContent-Title {
            padding-bottom: 55px !important;
        }
    }

   
    .page-wrapper, .page-wrapper .page-wrapper-row.full-height, body, html {
    	height: unset !important;
    }
    
    body {
        background-color: rgb(244, 245, 251) !important;
    }
    
    div.form-group label, div.form-group div, #myForm div.row div.col-md-6 {
    	
    }
    
    div.form-group label {
    	font-size: 15px;
    	color: #3B467E;
    	font-weight: bold;
    	text-align: right;
    	padding-bottom: 25px;
    	direction: rtl;
    }
    
    .portlet-title {
    	font-weight: bold;
    	font-size: 15px;
    	color: #fff;
    }
    
    .btn {
    	border: 1px solid transparent !important;
    	border-radius: 4px !important;
    	border-top-right-radius: 0 !important;
    	border-bottom-right-radius: 0 !important;
    }
    
    .form-control {
    	border: 1px solid #cccccc !important;
    	border-radius: 4px !important;
    	border-bottom-left-radius: 0 !important;
    	border-top-left-radius: 0 !important;
    }
    
    div.col-md-6 .form-control {
    	border: 1px solid #cccccc !important;
    	border-radius: 4px !important;
    	height: auto;
    }
    
    .actions .btn , #pagination_form .btn, .form-actions .row .col-md-9 .btn, .form-group .col-md-5 .btn{
    	border: 1px solid transparent !important;
    	border-radius: 4px !important;
    }
    
    .hidden {
    	display: none !important;
    }
    
    .table thead {
    	color: #fff;
    }
    
    .table thead tr th {
    	text-align: center !important;
    }
    
    footer img {
        display: block !important;
        margin: auto !important;
        left: auto  !important;
        transform: none  !important;
    }
    
    .menu-text{
        float: left !important;
        margin-top: -8px !important;
    }

    .menu-mobile{
        margin-bottom: 20px !important;
    }
    
    .navbar-nav>li>a:hover , .navbar-nav>li>a:focus{
        color: #fff !important;
        background-color: #ddd !important;
        border-radius: 4px !important;
        width: 100%;
    }
    
    .page-header {
        margin-top: 0px !important;
        margin-bottom: 0px !important;
        height: auto !important;
    }
    

    .table>thead>tr>th {
        background-color: #9fcad6 !important;
    }

    .font-blue {
        color: #3B467E !important;
    }
    
    .btn.blue-sharp:not(.btn-outline), .btn.blue-sharp:not(.btn-outline):hover , .btn.blue-sharp:hover {
        color: #FFFFFF;
        background-color: #3B467E !important;
        border-color: #357dbb;
        border-top-color: rgb(53, 125, 187);
        border-right-color: rgb(53, 125, 187);
        border-bottom-color: rgb(53, 125, 187);
        border-left-color: rgb(53, 125, 187);
    }

    .btn.btn-outline.blue-sharp{
      color: #3B467E  !important;
    }
    
    .btn.btn-outline.blue-sharp:hover, .btn.btn-outline.blue-sharp:focus{
      color: #fff  !important;
    }
    
    .bootstrap-switch .bootstrap-switch-handle-off.bootstrap-switch-primary, .bootstrap-switch .bootstrap-switch-handle-on.bootstrap-switch-primary {
        color: #fff;
        background: #3B467E !important;
    }
    
    .portlet.box.blue-sharp > .portlet-title {
        background-color: #3B467E !important;
    }
    
    .portlet.box.blue-sharp {
        border: 1px solid #3B467E;
        border-top: 0;
    }
    
    .calendars-ctrl {
        background-color: #3B467E !important;
    }

    .page-header .page-header-top .top-menu .navbar-nav > li.dropdown-dark .dropdown-menu {
        background: #3B467E !important;
        border: 0;
    }
    
    @media screen and (min-width: 768px){
    .modal-dialog {
        left: 0% !important;
        }
        .container{
            padding-left: 40px !important;
        }
    }
    
    .btn-success {
        color: #fff;
        background-color: #3B467E !important;
        border-color: #2bb8c4;
        border-radius: 4px !important;
        left: 43%;
        position: absolute;
        margin-top:5px;
    }
    .modal-footer {
        padding: 0px !important; 
        margin-top:5px;
        margin-bottom: 50px;
        text-align: right;
        border-top: 1px solid #e5e5e5;
    }
    
    .active-menu {
        background: #ddd !important; 
        color: #fff !important;
        transition: all .2s ease 0s, transform 0s ease 0s;
        border-radius: 4px !important;
        font-weight: bold;
       }
    
    body{
        background-image: none !important;
    }
    
    .table.table-light > thead > tr > th {
        color: #3B467E !important;
    }
    
    @media (min-width: 960px){
        .late-menu-item{
           width: 30% !important;
        }
    }
    
    .page-wrapper-row {
        padding-right: 15px;
        padding-left: 15px;
    }
    
    .navbar {
        background: transparent !important;
    }
    
    @media (max-width: 650px){
        .portlet.light>.portlet-title>.caption{
            width: 100%;
        }
        .portlet>.portlet-title>.actions {
            float: none !important;
        }
    }
    
    .navbar {
        border: none !important;
    }
    
    .page-header .page-header-top .top-menu .navbar-nav>li.dropdown-dark .dropdown-menu:after {
        border-bottom-color: #3B467E !important;
    }
    
    .custom-img{
	    width: 100px !important;
	    margin: 4px !important;
	}
	
	.custom-img2{
	    width: 80px !important;
	    margin: 4px !important;
	}
	
	.custom-img3{
	    width: 60px !important;
	    margin: 4px !important;
	}
	
	.title{
		font-size: x-large;
	    background-color: #2c2c8e;
	    color: white;
	    margin: 10px auto;
	    top: 35%;
	    text-align: center;
	    padding: 10px 19px 10px 21px;
	    z-index: 9999;
	    width: max-content;
	    overflow: auto;
	
	}
    

</style>



</head>
<!-- END HEAD -->

<?php $this->load->view('utils/date_scripts');?>


<body class="page-container-bg-solid">
	<div class="page-wrapper-row">
		<div class="page-wrapper-top">
		<div class="head_Container">

            <div class="row">
			<div class=" col-md-2 col-xs-12">
				<a href="https://almanhalschools.edu.sa"> <img
					src="<?=base_url()?>assets/layouts/layout3/img/logod.png" style=""
					class="custom-logo">
				</a>
			</div>
			<div class=" col-md-7 col-xs-12">
				<h1 class="title">شُركــــــــاؤنا</h1>
			</div>
			<div style="padding-bottom: 8px; padding-top: 30px"
				class=" col-md-3 col-xs-12" align="left">
				<a target="_blank" href="https://m.facebook.com/manhalschools/?locale2=ar_AR">
					<img class="button pulse" src="<?=base_url()?>assets/layouts/layout3/img/004-facebook.png" border="0">
				</a> 
				<a href="https://www.youtube.com/channel/UCdyb2lbSDAj_V8xorbDJmQQ" target="_blank">
					<img class="button pulse" src="<?=base_url()?>assets/layouts/layout3/img/002-youtube.png" border="0">
				</a> 
				<a href="https://twitter.com/manhalschools?s=21" target="_blank">
					<img class="button pulse" src="<?=base_url()?>assets/layouts/layout3/img/003-twitter.png" border="0">
				</a> 
				<a href="https://www.instagram.com/mhschools/?igshid=2s66hf7zhd99" target="_blank">
					<img class="button pulse" src="<?=base_url()?>assets/layouts/layout3/img/005-instagram.png" border="0">
				</a> 
				<a href="https://www.snapchat.com/add/mhschools" target="_blank">
					<img class="button pulse" src="<?=base_url()?>assets/layouts/layout3/img/001-snapchat.png" border="0">
				</a>
				<a href="https://wa.me/message/PYBQTZSBQ7NSK1" target="_blank">
					<img class="button pulse" src="<?=base_url()?>assets/layouts/layout3/img/whatsapp.png" border="0">
				</a>
			</div>
		</div>
		<div class="row">
			<div class=" col-md-2 col-xs-12"></div>
			<div class=" col-md-10 col-xs-12">
				<img src="<?=base_url()?>assets/layouts/layout3/img/logos2/Cognia_RGB_Logotype_FullColor.jpg" class="custom-img">
				<img src="<?=base_url()?>assets/layouts/layout3/img/logos2/British Council1-min.jpg" class="custom-img">
				<img src="<?=base_url()?>assets/layouts/layout3/img/logos2/ISO1-min.jpg" class="custom-img2">
				<img src="<?=base_url()?>assets/layouts/layout3/img/logos2/Nwea map1-min.jpg" class="custom-img2">
				<img src="<?=base_url()?>assets/layouts/layout3/img/logos2/talent.jpg" class="custom-img3">
				<img src="<?=base_url()?>assets/layouts/layout3/img/logos2/Classera1-min.jpg" class="custom-img">
				<img src="<?=base_url()?>assets/layouts/layout3/img/logos2/SAT1-min.jpg" class="custom-img">
			</div>
		</div>
                
			<?php if($this->session->userdata('adminid')!='') { ?>
				<div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9" style=" float: left !important;">
                        <!-- BEGIN HEADER -->
                        <div class="page-header" style="margin-top: 0px !important; margin-bottom: 0px !important; padding-top: 0px !important; padding-bottom: 0px !important;">
                        	<!-- BEGIN HEADER TOP -->
                        	<div class="page-header-top" style="height: auto;">
                    			<!-- BEGIN TOP NAVIGATION MENU -->
                    			<div class="top-menu" style="margin-top: 0px !important; margin-bottom: 0px !important; padding-top: 0px !important; padding-bottom: 0px !important;margin-left: 55px !important;">
                    				<ul class="nav navbar-nav pull-left">
                    					<li
                    						class="dropdown dropdown-extended dropdown-notification dropdown-dark"
                    						id="header_notification_bar"><a href="javascript:;"
                    						class="dropdown-toggle" data-toggle="dropdown"
                    						data-hover="dropdown" data-close-others="true" id="bell"> <i
                    							class="icon-bell" onclick="resetCounter()"></i>
                    							                     <?php if ($count_unseen !== 0) {?>
                                                                    <span id="notify_count"
                    							onclick="resetCounter()" class="badge badge-default">
                                                                    	
                                                                    		<?php echo $count_unseen?>
                                                                    </span>
                                                                    <?php }?>
                                                                </a>
                      
                                                                <ul class="dropdown-menu">
                    							<li>
                    								<ul id="notifications" class="dropdown-menu-list scroller"
                    									style="height: 250px;" data-handle-color="#637283">
                                                                        <?php
                    																																													for($i = 0; $i < count ( $notifications_list ); $i ++) {
                    																																												?>
                                                                           <li><a
                    										href="<?=site_url('Admin_panel/view_student_registration/'.$notifications_list[$i]["reg_id"])?>" >
                    											<span class="time"><?php echo $notifications_list[$i]["sent_date_time"]?></span>
                    											<span class="details">
                                                                                        <?= $notifications_list[$i]["message"] ?> </span>
                    									</a></li>
                    									
                                                                            <?php
                    																																													}
                    																																													?>
                                                                            
                                                                        </ul>
                    							</li>
                    						</ul>
                                                               
                                        </li>
                    					<!-- END Notifications Bell -->
                    
                    					<li class="dropdown dropdown-user dropdown-dark"><a
                    						href="javascript:;"> 
                    						
                    							<span class="username"><?=$admin_name?></span>
                    					</a>
                    						</li>
                    					<!-- END USER LOGIN DROPDOWN -->
                    
                    					<li class="droddown dropdown-separator"><span class="separator"></span></li>
                    					<li class=""><a href="<?=site_url('admin/logoff')?>"> 
                    						<img alt="" src="<?=base_url()?>assets/layouts/layout3/img/logout-icon.png"></a>
                    					</li>
                    					<!-- END QUICK SIDEBAR TOGGLER -->
                    				</ul>
                    			<br>	
                    
                    			</div>
                    			<!-- END TOP NAVIGATION MENU -->
                        	</div>
                        </div>
                        <!-- END HEADER -->
                        
					</div>
				</div>	
				
				
				<div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
    					
                        <nav class="navbar navbar-default yamm">
                            <div class="navbar-header">
                                <button id ="menu_mobile_id" type="button" class="navbar-toggle  pull-left" data-toggle="collapse" data-target="#MNavbar">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
    								<span class="icon-bar"></span>
                                </button>
                            </div>
    				      
    						
                            <div id="MNavbar" class="collapse navbar-collapse ">
                                <ul class="nav navbar-nav col-lg-12 col-sm-12 lg-md-12 ">
                                    
    									<?php if($admin_type == 'ADMIN'){?>
        									<li class="dropdown yamm-fw  <?php if (isset($view_dashboard_reports)) {?> active-menu <?php } ?>">
        										<a href="<?php echo site_url('admin_panel/view_dashboard_reports')?>"><?php echo DASHBOARD_REPORTS; ?>
        										</a>
        									</li>
    									<?php } ?>
    									
    									<li class="dropdown yamm-fw  <?php if (isset($view_registrations) && $view_late_regs_lvl == '0') {?> active-menu <?php } ?>">
    										<a href="<?php echo site_url('admin_panel/view_registrations')?>"><?php echo REGISTRATIONS;?> </a>
    									</li>
    									<?php if($admin_type == 'ADMIN'){?>
        									<li class="dropdown yamm-fw  late-menu-item <?php if (isset($view_late_regs_lvl) && $view_late_regs_lvl == '1') {?> active-menu <?php } ?>">
        										<a  href="<?php echo site_url('admin_panel/view_late_registrations/1/30/1/0/0/0/1')?>"><?php echo LATE_REGISTRATIONS." ".LATE_REGISTRATIONS_LEVEL1; ?>
        										</a>
        									</li>
        									<li class="dropdown yamm-fw late-menu-item <?php if (isset($view_late_regs_lvl) && $view_late_regs_lvl == '2') {?> active-menu <?php } ?>">
        										<a  href="<?php echo site_url('admin_panel/view_late_registrations/1/30/1/0/0/0/2')?>"><?php echo LATE_REGISTRATIONS." ".LATE_REGISTRATIONS_LEVEL2;; ?>
        										</a>
        									</li>
        									<li class="dropdown yamm-fw  <?php if (isset($view_agents)) {?> active-menu <?php } ?>">
        										<a href="<?php echo site_url('admin_panel/view_agents')?>"><?php echo AGENTS; ?>
        										</a>
        									</li>
    									<?php } ?>
                                </ul>
                            </div>
    						
                        </nav>
    					
                    </div>
                    
				</div>
                <?php }?>    

						
				</div>		
			</div>
		</div>

	<br>

	<!--[if lt IE 9]>
<script src="<?=base_url()?>assets/global/plugins/respond.min.js"></script>
<script src="<?=base_url()?>assets/global/plugins/excanvas.min.js"></script> 
<script src="<?=base_url()?>assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
	<!-- BEGIN CORE PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>

	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/moment.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/morris/morris.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/morris/raphael-min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/counterup/jquery.waypoints.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/counterup/jquery.counterup.min.js"
		type="text/javascript"></script>

	<script
		src="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/horizontal-timeline/horizontal-timeline.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.resize.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.categories.min.js"
		type="text/javascript"></script>

	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->
</body>
</html>