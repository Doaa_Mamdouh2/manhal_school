<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
	  <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />-->
        <!--<link href="<?=base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />-->
        <!--<link href="<?=base_url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />-->
        <link href="<?=base_url()?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css" />
        <!--<link href="<?=base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css" rel="stylesheet" type="text/css" />-->
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <!--<link href="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />-->
        <!--<link href="<?=base_url()?>assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />-->
        <!--<link href="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />-->
        <!--<link href="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />-->
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?=base_url()?>assets/global/css/components-rtl.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <!--<link href="<?=base_url()?>assets/global/css/plugins-rtl.min.css" rel="stylesheet" type="text/css" />-->
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?=base_url()?>assets/layouts/layout3/css/layout-rtl.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/layouts/layout3/css/themes/default-rtl.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <!--<link href="<?=base_url()?>assets/layouts/layout3/css/custom-rtl.min.css" rel="stylesheet" type="text/css" />-->
        <!-- END THEME LAYOUT STYLES -->
         
		

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<script>
$(document).ready(function(){
    $("#delBtn").click(function(){
        $("#delNotify").show();
    });
    $("#cancel").click(function(){
        $("#delNotify").hide();
    });
    $("#delNotify").hide();

    $('form').on('focus', 'input[type=number]', function(e) {
    	$(this).on('wheel', function(e) {
    		e.preventDefault();
    	});
        });

        $('form').on('blur', 'input[type=number]', function(e) {
        	$(this).off('wheel');
        });

        $('form').on('keydown', 'input[type=number]', function(e) {
    	if ( e.which == 38 || e.which == 40 )
    		e.preventDefault();
        }); 


        $('form').on('focus', 'input[type=number]', function (e) {
    	$(this).on('mousewheel.disableScroll', function (e) {
    		e.preventDefault();
    	});
        });
        $('form').on('blur', 'input[type=number]', function (e) {
        	$(this).off('mousewheel.disableScroll');
        });	 
    	 
});
</script>

<style type="text/css">
.page-header .page-header-menu {
    background: #0E4278;
}

.portlet.box.green>.portlet-title, .portlet.green, .portlet>.portlet-body.green {
    background-color: #0E4278;
}

.page-footer{
	background-color: #0E4278;
}

.page-wrapper .page-wrapper-middle {
	background: none !important;
}

.dropdown-menu{
	 background: #03366A !important;
}

.navbar-nav>li>a:hover {
    background: #0E4278 !important;
}

.menu-dropdown classic-menu-dropdown:hover {
 	background: #0E4278 !important;
}

.page-header .page-header-menu .hor-menu .navbar-nav>li.open>a, .page-header .page-header-menu .hor-menu .navbar-nav>li:hover>a, .page-header .page-header-menu .hor-menu .navbar-nav>li>a:active, .page-header .page-header-menu .hor-menu .navbar-nav>li>a:focus, .page-header .page-header-menu .hor-menu .navbar-nav>li>a:hover {
	background: #0E4278 !important;
	text-shadow: 2px 2px 2px rgba(255,255,255, 0.46);
}  

</style>
</head>
<!-- END HEAD -->


<body class="page-container-bg-solid">
	<div class="page-wrapper-row">
		<div class="page-wrapper-top">
			<!-- BEGIN HEADER -->
			<div class="page-header">
				<!-- BEGIN HEADER TOP -->
				<div class="page-header-top">
					<div class="container">
						<!-- BEGIN LOGO -->
						<div class="page-logo" style="height: auto !important;">
							<a href="http://www.mhschools.com.sa"> <img
								src="<?=base_url()?>assets/layouts/layout3/img/schoollogo.png"
								alt="logo" class="logo-default" style="padding-bottom: 15px;">
							</a>
						</div>
						<div  style="float: left;">
						
						</div>
						<!-- END LOGO -->
						<!-- BEGIN RESPONSIVE MENU TOGGLER -->
						<a href="javascript:;" class="menu-toggler"></a>
						<!-- END RESPONSIVE MENU TOGGLER -->
					</div>
				</div>
				<!-- END HEADER TOP -->
				<!-- BEGIN HEADER MENU -->
				<div class="page-header-menu">
					<div class="container">

						<!-- BEGIN MEGA MENU -->
						<!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
						<!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
						<div class="hor-menu  ">
							<ul class="nav navbar-nav">
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown "><a
									href="http://www.mhschools.com.sa/" > <?php echo MENU_ITEM_1; ?> <span class="arrow"></span>
								</a>
								</li>
								
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown "><a
									href="http://mhschools.com.sa/en/"> <?php echo MENU_ITEM_2; ?> <span class="arrow"></span>
								</a>
								</li>
									
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown "><a
									href="http://mhs.edu.sa/" target="_blank" > <?php echo MENU_ITEM_3; ?> <span class="arrow"></span>
								</a>
									<ul class="dropdown-menu pull-left">
										<li aria-haspopup="true" class=" "><a
											href="http://www.mhschools.com.sa/index.php/التعليم-الالكتروني/الجداول-والاختبارات" class="nav-link  " style="font-size: 12px;"> <?php echo MENU_ITEM_3_1; ?> </a></li>
										<li aria-haspopup="true" class=" "><a
											href="http://www.mhschools.com.sa/index.php/التعليم-الالكتروني/الخطة-الاسبوعية" class="nav-link  " style="font-size: 12px;"> <?php echo MENU_ITEM_3_2; ?> </a></li>
									</ul></li>
									
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown "><a
									href="http://www.mhschools.com.sa/index.php/الصور" > <?php echo MENU_ITEM_4; ?> <span class="arrow"></span>
								</a>
								</li>
								
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown "><a
									href="http://www.mhschools.com.sa/index.php" > <?php echo MENU_ITEM_5; ?> <span class="arrow"></span>
								</a>
									<ul class="dropdown-menu pull-left">
										<li aria-haspopup="true" class=" "><a
											href="http://www.mhschools.com.sa/index.php/الرسوم/فرع-المربع" class="nav-link  " style="font-size: 12px;"> <?php echo MENU_ITEM_5_1; ?> </a></li>
										<li aria-haspopup="true" class=" "><a
											href="http://www.mhschools.com.sa/index.php/الرسوم/فرع-التعاون" class="nav-link  " style="font-size: 12px;"> <?php echo MENU_ITEM_5_2; ?> </a></li>
									</ul></li>
								
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown "><a
									href="http://www.mhschools.com.sa/index.php/الفيديو" > <?php echo MENU_ITEM_6; ?> <span class="arrow"></span>
								</a>
								</li>
								
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown "><a
									href="http://www.mhschools.com.sa/index.php/اتصل-بنا" > <?php echo MENU_ITEM_7; ?> <span class="arrow"></span>
								</a>
									<ul class="dropdown-menu pull-left">
										<li aria-haspopup="true" class=" "><a
											href="http://www.mhschools.com.sa/index.php/اتصل-بنا/ت" class="nav-link  " style="font-size: 12px;"> <?php echo MENU_ITEM_7_1; ?> </a></li>
									</ul></li>
									
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown "><a
									href="http://www.mhschools.com.sa/index.php/التوظيف" > <?php echo MENU_ITEM_8; ?> <span class="arrow"></span>
								</a>
								</li>
								
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown "><a
									href="http://www.mhschools.com.sa/index.php/الفعاليات" > <?php echo MENU_ITEM_9; ?> <span class="arrow"></span>
								</a>
								</li>
								
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown "><a
									href="http://www.mhschools.com.sa/index.php/عن-المدارس" > <?php echo MENU_ITEM_10; ?> <span class="arrow"></span>
								</a>
								</li>
								
									
							</ul>
						</div>
						<!-- END MEGA MENU -->
					</div>
				</div>
				<!-- END HEADER MENU -->
			</div>
			<!-- END HEADER -->
		</div>
	</div>

	<br>
	
	   <!--[if lt IE 9]>
<script src="<?=base_url()?>assets/global/plugins/respond.min.js"></script>
<script src="<?=base_url()?>assets/global/plugins/excanvas.min.js"></script> 
<script src="<?=base_url()?>assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?=base_url()?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <!--<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>-->
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <!--<script src="<?=base_url()?>assets/global/plugins/moment.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>-->

        <!--<script src="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>-->

        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <!--<script src="<?=base_url()?>assets/global/scripts/app.min.js" type="text/javascript"></script>-->
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!--<script src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>-->
        <!-- END THEME LAYOUT SCRIPTS -->
</body>
</html>