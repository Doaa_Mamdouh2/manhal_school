<html>
<head>
<script>
// add the highlighting color if the filter is not selected
function mouseOver (state){
	var element = document.getElementById(state);
	if(!element.classList.contains("done"))
	element.className += " mouseover";
}

// remove the highlighting color if the filter is not selected
function mouseOut (state){
	var element = document.getElementById(state);
	if(!element.classList.contains("done"))
        element.classList.remove("mouseover");
	
}

// while clicking in certain filter add the class "done" (Green) and apply the action
function filter(state) {
   // removr the done class from the current filter
   var elements = document.getElementsByClassName("done");
    //for(var i = 0, length = elements.length; i < length; i++) {
         elements[0].classList.remove("done");
    //}
    // remove the mouseover class and add the "done" class for the selected filter
    var selectedDiv= document.getElementById(state);
    selectedDiv.classList.remove("mouseover");
    selectedDiv.className += " done";

    var view_late_regs_lvl = <?= $view_late_regs_lvl ?>;
    
    // applying action depending on values
	if(state == "display_all"){
		if(view_late_regs_lvl == "0"){
			window.location="<?=site_url('admin_panel/view_registrations').'/1/30/1/0/'.$selected_grade.'/0'?>";
		}else{
			window.location="<?=site_url('admin_panel/view_late_registrations').'/1/30/1/0/'.$selected_grade.'/0/'.$view_late_regs_lvl?>";
		}
	}else{
		if(view_late_regs_lvl == "0"){
			window.location="<?=site_url('admin_panel/view_registrations').'/1/30/1/'?>"+state+"<?php echo '/'.$selected_grade.'/0'?>";
		}else{
			window.location="<?=site_url('admin_panel/view_late_registrations').'/1/30/1/'?>"+state+"<?php echo '/'.$selected_grade.'/0/'.$view_late_regs_lvl?>";
		}
	}
}
</script>

<style type="text/css">
@media only screen and (max-width: 991px) {
	.status-box {
        width: 48%;
        margin: 1%;
        float: right !important;
	}
}
</style>

</head>

<body>
	<div class="portlet ">
	<?php if (isset($student_registrations)){?>
		<div class="portlet-title" style="border-bottom: none !important;">
			<div class="caption"><span
					class="caption-subject font-blue bold uppercase"><?php echo STATE_FILTER;?></span>
			</div>
		</div>
		<?php }?>
		
		<div class="portlet-body">
			<div class="mt-element-step">
				<div class="row step-thin">
					<div id="display_all" onclick="filter('display_all')"
						onmouseover="mouseOver('display_all')"
						onmouseout="mouseOut('display_all')"
						class=" <?php if($view_late_regs_lvl == '0'){?>col-pb-2 <?php }else{ ?> col-pb-6 <?php }?>bg-grey mt-step-col status-box 
						<?php if($selected_state == null || $selected_state == '0'){?>
					done
					<?php } ?>
					">
						<div class="mt-step-title uppercase font-grey-cascade"><?= DIAPLAY_ALL ?></div>
						<div class="mt-step-content font-grey-cascade"> <?= $registration_states_total_count?></div>
					</div>
					<?php
					for($i = 0; $i < count ( $ordered_states ); $i ++) {
						?>
						<div id="<?= $ordered_states[$i]["state_code"] ?>"
						<?php if(isset($student_registrations)){?>
						onclick="filter('<?= $ordered_states[$i]["state_code"] ?>')"
						onmouseover="mouseOver('<?= $ordered_states[$i]["state_code"] ?>')"
						onmouseout="mouseOut('<?= $ordered_states[$i]["state_code"] ?>')"
						<?php }?>
						class=" <?php if($view_late_regs_lvl == '0'){?>
						      <?php  if(mb_strlen(trim($ordered_states[$i]["state_name_ar"]), 'utf8') <= 4 ){?>
									col-pb-1
									<?php } else if (mb_strlen(trim($ordered_states[$i]["state_name_ar"]), 'utf8') <= 6){?>
									col-pb-2
									<?php } else if (mb_strlen(trim($ordered_states[$i]["state_name_ar"]), 'utf8') <= 12){?>
									col-pb-3
									<?php } else if (mb_strlen(trim($ordered_states[$i]["state_name_ar"]), 'utf8') <= 13){?>
									col-pb-4
									<?php } else {?>
									col-pb-5
									<?php }} else {?> col-pb-6 <?php }?>
							 bg-grey mt-step-col status-box 
						<?php if((isset ( $selected_state ) && $ordered_states [$i] ["state_code"] == $selected_state)){?>
							done
							<?php }?>
							">
						<!-- seeting name of each state -->
						<div class="mt-step-title uppercase font-grey-cascade"><?= trim($ordered_states[$i]["state_name_ar"]) ?></div>

						<div class="mt-step-content font-grey-cascade">
					<?php if(isset($student_registrations)){
					           echo $ordered_states [$i] ["count"];
    							} else {echo "";}
										?></div>
					</div>
						<?php }?>
				</div>
			</div>
		</div>
	</div>
</body>
</html>