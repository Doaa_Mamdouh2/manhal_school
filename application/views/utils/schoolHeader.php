<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title><?php echo SCHOOL_SITE_TITLE_NEW; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
	  <meta content="" name="author" />
        
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />-->
        <link href="<?=base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!--<link href="<?=base_url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />-->
        <link href="<?=base_url()?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css" />
        <!--<link href="<?=base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css" rel="stylesheet" type="text/css" />-->
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <!--<link href="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />-->
        <!--<link href="<?=base_url()?>assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />-->
        <!--<link href="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />-->
        <!--<link href="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />-->
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?=base_url()?>assets/global/css/components-rtl.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <!--<link href="<?=base_url()?>assets/global/css/plugins-rtl.min.css" rel="stylesheet" type="text/css" />-->
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?=base_url()?>assets/layouts/layout3/css/layout-rtl.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/layouts/layout3/css/themes/default-rtl.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <!--<link href="<?=base_url()?>assets/layouts/layout3/css/custom-rtl.min.css" rel="stylesheet" type="text/css" />-->
        <!-- END THEME LAYOUT STYLES -->
         
		<link rel="shortcut icon"
	href="<?=base_url()?>assets/layouts/layout3/img/shortcut-icon.png" />

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<script>
$(document).ready(function(){
    $("#delBtn").click(function(){
        $("#delNotify").show();
    });
    $("#cancel").click(function(){
        $("#delNotify").hide();
    });
    $("#delNotify").hide();

    $('form').on('focus', 'input[type=number]', function(e) {
    	$(this).on('wheel', function(e) {
    		e.preventDefault();
    	});
        });

        $('form').on('blur', 'input[type=number]', function(e) {
        	$(this).off('wheel');
        });

        $('form').on('keydown', 'input[type=number]', function(e) {
    	if ( e.which == 38 || e.which == 40 )
    		e.preventDefault();
        }); 


        $('form').on('focus', 'input[type=number]', function (e) {
    	$(this).on('mousewheel.disableScroll', function (e) {
    		e.preventDefault();
    	});
        });
        $('form').on('blur', 'input[type=number]', function (e) {
        	$(this).off('mousewheel.disableScroll');
        });	 
    	 
});
</script>

<style type="text/css">
.page-header .page-header-menu {
    background: #82bdcd !important;
}

.portlet.box.green>.portlet-title, .portlet.green, .portlet>.portlet-body.green {
    background-color: #82bdcd !important;
}

.page-footer{
	background-color: #82bdcd !important;
}

.page-wrapper .page-wrapper-middle {
	background: none !important;
}

.page-header .page-header-menu .hor-menu .navbar-nav>li.open>a, .page-header .page-header-menu .hor-menu .navbar-nav>li:hover>a, .page-header .page-header-menu .hor-menu .navbar-nav>li>a:active, .page-header .page-header-menu .hor-menu .navbar-nav>li>a:focus, .page-header .page-header-menu .hor-menu .navbar-nav>li>a:hover {
	background: #82bdcd !important;
	text-shadow: 2px 2px 2px rgba(255,255,255, 0.46);
}  

.btn.green:not(.btn-outline) {
    color: #FFF;
    background-color: #82bdcd !important;
    border-color: #82bdcd !important;
}

.form-horizontal .control-label {
    text-align: right !important;
}

.form-control{
    font-size: 12px !important;    
}

select.form-control{
    padding-top: 2px !important;
}


</style>

<link rel="stylesheet"  href="<?=base_url()?>css/bootstrap.css">
<link rel="stylesheet"  href="<?=base_url()?>css/bootstrap-rtl.css">
<link rel="stylesheet"  href="<?=base_url()?>css/style.css">
<link rel="stylesheet"  href="<?=base_url()?>css/hover.css"media="all">



</head>
<!-- END HEAD -->


<body class="page-container-bg-solid">
	<div class="">
			<!-- BEGIN HEADER -->
                             
		
            <div class="row">

              

              <div class=" col-md-4 col-xs-12">
                    <a href="https://almanhalschools.edu.sa"> 
					<img src="<?=base_url()?>assets/layouts/layout3/img/logod.png" style="" class="custom-logo">
					 </a></div>
					  <div class=" col-md-2 span-like">
					 		<img src="<?=base_url()?>assets/layouts/layout3/img/logod2.png"></div>
				  <div class=" col-md-3 span-like">	<img src="<?=base_url()?>assets/layouts/layout3/img/logod3.png"></div>

					 					<div style=" padding-bottom:8px;padding-top:30px " class=" col-md-3 col-xs-12" align="left">
					 						<a target="_blank" href="https://m.facebook.com/manhalschools/?locale2=ar_AR"><img class="button pulse" src="<?=base_url()?>assets/layouts/layout3/img/004-facebook.png" border="0"></a>
					 						<a href="https://www.youtube.com/channel/UCdyb2lbSDAj_V8xorbDJmQQ" target="_blank"><img class="button pulse" src="<?=base_url()?>assets/layouts/layout3/img/002-youtube.png" border="0"></a>
					 						<a href="https://twitter.com/manhalschools?s=21" target="_blank"><img class="button pulse" src="<?=base_url()?>assets/layouts/layout3/img/003-twitter.png" border="0"></a>
					 		<a href="https://www.instagram.com/mhschools/?igshid=2s66hf7zhd99" target="_blank"><img class="button pulse" src="<?=base_url()?>assets/layouts/layout3/img/005-instagram.png" border="0"></a>
					 <a href="https://www.snapchat.com/add/mhschools" target="_blank"><img class="button pulse" src="<?=base_url()?>assets/layouts/layout3/img/001-snapchat.png" border="0"></a>

					

					 						</div>
					

                </div>

          <div class="row">
                <div class="col-lg-12  col-md-12 col-sm-12">
			
                    <nav class="navbar navbar-default yamm">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle  pull-right" data-toggle="collapse" data-target="#MNavbar">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
								<span class="icon-bar"></span>
                            </button>
                        </div>
				      
						
                        <div id="MNavbar" class="collapse navbar-collapse ">
                            <ul class="nav navbar-nav col-lg-12 col-sm-12 lg-md-12 ">
                               
                               <li class="dropdown yamm-fw ">
                                   <a href="#" ><span style="font-size:16px;font-family:Tahoma; color:#fff !important;"></span> </a>
                                </li>
                                
                                 <li class="dropdown yamm-fw ">
                                   <a  href="https://almanhalschools.edu.sa"  ><i style="font-size:24px" class="fa fa-home"></i> </a>
                                    
                                </li>

                                
                                
                                <li class="dropdown yamm-fw ">
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle ">عن مدارسنا <b class="caret"></b></a>
                                    <ul class="dropdown-menu yamm-content">
                                        <div class="row ">
                                            <ul class="col-lg-6 col-sm-6 col-xs-6 list-unstyled ">
                                                <li class="yamm-fw"><a href="http://mhs.edu.sa/portal2/ContentsDescDrYTnifrew1.asp?DrYTnifrew1.aspx=20">عن مدارسنا</a></li>
                                                <li class="yamm-fw"><a href="http://mhs.edu.sa/portal2/rsa.asp">الرؤية والرسالة</a></li>
                                                 <li class="yamm-fw"><a href="http://mhs.edu.sa/portal2/ContentsDescDrYTnifrew1.asp?DrYTnifrew1.aspx=22">الوصول للمدارس</a></li>

                                            </ul>
                                            <ul class="col-lg-6  col-sm-6 col-xs-6 list-unstyled">
												  <li class="yamm-fw"><a href="http://mhs.edu.sa/portal2/ContentsDescDrYTnifrew1.asp?DrYTnifrew1.aspx=23">  الهيكل الاداري</a></li>
												   <li class="yamm-fw"><a href="http://mhs.edu.sa/portal2/ContentsDescDrYTnifrew1.asp?DrYTnifrew1.aspx=39">الرسوم  </a></li>
                                                <li class="yamm-fw"><a href="http://mhs.edu.sa/portal2/ContentsDescDrYTnifrew1.asp?DrYTnifrew1.aspx=25">دليل الهواتف</a></li>
					

                                            </ul>
                                        </div>
                                    </ul>
                                </li>
                            
                                
                                <li class="dropdown yamm-fw">
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">أخبار المراحل <b class="caret"></b></a>
                                    <ul class="dropdown-menu yamm-content">
                                        <div class="row">
                                            <ul class="col-lg-6 col-sm-6 col-xs-6 list-unstyled ">
                                                <li class="yamm-fw center"><a href="#">بنين</a></li>
                                                <div class="row">
                                                    <ul class="col-lg-6 col-sm-6   list-unstyled ">
                                                       
                                                        <li class="yamm-fw center"><a href="http://mhs.edu.sa/portal2/AllNews.asp?LevID=14">المرحلة الثانوية</a></li>
                                                        <li class="yamm-fw center"><a href="http://mhs.edu.sa/portal2/AllNews.asp?LevID=13">المرحلة المتوسطة</a></li>
                                                        <li class="yamm-fw center"><a href="http://mhs.edu.sa/portal2/AllNews.asp?LevID=12">المرحلة العليا</a></li>
                                                             <li class="yamm-fw center"><a href="http://mhs.edu.sa/portal2/AllNews.asp?LevID=11">المرحلة الأولية</a></li>


                                                    </ul>
                                                </div>

                                            </ul>
                                            <ul class="col-lg-6 col-sm-6  list-unstyled ">
                                                <li class="yamm-fw center"><a href="#">بنات</a></li>
                                                <div class="row">
                                                    <ul class="col-lg-6 col-sm-6 list-unstyled ">
                                                      
                                                                                                            
                                                        <li class="yamm-fw center"><a href="http://mhs.edu.sa/portal2/AllNews.asp?LevID=24">المرحلة الثانوية</a></li>
                                                        <li class="yamm-fw center"><a href="http://mhs.edu.sa/portal2/AllNews.asp?LevID=23">المرحلة المتوسطة</a></li>
                                                        <li class="yamm-fw center"><a href="http://mhs.edu.sa/portal2/AllNews.asp?LevID=22">المرحلة العليا</a></li>
                                                             <li class="yamm-fw center"><a href="http://mhs.edu.sa/portal2/AllNews.asp?LevID=21">المرحلة الأولية</a></li>
                                                             <li class="yamm-fw center"><a href="http://mhs.edu.sa/portal2/AllNews.asp?LevID=28">رياض الأطفال</a></li>

                                                    </ul>

                                                    
                                                </div>
                                            </ul>
                                        </div>
                                    </ul>
                                </li>
<li class="dropdown yamm-fw ">
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle ">القبول والتسجيل <b class="caret"></b></a>
                                    <ul class="dropdown-menu yamm-content">
                                        <div class="row ">
                                            <ul class="col-lg-6 col-sm-6 col-xs-6 list-unstyled ">
                                                <li class="yamm-fw"><a href="http://mhs.edu.sa/portal2/ContentsDescDrYTnifrew1.asp?DrYTnifrew1.aspx=41">شروط القبول والتسجيل</a></li>
                                                 <li class="yamm-fw"><a href="http://mhs.edu.sa/portal2/mreg.asp">التسجيل الإلكتروني</a></li>
                                                                       <li class="yamm-fw"><a href="http://mhs.edu.sa/portal2/C-Reg2.asp">متابعة التسجيل</a></li>
                                              

                                            </ul>
                                        </div>
                                    </ul>
                                </li>
                              
                                
                       
                                                
                               
<li class="dropdown yamm-fw ">
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle ">خدمات متنوعة <b class="caret"></b></a>
                                    <ul class="dropdown-menu yamm-content">
                                        <div class="row ">
                                            <ul class="col-lg-6 col-sm-6 col-xs-6 list-unstyled ">
                                                  <li class="yamm-fw"><a href="http://mhs.edu.sa/portal2/AllNews.asp?LevID=6">المقالات التربويه

</a></li>                                                   <li class="yamm-fw"><a href="http://mhs.edu.sa/portal2/Visitors.asp"> سجل الزوار
</a></li>
  <li class="yamm-fw"><a href="http://mhs.edu.sa/portal2/sites.asp"> مواقع ذات صلة 
</a></li>              
                                            </ul>
                                            
                                            
                                            <ul class="col-lg-6  col-sm-6 col-xs-6 list-unstyled">
                                              
                                                   <li class="yamm-fw"><a href="http://mhs.edu.sa/portal2/AllNews.asp">ارشيف الاخبار
</a></li>
                                                <li class="yamm-fw"><a href="http://mhs.edu.sa/portal2/EmploymentFormStep1.asp">التوظيف
</a></li>
                                                <li class="yamm-fw"><a href="http://mhs.edu.sa/portal2/Employment11.asp">متابعة طلب التوظيف
</a></li>


                                                 
                                            </ul>

                                            
                                           
                                        </div>
                                    </ul>
                                </li>
                           
                            </ul>
                        </div>
						
                    </nav>
				
                </div>
            </div>

          

			<!-- END HEADER -->
	</div>

	<br>
	
	   <!--[if lt IE 9]>
<script src="<?=base_url()?>assets/global/plugins/respond.min.js"></script>
<script src="<?=base_url()?>assets/global/plugins/excanvas.min.js"></script> 
<script src="<?=base_url()?>assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?=base_url()?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <!--<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>-->
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <!--<script src="<?=base_url()?>assets/global/plugins/moment.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>-->

        <!--<script src="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>-->

        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <!--<script src="<?=base_url()?>assets/global/scripts/app.min.js" type="text/javascript"></script>-->
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!--<script src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>-->
        <!--<script src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>-->
        <!-- END THEME LAYOUT SCRIPTS -->
</body>
</html>