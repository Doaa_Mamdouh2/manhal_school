<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>

<script src="<?php echo base_url()?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
    $("#delBtn").click(function(){
        $("#delNotify").removeClass('hidden');
		document.getElementById('delNotify').style.display = 'block';
		document.getElementById('delNotify').style.visibility = 'visible';
    });
    $("#cancel").click(function(){
        $("#delNotify").addClass('hidden');
    });
});

function delBtnClick(code, type) {
        document.getElementById('code_to_delete').value = code;
        document.getElementById('type_to_delete').value = type;

        $("#delNotify").removeClass('hidden');
		document.getElementById('delNotify').style.display = 'block';
		document.getElementById('delNotify').style.visibility = 'visible';
		$('#loading').addClass('hidden');
		document.body.scrollTop = 0;
		document.documentElement.scrollTop = 0;
}

function applyBusiness(){
	var code_value = document.getElementById('code_to_delete').value;
    var type_value = document.getElementById('type_to_delete').value;
	if (type_value == "agents" ) {
	  window.location =  "<?php echo site_url();?>Admin_panel/delete_agent/"+code_value;
	} else if (type_value == "registrations" ) {
	  window.location =  "<?php echo site_url();?>Admin_panel/delete_registration/"+code_value;
	}
 
}
</script>

</head>

<!-- END HEAD -->

<body>
	<div id="delNotify" class="alert alert-block alert-info fade in hidden">
		<h4 class="alert-heading"><?php echo INFO; ?></h4>
		<p><?php echo WANT_DELETE_RECORD; ?></p>
		<p>
			<input type="hidden" id="code_to_delete" value=""> <input
				type="hidden" id="type_to_delete" value=""> <a class="btn blue"
				href="javascript:applyBusiness();"> <?php echo OK; ?> </a> <a
				id="cancel" class="btn dark" href="javascript:;"> <?php echo CANCEL; ?></a>
		</p>
	</div>
</body>
</html>