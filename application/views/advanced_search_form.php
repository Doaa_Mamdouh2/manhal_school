<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<?php
// session_start ();
$this->load->view('localization/lang');
$this->load->view('localization/txt');
defineLocale();
defineStrings();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title><?php echo SCHOOL_SITE_TITLE_NEW. ' | '. ADVANCED_SEARCH; ?></title>
        <link rel="icon" href="<?php echo base_url()?>images/header_logo.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        
        <meta content="" name="author" />
        <link rel="stylesheet"
        	href="<?=base_url()?>assets/css/bootstrap.min.css">
        <link rel="shortcut icon"
        	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.png" />
        <link
        	href="<?=base_url()?>assets/global/plugins/bootstrap-table/bootstrap-table.min.css"
        	rel="stylesheet" type="text/css" />
        	
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        
        <script type="text/javascript">

            function miladyHandler() {
                var DOB_1 = $('#DOB_1').val();
                if (DOB_1 != "") {
                    var isValidD = isValidMiladyDate(DOB_1);
                    if (!isValidD) {
                        return false;
                    }
                }

                var str_array = DOB_1.split('/');
                var miladyDate = jqOld.calendars.instance('gregorian', 'ar').newDate();
                var hijriDate = jqOld.calendars.instance('ummalqura', 'ar').newDate();
                try {
                    miladyDate = miladyDate.date(str_array[2], str_array[0], str_array[1]);
                    hijriDate = hijriDate.fromJSDate(miladyDate.toJSDate());
                    //alert(hijriDate);
                    $("#DOB_2").val(hijriDate.formatDate());
                } catch (err) {
                    return false;
                }
            }

            function hijriHandler() {
                var DOB_2 = $('#DOB_2').val();
                if (DOB_2 != "") {
                    var isValidD = isValidDate(DOB_2);
                    if (!isValidD) {
                        return false;
                    }
                }

                try {
                    $.ajax({
                        type: "post",
                        url: '<?php echo site_url('Registration_controller/convertToMilady'); ?>',
                        cache: false,
                        data: {
                            'hijriDate': DOB_2
                        },
                        success: function (json) {
                            try {
                                var miladyDate = jQuery.parseJSON(json);
                                //reformat from 2017-05-10 to 05/16/2017
                                if (miladyDate != '') {
                                    var str_array = miladyDate.split('-');
                                    var year = str_array[0];
                                    var month = str_array[1];
                                    var day = str_array[2];
                                    miladyDate = month + "/" + day + "/" + year;
                                }
                                $("#DOB_1").val(miladyDate);
                                //alert( 'Success');
                            } catch (e) {
                                //alert('Exception while request..');
                            }
                        },
                        error: function () {
                            //alert('Error while request..');
                        }
                    });
                } catch (err) {
                    return false;
                }
            }

            function isValidDate(dateVar) {
                //Format: yyyy/mm/dd
                var dateReg = /^\d{4}\/\d{2}\/\d{2}$/;
                var isV = dateReg.test(dateVar);
                if (!isV) {
                    return false;
                }

                var str_array = dateVar.split('/');
                var hijriDate = jqOld.calendars.instance('ummalqura', 'ar').newDate();
                try {
                    hijriDate = hijriDate.date(str_array[0], str_array[1], str_array[2]);
                } catch (err) {
                    return false;
                }
                return true;
            }

            function isValidMiladyDate(dateVar) {
                //Format: yyyy/mm/dd
                var dateReg = /^\d{2}\/\d{2}\/\d{4}$/;
                var isV = dateReg.test(dateVar);
                if (!isV) {
                    return false;
                }

                var str_array = dateVar.split('/');
                var miladyDate = jqOld.calendars.instance('gregorian', 'ar').newDate();
                try {
                    miladyDate = miladyDate.date(str_array[2], str_array[0], str_array[1]);
                } catch (err) {
                    return false;
                }
                return true;
            }
            
       </script>    

    </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid">
        <div class="container shadow">
            <?php
            $this->load->view('utils/schoolHeaderAdmin');
            ?>

            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">

                            <!-- BEGIN PAGE CONTENT BODY -->
                            <div class="page-content">
                                <div class="">
                                    <div class="col-md-14">
                                        <div class="portlet box blue-sharp">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>
                                                    <?php
                                                    echo ADVANCED_SEARCH;
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <br> <br>
                                                <!-- BEGIN FORM-->
                                                <form id="myForm" role="form" method="post"
                                                      enctype="multipart/form-data"
                                                      action="<?= site_url('admin_panel/advanced_search/1/30/1/0/0/'.$view_late_regs_lvl) ?>"
                                                      class="form-horizontal">
                                                    <div class="form-body">

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo PARENT_NAME; ?></label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control "
                                                                               name="responsible_name" id="responsible_name">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo RESPONSIBLE_MOBILE; ?></label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control "
                                                                               name="responsible_mobile" id="responsible_mobile">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo RESPONSIBLE_EMAIL; ?></label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control "
                                                                               name="responsible_email" id="responsible_email">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                            
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo STUDENT_NAME; ?></label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control "
                                                                               name="student_name" id="student_name">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo STATE; ?> </label>
                                                                    <div class="col-md-6">
                                                                        <select class="form-control "
                                                                                name="current_state_code" id="current_state_code">
                                                                                <option value="" selected></option>
                                                                                    <?php
                                                                                    if ($available_states) {
                                                                                        for ($i = 0; $i < count($available_states); $i ++) {
                                                                                            $state_code = $available_states [$i] ["state_code"];
                                                                                            $state_name = $available_states [$i] ["state_name_ar"];
                                                                                            ?>
                                                                                    <option value="<?= $state_code ?>">
                                                                                        <?= $state_name ?></option>
                                                                                    <?php
                                                                                }
                                                                            } 
                                                                                ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo IS_CONTACTED; ?> </label>
                                                                    <div class="col-md-6">
                                                                        <table style="width: 100%">
                                                                            <tr>
                                                                                <td><input type="radio" name="is_contacted"
                                                                                           id="is_contacted" value="Y" ></td>
                                                                                <td><?php echo YES; ?></td>
                                                                                <td><input type="radio" name="is_contacted"
                                                                                           id="is_contacted" value="N" ></td>
                                                                                <td><?php echo NO; ?></td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo CREATION_DATE; ?></label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" placeholder="" id="creation_date"
                                                                               class="form-control dateMilady" name="creation_date">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--  
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo STUDENT_DOB_MILADY; ?></label>
                                                                    <div class="col-md-6">
																		<input type="text" placeholder="" id="DOB_1"
																			class="form-control dateMilady" name="DOB_1"
																			onchange="miladyHandler();"
																			onkeypress="this.onchange();"
																			onpropertychange="this.onchange();"
																			onpaste="this.onchange();" oninput="this.onchange();">
																		<span id="DOB_1_invalid_tooltip" class="tooltiptext"
																			style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
																	</div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo STUDENT_DOB_HIJRI; ?></label>
                                                                    <div class="col-md-6">
																		<input type="text" placeholder="" id="DOB_2"
																			class="form-control date" name="DOB_2"
																			onchange="hijriHandler()"
																			onkeypress="this.onchange();"
																			onpropertychange="this.onchange();"
																			onpaste="this.onchange();" oninput="this.onchange();">
																		<span id="DOB_2_invalid_tooltip" class="tooltiptext"
																			style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
																	</div>
                                                                </div>
                                                            </div>
                                                        </div>
														
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo STUDENT_IBAN; ?></label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control " name="IBAN"
                                                                               id="IBAN">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo STUDENT_IBAN_SRC; ?></label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control "
                                                                               name="IBAN_source" id="IBAN_source">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        -->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo DEPARTMENT; ?></label>
                                                                    <div class="col-md-6">
                                                                        <select class="form-control " name="department"
                                                                                id="department">
                                                                            <option value="" selected></option>
                                                                            <option value="BOYS"><?php echo BOYS; ?></option>
                                                                            <option value="GIRLS"><?php echo GIRLS; ?></option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo TRACK; ?></label>
                                                                    <div class="col-md-6">
                                                                        <select class="form-control " name="track"
                                                                                id="track">
                                                                            <option value="" selected></option>
                                                                            <option value="PUB"><?php echo PUBL; ?></option>
                                                                            <option value="INT"><?php echo INTERNATIONAL; ?></option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo STUDENT_GRADE; ?></label>
                                                                    <div class="col-md-6">
                                                                        <select class="form-control " name="grade"
																			id="grade">
																			<option value=""></option>
																			<?php
																				if ($educational_grade) {
																					for($i = 0; $i < count ( $educational_grade); $i ++) {
																						$educational_grade_id = $educational_grade[$i] ["id"];
																						$educational_grade_desc_ar = $educational_grade[$i] ["desc_ar"];
																						?>
																						<option value="<?php echo $educational_grade_id?>"> 
																							<?php echo $educational_grade_desc_ar?>
																						</option>
																						<?php
																					}
																				} 
																			?>
																		</select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo OLD_SCHOOL; ?></label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control "
                                                                               name="old_school" id="old_school" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                       <div class="row">
                                                           <div class="col-md-6">
																<div class="form-group">
																	<label class="col-md-4 control-label"><?php echo NOTIFICATION_DATE; ?> </label>
																	<div class="col-md-6">
																			<input type="text" class="form-control dateMilady"
																				name="notification_time" id="notification_time_gregorian"> 
																	</div>
																</div>
															</div>
                                                        	<?php if($admin_type != "AGENT"){ ?>
                                                        	<div class="col-md-6">
																<div class="form-group">
																	<label class="col-md-4 control-label"><?php echo CUSTOMER_SERVICE; ?></label>
																	<div class="col-md-6">
																		<select class="form-control " name="agent_id"
																			id="agent_id">
																			<option value=""></option>
																			<?php
																				if ($available_agents) {
																					for($i = 0; $i < count ( $available_agents); $i ++) {
																						$available_agent_id = $available_agents[$i] ["admin_id"];
																						$available_agent_name = $available_agents[$i] ["name"];
																						?>
																						<option value="<?php echo $available_agent_id?>">
																							<?php echo $available_agent_name?>
																						</option>
																						<?php
																					}
																				} 
																			?>
																		</select>
																	</div>
																</div>
															</div>
                                                        	<?php } ?>
    													</div>
    													<div class="row">
                                                           <div class="col-md-6">
																<div class="form-group">
																	<label class="col-md-4 control-label"><?php echo REG_DATE_FROM; ?> </label>
																	<div class="col-md-6">
																			<input type="text" class="form-control dateMilady"
																				name="reg_date_from" id="reg_date_from"> 
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label class="col-md-4 control-label"><?php echo REG_DATE_TO; ?> </label>
																	<div class="col-md-6">
																			<input type="text" class="form-control dateMilady"
																				name="reg_date_to" id="reg_date_to"> 
																	</div>
																</div>
															</div>
														</div>
    													<div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo KNOWING_WAY; ?></label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control " name="knowing_way"
                                                                               id="knowing_way">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
    													
                                                        <div class="row">
                                                        	<div class="form-group">
																<label class="col-md-6 control-label label-edit" style="max-width: 14%; margin-right: 1%;"><?php echo COMMENTS; ?> </label>
																<div class="col-md-6 text-edit comment-control" >
																	<textarea class="form-control boxsizingBorder" rows="4" cols="50"
																		id="comment" name="comment" ></textarea>
																</div>
															</div>
                                                        </div>


                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-9" align="center"
                                                                     style="margin: auto; width: 100%;">
                                                                    <button type="submit" class="btn btn-circle blue-sharp"><?php
                                                                        echo SEARCH;
                                                                        ?></button>

                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </form>
                                                <!-- END FORM-->

                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php $this->load->view('utils/schoolFooter'); ?>

<!-- BEGIN CORE PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-table/bootstrap-table.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/profile.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>

	<!-- END THEME LAYOUT SCRIPTS -->
	

<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135637603-1"></script>

<script>

  window.dataLayer = window.dataLayer || [];

  function gtag(){dataLayer.push(arguments);}

  gtag('js', new Date());

 

  gtag('config', 'UA-135637603-1');

</script>

    </body>

</html>