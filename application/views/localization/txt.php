<?php
function defineStrings($currLang = "ar") {
	switch($currLang) {
		case "en":
			define("SIGN_IN","Sign In");
			define("INSERT_USERNAME_PASSWORD","برجاء ادخال اسم المستخدم وكلمة المرور");
			define("INVALID_USERNAME_PASSWORD","اسم المستخدم او كلمة المرور غير صحيحة");
			define("FILL_THIS_FIELD","Fill This Field");
			define("REG_CODE","رقم الطلب");
			define("RESPONSIBLE_NAME","Parent/guardian Name");
			define("RESPONSIBLE_RELATIONSHIP","Parent/guardian Relationship");
			define("RESPONSIBLE_MOBILE","Mobile");
			define("RESPONSIBLE_EMAIL","Email");
			define("INVALID_EMAIL","Invalid Email");
			define("STUDENT_NAME","Student Full Name");
			define("STUDENT_GENDER","Gender");
			define("STUDENT_GENDER_VALUE_1","M");
			define("STUDENT_GENDER_VALUE_2","F");
			define("MALE","Male");
			define("FEMALE","Female");
			define("STUDENT_NATIONALITY","Nationality");
			define("STUDENT_DOB_MILADY","Birth Date Milady");
			define("STUDENT_DOB_HIJRI","Birth Date Hijri");
			define("STUDENT_IBAN","Student ID Number");
			define("STUDENT_IBAN_SRC","Student ID Number Source");
			define("SCHOOL","School");
			define("STUDENT_GRADE","Educational Grade");
			define("CUSTOMER_SERVICE","Customer Service");
			define("OLD_SCHOOL","Previous School");
			define("SCHOOL_VALUE_1","مدارس المنهل فرع التعاون");
			define("SCHOOL_VALUE_2","مدراس المنهل فرع المربع");
			define("STUDENT_GRADE_VALUE_1","حضانة");
			define("STUDENT_GRADE_VALUE_2","رياض اطفال");
			define("STUDENT_GRADE_VALUE_3","دبلوما أمريكية");
			define("STUDENT_GRADE_VALUE_4","المرحلة الإبتدائية");
			define("STUDENT_GRADE_VALUE_5","المرحلة المتوسطة");
			define("STUDENT_GRADE_VALUE_6","المرحلة الثانوية");
			define("STEP3_LINE1_M","Thank you for submitting your application to enroll your child in AlManhal schools,");
			define("STEP3_LINE1_F","Thank you for submitting your application to enroll your child in AlManhal schools,");
			define("STEP3_LINE2","We always strive to promote noble values and morals through different educational programs.");
			define("STEP3_LINE3"," The school staff will contact you to complete the registration process.");
			define("STEP3_LINE4","We reiterate our thanks and appreciation.");
			define("SAVE","Save");
			define("INVALID_DATE_VALUE_FORMAT","Invalid date format, should use the format yyyy/mm/dd");
			define("SCHOOL_SITE_TITLE_NEW","AlManhal Schools");
			define("REG_PAGE_TITLE","Student Registration Form");
			define("STEP_1_TITLE","Parent Information");
			define("STEP_2_TITLE","Student Information");
			define("STEP_3_TITLE","Registration successfully completed");
			
			define("PLEASE_EVALUATE_SERVICE","Please evaluate the service provided to you");
			define("SERVICE_LEVEL","Service Level");
			define("SEND","Send");
			define("EVALUATION_SENT","تم إرسال التقييم بنجاح شكرا لكم");
			define("MENU_ITEM_1", "الرئيسية");
			define("MENU_ITEM_2", "المسار الدولي");
			define("MENU_ITEM_3", "التعليم الالكتروني");
			define("MENU_ITEM_3_1", "الجداول والاختبارات");
			define("MENU_ITEM_3_2", "الخطة الاسبوعية");
			define("MENU_ITEM_4", "الصور");
			define("MENU_ITEM_5", "الرسوم");
			define("MENU_ITEM_5_1", "فرع المربع");
			define("MENU_ITEM_5_2", "فرع التعاون");
			define("MENU_ITEM_6", "الفيديو");
			define("MENU_ITEM_7", "اتصل بنا");
			define("MENU_ITEM_7_1", "الاتصال بمديرالموقع");
			define("MENU_ITEM_8", "التوظيف");
			define("MENU_ITEM_9", "الفعاليات");
			define("MENU_ITEM_10", "عن المدارس");
			define("NEXT", "Next");
			define("MAIL_SENT","تم ارسال ايميل للتاكيد");
			define("MAIL_NOT_SENT","Problem in sending mail");
			define("USERNAME","Username");
			define("PASSWORD","Password");
			define("LOGIN","LOGIN");
			define("STUDENT_REGISTRATIONS","Student Registrations");
			define("CREATION_DATE","Registration Date");
			define("MORE","Display More>>");
			define("NO_RECORDS_FOUND","No records found.");
			define("BACK","Back");
			define("REG_STUDENT_FORM","Registration Data");
			define("IS_CONTACTED","تم التواصل");
			define("YES","نعم");
			define("NO","لا");
			define("NUMBER_OF_REGS","عدد التسجيلات الحالية: ");
            define("ADVANCED_SEARCH", "ADVANCED SEARCH");
            define("CANCEL_SEARCH", "Cancel SEARCH");
            define("SEARCH", "SEARCH");
			define("COMMENTS","Comments");
			define("ADDITIONAL_DATA","Additional data");
			define("ADD_NOTIFICATION","Add Notification");
			define("NOTIFICATION_DATE","Notification Date");
            define("WRITE_COMMENT","Write Comment ....");
            define("POST","Post");
            define("STATE","State");
            define("SAVED_STATE","Saved State");
            define("PREVIOUS","Previous");
			define("PAGE_NUM","Page");
            define("NEXT", "next" );
			define("STUDENT_OR_PARENT_NAME","اسم الطالب أو ولي الأمر");
			define("PARENT_NAME","اسم ولي الأمر");
			define("MY_PROFILE","ملفي الشخصي");
			define("LOGOUT","خروج");
			define("STATE_FILTER", "التقسيم حسب حالة الطلب" );
			define("GRADE_FILTER", "التقسيم حسب المرحلة الدراسية" );
			define("DIAPLAY_ALL","عرض الكل");
			define("EDIT","تعديل");
			define("DELETE","مسح");
			define("QUICK_SEARCH","ابحث");
			define("SEND_NOTIF_TO_ADMIN","ارسال إلى مدير النظام");
			define("SEND_EVAL_TO_CLIENT","طلب تقييم الخدمة");
			define("SENT_ADMIN_MAIL_NOTIF_DONE","تم ارسال التنبيه للمدير بنجاح");
			define("SENT_EVAL_TO_CLIENT","تم ارسال طلب التقييم  بنجاح");
			define("CLOSE","غلق");
			define("HIDE","hide");
			define("SENDING_MAIL","يتم الارسال.....");
			define("PLEASE_REVIEW_THIS_REGISTRATION_REQUEST","برجاء مراجعة طلب التقديم");
			define("DATE_CAN_NOT_BE_LESS_THAN_TODAY","التاريخ لا يجب ان يكون أقل من تاريخ اليوم");
			define("INVALID_DATE_VALUE_FORMAT_MILADY", "قيمة التاريخ خاطئة. يجب إدخال قيمه صحيحة  و فى الصيغة: mm/dd/yyy");
			define("DASHBOARD_REPORTS","التقارير");
			define("DASHBOARD_REPORTS_SMALL_TITLE","الإحصاءات، والرسوم البيانية، والأحداث والتقارير الأخيرة");
			define("PROGRESS","تقدم");
			define("REPORT2_TITLE","عدد الطلبات");
			define("REPORT2_SMALL_TITLE","لكل يوم ولمدة 30 يوم");
			define("CREATED_REGISTRATIONS_COUNT","إجمالي عدد الطلبات المضافة");
			define("REGISTRATION","طلب");
			define("IN","في");
			define("REPORT3_TITLE","عدد الطلبات");
			define("TOTAL_NEW_REGISTRATIONS", "إجمالى الطلبات الجديدة");
			define("TOTAL_HAVE_REGISTRATIONS", "إجمالى الطلبات المنجزة");
			define("REPORT3_AGENT", "ممثل الخدمة");
			define("REPORT3_NEW", "طلبات جديدة");
			define("REPORT3_UNDER_PROGRESS", "طلبات تحت الدراسة");
			define("REPORT3_FINISHED", "طلبات انتهت");
			define("REPORT3_RATE", "نسبة الانجاز");
		    define("REPORT4_TITLE", "أداء ممثلى الخدمة");
		    define("TOTAL_REGISTRATIONS_COUNT","عدد الطلبات الإجمالى");
		    define("DELAYED_REGISTRATIONS_COUNT","عدد الطلبات المتأخرة");
		    define("DELAYED_RATE", "نسبة التأخير");
		    define("DASHBOARD_REPORTS_LOADING","جاري تجهيز التقرير");
		    define("MOBILE_NUMBER_INVALID","The maximum must be 15 numbers");
		    define("DASHBOARD_ACTIVITIES","متابعة الأداء");
		    define("LATE_REGISTRATIONS","الطلبات المتأخرة");
		    define("LATE_REGISTRATIONS_LEVEL1","(مستوى التأخير الأول)");
		    define("LATE_REGISTRATIONS_LEVEL2","(مستوى التأخير الثاني)");
		    define("CLIENT_EVAL","Client Evaluation");
		    define("REGISTRATIONS","Registrations");
		    define("KNOWING_WAY","Knowing Way");
		    define("AGENTS","Agents");
		    define("AGENT_NAME","Agent name");
		    define("MOBILE_NUMBER","Mobile number");
		    define("EMAIL","Email");
		    define("INFO", "Alert" );
		    define("WANT_DELETE_RECORD", "Are you sure you want to delete this record?" );
		    define("OK", "Ok" );
		    define("CANCEL", "Cancel" );
		    define("INSERT", "New" );
		    define("ADD_RECORD", "Add Agent" );
		    define("INCORRECT_REPEATED_PASSWORD", "Incorrect retyped password" );
		    define("REG_DATE_FROM", "Reg. date from " );
		    define("REG_DATE_TO", "Reg. date to " );
		    define("AGENT_GRADES", "المراحل الدراسية المسئول عنها" );
		    define("MOVE_REGS", "نقل طلبات التسجيل إلى" );
		    define("BRANCH", "الفرع" );
		    define("TRACK", "المسار" );
		    define("INTERNATIONAL", "الدولى" );
		    define("PUBL", "العام" );
		    define("DEPARTMENT","القسم");
		    define("BOYS","بنين");
		    define("GIRLS","بنات");
		    
			break;
		case "ar":
			define("SIGN_IN","تسجيل الدخول");
			define("USERNAME","اسم المستخدم");
			define("PASSWORD","كلمة السر");
			define("REPEAT_PASSWORD","اعادة كلمة السر");
			define("LOGIN","دخول");
			define("INSERT_USERNAME_PASSWORD","برجاء ادخال اسم المستخدم وكلمة المرور");
			define("INVALID_USERNAME_PASSWORD","اسم المستخدم او كلمة المرور غير صحيحة");
			define("FILL_THIS_FIELD","هذه الخانة مطلوبة");
			define("REG_CODE","رقم الطلب");
			define("RESPONSIBLE_NAME","اسم ولي الأمر");
			define("RESPONSIBLE_RELATIONSHIP","صلة القرابة");
			define("RESPONSIBLE_MOBILE","رقم الجوال");
			define("RESPONSIBLE_EMAIL","البريد الإلكتروني");
			define("INVALID_EMAIL","يجب إضافة البريد الألكتروني بطريقة صحيحة");
			define("STUDENT_NAME","اسم الطالب الرباعي");
			define("STUDENT_GENDER","الجنس");
			define("STUDENT_GENDER_VALUE_1","M");
			define("STUDENT_GENDER_VALUE_2","F");
			define("MALE","ذكر");
			define("FEMALE","انثى");
			define("STUDENT_NATIONALITY","الجنسية");
			define("STUDENT_DOB_MILADY","تاريخ الميلاد ميلادي");
			define("STUDENT_DOB_HIJRI","تاريخ الميلاد هجري");
			define("STUDENT_IBAN","رقم الهوية");
			define("STUDENT_IBAN_SRC","مصدرها");
			define("SCHOOL","المدرسة المراد الالتحاق بها");
			define("STUDENT_GRADE","المرحلة الدراسية");
			define("CUSTOMER_SERVICE","ممثل الخدمة");
			define("OLD_SCHOOL","المدرسة السابقة");
			define("SCHOOL_VALUE_1","مدارس المنهل فرع التعاون");
			define("SCHOOL_VALUE_2","مدراس المنهل فرع المربع");
			define("STUDENT_GRADE_VALUE_1","حضانة");
			define("STUDENT_GRADE_VALUE_2","رياض اطفال");
			define("STUDENT_GRADE_VALUE_3","دبلوما أمريكية");
			define("STUDENT_GRADE_VALUE_4","المرحلة الإبتدائية");
			define("STUDENT_GRADE_VALUE_5","المرحلة المتوسطة");
			define("STUDENT_GRADE_VALUE_6","المرحلة الثانوية");
			define("STEP3_LINE1_M"," نشكر لكم تقديم طلبكم لتسجيل ابنكم في<a style=\"color: #337ab7 !important;\" href=\"https://mhs.edu.sa/portal2/index.asp\" target=\"_blank\">	مدراس المنهل </a> ،،");
			define("STEP3_LINE1_F"," نشكر لكم تقديم طلبكم لتسجيل ابنتكم في<a style=\"color: #337ab7 !important;\" href=\"https://mhs.edu.sa/portal2/index.asp\" target=\"_blank\">	مدراس المنهل </a> ،،");
			define("STEP3_LINE2","نسعى دائما لتعزيز القيم والأخلاق النبيلة من خلال برامج تعليمية مختلفة..");
			define("STEP3_LINE3","سيقوم فريق عمل مدراس المنهل بالتواصل معكم لاستكمال إجراءات التسجيل.");
			define("STEP3_LINE4","نكرر الشكر والتقدير.");
			define("SAVE","حفظ");
			define("INVALID_DATE_VALUE_FORMAT","قيمة التاريخ خاطئة. يجب إدخال قيمه صحيحة  و فى الصيغة: yyyy/mm/dd");
			define("SCHOOL_SITE_TITLE_NEW","مدارس المنهل");
			define("REG_PAGE_TITLE","طلب التحاق طالب/ة بمدراس المنهل");
			define("STEP_1_TITLE","بيانات ولي الأمر");
			define("STEP_2_TITLE","بيانات الطالب");
			define("STEP_3_TITLE","تم التسجيل بنجاح");
			
			define("PLEASE_EVALUATE_SERVICE","نرجو تقييم الخدمة المقدمة إليك");
			define("SERVICE_LEVEL","مستوى الخدمة");
			define("SEND","إرسال");
			define("EVALUATION_SENT","تم إرسال التقييم بنجاح شكرا لكم");
			define("MENU_ITEM_1", "الرئيسية");
			define("MENU_ITEM_2", "المسار الدولي");
			define("MENU_ITEM_3", "التعليم الالكتروني");
			define("MENU_ITEM_3_1", "الجداول والاختبارات");
			define("MENU_ITEM_3_2", "الخطة الاسبوعية");
			define("MENU_ITEM_4", "الصور");
			define("MENU_ITEM_5", "الرسوم");
			define("MENU_ITEM_5_1", "فرع المربع");
			define("MENU_ITEM_5_2", "فرع التعاون");
			define("MENU_ITEM_6", "الفيديو");
			define("MENU_ITEM_7", "اتصل بنا");
			define("MENU_ITEM_7_1", "الاتصال بمديرالموقع");
			define("MENU_ITEM_8", "التوظيف");
			define("MENU_ITEM_9", "الفعاليات");
			define("MENU_ITEM_10", "عن المدارس");
			define("NEXT", "التالي");
			define("MAIL_SENT","تم ارسال ايميل للتاكيد");
			define("MAIL_NOT_SENT","هناك مشكله في ارسال الايميل");
			define("STUDENT_REGISTRATIONS","تسجيلات الطلبة");
			define("CREATION_DATE","تاريخ التسجيل");
			define("MORE","عرض المزيد>>");
			define("NO_RECORDS_FOUND","لا يوجد اى بيانات");
			define("BACK","رجوع");
			define("REG_STUDENT_FORM","بيانات طلب الالتحاق");
			define("IS_CONTACTED","تم التواصل");
			define("YES","نعم");
			define("NO","لا");
			define("NUMBER_OF_REGS","عدد التسجيلات الحالية: ");
			define("COMMENTS","تعليقات");
			define("ADDITIONAL_DATA","بيانات إضافية");
			define("ADD_NOTIFICATION","إضافة تذكير");
			define("NOTIFICATION_DATE","تاريخ التذكير");
            define("WRITE_COMMENT","أكتب التعليق ....");
            define("POST","نشر");
            define("STATE","الحالة");
            define("SAVED_STATE","تم حفظ الحالة");
            define("PREVIOUS","السابق");
			define("PAGE_NUM","الصفحة");
            define("NEXT", "التالي" );
            define("ADVANCED_SEARCH", "بحث متقدم");
            define("CANCEL_SEARCH", "إلغاء البحث");
            define("SEARCH", "بحث");
			define("STUDENT_OR_PARENT_NAME","اسم الطالب أو ولي الأمر");
			define("PARENT_NAME","اسم ولي الأمر");
			define("MY_PROFILE","ملفي الشخصي");
			define("LOGOUT","خروج");
			define("STATE_FILTER", "التقسيم حسب حالة الطلب" );
			define("GRADE_FILTER", "التقسيم حسب المرحلة الدراسية" );
			define("DIAPLAY_ALL","عرض الكل");
			define("EDIT","تعديل");
			define("DELETE","مسح");
			define("QUICK_SEARCH","ابحث");
			define("SEND_EVAL_TO_CLIENT","طلب تقييم الخدمة");
			define("SEND_NOTIF_TO_ADMIN","ارسال إلى مدير النظام");
			define("SENT_ADMIN_MAIL_NOTIF_DONE","تم ارسال التنبيه للمدير بنجاح");
			define("SENT_EVAL_TO_CLIENT","تم ارسال طلب التقييم  بنجاح");
			define("CLOSE","غلق");
			define("HIDE","hide");
			define("SENDING_MAIL","يتم الارسال.....");
			define("PLEASE_REVIEW_THIS_REGISTRATION_REQUEST","برجاء مراجعة طلب التقديم");
			define("DATE_CAN_NOT_BE_LESS_THAN_TODAY","التاريخ لا يجب ان يكون أقل من تاريخ اليوم");
			define("INVALID_DATE_VALUE_FORMAT_MILADY", "قيمة التاريخ خاطئة. يجب إدخال قيمه صحيحة  و فى الصيغة: mm/dd/yyy");
			define("DASHBOARD_REPORTS","التقارير");
			define("DASHBOARD_REPORTS_SMALL_TITLE","الإحصاءات، والرسوم البيانية، والأحداث والتقارير الأخيرة");
			define("PROGRESS","تقدم");
			define("REPORT2_TITLE","عدد الطلبات");
			define("REPORT2_SMALL_TITLE","لكل يوم ولمدة 30 يوم");
			define("CREATED_REGISTRATIONS_COUNT","إجمالي عدد الطلبات المضافة");
			define("REGISTRATION","طلب");
			define("IN","في");
			define("REPORT3_TITLE","عدد الطلبات");
			define("TOTAL_NEW_REGISTRATIONS", "إجمالى الطلبات الجديدة");
			define("TOTAL_HAVE_REGISTRATIONS", "إجمالى الطلبات المنجزة");
			define("REPORT3_AGENT", "ممثل الخدمة");
			define("REPORT3_NEW", "طلبات جديدة");
			define("REPORT3_UNDER_PROGRESS", "طلبات تحت الدراسة");
			define("REPORT3_FINISHED", "طلبات انتهت");
			define("REPORT3_RATE", "نسبة الانجاز");
			define("REPORT4_TITLE", "أداء ممثلى الخدمة");
			define("TOTAL_REGISTRATIONS_COUNT","عدد الطلبات الإجمالى");
			define("DELAYED_REGISTRATIONS_COUNT","عدد الطلبات المتأخرة");
			define("DELAYED_RATE", "نسبة التأخير");
			define("DASHBOARD_REPORTS_LOADING","جاري تجهيز التقرير");
			define("MOBILE_NUMBER_INVALID","يجب أن يكون الحد الأقصى 15 أرقام" );
			define("DASHBOARD_ACTIVITIES","متابعة الأداء");
			define("LATE_REGISTRATIONS","الطلبات المتأخرة");
			define("LATE_REGISTRATIONS_LEVEL1","(مستوى التأخير الأول)");
			define("LATE_REGISTRATIONS_LEVEL2","(مستوى التأخير الثاني)");
			define("CLIENT_EVAL","تقييم العميل للخدمة");
			define("REGISTRATIONS","طلبات التسجيل");
			define("KNOWING_WAY","ما الوسيلة التى تم معرفة المدرسة من خلالها؟");
			define("AGENTS","ممثلى الخدمة");
			define("AGENT_NAME","إسم ممثل الخدمة");
			define("MOBILE_NUMBER","رقم الجوال");
			define("EMAIL","البريد الإلكتروني");
			define("INFO", "تنبيه" );
			define("WANT_DELETE_RECORD", "هل أنت متأكد أنك تريد مسح هذا السجل؟" );
			define("OK", "نعم" );
			define("CANCEL", "إلغاء" );
			define("INSERT", "إضافة" );
			define("ADD_RECORD", "إضافة ممثل خدمة" );
			define("INCORRECT_REPEATED_PASSWORD", "اعادة كلمة السر الجديدة غير متطابقة مع كلمة السر الجديدة" );
			define("REG_DATE_FROM", "تاريخ التسجيل من" );
			define("REG_DATE_TO", "تاريخ التسجيل  إلى" );
			define("AGENT_GRADES", "المراحل الدراسية المسئول عنها" );
			define("MOVE_REGS", "نقل طلبات التسجيل إلى" );
			define("BRANCH", "الفرع" );
			define("TRACK", "المسار" );
			define("INTERNATIONAL", "الدولى" );
			define("PUBL", "العام" );
			define("DEPARTMENT","القسم");
			define("BOYS","بنين");
			define("GIRLS","بنات");
			break;
		default:
			define("WELCOME_TXT","Welcome!");
			define("CHOOSE_TXT","Choose Language");
			break;
	}
}
?>