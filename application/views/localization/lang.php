<?php
function defineLocale($currLang = "ar") {
	switch ($currLang) {
		case "en" :
			define ( "CHARSET", "ISO-8859-1" );
			define ( "LANGCODE", "en" );
			break;
		case "ar" :
			define ( "CHARSET", "ISO-8859-1" );
			define ( "LANGCODE", "ar" );
			break;
		default :
			define ( "CHARSET", "ISO-8859-1" );
			define ( "LANGCODE", "en" );
			break;
	}
//	header ( "Content-Type: text/html;charset= UTF8" );
//	header ( "Content-Language: " . LANGCODE );
}
?>