
<?php
// session_start();
$this->load->view('localization/lang');
$this->load->view('localization/txt');
defineLocale();
defineStrings();

?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title><?php echo SCHOOL_SITE_TITLE_NEW; ?></title>
<link rel="icon" href="<?php echo base_url()?>images/header_logo.png">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="author" />

<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-table/bootstrap-table.min.css"
	rel="stylesheet" type="text/css" />

<link
	href="<?= base_url() ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css"
	rel="stylesheet" type="text/css" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?= base_url() ?>assets/css/student_registrations.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/global/plugins/morris/morris.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/global/plugins/fullcalendar/fullcalendar.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?= base_url() ?>assets/global/css/components-rtl.min.css"
	rel="stylesheet" id="style_components" type="text/css" />
<link href="<?= base_url() ?>assets/global/css/plugins-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link
	href="<?= base_url() ?>assets/layouts/layout3/css/layout-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/layouts/layout3/css/themes/default-rtl.min.css"
	rel="stylesheet" type="text/css" id="style_color" />
<link
	href="<?= base_url() ?>assets/layouts/layout3/css/custom-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon"
	href="<?= base_url() ?>assets/layouts/layout3/img/shortcut-icon.png" />



<script type="text/javascript">
            function checkContacted(checkboxElem, reg_id) {
                if (checkboxElem.checked) {
                    update_is_contacted_value(reg_id, "Y");
                } else {
                    update_is_contacted_value(reg_id, "N");
                }
            }

            function update_is_contacted_value(reg_id, is_contacted) {
                $.ajax({
                    type: "post",
                    url: '<?php echo site_url('admin_panel/update_is_contacted_value'); ?>',
                    data: {
                        'reg_id': reg_id,
                        'is_contacted': is_contacted
                    },
                    complete: function (data) {
                        try {
                            console.log(data);
                            //alert( 'Success');
                        } catch (e) {
                            //alert('Exception while request..');
                        }

                    },
                    error: function () {
                        // alert('Error while request..');
                    }
                });
            }

            // pagination
            var page_size = 30;
            var allRecordsMap = new Array();
            allRecordsMap[<?= $current_page ?>] = <?php echo json_encode($student_registrations) ?>;
            var total_count = '<?= $total_count ?>';
            var clickedPage = <?= $current_page ?>;
            var view_late_regs_lvl = <?= $view_late_regs_lvl ?>;
            sessionStorage.setItem("clickedPage", "<?= $current_page ?>");
            sessionStorage.setItem("search_values", JSON.stringify(<?= json_encode($search_values) ?>));
            sessionStorage.setItem("is_search", "<?= $is_search ?>");
            sessionStorage.setItem("selected_state", "<?= $selected_state ?>");
            sessionStorage.setItem("selected_grade", "<?= $selected_grade ?>");
            sessionStorage.setItem("view_late_regs_lvl", "<?= $view_late_regs_lvl ?>");
            sessionStorage.setItem("query", "<?= $query ?>");

            function uploadRecordsPerPage(clickedPage) {
                sessionStorage.setItem("clickedPage", clickedPage + "");
                // console.log(clickedPage+" "+allRecordsMap[clickedPage]);
                state = <?= json_encode($selected_state) ?>;
                grade = <?= json_encode($selected_grade) ?>;
                if (true || allRecordsMap[clickedPage] == null) {
                	var url = "";
                    if(view_late_regs_lvl == '0'){
                    	url = '<?php echo site_url("admin_panel/view_registrations") ?>' + '/0' + '/' + page_size + '/' + clickedPage + '/'+ state + '/' + grade ;
                    }else{
                    	url = '<?php echo site_url("admin_panel/view_late_registrations") ?>' + '/0' + '/' + page_size + '/' + clickedPage + '/'+ state + '/' + grade + "/0/"+view_late_regs_lvl;
                    }
                    console.log('url='+url);
                    var data = {};
                    var type = 'GET';
                    var is_search = <?= $is_search ?>;
                    if (is_search != null && is_search == 1) {
                        url = '<?php echo site_url("admin_panel/advanced_search") ?>' + '/0' + '/' + page_size + '/' + clickedPage+ '/0/0/'+ view_late_regs_lvl;
                        data = <?= json_encode($search_values) ?>;
                        type = 'POST';
                    } 
                    if (is_search != null && is_search == 2) {
                        url = '<?php echo site_url("admin_panel/quick_search") ?>' + '/0' + '/' + page_size + '/' + clickedPage+ '/0/0/'+ view_late_regs_lvl;
                        data = <?= json_encode($search_values) ?>;
                        type = 'POST';
                    } 
                     console.log(data);
                    $.ajax({
                        url: url,
                        data: data,
                        type: type,
                        success: function (response) {
                               console.log(response);
                            var responseObject = JSON.parse(response);
                            allRecordsMap[clickedPage] = responseObject["student_registrations"];
                            sessionStorage.setItem("total_count", responseObject["total_count"]);
                            adjustPage(responseObject["total_count"], clickedPage);

                        },
                        error: function (request, status, error) {
                            alert(request.responseText);
                        }
                    });

                } else {
                    getNextBatch(clickedPage);
                    adjustPage(total_count, clickedPage);
                }
            }
            function adjustPage(count, clickedPage) {
                total_count = count;
                if (total_count <= 30) {
                    $("#pagination_div").css('display', 'none');
                } else {
                    var page_num = Math.ceil(total_count / page_size);
                    $("#pagination_div").css('display', 'block');
                    $("#total_pages_num").text(page_num);
                    $("#desired_page_num").val(clickedPage);
                    if (clickedPage == 1) {
                        $("#next_button").prop('disabled', false);
                        $("#prev_button").prop('disabled', true);
                    } else if (clickedPage == page_num) {
                        $("#next_button").prop('disabled', true);
                        $("#prev_button").prop('disabled', false);
                    } else {
                        $("#next_button").prop('disabled', false);
                        $("#prev_button").prop('disabled', false);
                    }
                }
                getNextBatch(clickedPage);

            }

            function leftPad(number, targetLength) {
                var output = number + '';
                while (output.length < targetLength) {
                    output = '0' + output;
                }
                return output;
            }
            
            function getNextBatch(clickedPage) {
                student_regs = allRecordsMap[clickedPage];
                $('#registrations').empty();
                /* <td><?= $responsible_name ?></td>
                 <td><?= $responsible_relationship ?> </td>
                 <td><?= $responsible_mobile ?></td>
                 <td><?= $creation_date ?></td>
                 <td><?= $grade ?></td>
                 <td><input type="checkbox" class="make-switch"
                 onchange="checkContacted(this, '<?= $reg_id ?>')"
<?php if ($is_contacted == "Y") { ?> checked <?php } ?>
                 data-on-color="primary" data-off-color="default"></td>
                 <td><a
                 href="<?= site_url('Admin_panel/view_student_registration/' . $reg_id) ?>"
                 class="btn btn-outline btn-circle btn-sm blue"> <i
                 class="fa fa-edit"></i> <?php echo MORE; ?>
                 </a></td> */
                var available_states = <?php echo json_encode($available_states) ?>;
                for (var i in student_regs) {
                    reg_url = "<?= site_url('Admin_panel/view_student_registration/') ?>" + student_regs[i]["reg_id"];
                    current_state_code = student_regs[i]["current_state_code"];
                    current_state_name = "";
                    if (available_states) {
                    	for (j = 0; j< available_states.length ;j++){
                    		state_code = available_states [j] ["state_code"];
                    		state_name = available_states [j] ["state_name_ar"];
                        	 if (current_state_code == state_code) {
                        	 	current_state_name = state_name;
                        	 	break;
                              }
                        }
                     }
					current_state_class = "";
					if (current_state_code=="NEW"){
						current_state_class = 'label label-sm label-new';
					 }else if (current_state_code=="REGISTERED"){
						 current_state_class = 'label label-sm label-under-action';
					 }else if (current_state_code=="NOT_INTERESTED_IN_REGISTRATION"){
						 current_state_class = 'label label-sm label-danger';
					 }else if (current_state_code=="START_REGISTRATION_PROCESS"){
						 current_state_class = 'label label-sm label-dec-four';
					 }else if (current_state_code=="NOT_ANSWERED"){
						 current_state_class = 'label label-sm label-dec-six';
					 }else if (current_state_code=="PAYMENT_OF_FEES"){
						 current_state_class = 'label label-sm label-modf-req';
					 }else if (current_state_code=="INTERESTED_IN_REGISTRATION"){
						 current_state_class = 'label label-sm label-adv';
					 }else if (current_state_code=="WRONG_INFORMATION"){
						 current_state_class = 'label label-sm label-exec';
					 }else if (current_state_code=="DUPLICATE"){
						 current_state_class = 'label label-sm label-under-rev';
					}
					var state_col = "class='".concat(current_state_class);
					state_col = state_col.concat("'");

					readonly_param = "";
					var admin_type = <?php echo json_encode($admin_type) ?>;
					if(admin_type == "ACCOUNTANT" && (current_state_code != "START_REGISTRATION_PROCESS" && 
                       		current_state_code!= "REGISTERED" &&
                       		current_state_code!= "PAYMENT_OF_FEES")){
						readonly_param = 'readonly =\"readonly\"';
					}

					var responsible_mobile = "";
					var responsible_mobile_part = "";
					responsible_mobile = student_regs[i]["responsible_mobile"] + "";
					responsible_mobile_part = responsible_mobile.substr(0,3)+ "...";
					
                    $('#registrations').append('<tr>' +
                    		'<td style="display: none;">' + student_regs[i]["reg_id"] + '</td>' +
                    		'<td>' + "R" + leftPad(student_regs[i]["reg_id"], 5) + '</td>' +
                            '<td>' + student_regs[i]["responsible_name"] + '</td>' +
                            '<td style="padding-top:8px;cursor: pointer; direction: ltr;" onclick="javascript:display_full_mobile()">' + responsible_mobile_part + '</td>' +
                            '<td style="display: none;">' + responsible_mobile + '</td>' +
                            '<td>' + student_regs[i]["creation_date"] + '</td>' +
                            '<td>' + (student_regs[i]["grade"] == null?"":student_regs[i]["grade"]) + '</td>' +
                            '<td>' +'<span '+ state_col+"'>" + current_state_name+ '</span></td>' +
                            '<td>' +
                            //'<div class="bootstrap-switch bootstrap-switch-wrapper ' + (student_regs[i]["is_contacted"] === "Y" ? 'bootstrap-switch-on ' : 'bootstrap-switch-off ') + 'bootstrap-switch-animate" style="width: 94px;">' +
                            //'<div class="bootstrap-switch-container" style="width: 138px; margin-left: ' + (student_regs[i]["is_contacted"] === "Y" ? '0px' : '-46px') + ';">' +
                            //'<span class="bootstrap-switch-handle-on bootstrap-switch-primary" style="width: 46px;">نعم</span>' +
                            //'<span class="bootstrap-switch-label" style="width: 46px;">&nbsp;</span>' +
                            //'<span class="bootstrap-switch-handle-off bootstrap-switch-default" style="width: 46px;">لا</span>' +
                            '<input type= "checkbox" class="make-switch"' +
                            'onchange="checkContacted(this, \'' + student_regs[i]["reg_id"] + '\')"' +
                            (student_regs[i]["is_contacted"] == "Y" ? ' checked ' : ' ') +
                            'data-on-color="primary" data-off-color="default"'+ readonly_param +'>' + '</td>' +
                            '<td><a href="' + reg_url + '" class="btn btn-outline btn-circle btn-sm blue-sharp">' +
                            '<i class="fa fa-edit"></i> <?php echo MORE; ?> </a></td></tr>');
                }
                $('html, body').animate({
                    scrollTop: $("#contentPage").offset().top
                }, 500);
                $('.make-switch').bootstrapSwitch();
                $('#loading').addClass('hidden');
                display_full_mobile();
            }


            function get_prev_page() {
                var current_page = parseInt($("#desired_page_num").val());
                var newpage = (current_page - 1);
                paginate_on_page(newpage);
            }

            function get_next_page() {
                var current_page = parseInt($("#desired_page_num").val());
                var newpage = (current_page + 1);
                paginate_on_page(newpage);
            }

            function paginate_on_page(newpage) {
                $('#loading').removeClass('hidden');
                var no_of_pages = Math.ceil(total_count / page_size);
                if (newpage == "" || parseInt(newpage) <= 0) {
                	newpage = 1;
                }
                if (parseInt(newpage) >= parseInt(no_of_pages)) {
                    newpage = no_of_pages;
                    $("#next_button").prop('disabled', true);
                } else {
                    $("#next_button").prop('disabled', false);
                }
                if (parseInt(newpage) > 1) {
                    $("#prev_button").prop('disabled', false);
                } else {
                    $("#prev_button").prop('disabled', true);
                }
                $("#calculated_current_page").val(newpage);
                $("#desired_page_num").val(newpage);
                uploadRecordsPerPage(newpage);
                display_full_mobile();
            }
            $(document).on('keyup', '#desired_page_num', function (event) {
                if (event.keyCode == 13) {
                    var newpage = $('#desired_page_num').val();
                    paginate_on_page(newpage);
                }
            });

        	function apply_quick_search() {
        		var searchVal = $("#quick_search_input").val().trim();
        		$('#loading').removeClass('hidden');
        		$('#search_text').removeClass('hidden');
        		document.getElementById('quickSearchForm').submit();
        	}
  			
        </script>

<style type="text/css">
.quick-search {
	padding-top: 9px !important;
	padding-bottom: 9px !important;
	background: #E5E5E5;
	border: #E5E5E5;
}

.btn-searchColor {
	padding-top: 9px !important;
	padding-bottom: 9px !important;
}
</style>

<style>
.scrollable-custom-class {
	max-width: 80% !important;
	margin: auto;
	text-align: center;
	-webkit-overflow-scrolling: touch;
}
</style>

<style type="text/css">
.table {
	max-width: none !important;
}

.table-scrollable {
	-webkit-overflow-scrolling: touch;
}

.bootstrap-switch-container {
	direction: ltr;
}
</style>


</head>
<!-- END HEAD -->

<body class="page-container-bg-solid">
	<div id="all-page" class="container shadow">
		<div class="row">
			<div class="col-md-13">
                    <?php
                    $this->load->view('utils/schoolHeaderAdmin');
                    ?>

                    <div class="page-wrapper-row full-height">
					<div class="page-wrapper-middle">
						<?php $this->load->view('utils/delete_alert'); ?>
						<!-- BEGIN CONTAINER -->
						<div class="page-container">
							<!-- BEGIN CONTENT -->
							<div class="page-content-wrapper">
								<!-- BEGIN CONTENT BODY -->
								<div id="loading" class="hidden">
									<img id="loading-image"
										src="<?= base_url() ?>images/Ajax-loader2.gif"
										alt="Loading...">
								</div>
								<!-- BEGIN PAGE HEAD-->
								<div class="container">
									<div class="page-content-inner">

										<!-- BEGIN PAGE TITLE -->
										<div id="contentPage" class="portlet light portlet-fit ">
											<div class="portlet-title">
												<div class="caption">
													<div class="page-title"
														style="padding-top: 0px !important; padding-bottom: 7px !important;">
														<h2>
															<span style="color: #0E4278 !important;"><?php echo STUDENT_REGISTRATIONS; ?>
                                                                    <small>
                                                                         <?php echo '(' . ($registration_states_total_count==''?$total_count: $registration_states_total_count). ')'; ?> 
                                                                     </small></span>
														</h2>

													</div>
												</div>
												<div class="actions"
													style="margin-left: 20px; margin-top: 10px; display: inline !important; float: left;">
													<form id="quickSearchForm" class="search-form"
														action="<?=site_url('admin_panel/quick_search/1/30/1/0/0/'.$view_late_regs_lvl)?>"
														method="POST">
														<div class="input-group" style="width: 240px;">
    						<?php if (isset($is_search) && ($is_search == 0 || $is_search == 2)) { ?>
    								<input id="quick_search_input" type="text"
																class="form-control no-validate"
																style="background: #E5E5E5; color: #3B467E; border: 0px !important; min-width: 150px !important;"
																value="<?php echo $query;?>"
																placeholder="<?php echo QUICK_SEARCH; ?>" name="query"><?php } ?>
        								
    								<span class="input-group-btn"> 
    									<?php if (isset($is_search) && ($is_search == 0 || $is_search == 2)) { ?>
        								<a href="javascript:apply_quick_search();"
																class="btn submit quick-search"> <i
																	class="icon-magnifier" style="color: #fff"></i>
															</a> <?php } ?>
            							<a id="advSearchBtn" class="btn dark btn-outline btn-circle blue-sharp"
																style="width: 100px;"
																href="<?php
                if (isset($is_search) && ($is_search == 1 || $is_search == 2)) {
                    if ($view_late_regs_lvl == '0') {
                        echo site_url('admin_panel/view_registrations');
                    } else {
                        echo site_url('admin_panel/view_late_registrations/1/30/1/0/0/0/' . $view_late_regs_lvl);
                    }
                } else {
                    echo site_url('admin_panel/advanced_search/1/30/1/0/0/' . $view_late_regs_lvl);
                }
                ?>"
																data-hover="dropdown" data-close-others="true">
                                            <?php
                                            
if (isset($is_search) && ($is_search == 1 || $is_search == 2)) {
                                                echo CANCEL_SEARCH;
                                            } else {
                                                echo ADVANCED_SEARCH;
                                            }
                                            ?>
                      					</a>
															</span>
														</div>
													</form>
												</div>
											</div>
										</div>


										<!-- BEGIN PAGE CONTENT BODY -->
										<div class="page-content">
											<div class="portlet-body">
                                                
				<?php
    if ($is_search == "0") {
        $this->load->view('utils/state_progress_bar');
        ?>
					<?php
        if (isset($registration_grades)) {
            $this->load->view('utils/grade_progress_bar');
        }
    }
    ?>
				

<div class="table-scrollable" class="scrollable-custom-class">
													<table class="table table-bordered table-hover">
														<thead>
															<tr>
																<th><?php echo REG_CODE; ?></th>
																<th><?php echo RESPONSIBLE_NAME; ?></th>
																<th><?php echo RESPONSIBLE_MOBILE; ?></th>
																<th><?php echo CREATION_DATE; ?></th>
																<th><?php echo STUDENT_GRADE; ?></th>
																<th><?php echo STATE; ?></th>
																<th><?php echo IS_CONTACTED; ?></th>
																<th></th>
															</tr>
														</thead>
														<tbody id="registrations">
                                                                <?php
                                                                if ($student_registrations) {
                                                                    for ($i = 0; $i < count($student_registrations); $i ++) {
                                                                        $reg_id = $student_registrations[$i]["reg_id"];
                                                                        $formatted_reg_id = sprintf("%'.05d", $reg_id);
                                                                        $reg_code = "R" . $formatted_reg_id;
                                                                        $responsible_name = $student_registrations[$i]["responsible_name"];
                                                                        $responsible_mobile = $student_registrations[$i]["responsible_mobile"];
                                                                        $responsible_mobile_part = mb_substr($responsible_mobile, 0, 3)."...";
                                                                        $creation_date = $student_registrations[$i]["creation_date"];
                                                                        $student_name = $student_registrations[$i]["student_name"];
                                                                        $school = $student_registrations[$i]["school"];
                                                                        $grade = $student_registrations[$i]["grade"];
                                                                        $is_contacted = $student_registrations[$i]["is_contacted"];
                                                                        $current_state_code = $student_registrations[$i]["current_state_code"];
                                                                        $current_state_name = "";
                                                                        if ($available_states) {
                                                                            for ($j = 0; $j < count($available_states); $j ++) {
                                                                                $state_code = $available_states[$j]["state_code"];
                                                                                $state_name = $available_states[$j]["state_name_ar"];
                                                                                if ($current_state_code == $state_code) {
                                                                                    $current_state_name = $state_name;
                                                                                }
                                                                            }
                                                                        }
                                                                        ?>
                                                                  <tr>
                                                                <td style="display: none;"><?= $reg_id?></td>
																<td><?= $reg_code?></td>
																<td><?= $responsible_name ?></td>
																<td style="padding-top:8px;cursor: pointer; direction: ltr;" onclick="javascript:display_full_mobile()" ><?= $responsible_mobile_part ?></td>
																<td style="display: none;"><?= $responsible_mobile ?></td>
																<td><?= $creation_date ?></td>
																<td><?= $grade==null?"":$grade ?></td>

																<td><span
																	class=<?php if ( $current_state_code=="NEW"){?> "label
																	label-sm
																	label-new"
                                                                    <?php }else if ( $current_state_code=="REGISTERED"){?>
                            											"label
																	label-sm
																	label-under-action"
                                                                    <?php }else if ( $current_state_code=="NOT_INTERESTED_IN_REGISTRATION"){?>
                            											"label
																	label-sm
																	label-danger"
                                                                    <?php }else if ( $current_state_code=="START_REGISTRATION_PROCESS"){?>
                            											"label
																	label-sm
																	label-dec-four"
                                                                    <?php }else if ( $current_state_code=="NOT_ANSWERED"){?>
                            											"label
																	label-sm
																	label-dec-six"
                                                                    <?php }else if ( $current_state_code=="PAYMENT_OF_FEES"){?>
                            											"label
																	label-sm
																	label-modf-req"
                                                                    <?php }else if ( $current_state_code=="INTERESTED_IN_REGISTRATION"){?>
                            											"label
																	label-sm
																	label-adv"
                                                                    <?php }else if ( $current_state_code=="WRONG_INFORMATION"){?>
                            											"label
																	label-sm
																	label-exec"
                                                                    <?php }else if ( $current_state_code=="DUPLICATE"){?>
                            											"label
																	label-sm
																	label-under-rev"
                                                                    <?php }?>> <?=$current_state_name;?></span></td>

																<td><input type="checkbox" class="make-switch"
																	onchange="checkContacted(this, '<?= $reg_id ?>')"
																	<?php if ($is_contacted == "Y") { ?> checked <?php } ?>
																	data-on-color="primary" data-off-color="default"
																	<?php
                                                                        
if ($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" && $current_state_code != "REGISTERED" && $current_state_code != "PAYMENT_OF_FEES")) {
                                                                            ?>
																	readonly="readonly" <?php }?>></td>
																<td>
																	<a
																		href="<?= site_url('Admin_panel/view_student_registration/' . $reg_id) ?>"
																		class="btn btn-outline btn-circle btn-sm blue-sharp"> 
																		<i class="fa fa-edit"></i> <?php echo MORE; ?>
                                                                    </a>
                                                                    <?php if ($admin_type == "ADMIN"){?>
	                                                                    <a id="delBtn"
																			href="javascript:delBtnClick('<?php echo $reg_id?>', 'registrations');"
																			class="btn btn-outline btn-circle dark btn-sm red"> 
																			<i class="fa fa-trash-o"></i> <?php echo DELETE; ?>
																		</a>
																	<?php }?>
                                                                </td>
															</tr>	
                                                                        <?php
                                                                    }
                                                                } else {
                                                                    ?><tr
																class="HeadBr2">
																<td colspan="16" style="color: red" align="center"><h4>
                                                                                <?php echo NO_RECORDS_FOUND; ?>
                                                                            </h4></td>
															</tr> <?php } ?>
                                                            </tbody>
													</table>
												</div>
												<!-- END BORDERED TABLE PORTLET-->


												<input type="hidden" name="calculated_current_page"
													id="calculated_current_page" value="1"> <input
													type="hidden" name="no_of_pages" id="no_of_pages"
													value="<?php echo $total_count; ?>">


														<div id="pagination_form" style="  margin: auto; text-align: center;">
															<input type="hidden" name="current_page"
																id="current_page" value="1"> <input type="button"
																class="bold btn blue-sharp" style="width: 7rem !important;"
																id="prev_button" <?php if ($current_page == 1) { ?>
																disabled <?php } ?> onClick="javascript:get_prev_page()"
																value="<?php echo PREVIOUS; ?>" /> <label><?php echo PAGE_NUM; ?>:</label>
															<input id="desired_page_num" class="no-spinners"
																type="number" style="width: 50px; text-align: center;"
																value="<?= $current_page ?>" min="1" />/ <span
																id="total_pages_num"><?= ceil($total_count / 30) ?></span>
															<input type="button" class="bold btn blue-sharp"
																style="width: 7rem !important;" id="next_button"
																<?php if ($current_page == ceil($total_count / 30)) { ?>
																disabled <?php } ?> onClick="javascript:get_next_page()"
																value="<?php echo NEXT; ?>" />
														</div>
														
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
</div>
</div>
	
	<?php $this->load->view('utils/schoolFooter');?>	
    <!--[if lt IE 9]>
    <script src="<?= base_url() ?>assets/global/plugins/respond.min.js"></script>
    <script src="<?= base_url() ?>assets/global/plugins/excanvas.min.js"></script> 
    <script src="<?= base_url() ?>assets/global/plugins/ie8.fix.min.js"></script> 
    <![endif]-->


			<!-- BEGIN CORE PLUGINS -->
			<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
				type="text/javascript"></script>
			<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
				type="text/javascript"></script>
			<!-- END CORE PLUGINS -->
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<script src="<?= base_url() ?>assets/global/plugins/moment.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/morris/morris.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/morris/raphael-min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/counterup/jquery.waypoints.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/counterup/jquery.counterup.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/fullcalendar/fullcalendar.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/flot/jquery.flot.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/flot/jquery.flot.resize.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/flot/jquery.flot.categories.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
				type="text/javascript"></script>

			<script
				src="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js"
				type="text/javascript"></script>

			<script
				src="<?=base_url()?>assets/global/plugins/bootstrap-table/bootstrap-table.min.js"
				type="text/javascript"></script>

			<script src="<?= base_url() ?>assets/pages/scripts/dashboard.min.js"
				type="text/javascript"></script>

			<!-- END PAGE LEVEL PLUGINS -->
			<!-- BEGIN THEME GLOBAL SCRIPTS -->
			<script src="<?=base_url()?>assets/global/scripts/app.min.js"
				type="text/javascript"></script>
			<!-- END THEME GLOBAL SCRIPTS -->
			<!-- BEGIN PAGE LEVEL SCRIPTS -->
			<script src="<?=base_url()?>assets/pages/scripts/profile.min.js"
				type="text/javascript"></script>
			<!-- END PAGE LEVEL SCRIPTS -->
			<!-- BEGIN THEME LAYOUT SCRIPTS -->
			<script
				src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
				type="text/javascript"></script>

			<!-- END THEME LAYOUT SCRIPTS -->

			<script>
      $(function () {
    
  		$('#quickSearchForm').on('keyup keypress', function(e) {
			  var keyCode = e.keyCode || e.which;
			  if (keyCode === 13) {
	        		var searchVal = $("#quick_search_input").val().trim();
	        		apply_quick_search();
			}
    	});
        $(document).on('keyup','#quick_search_input',function(event) { 
      	    if(event.keyCode == 13){
      	    	apply_quick_search();
      	    }
      	});
    		    
      });

      function display_full_mobile(){
			var table = document.getElementById("registrations");
			var rows = table.getElementsByTagName("tr");
			for (i = 0; i < rows.length; i++) {
			    var currentRow = table.rows[i];
			    var createClickHandler = function(row) {
			      return function() {
				    var reg_id_cell = row.getElementsByTagName("td")[0];
			        var cell = row.getElementsByTagName("td")[4];
			        var cell2 = row.getElementsByTagName("td")[5];
			        cell.innerHTML = cell2.innerHTML;
			        //Add transaction
			         $.ajax({
	                    type: "post",
	                    url: '<?php echo site_url('admin_panel/add_transaction_ajax'); ?>',
	                    data: {
	                        'reg_id': reg_id_cell.innerHTML,
	                    }
	                });
			      };
			    };
			    currentRow.getElementsByTagName("td")[4].onclick = createClickHandler(currentRow);
			}
		}

		display_full_mobile();
		
  </script>

<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135637603-1"></script>

<script>

  window.dataLayer = window.dataLayer || [];

  function gtag(){dataLayer.push(arguments);}

  gtag('js', new Date());

 

  gtag('config', 'UA-135637603-1');

</script>

</body>

</html>
