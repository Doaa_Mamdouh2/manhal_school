<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> 
<?php
// session_start ();
$this->load->view('localization/lang');
$this->load->view('localization/txt');
defineLocale();
defineStrings();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl" class="js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers no-applicationcache svg inlinesvg smil svgclippaths gr__alkifah_edu_sa gr__216_172_171_23" >
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title><?php echo SCHOOL_SITE_TITLE_NEW; ?></title>
        <link rel="icon" href="<?php echo base_url()?>images/header_logo.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="author" />
        <link rel="stylesheet"
        	href="<?=base_url()?>assets/css/bootstrap.min.css">
        <link rel="shortcut icon"
        	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.png" />
        <link
        	href="<?=base_url()?>assets/global/plugins/bootstrap-table/bootstrap-table.min.css"
        	rel="stylesheet" type="text/css" />
	
        <link href="<?= base_url() ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css" rel="stylesheet" type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

		<style>
			.checked {
			  color: orange;
			}
		</style>
        <script>

	        function checkValid() {
	        	var responsible_name = document.forms["myForm"]["responsible_name"].value;
	        	var responsible_email = document.forms["myForm"]["responsible_email"].value;
	
	        	if (responsible_name== "" ) {
	        		$("#responsible_name_tooltip").css("display","block");
	        		$("#responsible_name_tooltip").css("visibility","visible");
	        		return false;
	        	} else{
	        		$("#responsible_name_tooltip").css("display","none");
	        		$("#responsible_name_tooltip").css("visibility","none");
	        	}
	        	
	        	if (responsible_email == "" || !validateEmail(responsible_email)) {
	        		$("#responsible_email_tooltip").css("display","block");
	        		$("#responsible_email_tooltip").css("visibility","visible");
	        		return false;
	        	} else{
	        		$("#responsible_email_tooltip").css("display","none");
	        		$("#responsible_email_tooltip").css("visibility","none");
	        	}

	        	var other_way = $('#other_way').val();
	        	var knowing_way = $('#knowing_way').val();
	        	if (other_way == "" && knowing_way == "OTHER") {
	        		$("#other_way_tooltip").css("display","block");
	        		$("#other_way_tooltip").css("visibility","visible");
	        		return false;
	        	} else{
	        		hide_other_tooltip();
	        	}
	        }

	        function hide_other_tooltip(){
	        	$("#other_way_tooltip").css("display","none");
	        	$("#other_way_tooltip").css("visibility","none");
	        }
	
	
	        function validateEmail(email) 
	        {
	        	//var re = /\S+@\S+\.\S+/;
	        	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	        	return re.test(email);
	        }
            $(document).ready(function () {
               	var field = $('.my-text');
                 $('#mobile_number_part').click(function() {
                     $("#mobile_number_full").css("display","block");
                     $("#mobile_number_part").css("display","none");    
                     $("#additional_data").css("display","block");
                 	 field.insertBefore('#mobile_number_part'); 

                     $('html, body').animate({
                         scrollTop: $("#additional_data").offset().top
                     }, 500);
                              
                 });

                 var knowing_way = $("select#knowing_way").val();
                 if(knowing_way != "OTHER"){
                 	$(".hidden-textbox").hide();
                 }
	       		 $("select#knowing_way").change(function(){
	       		    var currentVal = $(this).val();
	       		    if(currentVal == "OTHER"){
	       		      $(".hidden-textbox").show();
	       		    }
	       		    else{
	       		      $(".hidden-textbox").hide();
	       		      hide_other_tooltip();
	       		    }
	       		 });  
            });
        </script>
        
        <script type="text/javascript">

            function miladyHandler() {
                var DOB_1 = $('#DOB_1').val();
                if (DOB_1 != "") {
                    var isValidD = isValidMiladyDate(DOB_1);
                    if (!isValidD) {
                        return false;
                    }
                }

                var str_array = DOB_1.split('/');
                var miladyDate = jqOld.calendars.instance('gregorian', 'ar').newDate();
                var hijriDate = jqOld.calendars.instance('ummalqura', 'ar').newDate();
                try {
                    miladyDate = miladyDate.date(str_array[2], str_array[0], str_array[1]);
                    hijriDate = hijriDate.fromJSDate(miladyDate.toJSDate());
                    //alert(hijriDate);
                    $("#DOB_2").val(hijriDate.formatDate());
                } catch (err) {
                    return false;
                }
            }

            function hijriHandler() {
                var DOB_2 = $('#DOB_2').val();
                if (DOB_2 != "") {
                    var isValidD = isValidDate(DOB_2);
                    if (!isValidD) {
                        return false;
                    }
                }

                try {
                    $.ajax({
                        type: "post",
                        url: '<?php echo site_url('Registration_controller/convertToMilady'); ?>',
                        cache: false,
                        data: {
                            'hijriDate': DOB_2
                        },
                        success: function (json) {
                            try {
                                var miladyDate = jQuery.parseJSON(json);
                                //reformat from 2017-05-10 to 05/16/2017
                                if (miladyDate != '') {
                                    var str_array = miladyDate.split('-');
                                    var year = str_array[0];
                                    var month = str_array[1];
                                    var day = str_array[2];
                                    miladyDate = month + "/" + day + "/" + year;
                                }
                                $("#DOB_1").val(miladyDate);
                                //alert( 'Success');
                            } catch (e) {
                                //alert('Exception while request..');
                            }
                        },
                        error: function () {
                            //alert('Error while request..');
                        }
                    });
                } catch (err) {
                    return false;
                }
            }




            function isValidDate(dateVar) {
                //Format: yyyy/mm/dd
                var dateReg = /^\d{4}\/\d{2}\/\d{2}$/;
                var isV = dateReg.test(dateVar);
                if (!isV) {
                    return false;
                }

                var str_array = dateVar.split('/');
                var hijriDate = jqOld.calendars.instance('ummalqura', 'ar').newDate();
                try {
                    hijriDate = hijriDate.date(str_array[0], str_array[1], str_array[2]);
                } catch (err) {
                    return false;
                }
                return true;
            }

            function isValidMiladyDate(dateVar) {
                //Format: yyyy/mm/dd
                var dateReg = /^\d{2}\/\d{2}\/\d{4}$/;
                var isV = dateReg.test(dateVar);
                if (!isV) {
                    return false;
                }

                var str_array = dateVar.split('/');
                var miladyDate = jqOld.calendars.instance('gregorian', 'ar').newDate();
                try {
                    miladyDate = miladyDate.date(str_array[2], str_array[0], str_array[1]);
                } catch (err) {
                    return false;
                }
                return true;
            }

            function returnRegisterationsPage() {
                var view_late_regs_lvl = sessionStorage.getItem("view_late_regs_lvl");
                var url = "";
                if(view_late_regs_lvl == '0'){
                	url = "<?= site_url('Admin_panel/view_registrations/1/30/') ?>" + sessionStorage.getItem("clickedPage")+ "/" +sessionStorage.getItem("selected_state")+ "/" + sessionStorage.getItem("selected_grade");
                }else{
                	url = "<?= site_url('Admin_panel/view_late_registrations/1/30/') ?>" + sessionStorage.getItem("clickedPage")+ "/" +sessionStorage.getItem("selected_state")+ "/" + sessionStorage.getItem("selected_grade") + "/0/" + sessionStorage.getItem("view_late_regs_lvl");
                }
                var is_search = sessionStorage.getItem("is_search");
                if (is_search != null && is_search == 1) {
                    url = '<?php echo site_url("admin_panel/advanced_search/1/30") ?>' + '/' + sessionStorage.getItem("clickedPage") + "/0/0/" + sessionStorage.getItem("view_late_regs_lvl");
                    var data = JSON.parse(sessionStorage.getItem("search_values"));
                    console.log(data["student_name"] + "======");
                    var form = document.getElementById('advancSrchForm');
                    form.action = url;
                    if (data["responsible_name"] != null && data["responsible_name"] != "undefined") {
                        document.forms["advancSrchForm"]["responsible_name"].value = data["responsible_name"];
                    }
                    if (data["responsible_mobile"] != null && data["responsible_mobile"] != "undefined") {
                        document.forms["advancSrchForm"]["responsible_mobile"].value = data["responsible_mobile"];
                    }
                    if (data["responsible_email"] != null && data["responsible_email"] != "undefined") {
                        document.forms["advancSrchForm"]["responsible_email"].value = data["responsible_email"];
                    }
                    if (data["student_name"] != null && data["student_name"] != "undefined") {
                        document.forms["advancSrchForm"]["student_name"].value = data["student_name"];
                    }
                    if (data["current_state_code"] != null && data["current_state_code"] != "undefined") {
                        document.forms["advancSrchForm"]["current_state_code"].value = data["current_state_code"];
                    }
                    if (data["is_contacted"] != null && data["is_contacted"] != "undefined") {
                        document.forms["advancSrchForm"]["is_contacted"].value = data["is_contacted"];
                    }
                    if (data["creation_date"] != null && data["creation_date"] != "undefined") {
                        document.forms["advancSrchForm"]["creation_date"].value = data["creation_date"];
                    }
                    if (data["department"] != null && data["department"] != "undefined") {
                        document.forms["advancSrchForm"]["department"].value = data["department"];
                    }
                    if (data["track"] != null && data["track"] != "undefined") {
                        document.forms["advancSrchForm"]["track"].value = data["track"];
                    }
                    /* if (data["DOB_1"] != null && data["DOB_1"] != "undefined") {
                        document.forms["advancSrchForm"]["DOB_1"].value = data["DOB_1"];
                    }
                    if (data["DOB_2"] != null && data["DOB_2"] != "undefined") {
                        document.forms["advancSrchForm"]["DOB_2"].value = data["DOB_2"];
                    }
                    if (data["IBAN"] != null && data["IBAN"] != "undefined") {
                        document.forms["advancSrchForm"]["IBAN"].value = data["IBAN"];
                    }
                    if (data["IBAN_source"] != null && data["IBAN_source"] != "undefined") {
                        document.forms["advancSrchForm"]["IBAN_source"].value = data["IBAN_source"];
                    } */
                    if (data["knowing_way"] != null && data["knowing_way"] != "undefined") {
                        document.forms["advancSrchForm"]["knowing_way"].value = data["knowing_way"];
                    }
                    if (data["grade"] != null && data["grade"] != "undefined") {
                        document.forms["advancSrchForm"]["grade"].value = data["grade"];
                    }
                    if (data["old_school"] != null && data["old_school"] != "undefined") {
                        document.forms["advancSrchForm"]["old_school"].value = data["old_school"];
                    }
                    if (data["notification_time"] != null && data["notification_time"] != "undefined") {
                        document.forms["advancSrchForm"]["notification_time"].value = data["notification_time"];
                    }
                    if (data["agent_id"] != null && data["agent_id"] != "undefined") {
                        document.forms["advancSrchForm"]["agent_id"].value = data["agent_id"];
                    }
                    if (data["comment"] != null && data["comment"] != "undefined") {
                        document.forms["advancSrchForm"]["comment"].value = data["comment"];
                    }
                    console.log(document.forms["advancSrchForm"]["student_name"].value);
                    document.getElementById('advancSrchForm').submit();
                }else if (is_search != null && is_search == 2) { 
                    var data = JSON.parse(sessionStorage.getItem("search_values"));
                	url = '<?php echo site_url("admin_panel/quick_search/1/30") ?>' + '/' + sessionStorage.getItem("clickedPage")+ "/0/0/" + sessionStorage.getItem("view_late_regs_lvl");
                    var form = document.getElementById('quickSrchForm');
                    form.action = url;
                    if (data["query"] != null && data["query"] != "undefined") {
                        document.forms["quickSrchForm"]["query"].value = data["query"];
                    }
                    document.getElementById('quickSrchForm').submit();
                }else {
                    window.open(url, "_self");
                }

            }

            function send_notif_to_admin(reg_id){
                console.log(reg_id);
            	var dialog = '';
	            dialog = bootbox.dialog({
					message: '<p><i class="fa fa-spin fa-spinner"></i> '+"<?php echo SENDING_MAIL; ?>"+' </p>',
					size: 'small',
					buttons: {
						confirm: {
							label: "<?php echo CLOSE; ?>",
							className: 'btn-success'
						}
					}
				});
            		
                $.ajax({
             		type    : "POST",  
             		url     : "<?php echo site_url('admin_panel/send_notif_to_admin' )?>",  
             		data    : {
            	 		reg_id: reg_id
                 	},
                 	complete : function(data){
                     	console.log(data);
                     	if(data.status){
            				dialog.init(function(){
            					dialog.find('.bootbox-body').html("<?php echo SENT_ADMIN_MAIL_NOTIF_DONE; ?>");
            					});
                     	}
                 	}
            	});

            }

            function send_eval_to_client(reg_id){
                console.log(reg_id);
            	var dialog = '';
	            dialog = bootbox.dialog({
					message: '<p><i class="fa fa-spin fa-spinner"></i> '+"<?php echo SENDING_MAIL; ?>"+' </p>',
					size: 'small',
					buttons: {
						confirm: {
							label: "<?php echo CLOSE; ?>",
							className: 'btn-success'
						}
					}
				});
            		
                $.ajax({
             		type    : "POST",  
             		url     : "<?php echo site_url('admin_panel/send_eval_to_client' )?>",  
             		data    : {
            	 		reg_id: reg_id
                 	},
                 	complete : function(data){
                     	console.log(data);
                     	if(data.status){
            				dialog.init(function(){
            					dialog.find('.bootbox-body').html("<?php echo SENT_EVAL_TO_CLIENT; ?>");
            					});
                     	}
                 	}
            	});

            }


            function checkValidForm2(){
        		var miladyDate =  jqOld.calendars.instance('gregorian','ar').newDate();
            	var notification_time = document.getElementById("notification_time").value;
        	   	var str_array = notification_time.split('/');
        		miladyDate = miladyDate.date(str_array[2], str_array[0], str_array[1]);
        	   	var todayMilady =  jqOld.calendars.instance('gregorian','ar').newDate();
        		if (miladyDate < todayMilady) {
        			$("#notification_time_tooltip3").css("display","block");
        			$("#notification_time_tooltip3").css("visibility","visible");			
        			return false;
        		} else{
        			$("#notification_time_tooltip3").css("display","none");
        			$("#notification_time_tooltip3").css("visibility","none");
        		}  	

            }
            
        </script>


    </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid">
        <div class="container shadow">
            <?php
            $this->load->view('utils/schoolHeaderAdmin');
            ?>
            <?php $this->load->view('utils/date_scripts'); ?>
            
            
                <div class="page-wrapper-row full-height">
                    <div class="page-wrapper-middle">
                        <!-- BEGIN CONTAINER -->
                        <div class="page-container">
                            <!-- BEGIN CONTENT -->
                            <div class="page-content-wrapper">
    
                                <!-- BEGIN PAGE CONTENT BODY -->
                                <div class="page-content">
                                    <div class="">
                                        <div class="col-md-14">
                                            <div class="portlet box blue-sharp">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i>
                                                        <?php
                                                        echo REG_STUDENT_FORM;
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <br> <br>
                                                    <!-- BEGIN FORM-->
                                                    <form id="myForm" role="form" method="post"
                                                    	  onsubmit="javascript:return checkValid();"
                                                          enctype="multipart/form-data"
                                                          action="<?php echo site_url('admin_panel/edit_student_registration/'.$reg_id)?>"
                                                          class="form-horizontal">
    
                                                        <input type="hidden" id="reg_id" name="reg_id" value="<?= $reg_id ?>">
                                                        
                                                        <?php if(($admin_type == "AGENT")||($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" &&
                                                                                   		$current_state_code!= "REGISTERED" &&
                                                                                   		$current_state_code!= "PAYMENT_OF_FEES"))){?>
                                                        	<input type="hidden" id="agent_id" name="agent_id" value="<?= $agent_id?>">
                                                        <?php }?>
                                                        
    
                                                        <div class="form-body">
    
    														<div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 control-label"><?php echo REG_CODE; ?></label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" class="form-control " value="<?= "R" . sprintf ( "%'.05d", $reg_id );?>"
                                                                                   name="reg_code" id="reg_code" readonly ="readonly">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 control-label"><?php echo RESPONSIBLE_NAME; ?><span
                                                                                class="required"> * </span></label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" class="form-control "
                                                                                   value="<?= $responsible_name ?>"
                                                                                   name="responsible_name" id="responsible_name"
                                                                                   onfocus="javascript:removeTooltip('responsible_name_tooltip');"
                                                                                   <?php if($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" && 
                                                                                   		$current_state_code!= "REGISTERED" &&
                                                                                   		$current_state_code!= "PAYMENT_OF_FEES")){?>
                                                                                   			readonly ="readonly"
                                                                                   <?php }?>>
                                                                            <span id="responsible_name_tooltip" class="tooltiptext"
                                                                                  style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 control-label mobile-num"><?php echo RESPONSIBLE_MOBILE; ?></label>
                                                                        <div class="col-md-6" >
                                                                        	<label class="mobile-num" id="mobile_number_part" style="padding-top:8px;cursor: pointer; direction: ltr;"> <?= mb_substr($responsible_mobile, 0, 3)."...";  ?> </label>
                                                                        	<label class="mobile-num" id="mobile_number_full" style="padding-top:8px;display: none; direction: ltr;"> <?= $responsible_mobile ?> </label>
                                                                        	<input type="hidden" id="responsible_mobile" name="responsible_mobile" value="<?= $responsible_mobile?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
    
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 control-label"><?php echo RESPONSIBLE_EMAIL; ?><span
                                                                                class="required"> * </span></label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" class="form-control "
                                                                                   value="<?= $responsible_email ?>"
                                                                                   name="responsible_email" id="responsible_email"
                                                                                   onfocus="javascript:removeTooltip('responsible_email_tooltip');"
                                                                                   <?php if($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" && 
                                                                                         		$current_state_code!= "REGISTERED" &&
                                                                                         		$current_state_code!= "PAYMENT_OF_FEES")){?>
                                                                                       			readonly ="readonly"
                                                                                  <?php }?>>
                                                                            <span id="responsible_email_tooltip"
                                                                                  class="tooltiptext" style="display: none; color: red"><?php echo INVALID_EMAIL; ?></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
    
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 control-label"><?php echo CREATION_DATE; ?></label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" placeholder="" id="creation_date"
                                                                                   value="<?= $creation_date ?>" 
                                                                                   class="form-control dateMilady" name="creation_date"
                                                                                   <?php if($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" && 
                                                                                         		$current_state_code!= "REGISTERED" &&
                                                                                         		$current_state_code!= "PAYMENT_OF_FEES")){?>
                                                                                       			readonly ="readonly"
                                                                                   <?php }?>>
                                                                        </div>
                                                                    </div>
                                                                </div>
    
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 control-label"><?php echo STUDENT_NAME; ?></label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" class="form-control "
                                                                                   value="<?= $student_name ?>"
                                                                                   name="student_name" id="student_name"
                                                                                   onfocus="javascript:removeTooltip('student_name_tooltip');"
                                                                                   <?php if($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" && 
                                                                                         		$current_state_code!= "REGISTERED" &&
                                                                                         		$current_state_code!= "PAYMENT_OF_FEES")){?>
                                                                                       			readonly ="readonly"
                                                                                   <?php }?>>
                                                                            <span id="student_name_tooltip" class="tooltiptext"
                                                                                  style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
    
                                                            </div>
    														<!--  
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 control-label"><?php echo STUDENT_DOB_MILADY; ?></label>
                                                                        <div class="col-md-6">
																			<input type="text" placeholder="" id="DOB_1"
																				class="form-control dateMilady" name="DOB_1" value="<?= $DOB_1 ?>"
																				onchange="miladyHandler();"
																				onkeypress="this.onchange();"
																				onpropertychange="this.onchange();"
																				onpaste="this.onchange();" oninput="this.onchange();"
																				<?php if($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" && 
                                                                                         		$current_state_code!= "REGISTERED" &&
                                                                                         		$current_state_code!= "PAYMENT_OF_FEES")){?>
                                                                                       			readonly ="readonly"
                                                                                <?php }?>>
																			<span id="DOB_1_invalid_tooltip" class="tooltiptext"
																				style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
																			<span id="DOB_1_tooltip" class="tooltiptext"
																				style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																		</div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 control-label"><?php echo STUDENT_DOB_HIJRI; ?></label>
                                                                        <div class="col-md-6">
																			<input type="text" placeholder="" id="DOB_2"
																				class="form-control date" name="DOB_2" value="<?= $DOB_2 ?>"
																				onchange="hijriHandler()"
																				onkeypress="this.onchange();"
																				onpropertychange="this.onchange();"
																				onpaste="this.onchange();" oninput="this.onchange();"
																				<?php if($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" && 
                                                                                         		$current_state_code!= "REGISTERED" &&
                                                                                         		$current_state_code!= "PAYMENT_OF_FEES")){?>
                                                                                       			readonly ="readonly"
                                                                                <?php }?>>
																			<span id="DOB_2_invalid_tooltip" class="tooltiptext"
																				style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
																			<span id="DOB_2_tooltip" class="tooltiptext"
																				style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																		</div>
                                                                    </div>
                                                                </div>
                                                            </div>
    
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 control-label"><?php echo STUDENT_IBAN; ?></label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" class="form-control " name="IBAN"
                                                                                   id="IBAN" value="<?= $IBAN ?>"
                                                                                   onfocus="javascript:removeTooltip('IBAN_tooltip');" 
                                                                                   <?php if($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" && 
                                                                                         		$current_state_code!= "REGISTERED" &&
                                                                                         		$current_state_code!= "PAYMENT_OF_FEES")){?>
                                                                                       			readonly ="readonly"
                                                                                   <?php }?>>
                                                                                   <span
                                                                                   id="IBAN_tooltip" class="tooltiptext"
                                                                                   style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 control-label"><?php echo STUDENT_IBAN_SRC; ?></label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" class="form-control "
                                                                                   name="IBAN_source" id="IBAN_source"
                                                                                   value="<?= $IBAN_source ?>"
                                                                                   onfocus="javascript:removeTooltip('IBAN_source_tooltip');"
                                                                                	   <?php if($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" && 
                                                                                        		$current_state_code!= "REGISTERED" &&
                                                                                        		$current_state_code!= "PAYMENT_OF_FEES")){?>
                                                                                      			readonly ="readonly"
                                                                                  <?php }?>>
                                                                            <span id="IBAN_source_tooltip" class="tooltiptext"
                                                                                  style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            -->
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 control-label"><?php echo DEPARTMENT; ?></label>
                                                                        <div class="col-md-6">
                                                                            <select class="form-control " name="department"
                                                                                    onfocus="javascript:removeTooltip('department_tooltip');"
                                                                                    id="department"
                                                                                    <?php if($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" && 
                                                                                          		$current_state_code!= "REGISTERED" &&
                                                                                          		$current_state_code!= "PAYMENT_OF_FEES")){?>
                                                                                        			disabled = "disabled"
                                                                                 	<?php }?>
                                                                                    >
                                                                                <option value=""></option>
                                                                                <option value="BOYS"
                                                                                <?php if ($department == "BOYS") { ?>
                                                                                            selected="selected" <?php } ?>><?php echo BOYS; ?></option>
                                                                                <option value="GIRLS"
                                                                                <?php if ($department == "GIRLS") { ?>
                                                                                            selected="selected" <?php } ?>><?php echo GIRLS; ?></option>
                                                                            </select> <span id="department_tooltip"
                                                                                            class="tooltiptext" style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 control-label"><?php echo TRACK; ?></label>
                                                                        <div class="col-md-6">
                                                                            <select class="form-control " name="track"
                                                                                    onfocus="javascript:removeTooltip('track_tooltip');"
                                                                                    id="track"
                                                                                    <?php if($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" && 
                                                                                          		$current_state_code!= "REGISTERED" &&
                                                                                          		$current_state_code!= "PAYMENT_OF_FEES")){?>
                                                                                        			disabled = "disabled"
                                                                                 	<?php }?>
                                                                                    >
                                                                                <option value=""></option>
                                                                                <option value="PUB"
                                                                                <?php if ($track == "PUB") { ?>
                                                                                            selected="selected" <?php } ?>><?php echo PUBL; ?></option>
                                                                                <option value="INT"
                                                                                <?php if ($track == "INT") { ?>
                                                                                            selected="selected" <?php } ?>><?php echo INTERNATIONAL; ?></option>
                                                                            </select> <span id="track_tooltip"
                                                                                            class="tooltiptext" style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
    
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 control-label"><?php echo STUDENT_GRADE; ?></label>
                                                                        <div class="col-md-6">
                                                                            <select class="form-control " name="grade"
																				onfocus="javascript:removeTooltip('grade_tooltip');"
                                                                                   <?php if($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" &&
                                                                                   		$current_state_code!= "REGISTERED" &&
                                                                                   		$current_state_code!= "PAYMENT_OF_FEES")){?> disabled = disabled <?php }?>
																				id="grade">
																				
																				<option value=""></option>
																				<?php
																					if ($educational_grade) {
																						for($i = 0; $i < count ( $educational_grade); $i ++) {
																							$educational_grade_id = $educational_grade[$i] ["id"];
																							$educational_grade_desc_ar = $educational_grade[$i] ["desc_ar"];
																							?>
																							<option value="<?php echo $educational_grade_id?>"
																							<?php if ($educational_grade_id == $grade){?>
																								selected="selected" <?php }?>> <?php echo $educational_grade_desc_ar?>
																							</option>
																							<?php
																						}
																					} else {
																				?>
																				<option value=""></option>
																				<?php } ?>
																			</select>
                                                                            <span id="grade_tooltip" class="tooltiptext"
                                                                                  style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 control-label"><?php echo OLD_SCHOOL; ?></label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" class="form-control "
                                                                                   name="old_school" id="old_school" 
                                                                                   value="<?= $old_school ?>"
                                                                                   onfocus="javascript:removeTooltip('old_school_tooltip');"
                                                                                   <?php if($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" && 
                                                                                        		$current_state_code!= "REGISTERED" &&
                                                                                        		$current_state_code!= "PAYMENT_OF_FEES")){?>
                                                                                      			readonly ="readonly"
                                                                                   <?php }?>>
                                                                            <span id="old_school_tooltip" class="tooltiptext"
                                                                                  style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            
	                                                            <div class="row">
	                                                            	<?php if($admin_type != "AGENT"){?>
	                                                            	<div class="col-md-6">
																		<div class="form-group">
																			<label class="col-md-4 control-label"><?php echo CUSTOMER_SERVICE; ?></label>
																			<div class="col-md-6">
																				<select class="form-control " name="agent_id"
																					onfocus="javascript:removeTooltip('agent_id_tooltip');"
	                                                                                   <?php if(($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" &&
	                                                                                   		$current_state_code!= "REGISTERED" &&
	                                                                                   		$current_state_code!= "PAYMENT_OF_FEES"))){?> disabled = disabled <?php }?>
																					id="agent_id">
																					
																					<option value=""></option>
																					<?php
																						if ($available_agents) {
																							for($i = 0; $i < count ( $available_agents); $i ++) {
																								$available_agent_id = $available_agents[$i] ["admin_id"];
																								$available_agent_name = $available_agents[$i] ["name"];
																								?>
																								<option value="<?php echo $available_agent_id?>"
																								<?php if ($available_agent_id == $agent_id){?>
																									selected="selected" <?php }?>> <?php echo $available_agent_name?>
																								</option>
																								<?php
																							}
																						} else {
																					?>
																					<option value=""></option>
																					<?php } ?>
																				</select> <span id="grade_tooltip" class="tooltiptext"
																					style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																			</div>
																		</div>
																	</div>
                                                                	<?php }?>
                                                                	<div class="col-md-6">
	                                                                    <div class="form-group">
	                                                                        <label class="col-md-4 control-label"><?php echo KNOWING_WAY; ?></label>
	                                                                        <div class="col-md-6">
	                                                                        
	                                                                        <select class="form-control " name="knowing_way"
																					onfocus="javascript:removeTooltip('other_way_tooltip');"
																					id="knowing_way"
																					<?php if($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" && 
	                                                                                        		$current_state_code!= "REGISTERED" &&
	                                                                                        		$current_state_code!= "PAYMENT_OF_FEES")){?>
	                                                                                      			disabled = "disabled"
	                                                                                  <?php }?>>
																				<option value=""></option>
																				<?php
																				if ($knowing_ways) {
																					for($i = 0; $i < count ( $knowing_ways); $i ++) {
																						$code = $knowing_ways[$i] ["code"];
																						$name = $knowing_ways[$i] ["name"];
																							?>
																							<option value="<?php echo $code ?>"
																							<?php if ($code == $knowing_way){?>
																							selected="selected" <?php }?>>
																							<?php echo $name?>
																							</option>
																							<?php
																						}
																					} 
																				?>
																				</select>
																				<br>
																				<input type="text" class="form-control hidden-textbox"
																					name="other_way" id="other_way"
																					onfocus="javascript:removeTooltip('other_way_tooltip');"
																					value="<?= $other_way ?>"
																					<?php if($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" && 
	                                                                                        		$current_state_code!= "REGISTERED" &&
	                                                                                        		$current_state_code!= "PAYMENT_OF_FEES")){?>
	                                                                                      			readonly ="readonly"
	                                                                                  <?php }?>>
																					
																				<span id="other_way_tooltip" class="tooltiptext"
																					style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
	                                                                        
	                                                                        
	                                                                            
	                                                                        </div>
	                                                                    </div>
                                                                	</div>
	                                                            </div>
                                                            
                                                            <?php if(isset($evaluation) && $evaluation != NULL){?>
	                                                            <div class="row">
	                                                                <div class="col-md-6">
	                                                                    <div class="form-group">
	                                                                        <label class="col-md-4 control-label"><?php echo CLIENT_EVAL; ?></label>
	                                                                        <div class="col-md-6" dir="ltr">
	                                                                            <span class="fa fa-star <?php if($evaluation == "1" ||
	                                                                            		$evaluation == "2" || $evaluation == "3" ||
	                                                                            		$evaluation == "4" || $evaluation== "5"){?> checked <?php }?>"></span>
																				<span class="fa fa-star <?php if($evaluation == "2" ||
																						$evaluation == "3" || $evaluation == "4" || 
																						$evaluation== "5"){?> checked <?php }?>"></span>
																				<span class="fa fa-star <?php if($evaluation == "3" ||
																						$evaluation == "4" || $evaluation== "5"){?> checked <?php }?>"></span>
																				<span class="fa fa-star <?php if($evaluation == "4" ||
																						$evaluation == "5"){?> checked <?php }?>"></span>
																				<span class="fa fa-star <?php if($evaluation == "5"){?> checked <?php }?>"></span>
	                                                                        </div>
	                                                                    </div>
	                                                                </div>
	                                                            </div>
	                                                        <?php }?>
    
                                                           
    
                                                            <div class="form-actions">
                                                                <div class="row">
                                                                    <div class="col-md-9" align="center"
                                                                         style="margin: auto; width: 100%;">
                                                                        <button type="button" onclick="returnRegisterationsPage();" class="btn btn-circle blue-sharp" style="margin-bottom: 10px !important;">
                                                                        <?php echo BACK; ?></button>
                                                                            
                                                                         <?php if(!($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" && 
                                                                                         		$current_state_code!= "REGISTERED" &&
                                                                                         		$current_state_code!= "PAYMENT_OF_FEES"))){ ?>
                                                                        	<button type="submit" class="btn btn-circle blue-sharp" style="margin-bottom: 10px !important;"><?php echo EDIT;?></button>
                                                                        <?php } ?>
               															<?php if($admin_type == "AGENT"){ ?>
                                                                        	<button type="button" class="btn btn-circle blue-sharp" style="margin-bottom: 10px !important;"
                                                                        	onclick="send_notif_to_admin('<?= $reg_id ?>');" ><?php echo SEND_NOTIF_TO_ADMIN;?></button>
                                                                        
                                                                        	<button type="button" class="btn btn-circle blue-sharp" style="margin-bottom: 10px !important;"
                                                                        	onclick="send_eval_to_client('<?= $reg_id ?>');" 
                                                                        	<?php if($send_eval == "N"){ ?>
                                                                        		 disabled = "disabled"
	                                                                        <?php } ?>>
                                                                        	<?php echo SEND_EVAL_TO_CLIENT;?>
                                                                        	</button>
                                                                        <?php } ?>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
    
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
    
                                                </div>
    
                                            </div>
                                        </div>
    
                                    </div>
                                </div>
                                
                                <div class="page-content" style="display: none;" id="additional_data">
                                    <div class="">
                                        <div class="col-md-14">
                                            <div class="portlet box blue-sharp">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i>
                                                        <?php echo ADDITIONAL_DATA; ?>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form form-horizontal">
                                                    <!-- BEGIN FORM-->
                                                    <form id="commentForm" role="form" method="post" 
                                                    	  onsubmit="javascript:return checkValidForm2();"
                                                          action="<?= site_url('admin_panel/add_transaction/' . $reg_id) ?>"
                                                          class="">
                                                        
                                                        <input type="hidden" id="old_state" name="old_state" value="<?php echo $current_state_code; ?>">
                                                        <div class="form-body">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                    	<?php 
                                                                    	if(count($requests) > 0){
                                                                    		for ($i = 0; $i < count($requests); $i ++) {
                                                                    			$reg_id = $requests[$i]['reg_id'];
                                                                    			$creation_date_time= $requests[$i]['creation_date_time'];
                                                                    	?>
	                                                                        	<a class="col-md-3" style="float:right !important; width: 100% !important;color:red;"  target="_blank" href="<?php echo site_url('Admin_panel/view_student_registration/' . $reg_id); ?>" >
	                                                                        		&nbsp;<?php echo "يوجد طلب آخر لنفس ولي الأمر بتاريخ " .$creation_date_time; ?>
	                                                                        	</a>
                                                                        <?php 
                                                                    		}
                                                                    	} 
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="col-md-3 control-label"><?php echo RESPONSIBLE_MOBILE; ?></label>
                                                                        <div class="col-md-6">
                                                                            <label style="padding-top:8px; direction: ltr;"> <?= $responsible_mobile ?> </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="col-md-3 control-label"><?php echo STATE; ?> </label>
                                                                        <div class="col-md-6">
                                                                            <select class="form-control "
                                                                                    name="current_state_code" id="current_state_code"
                                                                                    <?php if($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" &&
                                                                                   		$current_state_code!= "REGISTERED" &&
                                                                                   		$current_state_code!= "PAYMENT_OF_FEES")){?> disabled = disabled <?php }?>
                                                                                    >
                                                                                        <?php
                                                                                        if ($available_states) {
                                                                                            for ($i = 0; $i < count($available_states); $i ++) {
                                                                                                $state_code = $available_states [$i] ["state_code"];
                                                                                                $state_name = $available_states [$i] ["state_name_ar"];
                                                                                                ?>
                                                                                        <option value="<?= $state_code ?>"
                                                                                        <?php if ($state_code == $current_state_code) { ?>
                                                                                                    selected="selected" <?php } ?>>
                                                                                            <?= $state_name ?></option>
                                                                                        <?php
                                                                                    }
                                                                                } else {
                                                                                    ?>
                                                                                    <option value="" selected></option>
                                                                                    <?php
                                                                                }
                                                                                ?>		
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
    															<div class="col-md-6">
                                                                    <div class="form-group">
                                                                 	<label class="col-md-3 control-label"><?php echo IS_CONTACTED; ?> </label>
                                                                    <div class="col-md-6">
                                                                        <table style="width: 60%; margin-top: 10px;">
                                                                            <tr>
                                                                                <td><input type="radio" name="is_contacted"
                                                                                           id="is_contacted" value="Y" 
                                                                                           <?php if ($is_contacted == 'Y') { ?> checked <?php } ?>
                                                                                           <?php if($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" && 
                                                                                         		$current_state_code!= "REGISTERED" &&
                                                                                         		$current_state_code!= "PAYMENT_OF_FEES")){?>
                                                                                       			disabled ="true"
                                                                                <?php }?>></td>
                                                                                <td><?php echo YES; ?></td>
                                                                                <td><input type="radio" name="is_contacted"
                                                                                           id="is_contacted" value="N"  disa
                                                                                           <?php if ($is_contacted == 'N') { ?> checked <?php } ?>
                                                                                           <?php if($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" && 
                                                                                         		$current_state_code!= "REGISTERED" &&
                                                                                         		$current_state_code!= "PAYMENT_OF_FEES")){?>
                                                                                       			disabled ="true"
                                                                                <?php }?>></td>
                                                                                <td><?php echo NO; ?></td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                 </div>
                                                               </div>
                                                            </div>
                                                           <div class="row">
	                                                           <div class="col-md-6">
																	<div class="form-group">
																		<label class="col-md-3 control-label"><?php echo ADD_NOTIFICATION; ?> </label>
																		<div class="col-md-6">
																				<input type="text" class="form-control dateMilady"
																					name="notification_time" id="notification_time"
																					value="<?php echo  $notification_time; ?>"
																					<?php if($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" && 
                                                                                         		$current_state_code!= "REGISTERED" &&
                                                                                         		$current_state_code!= "PAYMENT_OF_FEES")){?>
                                                                                       			readonly ="readonly"
                                                                                   <?php }?>> <span
																					id="notification_time_tooltip" class="tooltiptext"
																					style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																				<span id="notification_time_tooltip2" class="tooltiptext"
																					style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT_MILADY; ?></span>
																				<span id="notification_time_tooltip3" class="tooltiptext"
																					style="display: none; color: red"><?php echo DATE_CAN_NOT_BE_LESS_THAN_TODAY; ?></span>
																		</div>
																	</div>
																</div>
	    													</div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                	<div class="form-group">
    																	<label class="col-md-4 control-label label-edit" style="max-width: 14%;"><?php echo COMMENTS; ?> </label>
    																	<div class="col-md-8 text-edit comment-control" >
    																		<textarea class="form-control boxsizingBorder" rows="4"
    																		    placeholder="<?php echo WRITE_COMMENT; ?>" cols="50"
    																			id="comment" name="comment" form="commentForm"
    																			<?php if($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" && 
                                                                                             		$current_state_code!= "REGISTERED" &&
                                                                                             		$current_state_code!= "PAYMENT_OF_FEES")){?>
                                                                                           			readonly ="readonly"
                                                                                <?php }?>><?php echo $comment;?></textarea>
    																	</div>
    																</div>
    															</div>
                                                            </div>

	    													<?php if((!($admin_type == "ACCOUNTANT" && ($current_state_code != "START_REGISTRATION_PROCESS" && 
                                                                                         		$current_state_code!= "REGISTERED" &&
                                                                                         		$current_state_code!= "PAYMENT_OF_FEES")))||
	    															($admin_type == "ADMIN") || ($admin_type == "AGENT")){?>
		                                                            <div class="form-actions">
		                                                                <div class="row">
		                                                                    <div class="col-md-9" align="center"
		                                                                         style="margin: auto; width: 100%;">
		                                                                        <button type="submit" class="btn btn-circle blue-sharp"><?php echo SAVE; ?></button>
		    
		                                                                    </div>
		                                                                    
		                                                                </div>
		                                                            </div>
    														<?php }?>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
    												<div class="form-body">
                                                        <?php
                                                        if ($transactions) {
                                                            for ($i = 0; $i < count($transactions); $i ++) {
                                                                ?>
                                                                <div class="form-group">
                                                                    <div class="col-md-12 control-label">
                                                                        <span class="tooltiptext"
                                                                              style="color: black; font-weight: bold"><?= $transactions[$i]["name"] ?></span>
                                                                        <br> <span style="color: black; font-weight: bold"><?php echo STATE . " : "; ?></span>
                                                                        	 <?php 
	                                                                        	 if ($available_states) {
	                                                                        	 	for ($j = 0; $j< count($available_states); $j++) {
	                                                                        	 		$state_code = $available_states [$j] ["state_code"];
	                                                                        	 		$state_name = $available_states [$j] ["state_name_ar"];
                                                                        	 ?>
                                                                                        <?php if ($state_code == $transactions[$i]["state_code"]) { ?>
                                                                                            <span><?= $state_name; ?></span>
                                                                             <?php
                                                                                    		}
                                                                               		 }
	                                                                        	 }
                                                                        	 ?>
                                                                        	 
                                                                        	 
                                                                        <?php if(isset($transactions[$i]["comment"]) && !empty($transactions[$i]["comment"])){?>
	                                                                        <br> <span style="color: black; font-weight: bold"><?php echo COMMENTS . " : "; ?></span>
	                                                                             <span><?= $transactions[$i]["comment"] ?></span> 
                                                                        <?php } ?>
                                                                        
                                                                        <br> <span style="color: black; font-weight: bold"><?php echo IS_CONTACTED . " : "; ?></span>
                                                                        	 <span><?php echo ($transactions[$i]["is_contacted"] == "Y")?YES:NO;?></span> 
                                                                        	 
                                                                        <?php if(isset($transactions[$i]["notification_time"]) && !empty($transactions[$i]["notification_time"])){?>
	                                                                        <br> <span style="color: black; font-weight: bold"><?php echo NOTIFICATION_DATE . " : "; ?></span>
	                                                                        	 <span><?= $transactions[$i]["notification_time"] ?></span>
                                                                        <?php } ?>
                                                                        
                                                                        <br> <span class="tooltiptext" style="color: grey;"><?= $transactions[$i]["date_time"] ?></span>
    
                                                                    </div>
                                                                </div>
                                                                <div class="form-group"></div>
                                                                <?php
                                                            }
                                                        } else {
                                                            ?>
                                                            <div class="form-group"></div>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <form id = "advancSrchForm" action="" method="post">
            <input type="hidden" name ="responsible_name" value="">
            <input type="hidden" name ="responsible_mobile" value="">
            <input type="hidden" name ="responsible_email" value="">
            <input type="hidden" name ="student_name" value="">
            <input type="hidden" name ="current_state_code" value="">
            <input type="hidden" name ="is_contacted" value="">
            <input type="hidden" name ="creation_date" value="">
            <input type="hidden" name ="department" value="">
            <input type="hidden" name ="track" value="">
            <!-- <input type="hidden" name ="DOB_1" value="">
            <input type="hidden" name ="DOB_2" value="">
            <input type="hidden" name ="IBAN" value="">
            <input type="hidden" name ="IBAN_source" value=""> -->
            <input type="hidden" name ="knowing_way" value="">
            <input type="hidden" name ="grade" value="">
            <input type="hidden" name ="old_school" value="">
            <input type="hidden" name ="notification_time" value="">
            <input type="hidden" name ="agent_id" value="">
            <input type="hidden" name ="comment" value="">
        </form>
        
        <form id = "quickSrchForm" action="" method="post">
            <input type="hidden" name ="query" value="">
        </form>
        
 <?php $this->load->view('utils/schoolFooter'); ?>

<!-- BEGIN CORE PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-table/bootstrap-table.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/profile.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>

	<script type="text/javascript"
        src="<?php echo base_url() ?>js/bootbox.min.js"></script>
	<!-- END THEME LAYOUT SCRIPTS -->

<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135637603-1"></script>

<script>

  window.dataLayer = window.dataLayer || [];

  function gtag(){dataLayer.push(arguments);}

  gtag('js', new Date());

 

  gtag('config', 'UA-135637603-1');

</script>

</body>
</html>