<!DOCTYPE html>

<?php
// session_start ();
$this->load->view('localization/lang');
$this->load->view('localization/txt');
defineLocale ($lang);
defineStrings ($lang);
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>

<link rel="icon" href="<?php echo base_url()?>images/header_logo.png">
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<!-- END HEAD -->

<style type="text/css">
	div.stars {
	  width: 270px;
	  display: inline-block;
	}
	
	input.star { display: none; }
	
	label.star {
	  float: right;
	  padding: 10px;
	  font-size: 36px;
	  color: #444;
	  transition: all .2s;
	}
	
	input.star:checked ~ label.star:before {
	  content: '\f005';
	  color: #FD4;
	  transition: all .25s;
	}
	
	input.star-5:checked ~ label.star:before {
	  color: #FE7;
	  text-shadow: 0 0 20px #952;
	}
	
	input.star-1:checked ~ label.star:before { color: #F62; }
	
	label.star:hover { transform: rotate(-15deg) scale(1.3); }
	
	label.star:before {
	  content: '\f006';
	  font-family: FontAwesome;
	}
</style>
<body class="page-container-bg-solid">
	<div id="all-page" class="container shadow">
		<div class="row">
			<div class="col-md-13">
                    <?php
                    $this->load->view('utils/schoolHeader');
                    ?>
                        
                       <div class="page-wrapper-row full-height" >
                          <div class="page-wrapper-middle">
            				<!-- BEGIN CONTAINER -->
            				<div class="page-container">
            					<!-- BEGIN CONTENT -->
            					<div class="page-content-wrapper">
            						<!-- BEGIN CONTENT BODY -->
            
            						<!-- BEGIN PAGE CONTENT BODY -->
            						<div class="page-content">
            							<div class="col-md-14">
            										
                                    		<div <?php if ($lang == 'ar') {?> dir="rtl" <?php } else { ?> dir="ltr" <?php }  ?> >
                                    		    <div class="contentPage" style="min-height: 340px; margin:auto; text-align:center;">
                                                    	<div class="webContentPage">
                                    						<h1 id="ContentPlaceHolder1_uxTitlePage"><?php echo EVALUATION_SENT; ?></h1>
                                                   		</div>
                                         		</div>
                                    		</div>
		
            							</div>
            						</div>
            					</div>
            				</div>
            			</div>
         </div>            
</div>
</div>
</div>

	
	<?php $this->load->view('utils/schoolFooter');?>	


<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135637603-1"></script>

<script>

  window.dataLayer = window.dataLayer || [];

  function gtag(){dataLayer.push(arguments);}

  gtag('js', new Date());

 

  gtag('config', 'UA-135637603-1');

</script>


</body>

</html>