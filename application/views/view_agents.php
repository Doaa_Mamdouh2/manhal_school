
<?php
// session_start();
$this->load->view('localization/lang');
$this->load->view('localization/txt');
defineLocale();
defineStrings();

?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title><?php echo SCHOOL_SITE_TITLE_NEW; ?></title>
<link rel="icon" href="<?php echo base_url()?>images/header_logo.png">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="author" />

<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-table/bootstrap-table.min.css"
	rel="stylesheet" type="text/css" />

<link
	href="<?= base_url() ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css"
	rel="stylesheet" type="text/css" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?= base_url() ?>assets/css/student_registrations.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/global/plugins/morris/morris.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/global/plugins/fullcalendar/fullcalendar.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?= base_url() ?>assets/global/css/components-rtl.min.css"
	rel="stylesheet" id="style_components" type="text/css" />
<link href="<?= base_url() ?>assets/global/css/plugins-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link
	href="<?= base_url() ?>assets/layouts/layout3/css/layout-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?= base_url() ?>assets/layouts/layout3/css/themes/default-rtl.min.css"
	rel="stylesheet" type="text/css" id="style_color" />
<link
	href="<?= base_url() ?>assets/layouts/layout3/css/custom-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon"
	href="<?= base_url() ?>assets/layouts/layout3/img/shortcut-icon.png" />



<script type="text/javascript">

            // pagination
            var page_size = 30;
            var allRecordsMap = new Array();
            allRecordsMap[<?= $current_page ?>] = <?php echo json_encode($agents) ?>;
            var total_count = '<?= $total_count ?>';
            var clickedPage = <?= $current_page ?>;
           
            sessionStorage.setItem("clickedPage", "<?= $current_page ?>");

            function uploadRecordsPerPage(clickedPage) {
                sessionStorage.setItem("clickedPage", clickedPage + "");
                // console.log(clickedPage+" "+allRecordsMap[clickedPage]);
                if (true || allRecordsMap[clickedPage] == null) {
                	var url = "";
                    url = '<?php echo site_url("admin_panel/view_agents") ?>' + '/0' + '/' + page_size + '/' + clickedPage ;
                    console.log('url='+url);
                    var data = {};
                    var type = 'GET';
                    console.log(data);
                    $.ajax({
                        url: url,
                        data: data,
                        type: type,
                        success: function (response) {
                               console.log(response);
                            var responseObject = JSON.parse(response);
                            allRecordsMap[clickedPage] = responseObject["agents"];
                            sessionStorage.setItem("total_count", responseObject["total_count"]);
                            adjustPage(responseObject["total_count"], clickedPage);

                        },
                        error: function (request, status, error) {
                            alert(request.responseText);
                        }
                    });

                } else {
                    getNextBatch(clickedPage);
                    adjustPage(total_count, clickedPage);
                }
            }
            function adjustPage(count, clickedPage) {
                total_count = count;
                if (total_count <= 30) {
                    $("#pagination_div").css('display', 'none');
                } else {
                    var page_num = Math.ceil(total_count / page_size);
                    $("#pagination_div").css('display', 'block');
                    $("#total_pages_num").text(page_num);
                    $("#desired_page_num").val(clickedPage);
                    if (clickedPage == 1) {
                        $("#next_button").prop('disabled', false);
                        $("#prev_button").prop('disabled', true);
                    } else if (clickedPage == page_num) {
                        $("#next_button").prop('disabled', true);
                        $("#prev_button").prop('disabled', false);
                    } else {
                        $("#next_button").prop('disabled', false);
                        $("#prev_button").prop('disabled', false);
                    }
                }
                getNextBatch(clickedPage);

            }

            function leftPad(number, targetLength) {
                var output = number + '';
                while (output.length < targetLength) {
                    output = '0' + output;
                }
                return output;
            }
            
            function getNextBatch(clickedPage) {
                agents = allRecordsMap[clickedPage];
                $('#agents').empty();
                for (var i in agents) {
                    agent_url = "<?= site_url('Admin_panel/add_edit_agent/') ?>" + agents[i]["admin_id"];
                    code = "agents";
                    $('#agents').append('<tr>' +
                    		'<td style="display: none;">' + agents[i]["admin_id"] + '</td>' +
                            '<td>' + agents[i]["name"] + '</td>' +
                            '<td>' + agents[i]["mobile_number"] + '</td>' +
                            '<td>' + agents[i]["email"] + '</td>' +
                            '<td>' + agents[i]["user_name"] + '</td>' +
                            
                            '<td><a href="' + agent_url + '" class="btn btn-outline btn-circle btn-sm blue">' +
                            '<i class="fa fa-edit"></i> <?php echo EDIT; ?> </a>'+

                            //'<a id="delBtn" href="javascript:delBtnClick(' + agents[i]["admin_id"] + ', ' + code + ');" class="btn btn-outline btn-circle btn-sm red">' +
                            //'<i class="fa fa-trash-o"></i> <?php //echo DELETE; ?> </a>'+

                            '</td></tr>'
                            );
                }
                $('html, body').animate({
                    scrollTop: $("#contentPage").offset().top
                }, 500);
                $('.make-switch').bootstrapSwitch();
                $('#loading').addClass('hidden');
            }


            function get_prev_page() {
                var current_page = parseInt($("#desired_page_num").val());
                var newpage = (current_page - 1);
                paginate_on_page(newpage);
            }

            function get_next_page() {
                var current_page = parseInt($("#desired_page_num").val());
                var newpage = (current_page + 1);
                paginate_on_page(newpage);
            }

            function paginate_on_page(newpage) {
                $('#loading').removeClass('hidden');
                var no_of_pages = Math.ceil(total_count / page_size);
                if (newpage == "" || parseInt(newpage) <= 0) {
                	newpage = 1;
                }
                if (parseInt(newpage) >= parseInt(no_of_pages)) {
                    newpage = no_of_pages;
                    $("#next_button").prop('disabled', true);
                } else {
                    $("#next_button").prop('disabled', false);
                }
                if (parseInt(newpage) > 1) {
                    $("#prev_button").prop('disabled', false);
                } else {
                    $("#prev_button").prop('disabled', true);
                }
                $("#calculated_current_page").val(newpage);
                $("#desired_page_num").val(newpage);
                uploadRecordsPerPage(newpage);
            }
            $(document).on('keyup', '#desired_page_num', function (event) {
                if (event.keyCode == 13) {
                    var newpage = $('#desired_page_num').val();
                    paginate_on_page(newpage);
                }
            });

        	function apply_quick_search() {
        		var searchVal = $("#quick_search_input").val().trim();
        		$('#loading').removeClass('hidden');
        		$('#search_text').removeClass('hidden');
        		document.getElementById('quickSearchForm').submit();
        	}
  			
        </script>

<style type="text/css">
.quick-search {
	padding-top: 9px !important;
	padding-bottom: 9px !important;
	background: #E5E5E5;
	border: #E5E5E5;
}

.btn-searchColor {
	padding-top: 9px !important;
	padding-bottom: 9px !important;
}
</style>

<style>
.scrollable-custom-class {
	max-width: 80% !important;
	margin: auto;
	text-align: center;
	-webkit-overflow-scrolling: touch;
}
</style>

<style type="text/css">
.table {
	max-width: none !important;
}

.table-scrollable {
	-webkit-overflow-scrolling: touch;
}

.bootstrap-switch-container {
	direction: ltr;
}
</style>


</head>
<!-- END HEAD -->

<body class="page-container-bg-solid">
	<div id="all-page" class="container shadow">
		<div class="row">
			<div class="col-md-13">
                    <?php
                    $this->load->view('utils/schoolHeaderAdmin');
                    ?>

                    <div class="page-wrapper-row full-height">
					<div class="page-wrapper-middle">
						<?php $this->load->view('utils/delete_alert'); ?>
						<!-- BEGIN CONTAINER -->
						<div class="page-container">
							<!-- BEGIN CONTENT -->
							<div class="page-content-wrapper">
								<!-- BEGIN CONTENT BODY -->
								<div id="loading" class="hidden">
									<img id="loading-image"
										src="<?= base_url() ?>images/Ajax-loader2.gif"
										alt="Loading...">
								</div>
								<!-- BEGIN PAGE HEAD-->
								<div class="container">
									<div class="page-content-inner">

										<!-- BEGIN PAGE TITLE -->
										<div id="contentPage" class="portlet light portlet-fit ">
											<div class="portlet-title">
												<div class="caption">
													<div class="page-title"
														style="padding-top: 0px !important; padding-bottom: 7px !important;">
														<h2>
															<span style="color: #0E4278 !important;"><?php echo AGENTS; ?>
                                                                    <small>
                                                                         <?php echo '(' . ($total_count). ')'; ?> 
                                                                     </small></span>
														</h2>

													</div>
												</div>
												
												<div class="actions">
													<div class="btn-group">
														<a class="btn dark btn-outline btn-circle btn-sm"
															href="<?php echo site_url('admin_panel/add_edit_agent/')?>"
															style="background-color: #3598dc; color: white;"
															data-hover="dropdown" data-close-others="true"> <?php echo INSERT; ?>
														</a>
													</div>
												</div>
											</div>
										</div>


										<!-- BEGIN PAGE CONTENT BODY -->
										<div class="page-content">
											<div class="portlet-body">
												<div class="table-scrollable" class="scrollable-custom-class">
													<table class="table table-bordered table-hover">
														<thead>
															<tr>
																<th><?php echo AGENT_NAME; ?></th>
																<th><?php echo MOBILE_NUMBER; ?></th>
																<th><?php echo EMAIL; ?></th>
																<th><?php echo USERNAME; ?></th>
																<!--  <th><?php echo PASSWORD; ?></th>-->
																<th></th>
															</tr>
														</thead>
														<tbody id="agents">
                                                                <?php
                                                                if ($agents) {
                                                                	for ($i = 0; $i < count($agents); $i ++) {
                                                                		$agent_id = $agents[$i]["admin_id"];
                                                                		$name = $agents[$i]["name"];
                                                                		$mobile_number = $agents[$i]["mobile_number"];
                                                                		$email = $agents[$i]["email"];
                                                                		$user_name = $agents[$i]["user_name"];
                                                                		//$password = $agents[$i]["password"];
                                                                       
                                                                        ?>
                                                                  <tr>
                                                                <td style="display: none;"><?= $agent_id?></td>
																<td><?= $name?></td>
																<td><?= $mobile_number?></td>
																<td><?= $email?> </td>
																<td><?= $user_name ?></td>
																<!--<td><?= $password ?></td>-->
																
                                                              	<td><a href="<?= site_url('Admin_panel/add_edit_agent/' . $agent_id) ?>"
																				class="btn btn-outline btn-circle btn-sm blue"> 
																				<i class="fa fa-edit"></i> <?php echo EDIT; ?>
																	</a>
																	<!--  <a id="delBtn"
																		href="javascript:delBtnClick('<?php echo $agent_id?>', 'agents');"
																		class="btn btn-outline btn-circle dark btn-sm red"> 
																		<i class="fa fa-trash-o"></i> <?php echo DELETE; ?>
																	</a>-->
																</td>

															</tr>	
                                                                        <?php
                                                                    }
                                                                } else {
                                                                    ?><tr
																class="HeadBr2">
																<td colspan="16" style="color: red" align="center"><h4>
                                                                                <?php echo NO_RECORDS_FOUND; ?>
                                                                            </h4></td>
															</tr> <?php } ?>
                                                            </tbody>
													</table>
												</div>
												<!-- END BORDERED TABLE PORTLET-->


												<input type="hidden" name="calculated_current_page"
													id="calculated_current_page" value="1"> <input
													type="hidden" name="no_of_pages" id="no_of_pages"
													value="<?php echo $total_count; ?>">


														<div id="pagination_form" style="  margin: auto; text-align: center;">
															<input type="hidden" name="current_page"
																id="current_page" value="1"> <input type="button"
																class="bold btn blue-sharp" style="width: 7rem !important;"
																id="prev_button" <?php if ($current_page == 1) { ?>
																disabled <?php } ?> onClick="javascript:get_prev_page()"
																value="<?php echo PREVIOUS; ?>" /> <label><?php echo PAGE_NUM; ?>:</label>
															<input id="desired_page_num" class="no-spinners"
																type="number" style="width: 50px; text-align: center;"
																value="<?= $current_page ?>" min="1" />/ <span
																id="total_pages_num"><?= ceil($total_count / 30) ?></span>
															<input type="button" class="bold btn blue-sharp"
																style="width: 7rem !important;" id="next_button"
																<?php if ($current_page == ceil($total_count / 30)) { ?>
																disabled <?php } ?> onClick="javascript:get_next_page()"
																value="<?php echo NEXT; ?>" />
														</div>
														
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
</div>
</div>
	
	<?php $this->load->view('utils/schoolFooter');?>	
    <!--[if lt IE 9]>
    <script src="<?= base_url() ?>assets/global/plugins/respond.min.js"></script>
    <script src="<?= base_url() ?>assets/global/plugins/excanvas.min.js"></script> 
    <script src="<?= base_url() ?>assets/global/plugins/ie8.fix.min.js"></script> 
    <![endif]-->


			<!-- BEGIN CORE PLUGINS -->
			<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
				type="text/javascript"></script>
			<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
				type="text/javascript"></script>
			<!-- END CORE PLUGINS -->
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<script src="<?= base_url() ?>assets/global/plugins/moment.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/morris/morris.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/morris/raphael-min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/counterup/jquery.waypoints.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/counterup/jquery.counterup.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/fullcalendar/fullcalendar.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/flot/jquery.flot.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/flot/jquery.flot.resize.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/flot/jquery.flot.categories.min.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
				type="text/javascript"></script>

			<script
				src="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js"
				type="text/javascript"></script>
			<script
				src="<?= base_url() ?>assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js"
				type="text/javascript"></script>

			<script
				src="<?=base_url()?>assets/global/plugins/bootstrap-table/bootstrap-table.min.js"
				type="text/javascript"></script>

			<script src="<?= base_url() ?>assets/pages/scripts/dashboard.min.js"
				type="text/javascript"></script>

			<!-- END PAGE LEVEL PLUGINS -->
			<!-- BEGIN THEME GLOBAL SCRIPTS -->
			<script src="<?=base_url()?>assets/global/scripts/app.min.js"
				type="text/javascript"></script>
			<!-- END THEME GLOBAL SCRIPTS -->
			<!-- BEGIN PAGE LEVEL SCRIPTS -->
			<script src="<?=base_url()?>assets/pages/scripts/profile.min.js"
				type="text/javascript"></script>
			<!-- END PAGE LEVEL SCRIPTS -->
			<!-- BEGIN THEME LAYOUT SCRIPTS -->
			<script
				src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
				type="text/javascript"></script>
			<script
				src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
				type="text/javascript"></script>

			<!-- END THEME LAYOUT SCRIPTS -->

			<script>
      $(function () {
    
  		$('#quickSearchForm').on('keyup keypress', function(e) {
			  var keyCode = e.keyCode || e.which;
			  if (keyCode === 13) {
	        		var searchVal = $("#quick_search_input").val().trim();
	        		apply_quick_search();
			}
    	});
        $(document).on('keyup','#quick_search_input',function(event) { 
      	    if(event.keyCode == 13){
      	    	apply_quick_search();
      	    }
      	});
    		    
      });

  
		
  </script>

<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135637603-1"></script>

<script>

  window.dataLayer = window.dataLayer || [];

  function gtag(){dataLayer.push(arguments);}

  gtag('js', new Date());

 

  gtag('config', 'UA-135637603-1');

</script>

</body>

</html>
