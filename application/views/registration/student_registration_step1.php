<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
// session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5WN5RLJ');</script>
<!-- End Google Tag Manager -->

<link rel="icon" href="<?php echo base_url()?>images/header_logo.png">


<script type="text/javascript">
	
function checkValid() {
	var responsible_name = document.forms["myForm"]["responsible_name"].value;
	var responsible_mobile = document.forms["myForm"]["responsible_mobile"].value;
	var responsible_email = document.forms["myForm"]["responsible_email"].value;

	if (responsible_name== "" ) {
		$("#responsible_name_tooltip").css("display","block");
		$("#responsible_name_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#responsible_name_tooltip").css("display","none");
		$("#responsible_name_tooltip").css("visibility","none");
	}
	
	if (responsible_mobile== "" ) {
		$("#responsible_mobile_tooltip").css("display","block");
		$("#responsible_mobile_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#responsible_mobile_tooltip").css("display","none");
		$("#responsible_mobile_tooltip").css("visibility","none");
	}

	if (responsible_mobile.length > 15) {
		$("#responsible_mobile_tooltip2").css("display","block");
		$("#responsible_mobile_tooltip2").css("visibility","visible");
		return false;
	} else{
		$("#responsible_mobile_tooltip2").css("display","none");
		$("#responsible_mobile_tooltip2").css("visibility","none");
	}

	
	if (responsible_email == "" || !validateEmail(responsible_email)) {
		$("#responsible_email_tooltip").css("display","block");
		$("#responsible_email_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#responsible_email_tooltip").css("display","none");
		$("#responsible_email_tooltip").css("visibility","none");
	}
	
	$("#btnSubmit").attr("disabled", true);
}


function validateEmail(email) 
{
	//var re = /\S+@\S+\.\S+/;
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

function numberKeyDown(e) {
	var keynum;
    if(window.event) { // IE                    
      keynum = e.keyCode;
    } else if(e.which){ // Netscape/Firefox/Opera                   
      keynum = e.which;
    }

    //console.log(keynum);
    
 // Allow: backspace, delete, tab, escape, enter and . 
    if ($.inArray(keynum, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (keynum === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
        // Allow: Ctrl+C, Command+C
        (keynum === 67 && (e.ctrlKey === true || e.metaKey === true)) || 
        // Allow: Ctrl+X, Command+X
        (keynum === 88 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (keynum >= 35 && keynum <= 37) ||
        // + for mobile numbers
        (keynum === 107 )) {
             // let it happen, don't do anything
             return;
    }
    
    if(((e.shiftKey || (keynum < 48 || keynum > 57)) && (keynum < 96 || keynum > 105)) || (keynum >= 38 && keynum <= 40)){
		 e.preventDefault();
	}
}

</script>
<meta charset="utf-8" />
<title><?php echo SCHOOL_SITE_TITLE_NEW; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="author" />


<!-- Global site tag (gtag.js) - Google Ads: 806135812 --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-806135812"></script>


<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-806135812');
  gtag('config', 'UA-135637603-1');

</script>

<!-- Twitter universal website tag code -->
<script>
    !function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
    },s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
    a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
    // Insert Twitter Pixel ID and Standard Event data below
    twq('init','nxhdl');
    twq('track','PageView');
</script>
<!-- End Twitter universal website tag code -->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '243743377195546');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=243743377195546&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Event snippet for Reg conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-806135812/pGc4COnVkYEBEITQsoAD'});
</script>

</head>

<!-- END HEAD -->


<body class="page-container-bg-solid">
	<div class="container shadow">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5WN5RLJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->	   

<?php

$this->load->view ( 'utils/schoolHeader2' );

?>
		<div class="page-wrapper-row full-height">
			<div class="page-wrapper-middle">
				<!-- BEGIN CONTAINER -->
				<div class="page-container">
					<!-- BEGIN CONTENT -->
					<div class="page-content-wrapper">
						<!-- BEGIN CONTENT BODY -->
						<!-- BEGIN PAGE HEAD-->
						<div class="container">
								<!-- BEGIN PAGE TITLE -->
								<div class="page-title" style="padding-top: 0px !important; padding-bottom: 7px !important;">
									<h2>
										<span style="color: #0E4278 !important;"><?php echo REG_PAGE_TITLE; ?></span>
									</h2>
								</div>
								<!-- END PAGE TITLE -->
						</div>

						<!-- BEGIN PAGE CONTENT BODY -->
						<div class="page-content">
								<div class="col-md-14">
									<div class="portlet box green">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-gift"></i>
												<?php
												echo STEP_1_TITLE;
												?>
											</div>
										</div>
										<div class="portlet-body form">
											<br> <br>
											<!-- BEGIN FORM-->
											<form id="myForm" role="form" method="post"
												onsubmit="javascript:return checkValid();"
												enctype="multipart/form-data"
												action="<?=site_url('Registration_controller/execute_step1/')?>"
												class="form-horizontal">
												<div class="form-body">

													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo RESPONSIBLE_NAME; ?><span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="responsible_name" id="responsible_name"
																		onfocus="javascript:removeTooltip('responsible_name_tooltip');">
																	<span id="responsible_name_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo RESPONSIBLE_MOBILE; ?><span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="text" class="form-control  no-spinners" onkeydown="numberKeyDown(event)"
																		name="responsible_mobile" id="responsible_mobile"
																		onfocus="javascript:removeTooltip('responsible_mobile_tooltip');">
																	<span id="responsible_mobile_tooltip"
																		class="tooltiptext" style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																	<span id="responsible_mobile_tooltip2"
        																	class="tooltiptext" style="display: none; color: red"><?php echo MOBILE_NUMBER_INVALID; ?></span>
																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo RESPONSIBLE_EMAIL; ?><span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="responsible_email" id="responsible_email"
																		onfocus="javascript:removeTooltip('responsible_email_tooltip');">
																	<span id="responsible_email_tooltip"
																		class="tooltiptext" style="display: none; color: red"><?php echo INVALID_EMAIL; ?></span>
																</div>
															</div>
														</div>
													</div>

													<div class="form-actions">
														<div class="row">
															<div class="col-md-9" align="center"
																style="margin: auto; width: 100%;">
																<button id="btnSubmit" type="submit" class="btn btn-circle green"><?php
																	echo NEXT;
																	
																	?></button>

															</div>
														</div>
													</div>

												</div>
											</form>
											<!-- END FORM-->
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
    <?php $this->load->view('utils/schoolFooter2');?>

	<!-- BEGIN CORE PLUGINS -->
<!--	<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/profile.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->


<!-- Snap Pixel Code -->

<script type='text/javascript'>

	(function(e,t,n){if(e.snaptr)return;var a=e.snaptr=function()
	
	{a.handleRequest?a.handleRequest.apply(a,arguments):a.queue.push(arguments)};
	
	a.queue=[];var s='script';r=t.createElement(s);r.async=!0;
	
	r.src=n;var u=t.getElementsByTagName(s)[0];
	
	u.parentNode.insertBefore(r,u);})(window,document,
	
	'https://sc-static.net/scevent.min.js');
	
	 
	
	snaptr('init', 'f66ada9e-a2f7-4398-8706-4cacaa5c5431', {
	
	'user_email': '__INSERT_USER_EMAIL__'
	
	});
	
	snaptr('track', 'PAGE_VIEW');
	
</script>

<!-- End Snap Pixel Code -->
</body>

</html>