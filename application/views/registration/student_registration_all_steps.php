<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
// session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>

<meta charset="utf-8" />
<title><?php echo SCHOOL_SITE_TITLE_NEW; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="author" />
<link href="<?= base_url() ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />

		
<script type="text/javascript">
    
function execute_step1(){
    var valid = checkValid_step1();
    if (valid){
        var datastring = $("#myForm_step1").serialize();
		var url = "<?php echo site_url('Registration_controller/execute_step1_ajax/')?>";
		$('#loading').removeClass('hidden');
		//$('#loading').fadeIn().delay(1000).fadeOut();
		$.post(url, datastring, function(data) {
	       var results = JSON.parse(data);
	       var student_registration_id = results.student_registration_id;
	       console.log(student_registration_id);
	       $("#student_registration_id").val(student_registration_id);
	       $("#step1_div").addClass("hidden");
	       $("#step2_div").removeClass("hidden");
	       $('#loading').addClass('hidden');
		});
    }
}
	
function execute_step2(){
    var valid = checkValid_step2();
    if (valid){
        var datastring = $("#myForm_step2").serialize();
		var url = "<?php echo site_url('Registration_controller/execute_step2_ajax/')?>";
		$('#loading').removeClass('hidden');
		//$('#loading').fadeIn().delay(1000).fadeOut();
		$.post(url, datastring, function(data) {
	       var results = JSON.parse(data);
	       var sent = results.sent;
           var student_gender = results.student_gender;
	       console.log(sent + "  " + student_gender);
	       $("#step2_div").addClass("hidden");
	       $("#step3_div").removeClass("hidden");
	       if(student_gender == "M"){
	           $("#male_id").removeClass("hidden");
	       }else{
	           $("#female_id").removeClass("hidden");
	       }
	       $('#loading').addClass('hidden');
		});
    }
}



function checkValid_step1() {
	var responsible_name = document.getElementById("responsible_name").value;
	var responsible_mobile = document.getElementById("responsible_mobile").value;
	var responsible_email = document.getElementById("responsible_email").value;

	if (responsible_name== "" ) {
		$("#responsible_name_tooltip").css("display","block");
		$("#responsible_name_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#responsible_name_tooltip").css("display","none");
		$("#responsible_name_tooltip").css("visibility","none");
	}
	
	if (responsible_mobile== "" ) {
		$("#responsible_mobile_tooltip").css("display","block");
		$("#responsible_mobile_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#responsible_mobile_tooltip").css("display","none");
		$("#responsible_mobile_tooltip").css("visibility","none");
	}
	
	if (responsible_email == "" || !validateEmail(responsible_email)) {
		$("#responsible_email_tooltip").css("display","block");
		$("#responsible_email_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#responsible_email_tooltip").css("display","none");
		$("#responsible_email_tooltip").css("visibility","none");
	}
	return true;
}


function validateEmail(email) 
{
	//var re = /\S+@\S+\.\S+/;
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

function checkValid_step2() {

	var DOB_1 = $('#DOB_1').val();
	if(DOB_1 != ""){
		var isValidD = isValidMiladyDate(DOB_1);
	   	if(!isValidD){
			$("#DOB_1_invalid_tooltip").css("display","block");
			$("#DOB_1_invalid_tooltip").css("visibility","visible");
	   			
	   		return false;
	   	}else{
			$("#DOB_1_invalid_tooltip").css("display","none");
			$("#DOB_1_invalid_tooltip").css("visibility","none");
	   	}
	}

	var DOB_2 = $('#DOB_2').val();
	if(DOB_2 != ""){
		var isValidD = isValidDate(DOB_2);
	   	if(!isValidD){
			$("#DOB_2_invalid_tooltip").css("display","block");
			$("#DOB_2_invalid_tooltip").css("visibility","visible");
	   			
	   		return false;
	   	}else{
			$("#DOB_2_invalid_tooltip").css("display","none");
			$("#DOB_2_invalid_tooltip").css("visibility","none");
	   	}
	}
	return true;
}


function miladyHandler() {
	var DOB_1 = $('#DOB_1').val();
	if(DOB_1 != ""){
		var isValidD = isValidMiladyDate(DOB_1);
	   	if(!isValidD){
	   		return false;
	   	}  	
	}
	
	var str_array = DOB_1.split('/');
	var miladyDate =  jqOld.calendars.instance('gregorian','ar').newDate();
	var hijriDate =  jqOld.calendars.instance('ummalqura','ar').newDate();
	try {
		miladyDate = miladyDate.date(str_array[2], str_array[0], str_array[1]);
		hijriDate = hijriDate.fromJSDate(miladyDate.toJSDate());
		//alert(hijriDate);
		$("#DOB_2").val(hijriDate.formatDate());
	}
	catch(err) {
	   return false;
	}
}

function hijriHandler() {
	var DOB_2 = $('#DOB_2').val();
	if(DOB_2 != ""){
		var isValidD = isValidDate(DOB_2);
	   	if(!isValidD){
	   		return false;
	   	}
	}

	try {
	 $.ajax({
		  type: "post",
		  url: '<?php echo site_url('Registration_controller/convertToMilady'); ?>',
		  cache: false,
		  data: { 
		      'hijriDate': DOB_2
		  },
		  success: function(json){      
		  try{  
		   var miladyDate = jQuery.parseJSON(json);
		   //reformat from 2017-05-10 to 05/16/2017
		   if(miladyDate !=''){
			   var str_array = miladyDate.split('-');
			   var year = str_array[0];
			   var month = str_array[1];
			   var day = str_array[2];
			   miladyDate = month+"/"+day+"/"+year;
		   }
		   $("#DOB_1").val(miladyDate);
		   //alert( 'Success');
		  }catch(e) {  
		   //alert('Exception while request..');
		  }  
		  },
		  error: function(){      
		   //alert('Error while request..');
		  }
		 });
	}
	catch(err) {
	   return false;
	}
}


function isValidDate(dateVar){
	//Format: yyyy/mm/dd
	var dateReg = /^\d{4}\/\d{2}\/\d{2}$/;
	var isV = dateReg.test(dateVar);
	if(!isV){
		return false;
	} 
	
	var str_array = dateVar.split('/');
	var hijriDate =  jqOld.calendars.instance('ummalqura','ar').newDate();
	try {
	   hijriDate = hijriDate.date(str_array[0], str_array[1], str_array[2]);
	}
	catch(err) {
	   return false;
	}
	return true;
}

function isValidMiladyDate(dateVar){
	//Format: yyyy/mm/dd
	var dateReg = /^\d{2}\/\d{2}\/\d{4}$/;
	var isV = dateReg.test(dateVar);
	if(!isV){
		return false;
	} 
	
	var str_array = dateVar.split('/');
	var miladyDate =  jqOld.calendars.instance('gregorian','ar').newDate();
	try {
		miladyDate = miladyDate.date(str_array[2], str_array[0], str_array[1]);
	}
	catch(err) {
	   return false;
	}
	return true;
}

</script>
<style type="text/css">
	#thanksDiv{
	    padding: 20px !important;
    	font-size: larger;
    	line-height: normal;
	}
	#loading {
	   width: 100%;
	   height: 100%;
	   top: 0;
	   left: 0;
	   position: fixed;
	   display: block;
	   opacity: 0.7;
	   background-color: #fff;
	   z-index: 1059;
	   text-align: center;
	}

	#loading-image {
	  position: relative;
	  top: 35%;
	  /*left: 40%;*/
	  z-index: 1060;
	  margin: auto;
	  text-align: center;
	}
    
</style>


<link rel="stylesheet" type="text/css"
	href="<?=base_url()?>assets/css/jquery.calendars.picker.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?=base_url()?>assets/js/jquery.plugin.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.all.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.plus.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.ummalqura.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.ummalqura-ar.js"></script>

<script>
var jqOld = jQuery.noConflict();
jqOld( function() {
	jqOld( "input.date" ).calendarsPicker({calendar: jqOld.calendars.instance('ummalqura','ar')});
  } );
 jqOld( function() {
	    jqOld( "input.dateMilady" ).calendarsPicker({calendar: jqOld.calendars.instance('gregorian','ar')});
  } );
 </script>


</head>
<!-- END HEAD -->

<body class="page-container-bg-solid">


    <div id="loading" class="hidden">
        <img id="loading-image" src="<?php echo base_url(); ?>images/Ajax-loader2.gif" alt="LOADING..." />
    </div>
	<div class="container shadow">
<?php
$this->load->view ( 'utils/schoolHeader' );
?>
		<div class="page-wrapper-row full-height">
			<div class="page-wrapper-middle">
				<!-- BEGIN CONTAINER -->
				<div class="page-container">
					<!-- BEGIN CONTENT -->
					<div class="page-content-wrapper">
						<!-- BEGIN CONTENT BODY -->
						<!-- BEGIN PAGE HEAD-->
						<div class="container">
								<!-- BEGIN PAGE TITLE -->
								<div class="page-title" style="padding-top: 0px !important; padding-bottom: 7px !important;">
									<h2>
										<span style="color: #0E4278 !important;"><?php echo REG_PAGE_TITLE; ?></span>
									</h2>
								</div>
								<!-- END PAGE TITLE -->
						</div>

						<!-- BEGIN PAGE CONTENT BODY -->
						<div class="page-content">
								<div class="col-md-14">
									<!-- start Step 1-->
								    <div id="step1_div">
									<div class="portlet box green">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-gift"></i>
												<?php
												echo STEP_1_TITLE;
												?>
											</div>
										</div>
										<div class="portlet-body form">
											<br> <br>
											<!-- BEGIN FORM-->
											<form id="myForm_step1" role="form" method="post"
												onsubmit="javascript:return checkValid_step1();"
												enctype="multipart/form-data"
												class="form-horizontal">
												<div class="form-body">

													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo RESPONSIBLE_NAME; ?><span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="responsible_name" id="responsible_name"
																		onfocus="javascript:removeTooltip('responsible_name_tooltip');">
																	<span id="responsible_name_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo RESPONSIBLE_RELATIONSHIP; ?></label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="responsible_relationship"
																		id="responsible_relationship"
																		onfocus="javascript:removeTooltip('responsible_relationship_tooltip');">
																	<span id="responsible_relationship_tooltip"
																		class="tooltiptext" style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo RESPONSIBLE_MOBILE; ?><span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="responsible_mobile" id="responsible_mobile"
																		onfocus="javascript:removeTooltip('responsible_mobile_tooltip');">
																	<span id="responsible_mobile_tooltip"
																		class="tooltiptext" style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo RESPONSIBLE_EMAIL; ?><span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="responsible_email" id="responsible_email"
																		onfocus="javascript:removeTooltip('responsible_email_tooltip');">
																	<span id="responsible_email_tooltip"
																		class="tooltiptext" style="display: none; color: red"><?php echo INVALID_EMAIL; ?></span>
																</div>
															</div>
														</div>
													</div>

													<div class="form-actions">
														<div class="row">
															<div class="col-md-9" align="center"
																style="margin: auto; width: 100%;">
																<button type="button" onClick="javascript:execute_step1()" class="btn btn-circle green"><?php
																	echo NEXT;
																	
																	?></button>

															</div>
														</div>
													</div>

												</div>
											</form>
											<!-- END FORM-->
										</div>
									</div>
									</div>
									<!-- END Step 1-->
									<!-- start Step 2-->
								    <div id="step2_div" class="hidden">
								        <div class="portlet box green">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-gift"></i>
												<?php
												echo STEP_2_TITLE;
												?>
											</div>
										</div>
										<div class="portlet-body form">
											<br> <br>
											<!-- BEGIN FORM-->
											<form id="myForm_step2" role="form" method="post"
												onsubmit="javascript:return checkValid();"
												enctype="multipart/form-data"
												class="form-horizontal">

												<input type="hidden" id="student_registration_id"
													name="student_registration_id"
													value="">

												<div class="form-body">

													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo STUDENT_NAME; ?></label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="student_name" id="student_name"
																		onfocus="javascript:removeTooltip('student_name_tooltip');">
																	<span id="student_name_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo STUDENT_GENDER; ?></label>
																<div class="col-md-6">
																	<select class="form-control " name="student_gender"
																		onfocus="javascript:removeTooltip('student_gender_tooltip');"
																		id="student_gender">
																		<option value="<?php echo STUDENT_GENDER_VALUE_1; ?>"><?php echo MALE; ?></option>
																		<option value="<?php echo STUDENT_GENDER_VALUE_2; ?>"><?php echo FEMALE; ?></option>
																	</select> <span id="student_gender_tooltip"
																		class="tooltiptext" style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo STUDENT_NATIONALITY; ?></label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="nationality" id="nationality"
																		onfocus="javascript:removeTooltip('nationality_tooltip');">
																	<span id="nationality_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo STUDENT_DOB_MILADY; ?></label>
																<div class="col-md-6">
																	<input type="text" placeholder="" id="DOB_1"
																		class="form-control dateMilady" name="DOB_1"
																		onchange="miladyHandler();"
																		onkeypress="this.onchange();"
																		onpropertychange="this.onchange();"
																		onpaste="this.onchange();" oninput="this.onchange();">
																	<span id="DOB_1_invalid_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
																	<span id="DOB_1_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo STUDENT_DOB_HIJRI; ?></label>
																<div class="col-md-6">
																	<input type="text" placeholder="" id="DOB_2"
																		class="form-control date" name="DOB_2"
																		onchange="hijriHandler()"
																		onkeypress="this.onchange();"
																		onpropertychange="this.onchange();"
																		onpaste="this.onchange();" oninput="this.onchange();">
																	<span id="DOB_2_invalid_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
																	<span id="DOB_2_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo STUDENT_IBAN; ?></label>
																<div class="col-md-6">
																	<input type="text" class="form-control " name="IBAN"
																		id="IBAN"
																		onfocus="javascript:removeTooltip('IBAN_tooltip');"> <span
																		id="IBAN_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo STUDENT_IBAN_SRC; ?></label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="IBAN_source" id="IBAN_source"
																		onfocus="javascript:removeTooltip('IBAN_source_tooltip');">
																	<span id="IBAN_source_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo SCHOOL; ?></label>
																<div class="col-md-6">
																	<select class="form-control " name="school"
																		onfocus="javascript:removeTooltip('school_tooltip');"
																		id="school">
																		<option value=""></option>
																		<option value="<?php echo SCHOOL_VALUE_1; ?>"><?php echo SCHOOL_VALUE_1; ?></option>
																		<option value="<?php echo SCHOOL_VALUE_2; ?>"><?php echo SCHOOL_VALUE_2; ?></option>
																	</select> <span id="school_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo STUDENT_GRADE; ?></label>
																<div class="col-md-6">
																	<select class="form-control " name="grade"
																		onfocus="javascript:removeTooltip('grade_tooltip');"
																		id="grade">
																		<option value=""></option>
																		<option value="<?php echo STUDENT_GRADE_VALUE_1; ?>"><?php echo STUDENT_GRADE_VALUE_1; ?></option>
																		<option value="<?php echo STUDENT_GRADE_VALUE_2; ?>"><?php echo STUDENT_GRADE_VALUE_2; ?></option>
																		<option value="<?php echo STUDENT_GRADE_VALUE_3; ?>"><?php echo STUDENT_GRADE_VALUE_3; ?></option>
																		<option value="<?php echo STUDENT_GRADE_VALUE_4; ?>"><?php echo STUDENT_GRADE_VALUE_4; ?></option>
																		<option value="<?php echo STUDENT_GRADE_VALUE_5; ?>"><?php echo STUDENT_GRADE_VALUE_5; ?></option>
																		<option value="<?php echo STUDENT_GRADE_VALUE_6; ?>"><?php echo STUDENT_GRADE_VALUE_6; ?></option>
																	</select> <span id="grade_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo OLD_SCHOOL; ?></label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="old_school" id="old_school"
																		onfocus="javascript:removeTooltip('old_school_tooltip');">
																	<span id="old_school_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
													</div>

													<div class="form-actions">
														<div class="row">
															<div class="col-md-9" align="center"
																style="margin: auto; width: 100%;">
																<button type="button" class="btn btn-circle green" onClick="javascript:execute_step2()" ><?php
																echo NEXT;
																
																?></button>

															</div>
														</div>
													</div>

												</div>
											</form>
											<!-- END FORM-->
										</div>
									</div>
								    </div>
								    <!-- END Step 2-->
									<!-- start Step 3-->
								    <div id="step3_div" class="hidden">
								        									<div class="portlet box green">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-gift"></i>
												<?php
												echo STEP_3_TITLE;
												?>
											</div>
										</div>
										<div class="portlet-body form">
										<div id="thanksDiv" class="caption">
										    <div id="male_id" class="hidden" >
										        <?php echo STEP3_LINE1_M; ?>
										    </div>
										    <div id="female_id" class="hidden" >
										        <?php echo STEP3_LINE1_F; ?>
										    </div>
										    <br>
												<?php
												echo STEP3_LINE2;
												?>
												
												<?php
												echo STEP3_LINE3;
												?><br>
												<?php
												echo STEP3_LINE4;
												?>
											</div>
											
										
											
										</div>
										
										
												
									</div>
								    </div>
								    <!-- END Step 3-->
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
    <?php $this->load->view('utils/schoolFooter');?>

	<!-- BEGIN CORE PLUGINS -->
<!--	<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/profile.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->


		     
</body>

</html>