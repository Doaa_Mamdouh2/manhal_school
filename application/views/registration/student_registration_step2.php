<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
//session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5WN5RLJ');</script>
<!-- End Google Tag Manager -->

<link rel="icon" href="<?php echo base_url()?>images/header_logo.png">

<link rel="stylesheet" type="text/css"
	href="<?=base_url()?>assets/css/jquery.calendars.picker.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?=base_url()?>assets/js/jquery.plugin.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.all.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.plus.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.ummalqura.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.ummalqura-ar.js"></script>

<script>
var jqOld = jQuery.noConflict();
jqOld( function() {
	jqOld( "input.date" ).calendarsPicker({calendar: jqOld.calendars.instance('ummalqura','ar')});
  } );
 jqOld( function() {
	    jqOld( "input.dateMilady" ).calendarsPicker({calendar: jqOld.calendars.instance('gregorian','ar')});
  } );
 </script>
 
<script type="text/javascript">
	
function checkValid() {

	/* var DOB_1 = $('#DOB_1').val();
	if(DOB_1 != ""){
		var isValidD = isValidMiladyDate(DOB_1);
	   	if(!isValidD){
			$("#DOB_1_invalid_tooltip").css("display","block");
			$("#DOB_1_invalid_tooltip").css("visibility","visible");
	   			
	   		return false;
	   	}else{
			$("#DOB_1_invalid_tooltip").css("display","none");
			$("#DOB_1_invalid_tooltip").css("visibility","none");
	   	}
	}

	var DOB_2 = $('#DOB_2').val();
	if(DOB_2 != ""){
		var isValidD = isValidDate(DOB_2);
	   	if(!isValidD){
			$("#DOB_2_invalid_tooltip").css("display","block");
			$("#DOB_2_invalid_tooltip").css("visibility","visible");
	   			
	   		return false;
	   	}else{
			$("#DOB_2_invalid_tooltip").css("display","none");
			$("#DOB_2_invalid_tooltip").css("visibility","none");
	   	}
	} */

	var other_way = $('#other_way').val();
	var knowing_way = $('#knowing_way').val();
	if (other_way == "" && knowing_way == "OTHER") {
		$("#other_way_tooltip").css("display","block");
		$("#other_way_tooltip").css("visibility","visible");
		return false;
	} else{
		hide_other_tooltip();
	}
}

function hide_other_tooltip(){
	$("#other_way_tooltip").css("display","none");
	$("#other_way_tooltip").css("visibility","none");
}


function miladyHandler() {
	var DOB_1 = $('#DOB_1').val();
	if(DOB_1 != ""){
		var isValidD = isValidMiladyDate(DOB_1);
	   	if(!isValidD){
	   		return false;
	   	}  	
	}
	
	var str_array = DOB_1.split('/');
	var miladyDate =  jqOld.calendars.instance('gregorian','ar').newDate();
	var hijriDate =  jqOld.calendars.instance('ummalqura','ar').newDate();
	try {
		miladyDate = miladyDate.date(str_array[2], str_array[0], str_array[1]);
		hijriDate = hijriDate.fromJSDate(miladyDate.toJSDate());
		//alert(hijriDate);
		$("#DOB_2").val(hijriDate.formatDate());
	}
	catch(err) {
	   return false;
	}
}

function hijriHandler() {
	var DOB_2 = $('#DOB_2').val();
	if(DOB_2 != ""){
		var isValidD = isValidDate(DOB_2);
	   	if(!isValidD){
	   		return false;
	   	}
	}

	try {
	 $.ajax({
		  type: "post",
		  url: '<?php echo site_url('Registration_controller/convertToMilady'); ?>',
		  cache: false,
		  data: { 
		      'hijriDate': DOB_2
		  },
		  success: function(json){      
		  try{  
		   var miladyDate = jQuery.parseJSON(json);
		   //reformat from 2017-05-10 to 05/16/2017
		   if(miladyDate !=''){
			   var str_array = miladyDate.split('-');
			   var year = str_array[0];
			   var month = str_array[1];
			   var day = str_array[2];
			   miladyDate = month+"/"+day+"/"+year;
		   }
		   $("#DOB_1").val(miladyDate);
		   //alert( 'Success');
		  }catch(e) {  
		   //alert('Exception while request..');
		  }  
		  },
		  error: function(){      
		   //alert('Error while request..');
		  }
		 });
	}
	catch(err) {
	   return false;
	}
}




function isValidDate(dateVar){
	//Format: yyyy/mm/dd
	var dateReg = /^\d{4}\/\d{2}\/\d{2}$/;
	var isV = dateReg.test(dateVar);
	if(!isV){
		return false;
	} 
	
	var str_array = dateVar.split('/');
	var hijriDate =  jqOld.calendars.instance('ummalqura','ar').newDate();
	try {
	   hijriDate = hijriDate.date(str_array[0], str_array[1], str_array[2]);
	}
	catch(err) {
	   return false;
	}
	return true;
}

function isValidMiladyDate(dateVar){
	//Format: yyyy/mm/dd
	var dateReg = /^\d{2}\/\d{2}\/\d{4}$/;
	var isV = dateReg.test(dateVar);
	if(!isV){
		return false;
	} 
	
	var str_array = dateVar.split('/');
	var miladyDate =  jqOld.calendars.instance('gregorian','ar').newDate();
	try {
		miladyDate = miladyDate.date(str_array[2], str_array[0], str_array[1]);
	}
	catch(err) {
	   return false;
	}
	return true;
}


</script>

<meta charset="utf-8" />
<title><?php echo SCHOOL_SITE_TITLE_NEW; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="author" />
<!-- Global site tag (gtag.js) - Google Ads: 806135812 --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-806135812"></script>


<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-806135812');
  gtag('config', 'UA-135637603-1');

</script>

<!-- Twitter universal website tag code -->
<script>
    !function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
    },s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
    a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
    // Insert Twitter Pixel ID and Standard Event data below
    twq('init','nxhdl');
    twq('track','PageView');
</script>
<!-- End Twitter universal website tag code -->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '243743377195546');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=243743377195546&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Event snippet for Reg conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-806135812/pGc4COnVkYEBEITQsoAD'});
</script>

</head>
<!-- END HEAD -->
<body class="page-container-bg-solid">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5WN5RLJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


	<div class="container shadow">
<?php

$this->load->view ( 'utils/schoolHeader2' );

?>


		<div class="page-wrapper-row full-height">
			<div class="page-wrapper-middle">
				<!-- BEGIN CONTAINER -->
				<div class="page-container">
					<!-- BEGIN CONTENT -->
					<div class="page-content-wrapper">
						<!-- BEGIN CONTENT BODY -->
						<!-- BEGIN PAGE HEAD-->
							<div class="container">
								<!-- BEGIN PAGE TITLE -->
								<div class="page-title" style="padding-top: 0px !important; padding-bottom: 7px !important;">
									<h2>
											<span style="color: #0E4278 !important;"><?php echo REG_PAGE_TITLE; ?></span>
									</h2>
								</div>
								<!-- END PAGE TITLE -->
							</div>

						<!-- BEGIN PAGE CONTENT BODY -->
						<div class="page-content">
								<div class="col-md-14">
									<div class="portlet box green">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-gift"></i>
												<?php
												echo STEP_2_TITLE;
												?>
											</div>
										</div>
										<div class="portlet-body form">
											<br> <br>
											<!-- BEGIN FORM-->
											<form id="myForm" role="form" method="post"
												onsubmit="javascript:return checkValid();"
												enctype="multipart/form-data"
												action="<?=site_url('Registration_controller/execute_step2/')?>"
												class="form-horizontal">

												<input type="hidden" id="student_registration_id"
													name="student_registration_id"
													value="<?php echo $student_registration_id ?>">
													
												<input type="hidden" id="branch_id" name="branch_name" value="">
												<input type="hidden" id="track_id" name="track_name" value="">

												<div class="form-body">

													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo STUDENT_NAME; ?></label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="student_name" id="student_name"
																		onfocus="javascript:removeTooltip('student_name_tooltip');">
																	<span id="student_name_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo BRANCH; ?></label>
																<div class="col-md-6">
																	<select class="form-control " name="school"
																		onfocus="javascript:removeTooltip('school_tooltip');"
																		id="school">
																		<option value=""></option>
																		<?php
																			if ($schools) {
																				for($i = 0; $i < count ($schools); $i ++) {
																					$school_id = $schools[$i]["id"];
																					$school_name = $schools[$i]["name"];
																					?>
																					<option value="<?php echo $school_id?>"> <?php echo $school_name?></option>
																					<?php
																				}
																			} 
																		?>
																	</select> <span id="school_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
														<!-- 
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo STUDENT_GENDER; ?></label>
																<div class="col-md-6">
																	<select class="form-control " name="student_gender"
																		onfocus="javascript:removeTooltip('student_gender_tooltip');"
																		id="student_gender">
																		<option value="<?php echo STUDENT_GENDER_VALUE_1; ?>"><?php echo MALE; ?></option>
																		<option value="<?php echo STUDENT_GENDER_VALUE_2; ?>"><?php echo FEMALE; ?></option>
																	</select> <span id="student_gender_tooltip"
																		class="tooltiptext" style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
														-->
													</div>
													<!-- 
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo STUDENT_NATIONALITY; ?></label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="nationality" id="nationality"
																		onfocus="javascript:removeTooltip('nationality_tooltip');">
																	<span id="nationality_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
													</div>
													 
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo STUDENT_DOB_MILADY; ?></label>
																<div class="col-md-6">
																	<input type="text" placeholder="" id="DOB_1"
																		class="form-control dateMilady" name="DOB_1"
																		onchange="miladyHandler();"
																		onkeypress="this.onchange();"
																		onpropertychange="this.onchange();"
																		onpaste="this.onchange();" oninput="this.onchange();">
																	<span id="DOB_1_invalid_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
																	<span id="DOB_1_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo STUDENT_DOB_HIJRI; ?></label>
																<div class="col-md-6">
																	<input type="text" placeholder="" id="DOB_2"
																		class="form-control date" name="DOB_2"
																		onchange="hijriHandler()"
																		onkeypress="this.onchange();"
																		onpropertychange="this.onchange();"
																		onpaste="this.onchange();" oninput="this.onchange();">
																	<span id="DOB_2_invalid_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
																	<span id="DOB_2_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo STUDENT_IBAN; ?></label>
																<div class="col-md-6">
																	<input type="text" class="form-control " name="IBAN"
																		id="IBAN"
																		onfocus="javascript:removeTooltip('IBAN_tooltip');"> <span
																		id="IBAN_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo STUDENT_IBAN_SRC; ?></label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="IBAN_source" id="IBAN_source"
																		onfocus="javascript:removeTooltip('IBAN_source_tooltip');">
																	<span id="IBAN_source_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
													</div>
													-->
													
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo DEPARTMENT; ?></label>
																<div class="col-md-6">
																	<select class="form-control " name="department"
																		onfocus="javascript:removeTooltip('department_tooltip');"
																		id="department">
																		<option value=""></option>
																		<option value="BOYS"><?php echo BOYS; ?></option>
																		<option value="GIRLS"><?php echo GIRLS; ?></option>
																	</select> <span id="department_tooltip"
																		class="tooltiptext" style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo TRACK; ?></label>
																<div class="col-md-6">
																	<select class="form-control " name="track"
																		onfocus="javascript:removeTooltip('school_tooltip');"
																		id="track"
																		onchange="change_grades_menu()">
																		<option value=""></option>
																		<option value="PUB"> <?php echo PUBL?></option>
																		<option value="INT"> <?php echo INTERNATIONAL?></option>
																					
																	</select> <span id="track_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo STUDENT_GRADE; ?></label>
																<div class="col-md-6">
																	<select class="form-control " name="grade"
																		onfocus="javascript:removeTooltip('grade_tooltip');"
																		id="grade">
																		
																	</select> <span id="grade_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo OLD_SCHOOL; ?></label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="old_school" id="old_school"
																		onfocus="javascript:removeTooltip('old_school_tooltip');">
																	<span id="old_school_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
														
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo KNOWING_WAY; ?></label>
																<div class="col-md-6">
																	<select class="form-control " name="knowing_way"
																		onfocus="javascript:removeTooltip('other_way_tooltip');"
																		id="knowing_way">
																	<option value=""></option>
																	<?php
																	if ($knowing_ways) {
																		for($i = 0; $i < count ( $knowing_ways); $i ++) {
																			$code= $knowing_ways[$i] ["code"];
																			$name = $knowing_ways[$i] ["name"];
																				?>
																				<option value="<?php echo $code ?>"> <?php echo $name?></option>
																				<?php
																			}
																		} 
																	?>
																	</select>
																	<br>
																	<input type="text" class="form-control hidden-textbox"
																		name="other_way" id="other_way"
																		onfocus="javascript:removeTooltip('other_way_tooltip');">
																		
																	<span id="other_way_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
													</div>
													<div class="form-actions">
														<div class="row">
															<div class="col-md-9" align="center"
																style="margin: auto; width: 100%;">
																<button type="submit" class="btn btn-circle green"  ><?php
																echo NEXT;
																
																?></button>

															</div>
														</div>
													</div>

												</div>
											</form>
											<!-- END FORM-->
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
    <?php $this->load->view('utils/schoolFooter2');?>
	
											<!-- BEGIN CORE PLUGINS -->
	<!--<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/profile.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->
	<script>
		$(document).ready(function(){
		  change_list();
		  $(".hidden-textbox").hide();
		  $("select#knowing_way").change(function(){
		    var currentVal = $(this).val();
		    if(currentVal == "OTHER"){
		      $(".hidden-textbox").show();
		    }
		    else{
		      $(".hidden-textbox").hide();
		      hide_other_tooltip();
		    }
		  });  

		  $("select#school").change(function(){
		    var currentVal = $(this).val();
		    $("#branch_id").val(currentVal);
		    change_list();
		  }); 
		  
		  $("select#track").change(function(){
		    var currentVal = $(this).val();
		    $("#track_id").val(currentVal);
		    change_list();
		  }); 

		});

		function change_list(){
			var educational_grade = <?php echo json_encode($educational_grade) ?>;
			  $('#grade').empty();
			  $('#grade').append('<option value=""></option>');
			  var track_id = $('#track_id').val();
			  var branch_id = $('#branch_id').val();
			  //alert(branch_id);
			  for (var i in educational_grade) {
				  educational_grade_id = educational_grade[i] ["id"];
				  educational_grade_desc_ar = educational_grade[i] ["desc_ar"];
			  	  educational_grade_desc_en = educational_grade[i] ["desc_en"];
				  educational_grade_desc = educational_grade_desc_ar;
				  <?php if($lang == "en"){?>
						educational_grade_desc = educational_grade_desc_en;
				  <?php }?>
				  if(track_id == "INT"){
					  if(branch_id == "1" ){// تعاون
						  if(educational_grade_id == "1"){
							  continue;
						  }
					  }else if(branch_id == "2" ){// مربع
						  if((educational_grade_id == "1") || (educational_grade_id == "3") || (educational_grade_id == "6")){
							  continue;
						  }
					 }
				  }
				  $('#grade').append('<option value=" '+ educational_grade_id +'"> '+ educational_grade_desc + '</option>');
			  }
		}
	</script>

</body>

</html>