<?php
class Admin_model extends CI_Model
{
	function getAgents($page_size, $page_number) {
		
		$offset = ($page_number * $page_size) - $page_size;
		$query = $this->db->query("select * from admin where type = 'AGENT' and active = 1 ".
						" LIMIT " . $page_size . " OFFSET " . $offset);
		log_message ( 'error', 'regs query:: ' . $this->db->last_query () );
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return "E000100";
		} else {
			$agents = $query->result_array();
		}
		$this->db->trans_complete();
		return $agents;
	}
	
	function getAgentsCount() {
		
		$query = $this->db->query("select * from admin where type = 'AGENT'  and active= 1 ");
		log_message ( 'error', 'regs query:: ' . $this->db->last_query () );
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return "E000100";
		} else {
			$count = $query->num_rows();
		}
		$this->db->trans_complete();
		return $count;
	}
	
}
?>