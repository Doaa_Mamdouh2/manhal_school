<?php

class Activities_model extends CI_Model
{

    function getUsersActivities()
    {
        $this->db->select('activities_log.id, activities_log.user_id , activities_log.activity_type_id , activities_log.date_time ,
                 activities_log.reg_id, activities_log.user_IP,
				 admin.user_name, admin.name, admin.type, activity_type.name as activity_type_name');
        $this->db->from('activities_log');
        $this->db->join('admin', 'admin.admin_id = activities_log.user_id');
        $this->db->join('activity_type', 'activity_type.id = activities_log.activity_type_id');
        $this->db->order_by('activities_log.date_time', 'desc');
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            $records = $query->result_array();
            return $records;
        } else {
            return false;
        }
    }
    
    function getDashboardActivities($offset, $page_size)
    {
    	$this->load->model ( "common" );
    	
    	$conditionsString = " activities_log.id is NOT NULL  ";
    	
    	$this->db->select('activities_log.id, activities_log.user_id , activities_log.activity_type_id , activities_log.date_time ,
                 activities_log.reg_id, activities_log.user_IP,
				 admin.user_name, admin.name, admin.type, activity_type.name as activity_type_name');
    	$this->db->from('activities_log');
    	$this->db->join('admin', 'admin.admin_id = activities_log.user_id');
    	$this->db->join('activity_type', 'activity_type.id = activities_log.activity_type_id');
    	$this->db->where($conditionsString);
    	$this->db->order_by('activities_log.date_time', 'desc');
    	
    	
    	$num_results = 0;
    	try {
    		// open1 here we copy $this->db in to tempdb and apply count_all_results() function on to this tempdb
    		$tempdb = clone $this->db;
    		$temp_query = $tempdb->get ();
    		$num_results = $temp_query->num_rows ();
    		log_message ( 'error', 'num_results = ' . $num_results );
    	} catch ( Exception $e ) {
    		log_message ( 'error', $e->getMessage () );
    	}
    	
    	
    	$this->db->limit ( $page_size , $offset );
    	
    	$query = $this->db->get();
    	log_message("error", 'reports query usage:: '. $this->db->last_query ());
    	$i = 0;
    	$data = array();
    	if ($query->num_rows() > 0) {
    		$records = $query->result_array();
    		foreach ($records as $record) {
    			$msg_part1 = "";
    			if($record['type'] == "AGENT"){
    				$msg_part1 = "قام ممثل الخدمة ". " ". $record['name'];
    			}elseif ($record['type'] == "ACCOUNTANT"){
    				$msg_part1 = "قام المحاسب ". " ". $record['name'];
    			}
    			
    			$data[$i]['message'] = $msg_part1 . " " . $record['activity_type_name'] . ' ';
    			
    			if ($record['activity_type_id'] == "3") { //edit request
    				$reg_record = $this->common->getOneRow ( 'student_registration', "where reg_id = '".$record['reg_id']."'" ); 
    				$student_name = $reg_record["student_name"];
    				$request_url = site_url('Admin_panel/view_student_registration/' . $record['reg_id']);
    				$reg_code = " (".get_formatted_reg_id($record ['reg_id']).") ";
    				$data[$i]['message'] = $data[$i]['message'] . "<a style='color: #337ab7 !important;'  href='".$request_url."'>".$reg_code."</a>"."  ";
    				if($student_name != NULL && $student_name != ""){
    				    $data[$i]['message'] = $data[$i]['message'] . " الخاص بالطالب/ة".": <b>".$student_name."</b>";
    				}
    			}
    			
    			$data[$i]['date_time'] = $record['date_time'];
    			date_default_timezone_set('Asia/Riyadh');
    			$date = new DateTime($record['date_time']);
    			$data[$i]['date_time_diff'] = self::formatDateDiff($date);
    			
    			$i = $i + 1;
    		}
    	}
    	
    	if (count($data) > 0) {
    		self::array_sort_by_column($data, "date_time", SORT_DESC);
    		$data [0] ['count_all'] = $num_results;
    		return $data;
    	} else {
    		return false;
    	}
    	
    }
    
    function array_sort_by_column(&$array, $column1, $dir1 = SORT_ASC)
    {
        $sort_column = array();
        foreach ($array as $key => $row) {
            $sort_column[$column1][$key] = $row[$column1];
        }
        array_multisort($sort_column[$column1], $dir1, $array);
    }

    /**
     * A sweet interval formatting, will use the two biggest interval parts.
     * On small intervals, you get minutes and seconds.
     * On big intervals, you get months and days.
     * Only the two biggest parts are used.
     *
     * @param DateTime $start
     * @param DateTime|null $end
     * @return string
     */
    function formatDateDiff($start, $end = null)
    {
        date_default_timezone_set('Asia/Riyadh');
        if (! ($start instanceof DateTime)) {
            $start = new DateTime($start);
        }
        
        if ($end === null) {
            $end = new DateTime();
        }
        
        if (! ($end instanceof DateTime)) {
            $end = new DateTime($start);
        }
        // $end->setTimezone(new DateTimeZone('Asia/Riyadh'));
        $interval = $end->diff($start);
        // echo $end->format('Y-m-d H:i:s').' '.$start->format('Y-m-d H:i:s').' '.($end->format('Y-m-d H:i:s') - $start->format('Y-m-d H:i:s'));
        
        $getTimeString = function ($nb, $str, $value, $from_str) {
            
            $doPlural = function ($nb, $str) {
                if ($nb == 0) {
                    return "الان";
                } else if ($nb == 1 || $nb > 10) {
                    return $str;
                } else if ($nb == 2) {
                    switch ($str) {
                        case "سنة":
                            return "سنتان";
                            break;
                        case "شهر":
                            return "شهران";
                            break;
                        case "يوم":
                            return "يومان";
                            break;
                        case "ساعة":
                            return "ساعتان";
                            break;
                        case "دقيقة":
                            return "دقيقتان";
                            break;
                        case "ثانية":
                            return "ثانيتان";
                            break;
                        default:
                            return "ثانيتان";
                    }
                } else if ($nb >= 3 && $nb <= 10) {
                    switch ($str) {
                        case "سنة":
                            return "سنين";
                            break;
                        case "شهر":
                            return "شهور";
                            break;
                        case "يوم":
                            return "أيام";
                            break;
                        case "ساعة":
                            return "ساعات";
                            break;
                        case "دقيقة":
                            return "دقائق";
                            break;
                        case "ثانية":
                            return "ثوانى";
                            break;
                        default:
                            return "ثوانى";
                    }
                } else {
                    return $str;
                }
            }; // adds plurals
            
            if ($nb == 0) {
                return $doPlural($nb, $str);
            } else if ($nb == 1) {
                return $from_str . $doPlural($nb, $str);
            } else if ($nb > 10) {
                return $from_str . $value . $doPlural($nb, $str);
            } else if ($nb == 2) {
                return $from_str . $doPlural($nb, $str);
            } else if ($nb >= 3 && $nb <= 10) {
                return $from_str . $value . $doPlural($nb, $str);
            }
        }; // getTimeString
        
        $format = array();
        if ($interval->y !== 0) {
            $format[] = $getTimeString($interval->y, "سنة", "%y ", "منذ ");
        } else if ($interval->m !== 0) {
            $format[] = $getTimeString($interval->m, "شهر", "%m ", "منذ ");
        } else if ($interval->d !== 0) {
            $format[] = $getTimeString($interval->d, "يوم", "%d ", "منذ ");
        } else if ($interval->h !== 0) {
            $format[] = $getTimeString($interval->h, "ساعة", "%h ", "منذ ");
        } else if ($interval->i !== 0) {
            $format[] = $getTimeString($interval->i, "دقيقة", "%i ", "منذ ");
        } else if ($interval->s !== 0) {
            /*
             * if(!count($format)) {
             * return "منذ أقل من دقيقة";
             * } else {
             * $format[] = "%s ".$doPlural($interval->s, "ثانية");
             * }
             */
            $format[] = $getTimeString($interval->s, "ثانية", "%s ", "منذ ");
        } else {
            $format[] = $getTimeString($interval->s, "ثانية", "%s ", "منذ ");
        }
        
        // We use the two biggest parts
        if (count($format) > 1) {
            $format = array_shift($format) . " و " . array_shift($format);
        } else {
            $format = array_pop($format);
        }
        $format;
        // Prepend 'since ' or whatever you like
        return $interval->format($format);
    }
    
}
?>