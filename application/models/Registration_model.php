<?php

class Registration_model extends CI_Model {

    /*
     * $search_type = 1 for advanced search , =2 for quick search
     */
    function getRegistrations($page_size, $page_number, $search_values = null, $search_type = null, $user_type_code, $admin_id = null, $state = '0', $grade = '0') {
        $this->db->trans_begin();
        $where_condition = "";
        if ($search_values != null && $search_type == '1') {
            $where_condition = self::advanced_search_condition($search_values);
        }
        if ($search_values != null && $search_type == '2') {
            $where_condition = self::quick_search_condition($search_values);
        }
        if(isset($admin_id) && $admin_id != null && $user_type_code == "AGENT"){
            $new_cond = "student_registration.agent_id = '" . $admin_id . "'";
            $where_condition = $where_condition != "" ? ($where_condition . " AND ".$new_cond): ("where " .$new_cond);
        }
        if(isset($state) && $state != null && $state != '0'){
            $new_cond = "student_registration.current_state_code = '" . $state . "'";
            $where_condition = $where_condition != "" ? ($where_condition . " AND ".$new_cond): ("where " .$new_cond);
        }
        if(isset($grade) && $grade != null && $grade != '0'){
            $new_cond = "student_registration.grade = '" . $grade . "'";
            $where_condition = $where_condition != "" ? ($where_condition . " AND ".$new_cond): ("where " .$new_cond);
        }
        $offset = ($page_number * $page_size) - $page_size;
        $query = $this->db->query("select student_registration.reg_id,student_registration.responsible_name,
									student_registration.responsible_mobile,
									student_registration.responsible_email,student_registration.creation_date,
									student_registration.student_name,student_registration.school,student_registration.is_contacted,
                                    student_registration.current_state_code,
									`educational_grade`.`desc_ar` as grade
									FROM student_registration
									LEFT JOIN educational_grade
									ON student_registration.grade = educational_grade.id
								 " . $where_condition . " order by creation_date_time DESC "
                			   . " LIMIT " . $page_size . " OFFSET " . $offset);
        log_message ( 'error', 'regs query:: ' . $this->db->last_query () );
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else {
            $registrations = $query->result_array();
        }
        $this->db->trans_complete();
        return $registrations;
    }
    
    function getExportedRegistrationsData($from_date = null, $to_date = null, $not_interested = 0) {
    	$where_condition= "";
    	if(isset($from_date) && $from_date != null){
    		$new_cond = "creation_date >= '" . $from_date . "'";
    		$where_condition = $where_condition != "" ? ($where_condition . " AND ".$new_cond): ("where " .$new_cond);
    	}
    	if(isset($to_date) && $to_date != null){
    		$new_cond = "creation_date <= '" . $to_date. "'";
    		$where_condition = $where_condition != "" ? ($where_condition . " AND ".$new_cond): ("where " .$new_cond);
    	}
    	if(isset($not_interested) && $not_interested != 0){
    		$new_cond = "student_registration.current_state_code = 'NOT_INTERESTED_IN_REGISTRATION'";
    		$where_condition = $where_condition != "" ? ($where_condition . " AND ".$new_cond): ("where " .$new_cond);
    	}
    	
    	//echo $where_condition;
    	$query = $this->db->query("select student_registration.reg_id,student_registration.responsible_name,
									student_registration.responsible_mobile,
									student_registration.responsible_email,student_registration.creation_date,
									student_registration.student_name,student_registration.school,student_registration.is_contacted,
                                    student_registration.current_state_code,student_registration.knowing_way,
									student_registration.department,student_registration.track,student_registration.comment,
									`educational_grade`.`desc_ar` as grade,
									`admin`.`name` as agent_name,
									`state`.`state_name_ar` as state_name_ar
									FROM student_registration 
									LEFT JOIN educational_grade 
									ON student_registration.grade = educational_grade.id 
									LEFT JOIN admin 
									ON student_registration.agent_id = admin.admin_id 
									LEFT JOIN state 
									ON student_registration.current_state_code = state.state_code ".
    								$where_condition.
								    " order by creation_date_time DESC");
    	
    	$registration_data = array ();
    	if ($query->num_rows () > 0) {
    		$registrations = $query->result_array();
    		$i = 0;
    		foreach ( $registrations as $record ) {
    			$registration_data[$i] ['reg_id'] = $record ['reg_id'];
    			$registration_data[$i] ['responsible_name'] = $record ['responsible_name'];
    			$registration_data[$i] ['responsible_mobile'] = $record ['responsible_mobile'];
    			$registration_data[$i] ['responsible_email'] = $record ['responsible_email'];
    			$registration_data[$i] ['creation_date'] = $record ['creation_date'];
    			$registration_data[$i] ['student_name'] = $record ['student_name'];
    			$registration_data[$i] ['school'] = $record ['school'];
    			$registration_data[$i] ['is_contacted'] = $record ['is_contacted'];
    			$registration_data[$i] ['current_state_code'] = $record ['current_state_code'];
    			$registration_data[$i] ['knowing_way'] = $record ['knowing_way'];
    			$registration_data[$i] ['department'] = $record ['department'];
    			$registration_data[$i] ['track'] = $record ['track'];
    			$registration_data[$i] ['comment'] = $record ['comment'];
    			$registration_data[$i] ['grade'] = $record ['grade'];
    			$registration_data[$i] ['agent_name'] = $record ['agent_name'];
    			$registration_data[$i] ['state_name_ar'] = $record ['state_name_ar'];
    			
    			$i = $i + 1;
    		}
    	}
    	return $registration_data;
    }


    /*
     * $search_type = 1 for advanced search , =2 for quick search
     */
    function countRegistrations($search_values = null, $search_type = null, $user_type_code, $admin_id = null, $state = null, $grade = null) {
        $this->db->trans_begin();
        $where_condition = "";
        if ($search_values != null && $search_type == '1') {
            $where_condition = self::advanced_search_condition($search_values);
        }
        if ($search_values != null && $search_type == '2') {
            $where_condition = self::quick_search_condition($search_values);
        }
        if(isset($admin_id) && $admin_id != null && $user_type_code == "AGENT"){
            $new_cond = "student_registration.agent_id = '" . $admin_id . "'";
            $where_condition = $where_condition != "" ? ($where_condition . " AND ".$new_cond): ("where " .$new_cond);
        }
        if(isset($state) && $state != null && $state != '0'){
            $new_cond = "student_registration.current_state_code = '" . $state . "'";
            $where_condition = $where_condition != "" ? ($where_condition . " AND ".$new_cond): ("where " .$new_cond);
        }
        if(isset($grade) && $grade != null && $grade != '0'){
            $new_cond = "student_registration.grade = '" . $grade . "'";
            $where_condition = $where_condition != "" ? ($where_condition . " AND ".$new_cond): ("where " .$new_cond);
        }
        $query = $this->db->query("SELECT *
                    FROM student_registration " . $where_condition . " order by creation_date_time DESC ");
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else {
            $count = $query->num_rows();
        }
        $this->db->trans_complete();
        return $count;
    }

    private function advanced_search_condition($search_values) {
        $where_condition = "";
        $responsible_name_cond = "";
        $responsible_mobile_cond = "";
        $responsible_email_cond = "";
        $student_name_cond = "";
        $state_cond = "";
        $is_contacted_cond = "";
        $creation_date_cond = "";
        $department_cond = "";
        $track_cond = "";
        $DOB_milady_cond = "";
        $IBAN_cond = "";
        $IBAN_source_cond = "";
        $knowing_way_cond = "";
        $grade_cond = "";
        $old_school_cond = "";
        $notification_time_cond = "";
        $agent_id_cond = "";
        $comment_cond = "";
        $reg_date_from_cond = "";
        $reg_date_to_cond = "";
        
        if ($search_values["responsible_name"] != null && $search_values["responsible_name"] != "") {
            $responsible_name_cond = "responsible_name like '%" . $search_values["responsible_name"] . "%'";
        }
        if ($search_values["responsible_mobile"] != null && $search_values["responsible_mobile"] != "") {
            $responsible_mobile_cond = "responsible_mobile like '%" . $search_values["responsible_mobile"] . "%'";
        }
        if ($search_values["responsible_email"] != null && $search_values["responsible_email"] != "") {
            $responsible_email_cond = "responsible_email like '%" . $search_values["responsible_email"] . "%'";
        }
        if ($search_values["student_name"] != null && $search_values["student_name"] != "") {
            $student_name_cond = "student_name like '%" . $search_values["student_name"] . "%'";
        }
        if ($search_values["current_state_code"] != null && $search_values["current_state_code"] != "") {
            $state_cond = "current_state_code = '" . $search_values["current_state_code"] . "'";
        }
        if ($search_values["is_contacted"] != null && $search_values["is_contacted"] != "") {
            $is_contacted_cond = "is_contacted = '" . $search_values["is_contacted"] . "'";
        }
        if ($search_values["creation_date"] != null && $search_values["creation_date"] != "") {
            $date = date('Y-m-d', strtotime($search_values["creation_date"]));
            $creation_date_cond = "creation_date = '" . $date . "'";
        }
        if ($search_values["reg_date_from"] != null && $search_values["reg_date_from"] != "") {
        	$date = date('Y-m-d', strtotime($search_values["reg_date_from"]));
        	$reg_date_from_cond = "creation_date >= '" . $date . "'";
        }
        if ($search_values["reg_date_to"] != null && $search_values["reg_date_to"] != "") {
        	$date = date('Y-m-d', strtotime($search_values["reg_date_to"]));
        	$reg_date_to_cond = "creation_date <= '" . $date . "'";
        }
        if ($search_values["department"] != null && $search_values["department"] != "") {
            $department_cond = "department = '" . $search_values["department"] . "'";
        }
        if ($search_values["track"] != null && $search_values["track"] != "") {
        	$track_cond = "track = '" . $search_values["track"] . "'";
        }
        /* if ($search_values["DOB_1"] != null && $search_values["DOB_1"] != "") {
            $date = date('Y-m-d', strtotime($search_values["DOB_1"]));
            $DOB_milady_cond = "DOB = '" . $date . "'";
        } */
        /* if ($DOB_milady_cond == "" && $search_values["DOB_2"] != null && $search_values["DOB_2"] != "") {
            $this->load->library ( '../controllers/dates_conversion' );
            $milady_date = $this->dates_conversion->HijriToGregorian ($search_values["DOB_2"]);
            $DOB_milady_cond = "DOB = '" . $milady_date . "'";
        } */
       /*  if ($search_values["IBAN"] != null && $search_values["IBAN"] != "") {
            $IBAN_cond = "IBAN like '%" . $search_values["IBAN"] . "%'";
        }
        if ($search_values["IBAN_source"] != null && $search_values["IBAN_source"] != "") {
            $IBAN_source_cond = "IBAN_source like '%" . $search_values["IBAN_source"] . "%'";
        } */
        if ($search_values["knowing_way"] != null && $search_values["knowing_way"] != "") {
        	$knowing_way_cond = "knowing_way like '%" . $search_values["knowing_way"] . "%' or other_way like '%" . $search_values["knowing_way"]. "%'";
        }
        if ($search_values["grade"] != null && $search_values["grade"] != "") {
            $grade_cond = "grade = '" . $search_values["grade"] . "'";
        }
        if ($search_values["old_school"] != null && $search_values["old_school"] != "") {
            $old_school_cond = "old_school like '%" . $search_values["old_school"] . "%'";
        }
        if ($search_values["notification_time"] != null && $search_values["notification_time"] != "") {
            $date = date('Y-m-d', strtotime($search_values["notification_time"]));
            $notification_time_cond = "notification_time = '" . $date . "'";
        }
        if ($search_values["agent_id"] != null && $search_values["agent_id"] != "") {
            $agent_id_cond = "agent_id = '" . $search_values["agent_id"] . "'";
        }
        if ($search_values["comment"] != null && $search_values["comment"] != "") {
            $comment_cond = "comment like '%" . $search_values["comment"] . "%'";
        }
        
        
        if ($responsible_name_cond != "") {
            $where_condition = ($where_condition=="")? $responsible_name_cond : $where_condition. ' AND ' .$responsible_name_cond;
        }
        if ($responsible_mobile_cond != "") {
            $where_condition = ($where_condition=="")? $responsible_mobile_cond : $where_condition. ' AND ' .$responsible_mobile_cond;
        }       
        if ($responsible_email_cond != "") {
            $where_condition = ($where_condition=="")? $responsible_email_cond : $where_condition. ' AND ' .$responsible_email_cond;
        } 
        if ($student_name_cond != "") {
            $where_condition = ($where_condition=="")? $student_name_cond : $where_condition. ' AND ' .$student_name_cond;
        }
        if ($state_cond != "") {
            $where_condition = ($where_condition=="")? $state_cond : $where_condition. ' AND ' .$state_cond;
        }
        if ($is_contacted_cond != "") {
            $where_condition = ($where_condition=="")? $is_contacted_cond : $where_condition. ' AND ' .$is_contacted_cond;
        }
        if ($creation_date_cond != "") {
            $where_condition = ($where_condition=="")? $creation_date_cond : $where_condition. ' AND ' .$creation_date_cond;
        }
        if ($reg_date_from_cond != "") {
        	$where_condition = ($where_condition=="")? $reg_date_from_cond: $where_condition. ' AND ' .$reg_date_from_cond;
        }
        if ($reg_date_to_cond != "") {
        	$where_condition = ($where_condition=="")? $reg_date_to_cond: $where_condition. ' AND ' .$reg_date_to_cond;
        }
        if ($department_cond != "") {
            $where_condition = ($where_condition=="")? $department_cond : $where_condition. ' AND ' .$department_cond;
        }
        if ($track_cond != "") {
        	$where_condition = ($where_condition=="")? $track_cond: $where_condition. ' AND ' .$track_cond;
        }
        if ($DOB_milady_cond != "") {
            $where_condition = ($where_condition=="")? $DOB_milady_cond : $where_condition. ' AND ' .$DOB_milady_cond;
        }
        if ($IBAN_cond != "") {
            $where_condition = ($where_condition=="")? $IBAN_cond : $where_condition. ' AND ' .$IBAN_cond;
        }
        if ($IBAN_source_cond != "") {
            $where_condition = ($where_condition=="")? $IBAN_source_cond : $where_condition. ' AND ' .$IBAN_source_cond;
        }
        if ($knowing_way_cond != "") {
        	$where_condition = ($where_condition=="")? $knowing_way_cond : $where_condition. ' AND ' .$knowing_way_cond;
        }
        if ($grade_cond != "") {
            $where_condition = ($where_condition=="")? $grade_cond : $where_condition. ' AND ' .$grade_cond;
        }
        if ($old_school_cond != "") {
            $where_condition = ($where_condition=="")? $old_school_cond : $where_condition. ' AND ' .$old_school_cond;
        }
        if ($notification_time_cond != "") {
            $where_condition = ($where_condition=="")? $notification_time_cond : $where_condition. ' AND ' .$notification_time_cond;
        }        
        if ($agent_id_cond != "") {
            $where_condition = ($where_condition=="")? $agent_id_cond : $where_condition. ' AND ' .$agent_id_cond;
        }
        if ($comment_cond != "") {
            $where_condition = ($where_condition=="")? $comment_cond : $where_condition. ' AND ' .$comment_cond;
        }        
        
        if ($where_condition != "") {
            $where_condition = "where " . $where_condition;
        }
        return $where_condition;
    }
    
    /*
     * ( بحث سريع (يبحث في اسم الطالب ورقم الجوال
     * search in student_name & responsible_mobile  
     */
    private function quick_search_condition($search_values) {
        $where_condition = "";
        if ($search_values["query"] != null && $search_values["query"] != "") {
            $where_condition = "(student_name LIKE '%" . $search_values["query"] . "%' OR responsible_mobile LIKE '%" . $search_values["query"] . "%')";
        }
        if ($where_condition != "") {
            $where_condition = "where " . $where_condition;
        }
        return $where_condition;
    }

    /*
     * query: SELECT grade, count(grade) FROM `student_registration`  GROUP by grade ORDER BY `student_registration`.`grade` ASC 
     * SELECT grade, count(grade) FROM `student_registration` where agent_id = 2 GROUP by grade ORDER BY `student_registration`.`grade` ASC 
     */
    function getAllGrades($user_type_code, $admin_id) {
        $this->db->select ( 'educational_grade.id, educational_grade.desc_ar, educational_grade.desc_en' );
        $this->db->from ( 'educational_grade' );
        $this->db->order_by ( 'id', 'asc' );
        $query = $this->db->get ();
        $educational_grades = array ();
        $sum = 0;
        if ($query->num_rows () > 0) {
            $records = $query->result_array ();
            $i = 0;
            foreach ( $records as $record ) {
                $educational_grades [$i] ["grade_id"] = $record ['id'];
                $educational_grades [$i] ["desc_ar"] = $record ['desc_ar'];
                $educational_grades [$i] ["desc_en"] = $record ['desc_en'];
                switch ($user_type_code) {
                    case "ADMIN" :
                        $educational_grades [$i] ["count"] = self::getRegistrationsGradesCount ($educational_grades [$i] ["grade_id"] );
                        break;
                    case "AGENT" :
                        $educational_grades [$i] ["count"] = self::getRegistrationsGradesCount ($educational_grades [$i] ["grade_id"], $admin_id);
                        break;
                    case "ACCOUNTANT" :
                        $educational_grades [$i] ["count"] = self::getRegistrationsGradesCount ($educational_grades [$i] ["grade_id"] );
                        break;
                    default :
                        $educational_grades [$i] ["count"] = self::getRegistrationsGradesCount ($educational_grades [$i] ["grade_id"] );
                }
                $sum = $sum + $educational_grades [$i] ["count"];
                $i = $i + 1;
            }
            $output = array();
            $output ['sum'] = $sum;
            $output ['educational_grades'] = $educational_grades;
            return $output;
        }
        return null;
    }
    
    function getRegistrationsGradesCount($grade_id, $admin_id = null) {
        $conditionsString = "student_registration.grade ='" . $grade_id."'";
        if(isset($admin_id)){
            $conditionsString = $conditionsString . " AND student_registration.agent_id = '" . $admin_id . "'";
        }
        $this->db->select ( 'count(student_registration.reg_id)' );
        $this->db->from ( 'student_registration' );
        $this->db->where ( $conditionsString );
        $query = $this->db->get ();
        $result = $query->row_array ();
        $count = $result ['count(student_registration.reg_id)'];
        return $count;
    }
    
    /*
     * Query : SELECT current_state_code, count(current_state_code) FROM `student_registration` GROUP by current_state_code ORDER BY `student_registration`.`current_state_code` DESC
     * 
     *  or 
     *  
     *  SELECT current_state_code, count(current_state_code) FROM `student_registration` where agent_id = 2 GROUP by current_state_code ORDER BY `student_registration`.`current_state_code` DESC 
     */
    function getAllOrderedStates($user_type_code, $admin_id) {
        $this->db->select ( 'state.id, state.state_code, state.state_name_ar, state.state_name_en, state.description' );
        $this->db->from ( 'state' );
        $this->db->order_by ( 'id', 'asc' );
        $query = $this->db->get ();
        $states = array ();
        $sum = 0;
        if ($query->num_rows () > 0) {
            $records = $query->result_array ();
            $i = 0;
            foreach ( $records as $record ) {
                $states [$i] ["state_id"] = $record ['id'];
                $states [$i] ["state_code"] = $record ['state_code'];
                $states [$i] ["state_name_ar"] = $record ['state_name_ar'];
                $states [$i] ["state_name_en"] = $record ['state_name_en'];
                $states [$i] ["description"] = $record ['description'];
                switch ($user_type_code) {
                    case "ADMIN" :
                        $states [$i] ["count"] = self::getRegistrationsStatesCount ($states [$i] ["state_code"] );
                        break;
                    case "AGENT" :
                        $states [$i] ["count"] = self::getRegistrationsStatesCount ($states [$i] ["state_code"], $admin_id);
                        break;
                    case "ACCOUNTANT" :
                        $states [$i] ["count"] = self::getRegistrationsStatesCount ($states [$i] ["state_code"] );
                        break;
                    default :
                        $states [$i] ["count"] = self::getRegistrationsStatesCount ($states [$i] ["state_code"] );
                }
                $sum = $sum + $states [$i] ["count"];
                $i = $i + 1;
            }
            $output = array();
            $output ['sum'] = $sum;
            $output ['states'] = $states;
            return $output;
        }
        return null;
    }
    
    function getRegistrationsStatesCount($state_code, $admin_id = null) {
        $conditionsString = "student_registration.current_state_code ='" . $state_code."'";
        if(isset($admin_id)){
            $conditionsString = $conditionsString . " AND student_registration.agent_id = '" . $admin_id . "'";
        }
        $this->db->select ( 'count(student_registration.reg_id)' );
        $this->db->from ( 'student_registration' );
        $this->db->where ( $conditionsString );
        $query = $this->db->get ();
        $result = $query->row_array ();
        $count = $result ['count(student_registration.reg_id)'];
        return $count;
    }
    
    function getRegistrationsCountInLast60Days() {
        date_default_timezone_set ( 'Asia/Riyadh' );
        $end_date = date ( "Y-m-d" );
        $from_date = date ( "Y-m-d", strtotime ( "-29 day", strtotime ( $end_date) ) );
        $reportData = array ();
        $i = 0;
        while ( strtotime ( $end_date) >=  strtotime ( $from_date ) ) {
            $end_date = date ( "Y-m-d", strtotime ( $end_date ) );
            $reportData [$i] ['date'] = $end_date;
            $reportData [$i] ['created'] =  self::getNumberOfCreatedRegistrationInDay($end_date);
            $end_date= date ( "Y-m-d", strtotime ( "-1 day", strtotime ( $end_date) ) );
            $i ++;
        }
        return $reportData;
    }
    
    function getNumberOfCreatedRegistrationInDay($date){
        $conditionsString = "student_registration.creation_date ='" . $date."'";
        $this->db->select ( 'count(student_registration.reg_id)' );
        $this->db->from ( 'student_registration' );
        $this->db->where ( $conditionsString );
        $query = $this->db->get ();
        $result = $query->row_array ();
        $count = $result ['count(student_registration.reg_id)'];
        return $count;
    }
    
    function getAgentsRegistrationsCounts(){
        $agents_registrationsCounts = array();
        $sum_new = 0;
        $sum_have_progress = 0;
        $output = array();
        $agents = $this->common->getAllRow("admin", "where type = 'AGENT' and active = 1");
        
        for($i = 0; $i < count ( $agents ); $i ++) {
            $agent_record = $agents[$i];
            $agents_registrationsCounts [$i] ['agent_id'] =  $agent_record['admin_id'];
            $agents_registrationsCounts [$i] ['agent_name'] =  $agent_record['name'];
            
            $conditionsString1 = "student_registration.agent_id = '" . $agent_record['admin_id'] . "' ";
            
            $conditionsString2 = "student_registration.current_state_code IN ('NEW')";
            $this->db->select ( 'count(student_registration.reg_id)' );
            $this->db->from ( 'student_registration' );
            $this->db->where ( $conditionsString1." AND ". $conditionsString2);
            $query = $this->db->get ();
            $result = $query->row_array ();
            $agents_registrationsCounts [$i] ['count1'] = $result ['count(student_registration.reg_id)'];
            $sum_new = $sum_new + $agents_registrationsCounts [$i] ['count1'];
            
            $conditionsString2 = "student_registration.current_state_code IN ('NOT_ANSWERED', 'INTERESTED_IN_REGISTRATION', 'START_REGISTRATION_PROCESS')";
            $this->db->select ( 'count(student_registration.reg_id)' );
            $this->db->from ( 'student_registration' );
            $this->db->where ( $conditionsString1." AND ". $conditionsString2);
            $query = $this->db->get ();
            $result = $query->row_array ();
            $agents_registrationsCounts [$i] ['count2'] = $result ['count(student_registration.reg_id)'];
            $sum_have_progress = $sum_have_progress + $agents_registrationsCounts [$i] ['count2'];
            
            $conditionsString2 = "student_registration.current_state_code IN ('REGISTERED', 'PAYMENT_OF_FEES')";
            $this->db->select ( 'count(student_registration.reg_id)' );
            $this->db->from ( 'student_registration' );
            $this->db->where ( $conditionsString1." AND ". $conditionsString2);
            $query = $this->db->get ();
            $result = $query->row_array ();
            $agents_registrationsCounts [$i] ['count3'] = $result ['count(student_registration.reg_id)'];
            $sum_have_progress = $sum_have_progress + $agents_registrationsCounts [$i] ['count3'];
        }
        
        $output ['agents_registrationsCounts'] = $agents_registrationsCounts;
        $output ['sum_new'] = $sum_new;
        $output ['sum_have_progress'] = $sum_have_progress;
        
        return $output;
    }
    
    function getAgentsPerformance(){
        $agents_performance = array();
        $sum_all = 0;
        $sum_delayed = 0;
        $output = array();
        date_default_timezone_set ( 'Asia/Riyadh' );
        $current_date = date ( "Y-m-d H:i:s" );
        $today = new DateTime ( $current_date );
        $this->load->helper ( array ( 'Common_functions'  ) );
        
        $agents = $this->common->getAllRow("admin", "where type = 'AGENT'");
        
        for($i = 0; $i < count ( $agents ); $i ++) {
            $agent_record = $agents[$i];
            $agents_performance [$i] ['agent_id'] =  $agent_record['admin_id'];
            $agents_performance [$i] ['agent_name'] =  $agent_record['name'];
            
            $conditionsString1 = "student_registration.agent_id = '" . $agent_record['admin_id'] . "' ";
            
            $this->db->select ( 'count(student_registration.reg_id)' );
            $this->db->from ( 'student_registration' );
            $this->db->where ( $conditionsString1);
            $query = $this->db->get ();
            $result = $query->row_array ();
            $agents_performance [$i] ['count1'] = $result ['count(student_registration.reg_id)'];
            $sum_all = $sum_all + $agents_performance [$i] ['count1'];
            
            $agents_performance [$i] ['count2'] = 0;
            $conditionsString2 = "student_registration.current_state_code IN ('NEW', 'NOT_ANSWERED', 'INTERESTED_IN_REGISTRATION')";
            $this->db->select ( 'reg_id, current_state_code, state_changed_date, creation_date_time, agent_id' );
            $this->db->from ( 'student_registration' );
            $this->db->where ( $conditionsString1." AND ". $conditionsString2);
            $query = $this->db->get ();
            if ($query->num_rows () > 0) {
                $records = $query->result_array ();
                foreach ( $records as $record ) {
                    $late = self::is_late_reg($record, "1");
                    if( $late == '1'){
                        $agents_performance [$i] ['count2'] ++;
                    }
                }
            }
            $sum_delayed = $sum_delayed + $agents_performance [$i] ['count2'];
        }
        
        $output ['agents_performance'] = $agents_performance;
        $output ['sum_all'] = $sum_all;
        $output ['sum_delayed'] = $sum_delayed;
        
        return $output;
    }
    
    function getLateRegistrations($page_size, $page_number, $search_values = null, $search_type = null, $user_type_code, $admin_id = null, $state = '0', $grade = '0', $level) {
        $this->db->trans_begin();
        $output = array();
        $late_registrations = array();
        $total_count = 0;
        $educational_grades = array ();
        $grades_sum = 0;
        $states = array();
        $states_sum = 0;
        
        //get grades and initialize count
        $this->db->select ( 'educational_grade.id, educational_grade.desc_ar, educational_grade.desc_en' );
        $this->db->from ( 'educational_grade' );
        $this->db->order_by ( 'id', 'asc' );
        $query = $this->db->get ();
        if ($query->num_rows () > 0) {
            $records = $query->result_array ();
            $i = 0;
            foreach ( $records as $record ) {
                $educational_grades [$i] ["grade_id"] = $record ['id'];
                $educational_grades [$i] ["desc_ar"] = $record ['desc_ar'];
                $educational_grades [$i] ["desc_en"] = $record ['desc_en'];
                $educational_grades [$i] ["count"] = 0;
                $i = $i + 1;
            }
        }
        //get states and initialize count
        $this->db->select ( 'state.id, state.state_code, state.state_name_ar, state.state_name_en, state.description' );
        $this->db->from ( 'state' );
        $this->db->where ("state_code IN ('NEW', 'NOT_ANSWERED', 'INTERESTED_IN_REGISTRATION')");
        $this->db->order_by ( 'id', 'asc' );
        $query = $this->db->get ();
        if ($query->num_rows () > 0) {
            $records = $query->result_array ();
            $i = 0;
            foreach ( $records as $record ) {
                $states [$i] ["state_id"] = $record ['id'];
                $states [$i] ["state_code"] = $record ['state_code'];
                $states [$i] ["state_name_ar"] = $record ['state_name_ar'];
                $states [$i] ["state_name_en"] = $record ['state_name_en'];
                $states [$i] ["description"] = $record ['description'];
                $states [$i] ["count"] = 0;
                $i = $i + 1;
            }
        }
        
        //first get grades & states arrays with counts
        $conditionsString = "student_registration.current_state_code IN ('NEW', 'NOT_ANSWERED', 'INTERESTED_IN_REGISTRATION')";
        $query = $this->db->query("select student_registration.reg_id,student_registration.responsible_name,
									student_registration.responsible_mobile,
									student_registration.responsible_email,student_registration.creation_date,student_registration.creation_date_time,
									student_registration.student_name,student_registration.school,student_registration.is_contacted,
                                    student_registration.current_state_code, student_registration.state_changed_date,
									`educational_grade`.`desc_ar` as grade, student_registration.grade AS grade_id
									FROM student_registration
									LEFT JOIN educational_grade
									ON student_registration.grade = educational_grade.id
								 " . " where ".$conditionsString . " order by creation_date_time DESC ");
        log_message ( 'error', 'regs query:: ' . $this->db->last_query () );
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else {
            $all_registrations = $query->result_array();
            foreach ( $all_registrations as $record ) {
                $late = self::is_late_reg($record, $level);
                log_message("error", "record with reg id = ".$record['reg_id']."  late = ".$late);
                if( $late == '1'){
                    for($j=0; $j< count($educational_grades) ; $j++){
                        if($record['grade_id'] == $educational_grades [$j] ["grade_id"]){
                            $educational_grades [$j] ["count"]++;
                            $grades_sum++;
                        }
                    }//end grades loop
                    for($j=0; $j< count($states) ; $j++){
                        if($record['current_state_code'] == $states [$j] ["state_code"]){
                            $states [$j] ["count"]++;
                            $states_sum++;
                        }
                    }//end states loop
                }
            }
        }
        $output ['educational_grades'] = $educational_grades;
        $output ['grades_sum'] = $grades_sum;
        $output ['states'] = $states;
        $output ['states_sum'] = $states_sum;
        
        //second get the regs with the search fields and either the state filter or grade filter
        $where_condition = "";
        if ($search_values != null && $search_type == '1') {
            $where_condition = self::advanced_search_condition($search_values);
        }
        if ($search_values != null && $search_type == '2') {
            $where_condition = self::quick_search_condition($search_values);
        }
        if(isset($admin_id) && $admin_id != null && $user_type_code == "AGENT"){
            $new_cond = "student_registration.agent_id = '" . $admin_id . "'";
            $where_condition = $where_condition != "" ? ($where_condition . " AND ".$new_cond): ("where " .$new_cond);
        }
        if(isset($state) && $state != null && $state != '0'){
            $new_cond = "student_registration.current_state_code = '" . $state . "'";
            $where_condition = $where_condition != "" ? ($where_condition . " AND ".$new_cond): ("where " .$new_cond);
        }
        if(isset($grade) && $grade != null && $grade != '0'){
            $new_cond = "student_registration.grade = '" . $grade . "'";
            $where_condition = $where_condition != "" ? ($where_condition . " AND ".$new_cond): ("where " .$new_cond);
        }
        
        $where_condition = $where_condition != "" ? ($where_condition . " AND ".$conditionsString): ("where " .$conditionsString);
        $query = $this->db->query("select student_registration.reg_id,student_registration.responsible_name,
									student_registration.responsible_mobile,
									student_registration.responsible_email,student_registration.creation_date,student_registration.creation_date_time,
									student_registration.student_name,student_registration.school,student_registration.is_contacted,
                                    student_registration.current_state_code, student_registration.state_changed_date,
									`educational_grade`.`desc_ar` as grade, student_registration.grade AS grade_id
									FROM student_registration
									LEFT JOIN educational_grade
									ON student_registration.grade = educational_grade.id
								 " . $where_condition . " order by creation_date_time DESC ");
        log_message ( 'error', 'regs query:: ' . $this->db->last_query () );
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else {
            $all_registrations = $query->result_array();
            $i = 0;
            foreach ( $all_registrations as $record ) {
                $late = self::is_late_reg($record, $level);
                log_message("error", "record with reg id = ".$record['reg_id']."  late = ".$late);
                if( $late == '1'){
                    $late_registrations[$i] = $record;
                    $i = $i + 1;
                }
            }
            
            //log_message("error", "late_registrations: ".print_r($late_registrations, TRUE));
            
            //handling pagination
            $total_count = count ( $late_registrations );
            $offset = ($page_number - 1) * $page_size;
            if ($total_count > 0) { //start slicing the array
                $late_registrations = array_slice ( $late_registrations, $offset, $page_size );
            }
        
        }
        $output ['total_count'] = $total_count;
        $output ['late_registrations'] = $late_registrations;
        
        $this->db->trans_complete();
        return $output;
    }
    
    function is_late_reg($record, $level){
        //log_message("error", "record= ".print_r($record, true));
        date_default_timezone_set ( 'Asia/Riyadh' );
        $current_date = date ( "Y-m-d H:i:s" );
        $today = new DateTime ( $current_date );
        $this->load->helper ( array ( 'Common_functions'  ) );
        $late = '0';
        $state = $record['current_state_code'];
        $change_date = new DateTime ( $record ['state_changed_date'] );
        $creation_date_time = new DateTime ( $record ['creation_date_time'] );
        
        if($state == 'NEW' && $record ['creation_date_time'] != NULL && $record ['creation_date_time'] != ""){
            $delta_h = round((($today->getTimestamp() - $creation_date_time->getTimestamp()) / 3600), 1);
            $number_working_days = number_of_working_days($creation_date_time->format('Y-m-d'), $today->format('Y-m-d'), array());
            log_message("error", "reg_id = ".$record['reg_id']." with state =$state number_of_working_days between ".$creation_date_time->format('Y-m-d')." and ".$today->format('Y-m-d'). " = $number_working_days". " and number_of_hours = ".$delta_h);
            if(($level == '1' && $delta_h >= 24 && $number_working_days >= 1) ||
                ($level == '2' && $delta_h >= 48 && $number_working_days >= 2) ){
                    $late = '1';
            }
        }
        if($state == 'NOT_ANSWERED' && $record ['state_changed_date'] != NULL && $record ['state_changed_date'] != ""){
            $delta_h = round((($today->getTimestamp() - $change_date->getTimestamp()) / 3600), 1);
            $number_working_days = number_of_working_days($change_date->format('Y-m-d'), $today->format('Y-m-d'), array());
            log_message("error", "reg_id = ".$record['reg_id']." with state =$state number_of_working_days between ".$change_date->format('Y-m-d')." and ".$today->format('Y-m-d'). " = $number_working_days". " and number_of_hours = ".$delta_h);
            if(($level == '1' && $delta_h >= 24 && $number_working_days >= 1) ||
                ($level == '2' && $delta_h >= 48 && $number_working_days >= 2) ){
                    $late = '1';
            }
        }
        if($state == 'INTERESTED_IN_REGISTRATION' && $record ['state_changed_date'] != NULL && $record ['state_changed_date'] != ""){
            $number_working_days = number_of_working_days($change_date->format('Y-m-d'), $today->format('Y-m-d'), array());
            log_message("error", "reg_id = ".$record['reg_id']." with state =$state number_of_working_days between ".$change_date->format('Y-m-d')." and ".$today->format('Y-m-d'). " = $number_working_days");
            if(($level == '1' && $number_working_days >= 6) ||
                ($level == '2' && $number_working_days >= 12) ){
                    $late = '1';
            }
        }
        return $late;
    }
    
    
    
}

?>