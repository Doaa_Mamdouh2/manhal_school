<?php
class Comments_model extends CI_Model
{
	function getRegComments($student_reg_id) {
		$conditionsString = "comment.student_reg_id = ".$student_reg_id;	
		$this->db->select('comment.admin_id, comment.comment, comment.date_time, comment.id, admin.user_name, admin.name');
		$this->db->from('comment');
		$this->db->join('admin', 'admin.admin_id = comment.admin_id');
		$this->db->where($conditionsString);
		$this->db->order_by("date_time", "desc");
		$query = $this->db->get();
		
		if($query->num_rows()>0) {
			$records = $query->result_array();
			$i = 0;
			$comments_data = array();
			foreach($records as $record ) {
				   $comments_data[$i]['id'] = $record['id'];
				   $comments_data[$i]['admin_id'] = $record['admin_id'];
				   $comments_data[$i]['comment'] = $record['comment'];
				   $comments_data[$i]['date_time'] = $record['date_time'];
				   $comments_data[$i]['user_name'] = $record['user_name'];
				   $comments_data[$i]['name'] = $record['name'];
	              
	               $i = $i + 1;
	        }
			return $comments_data;
		} else {
			return false;
		}
	}
	
	
}
?>