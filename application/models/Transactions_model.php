<?php
class Transactions_model extends CI_Model
{
	function getRegTransactions($student_reg_id) {
		$conditionsString = "registration_transactions.reg_id = ".$student_reg_id;
		$this->db->select('registration_transactions.is_contacted, registration_transactions.notification_time, registration_transactions.state_code, registration_transactions.admin_id,
						   registration_transactions.comment, registration_transactions.date_time, registration_transactions.id,
						   admin.user_name, admin.name');
		$this->db->from('registration_transactions');
		$this->db->join('admin', 'admin.admin_id = registration_transactions.admin_id');
		$this->db->where($conditionsString);
		$this->db->order_by("date_time", "desc");
		$query = $this->db->get();
		
		if($query->num_rows()>0) {
			$records = $query->result_array();
			$i = 0;
			$transactions_data = array();
			foreach($records as $record ) {
				$transactions_data[$i]['id'] = $record['id'];
				$transactions_data[$i]['admin_id'] = $record['admin_id'];
				$transactions_data[$i]['comment'] = $record['comment'];
				$transactions_data[$i]['date_time'] = $record['date_time'];
				$transactions_data[$i]['user_name'] = $record['user_name'];
				$transactions_data[$i]['name'] = $record['name'];
				$transactions_data[$i]['is_contacted'] = $record['is_contacted'];
				$transactions_data[$i]['state_code'] = $record['state_code'];
				$transactions_data[$i]['notification_time'] = $record['notification_time'];
				
				$i = $i + 1;
			}
			return $transactions_data;
		} else {
			return false;
		}
	}
	
	
}
?>