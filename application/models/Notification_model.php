<?php
class Notification_model extends CI_Model {
	function getNotifications() {
		$conditionsString = "notifications.notification_code not in ('M008', 'M009', 'M010')";
		$this->db->select ( 'notifications.notification_code, notifications.notification_name, notifications.send_time, notifications.active,
							 notifications.sms_active, state.state_code, state.state_name, user_type.user_type_code, user_type.user_type_name' );
		$this->db->from ( 'notifications' );
		$this->db->join ( 'state', 'state.state_code = notifications.case_state' );
		$this->db->join ( 'user_type', 'user_type.user_type_code = notifications.receiver' );
		$this->db->order_by ( "notification_code" );
		$this->db->where($conditionsString);
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			return $records;
		} else {
			return false;
		}
	}
	function getNotificationData($notification_code) {
		$conditionsString = "notifications.notification_code = '" . $notification_code . "'";
		$this->db->select ( 'notifications.notification_code, notifications.notification_name, notifications.send_time, notifications.active,
							 notifications.sms_active, state.state_code, state.state_name, user_type.user_type_code, user_type.user_type_name' );
		$this->db->from ( 'notifications' );
		$this->db->join ( 'state', 'state.state_code = notifications.case_state' );
		$this->db->join ( 'user_type', 'user_type.user_type_code = notifications.receiver' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			return $records [0];
		} else {
			return false;
		}
	}
	function getDeliveredNotificationId() {
		$this->db->select ( 'email_id' );
		$this->db->from ( 'delivered_notifications' );
		$this->db->order_by ( 'email_id', 'asc' );
		$query = $this->db->get ();
		$tran_num = 0;
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			foreach ( $records as $record ) {
				if (strlen ( $record ['email_id'] ) == 8) {
					$tran_num = substr ( $record ['email_id'], 1, 7 );
					$tran_num ++;
				}
			}
		}
		$tran_num = sprintf ( "%'.07d", $tran_num );
		return "E" . $tran_num;
	}
	function getLatestDeliveredNotificationsForUser($user_id) {
		$conditionsString = "delivered_notifications.to_user_id = '" . $user_id . "' AND delivered_notifications.message != ''";
		$this->db->select ( 'delivered_notifications.email_id, delivered_notifications.notification_type_code, delivered_notifications.reg_id,
						 delivered_notifications.sent_date_time, delivered_notifications.waiting_days, delivered_notifications.seen,
						 delivered_notifications.message' );
		$this->db->from ( 'delivered_notifications' );
		$this->db->where ( $conditionsString );
		//$this->db->order_by ( "email_id", "desc" );
		$this->db->order_by ( "sent_date_time", "desc" );
		$this->db->limit(1000);
		$query = $this->db->get ();
		//log_message ( 'error', 'notif query:: ' . $this->db->last_query () );
		$data = array ();
		$notifications = array ();
		$count_unseen = 0;
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			$i = 0;
			foreach ( $records as $record ) {
				if ($record ['seen'] == 'Y' && $i == 10) {
					break;
				} else {
					$notifications [$i] ['email_id'] = $record ['email_id'];
					$notifications [$i] ['notification_type_code'] = $record ['notification_type_code'];
					$notifications [$i] ['reg_id'] = $record ['reg_id'];
					$notifications [$i] ['sent_date_time'] = self::getTimeDiff ( $record ['sent_date_time'] );
					$notifications [$i] ['waiting_days'] = $record ['waiting_days'];
					$notifications [$i] ['seen'] = $record ['seen'];
					$notifications [$i] ['message'] = $record ['message'];
					if ($record ['seen'] == 'N') {
						$count_unseen ++;
					}
				}
				$i ++;
			}
		}
		//log_message('error', 'count_unseen= '.$count_unseen.' , notifications: '.print_r($notifications, TRUE));
		$data ['notifications_list'] = $notifications;
		$data ['count_unseen'] = $count_unseen;
		return $data;
	}
	function getUnseenNotificationsForUser($user_id) {
		$conditionsString = "delivered_notifications.to_user_id = '" . $user_id . "' AND delivered_notifications.message != '' AND delivered_notifications.seen = 'N'";
		$this->db->select ( 'delivered_notifications.email_id, delivered_notifications.notification_type_code, delivered_notifications.reg_id,
						 delivered_notifications.sent_date_time, delivered_notifications.waiting_days, delivered_notifications.seen,
						 delivered_notifications.message' );
		$this->db->from ( 'delivered_notifications' );
		$this->db->where ( $conditionsString );
		$this->db->order_by ( "email_id", "desc" );
		$query = $this->db->get ();
		
		$data = array ();
		$notifications = array ();
		$count_unseen = 0;
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			$i = 0;
			foreach ( $records as $record ) {
				$notifications [$i] ['email_id'] = $record ['email_id'];
				$notifications [$i] ['notification_type_code'] = $record ['notification_type_code'];
				$notifications [$i] ['reg_id'] = $record ['reg_id'];
				$notifications [$i] ['sent_date_time'] = self::getTimeDiff ( $record ['sent_date_time'] );
				$notifications [$i] ['waiting_days'] = $record ['waiting_days'];
				$notifications [$i] ['seen'] = $record ['seen'];
				$notifications [$i] ['message'] = $record ['message'];
				if ($record ['seen'] == 'N') {
					$count_unseen ++;
				}
				$i ++;
			}
		}
		$data ['notifications_list'] = $notifications;
		$data ['count_unseen'] = $count_unseen;
		return $data;
	}
	function getTimeDiff($dateString) {
		date_default_timezone_set ( 'Asia/Riyadh' );
		$date = new DateTime ( $dateString );
		$diff = self::formatDateDiff ( $date );
		return $diff;
	}
	
	/**
	 * A sweet interval formatting, will use the two biggest interval parts.
	 * On small intervals, you get minutes and seconds.
	 * On big intervals, you get months and days.
	 * Only the two biggest parts are used.
	 *
	 * @param DateTime $start        	
	 * @param DateTime|null $end        	
	 * @return string
	 */
	function formatDateDiff($start, $end = null) {
		date_default_timezone_set ( 'Asia/Riyadh' );
		if (! ($start instanceof DateTime)) {
			$start = new DateTime ( $start );
		}
		
		if ($end === null) {
			$end = new DateTime ();
		}
		
		if (! ($end instanceof DateTime)) {
			$end = new DateTime ( $start );
		}
		// $end->setTimezone(new DateTimeZone('Asia/Riyadh'));
		$interval = $end->diff ( $start );
		// echo $end->format('Y-m-d H:i:s').' '.$start->format('Y-m-d H:i:s').' '.($end->format('Y-m-d H:i:s') - $start->format('Y-m-d H:i:s'));
		
		$getTimeString = function ($nb, $str, $value, $from_str) {
			
			$doPlural = function ($nb, $str) {
				if ($nb == 0) {
					return "الان";
				} else if ($nb == 1 || $nb > 10) {
					return $str;
				} else if ($nb == 2) {
					switch ($str) {
						case "سنة" :
							return "سنتان";
							break;
						case "شهر" :
							return "شهران";
							break;
						case "يوم" :
							return "يومان";
							break;
						case "ساعة" :
							return "ساعتان";
							break;
						case "دقيقة" :
							return "دقيقتان";
							break;
						case "ثانية" :
							return "ثانيتان";
							break;
						default :
							return "ثانيتان";
					}
				} else if ($nb >= 3 && $nb <= 10) {
					switch ($str) {
						case "سنة" :
							return "سنين";
							break;
						case "شهر" :
							return "شهور";
							break;
						case "يوم" :
							return "أيام";
							break;
						case "ساعة" :
							return "ساعات";
							break;
						case "دقيقة" :
							return "دقائق";
							break;
						case "ثانية" :
							return "ثوانى";
							break;
						default :
							return "ثوانى";
					}
				} else {
					return $str;
				}
			}; // adds plurals
			
			if ($nb == 0) {
				return $doPlural ( $nb, $str );
			} else if ($nb == 1) {
				return $from_str . $doPlural ( $nb, $str );
			} else if ($nb > 10) {
				return $from_str . $value . $doPlural ( $nb, $str );
			} else if ($nb == 2) {
				return $from_str . $doPlural ( $nb, $str );
			} else if ($nb >= 3 && $nb <= 10) {
				return $from_str . $value . $doPlural ( $nb, $str );
			}
		}; // getTimeString
		
		$format = array ();
		if ($interval->y !== 0) {
			$format [] = $getTimeString ( $interval->y, "سنة", "%y ", "منذ " );
		} else if ($interval->m !== 0) {
			$format [] = $getTimeString ( $interval->m, "شهر", "%m ", "منذ " );
		} else if ($interval->d !== 0) {
			$format [] = $getTimeString ( $interval->d, "يوم", "%d ", "منذ " );
		} else if ($interval->h !== 0) {
			$format [] = $getTimeString ( $interval->h, "ساعة", "%h ", "منذ " );
		} else if ($interval->i !== 0) {
			$format [] = $getTimeString ( $interval->i, "دقيقة", "%i ", "منذ " );
		} else if ($interval->s !== 0) {
			/*
			 * if(!count($format)) {
			 * return "منذ أقل من دقيقة";
			 * } else {
			 * $format[] = "%s ".$doPlural($interval->s, "ثانية");
			 * }
			 */
			$format [] = $getTimeString ( $interval->s, "ثانية", "%s ", "منذ " );
		} else {
			$format [] = $getTimeString ( $interval->s, "ثانية", "%s ", "منذ " );
		}
		
		// We use the two biggest parts
		if (count ( $format ) > 1) {
			$format = array_shift ( $format ) . " و " . array_shift ( $format );
		} else {
			$format = array_pop ( $format );
		}
		$format;
		// Prepend 'since ' or whatever you like
		return $interval->format ( $format );
	}
}

?>